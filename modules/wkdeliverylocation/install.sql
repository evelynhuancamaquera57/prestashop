CREATE TABLE IF NOT EXISTS `PREFIX_wk_sml_customer_location` (
  `id_location` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lat` float(10) NOT NULL,
  `lng` float(10) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY  (`id_location`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;
CREATE TABLE IF NOT EXISTS `PREFIX_wk_sml_location_mapping` (
  `id_mapping` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_element` int(10) unsigned NOT NULL,
  `id_location` int(10) unsigned NOT NULL,
  `element_type` enum('address', 'order', 'customer'),
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY  (`id_mapping`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;