<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_69aa1a4bd631e8f0b2c2056834c3f3a4'] = 'Lieu de livraison';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_9635ad7a00768637aa04b2803a133873'] = ' Définir le lieu de livraison sur la carte';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_4a95d1d2be82cd5023579f032b94c88e'] = ' Définir l\'emplacement';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_d2c51a3531b82662e6678b990aec8140'] = ' Entrez l\'emplacement';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_b1c94ca2fbc3e78fc30069c8d0f01680'] = ' Tout';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_9f94670e302168bfb258bd8d8dee352e'] = 'Les établissements';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_284b47b0bb63ae2df3b29f0e691d6fcf'] = 'Adresses';
$_MODULE['<{wkdeliverylocation}prestashop>wk-sml-map-popup_4b17f855e879dd8e90a4009a41b0296d'] = 'Géocodes';
