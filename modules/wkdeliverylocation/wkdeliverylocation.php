<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

if (!defined('_PS_VERSION_')) {
    exit;
}
require_once dirname(__FILE__).'/classes/WkSmlClassInclude.php';

class WkDeliveryLocation extends Module
{
    const INSTALL_SQL_FILE = 'install.sql';
    public function __construct()
    {
        $this->name = 'wkdeliverylocation';
        $this->version = '5.0.0';
        $this->author = 'Webkul';
        $this->module_key = '6162feadfdb3661464db7ec36215e160';
        $this->tab = 'front_office_features';
        $this->ps_versions_compliancy = array('min'=>'1.7', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Prestashop Set Your Delivery Location');
        $this->description = $this->l('Let your customer select address from map');
        $this->confirmUninstall = $this->l('Are you sure?');
    }

    /**
     * Install override
     *
     * @return void
     */
    public function install()
    {
        if (!parent::install()
            || !$this->createTables()
            || !$this->registerPsHooks()
            || !$this->defaultConfigurationData()) {
            return false;
        }
        return true;
    }

    /**
     * Register hooks
     *
     * @return void
     */
    private function registerPsHooks()
    {
        $hookList = array(
            'actionFrontControllerSetMedia',
            'actionAdminControllerSetMedia',
            'displayOrderConfirmation1',
            'displayOrderDetail',
            'displayAdminOrder',
            'actionObjectAddressAddAfter',
            'actionObjectAddressDeleteAfter',
            'actionObjectAddressUpdateAfter',
            'actionObjectCustomerAddressAddAfter',
            'actionObjectCustomerAddressDeleteAfter',
            'actionObjectCustomerAddressUpdateAfter',
            'actionValidateOrder',
        );

        foreach ($hookList as $hook) {
            if (!$this->registerHook($hook)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Default configuration for this module
     *
     * @return void
     */
    private function defaultConfigurationData()
    {
        $configurationData = array(
            'WK_SML_DEFAULT_MAP_LOCATION' => '28.2873, 77.1852',
            'WK_SML_DEFAULT_ZOOM' => '13',
            'WK_SML_LOCATION_ZOOM' => '17',
            'WK_SML_MAP_ON_CONFIRMATION' => '1',
            'WK_SML_MAP_ON_DETAILS' => '1'
        );
        //if prestashop settings already have a google api key set, then pick this
        if (!$psApiKey = Configuration::get('PS_API_KEY')) {
            $configurationData['WK_SML_GOOGLE_API_KEY'] = '';
        } else {
            $configurationData['WK_SML_GOOGLE_API_KEY'] = $psApiKey;
        }
        foreach ($configurationData as $key => $value) {
            if (!Configuration::updateValue($key, $value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Create google map url with libraries passed as string params
     *
     * @param string $includeLib
     * @return void
     */
    public static function getMapJsUrl($includeLib = '')
    {
        $protocol = (Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE')) ?
        'https' :
        'http';
        if (!$api_key = Configuration::get('WK_SML_GOOGLE_API_KEY')) {
            //If api key is not found in module settings, then check in Prestashop settings
            $api_key = (Configuration::get('PS_API_KEY')) ? 'key=' . Configuration::get('PS_API_KEY') : '';
        }
        return $protocol . '://maps.googleapis.com/maps/api/js?key=' . $api_key . $includeLib;
    }

    /**
     * Hook for Address add in front office
     *
     * @param [type] $params
     * @return void
     */
    public function hookActionObjectAddressAddAfter($params)
    {
        //After address add in front
        $this->addAddress($params);
        return true;
    }

    /**
     * Hook For address add in back office
     *
     * @param [type] $params
     * @return void
     */
    public function hookActionObjectCustomerAddressAddAfter($params)
    {
        //After address add in back office
        $this->addAddress($params);
        return true;
    }

    /**
     * Hook for address update in front office
     *
     * @return void
     */
    public function hookActionObjectAddressUpdateAfter()
    {
        //After address update in front
        $this->updateAddress();
        return true;
    }

    /**
     * Hook for address update in backoffice
     *
     * @return void
     */
    public function hookActionObjectCustomerAddressUpdateAfter()
    {
        //After address update in back
        $this->updateAddress();
        return true;
    }

    /**
     * Hook for address delete in front office
     *
     * @param [type] $params
     * @return void
     */
    public function hookActionObjectAddressDeleteAfter($params)
    {
        //After address delete in front
        $this->deleteAddress($params);
        return true;
    }

    /**
     * Hook for address delete in back office
     *
     * @param [type] $params
     * @return void
     */
    public function hookActionObjectCustomerAddressDeleteAfter($params)
    {
        //After address delete in back
        $this->deleteAddress($params);
        return true;
    }

    /**
     * Hook for location create on order validation
     *
     * @param [type] $params
     * @return void
     */
    public function hookActionValidateOrder($params)
    {
        $idOrder = $params['order']->id;
        $custLocObj = new WkSmlCustomerLocation();
        if (!$custLocObj->getLocationByIdOrder($idOrder)) {
            //if location not set for this order
            //(this ckeck is to prevent location creation on confirmation page refresh)
            $orderObj = new Order($idOrder);
            $orderDetails = $orderObj->getFields();
            $idAddress = $orderDetails['id_address_delivery'];
            $custLocation = $custLocObj->getLocationByIdAddress($idAddress);
            if ($custLocation) {
                //Location is created, now create mapping
                $locMapping = new WkSmlLocationMapping();
                $locMapping->id_element = $idOrder;
                $locMapping->element_type = 'order';
                $locMapping->id_location = $custLocation['id_location'];
                $locMapping->save();
            }
        }
        return true;
    }

    /**
     * Hook to add JS on various Admin Pages
     *
     * @return void
     */
    public function hookActionAdminControllerSetMedia()
    {
        if ($this->context->controller->controller_name == 'AdminAddresses') {
            if (Tools::getValue('id_address') && (!Tools::getValue('conf'))) {
                $this->context->controller->addJS(
                    self::getMapJsUrl('&libraries=places')
                );
                $this->addMapJsToController(true);
                Media::addJsDef(
                    array(
                        'iconUrl' => $this->getIconUrl(),
                        'getDirections' => $this->l('Get Directions'),
                        'defaultZoom' => Configuration::get('WK_SML_DEFAULT_ZOOM'),
                        'locationZoom' => Configuration::get('WK_SML_LOCATION_ZOOM'),
                        'mapTemplate' => $this->context->smarty->fetch(
                            _PS_MODULE_DIR_.$this->name.'/views/templates/admin/wk-sml-map-popup.tpl'
                        ),
                    )
                );
                $this->context->controller->addJS(
                    $this->_path.'views/js/admin/wkadminmaplocation.js'
                );
            }
        } elseif ($this->context->controller->controller_name == 'AdminOrders') {
            //get location for order
            $idOrder = Tools::getValue('id_order');
            if ($idOrder) {
                $custLocationObj = new WkSmlCustomerLocation();
                $delLocation = $custLocationObj->getLocationByIdOrder($idOrder);
                $defaultLocation = explode(',', Configuration::get('WK_SML_DEFAULT_MAP_LOCATION'));
                $delLocId = $delLocation ? $delLocation['id_location'] : 0;
                $delLat = $delLocation ? $delLocation['lat'] : $defaultLocation[0];
                $delLng = $delLocation ? $delLocation['lng'] : $defaultLocation[1];

                Media::addJsDef(
                    array(
                        'orderId' => $idOrder,
                        'iconUrl' => $this->getIconUrl(),
                        'defaultZoom' => Configuration::get('WK_SML_DEFAULT_ZOOM'),
                        'locationZoom' => Configuration::get('WK_SML_LOCATION_ZOOM'),
                        'locationId' => $delLocId,
                        'getDirections' => $this->l('Get Directions'),
                        'lats'=> (float)$delLat,
                        'lngs'=>(float)$delLng,
                        'setLocationLink' => $this->context->link->getModuleLink($this->name, 'wkmaplocation'),
                    )
                );
                $this->context->controller->addJS($this->_path.'views/js/admin/wkadminordersmap.js');
                $this->context->controller->addCSS($this->_path.'views/css/wk_sml_map.css');
            }
        }
    }

    /**
     * Hook to render template on Admin order page
     *
     * @return void
     */
    public function hookDisplayAdminOrder()
    {
        $idOrder = Tools::getValue('id_order');
        $custLocationObj = new WkSmlCustomerLocation();
        $delLocation = $custLocationObj->getLocationByIdOrder($idOrder);
        if (!$delLocation) {
            return;
        }
        return $this->context->smarty->fetch(
            _PS_MODULE_DIR_.$this->name.'/views/templates/admin/wk-sml-order-detail-admin.tpl',
            array(
                'module_dir' => _PS_MODULE_DIR_
            )
        );
    }

    /**
     * Hook to add JS & CSS on varoius front office pages
     *
     * @return void
     */
    public function hookActionFrontControllerSetMedia()
    {
        if ($this->context->controller->php_self == 'address') {
            $this->context->controller->registerJavascript(
                'wk-sml-address-google-map-js',
                self::getMapJsUrl('&libraries=places'),
                array(
                    'server' => 'remote'
                )
            );
            $this->addMapJsToController();
            $this->context->controller->addCSS($this->_path.'views/css/wk_sml_orders.css');
        } elseif ($this->context->controller->php_self == 'order') {
            $this->context->controller->registerJavascript(
                'wk-sml-order-google-map-js',
                self::getMapJsUrl('&libraries=places'),
                array(
                    'server' => 'remote'
                )
            );
            $loc = $this->getLocation(true);
            $defaultLocation = explode(',', Configuration::get('WK_SML_DEFAULT_MAP_LOCATION'));
            $idAddress = $loc['idAddress'];
            $delLocId = $loc['delLocId'];
            $delLat = $loc['delLat'];
            $delLng = $loc['delLng'];
            $isEditing = ((Tools::getValue('newAddress') == 'delivery')
            || (Tools::getValue('editAddress') == 'delivery'));
            if (Tools::getValue('newAddress') == 'delivery') {
                $delLat = $defaultLocation[0];
                $delLng = $defaultLocation[1];
            }
            Media::addJsDef(
                array(
                    'iconUrl' => $this->getIconUrl(),
                    'defaultZoom' => Configuration::get('WK_SML_DEFAULT_ZOOM'),
                    'locationZoom' => Configuration::get('WK_SML_LOCATION_ZOOM'),
                    'locationId' => $delLocId,
                    'lats'=> (float)$delLat,
                    'lngs'=>(float)$delLng,
                    'idAddress' => $idAddress,
                    'isEditing' => $isEditing,
                    'isNewAddress' => (Tools::getValue('newAddress') == 'delivery'),
                    'getLocationLink' => $this->context->link->getModuleLink($this->name, 'wkmaplocation'),
                    'getDirections' => $this->l('Get Directions'),
                    'mapTemplate' => $this->fetch(
                        'module:'.$this->name.'/views/templates/front/_partials/wk-sml-map.tpl',
                        array(
                            'isEditing' => $isEditing,
                            'isOrder' => true,
                            'idAddress' => $idAddress,
                        )
                    ),
                )
            );
            $this->context->controller->registerStylesheet(
                'wk_sml_maplocation_css',
                'modules/'.$this->name.'/views/css/wk_sml_map.css'
            );
            $this->context->controller->addCSS($this->_path.'views/css/wk_sml_orders.css');
            $this->context->controller->registerJavascript(
                'wksml-checkoutmap-js',
                'modules/'.$this->name.'/views/js/front/wkcheckoutmap.js'
            );
        } elseif ($this->context->controller->php_self == ('order-confirmation' || 'order-detail')) {
            $this->context->controller->registerJavascript(
                'wk-sml-orderdetail-google-map-js',
                self::getMapJsUrl('&libraries=places'),
                array(
                    'server' => 'remote'
                )
            );
            $idOrder = Tools::getValue('id_order');
            $defaultLocation = explode(',', Configuration::get('WK_SML_DEFAULT_MAP_LOCATION'));
            $custLocationObj = new WkSmlCustomerLocation();
            $delLocation = $custLocationObj->getLocationByIdOrder($idOrder);
            $delLocId = $delLocation ? $delLocation['id_location'] : 0;
            $delLat = $delLocation ? $delLocation['lat'] : $defaultLocation[0];
            $delLng = $delLocation ? $delLocation['lng'] : $defaultLocation[1];
            if ($delLocId != 0) {
                Media::addJsDef(
                    array(
                        'iconUrl' => $this->getIconUrl(),
                        'defaultZoom' => Configuration::get('WK_SML_DEFAULT_ZOOM'),
                        'locationZoom' => Configuration::get('WK_SML_LOCATION_ZOOM'),
                        'getDirections' => $this->l('Get Directions'),
                        'locationId' => $delLocId,
                        'lats'=> (float)$delLat,
                        'lngs'=>(float)$delLng,
                    )
                );
                $this->context->controller->registerStylesheet(
                    'wk_sml_maplocation_css',
                    'modules/'.$this->name.'/views/css/wk_sml_map.css'
                );
                $this->context->controller->registerJavascript(
                    'wksml-checkoutmap-js',
                    'modules/'.$this->name.'/views/js/front/wkorderconfirmation.js'
                );
            }
            $this->context->controller->addCSS($this->_path.'views/css/wk_sml_orders.css');
        }
    }

    /**
     * Hook to dislpay map on order details
     *
     * @param [type] $params
     * @return void
     */
    public function hookDisplayOrderDetail($params)
    {
        $idOrder = Tools::getValue('id_order');
        $custLocationObj = new WkSmlCustomerLocation();
        $delLocation = $custLocationObj->getLocationByIdOrder($idOrder);
        if (!Configuration::get('WK_SML_MAP_ON_DETAILS') ||!$delLocation) {
            return;
        }
        return $this->context->smarty->fetch(
            'module:'.$this->name.'/views/templates/front/wk-order.tpl',
            array(
                'isOrder' => true,
                'isConfirmation' => false,
                'idAddress' => 0
            )
        );
    }

    /**
     * Hook to display map on order confirmation
     *
     * @return void
     */
    public function hookDisplayOrderConfirmation1()
    {
        $idOrder = Tools::getValue('id_order');
        $custLocationObj = new WkSmlCustomerLocation();
        $delLocation = $custLocationObj->getLocationByIdOrder($idOrder);
        if (!Configuration::get('WK_SML_MAP_ON_CONFIRMATION') || !$delLocation) {
            return;
        }
        return $this->context->smarty->fetch(
            'module:'.$this->name.'/views/templates/front/wk-order.tpl',
            array(
                'isOrder' => true,
                'isConfirmation' => true,
                'idAddress' => 0
            )
        );
    }

    /**
     * Create location for new address address
     *
     * @param [type] $params
     * @return void
     */
    private function addAddress($params)
    {
        if (Tools::getValue('wk_sml_lats') && Tools::getValue('wk_sml_lngs')) {
            $oldIdAddress = Tools::getValue('id_address');
            $newAddressId = $params['object']->id;
            $locMappingObj = new WkSmlCustomerLocation();

            if (($oldIdAddress == 0) || ($oldIdAddress == '')) {
                //new address, create location
                $locMappingObj->lat = Tools::getValue('wk_sml_lats');
                $locMappingObj->lng = Tools::getValue('wk_sml_lngs');

                if ($locMappingObj->save()) {
                    //create mapping for new address
                    $mappingObj = new WkSmlLocationMapping();
                    $mappingObj->id_element = $newAddressId;
                    $mappingObj->id_location = $locMappingObj->id;
                    $mappingObj->element_type = "address";
                    $mappingObj->save();
                }
            } else {
                //old address is saved and new address is created, hence copying old location mapping for new address
                if ($location = $locMappingObj->getLocationByIdAddress($oldIdAddress)) {
                    //address mapping exists
                    $mappingObj = new WkSmlLocationMapping($location['id_mapping']);
                    $mappingObj->element_type = "address";
                    $mappingObj->id_element = $newAddressId;
                    $mappingObj->id_location = $location['id_location'];
                    $mappingObj->save();
                }
            }
        }
    }

    /**
     * Create/update location on address update
     *
     * @return void
     */
    private function updateAddress()
    {
        if (Tools::getValue('wk_sml_lats') && Tools::getValue('wk_sml_lngs')) {
            $idAddress = Tools::getValue('id_address');
            $locMappingObj = new WkSmlCustomerLocation();

            if ($location = $locMappingObj->getLocationByIdAddress($idAddress)) {
                //address mapping exists
                if (!$locMappingObj->isOrderExistsWithLocationId($location['id_location'])) {
                    // if no other mapping exist with this id_location, update the same with new lat/lng
                    $locMappingObj = new WkSmlCustomerLocation($location['id_location']);
                }
                $locMappingObj->lat = Tools::getValue('wk_sml_lats');
                $locMappingObj->lng = Tools::getValue('wk_sml_lngs');
                $locMappingObj->save();
                $mappingObj = new WkSmlLocationMapping($location['id_mapping']);
            } else {
                // not exist, create new location & mapping
                $locMappingObj->lat = Tools::getValue('wk_sml_lats');
                $locMappingObj->lng = Tools::getValue('wk_sml_lngs');
                $locMappingObj->save();
                $mappingObj = new WkSmlLocationMapping();
            }
            $mappingObj->element_type = "address";
            $mappingObj->id_element = $idAddress;
            $mappingObj->id_location = $locMappingObj->id;
            $mappingObj->save();
        }
    }

    /**
     * Delete location on address delete
     *
     * @param [type] $params
     * @return void
     */
    private function deleteAddress($params)
    {
        $idAddress = $params['object']->id;
        $custLocationObj = new WkSmlCustomerLocation();
        $addressMapping = $custLocationObj->getLocationByIdAddress($idAddress);
        if ($addressMapping) {
            $mappingObj = new WkSmlLocationMapping($addressMapping['id_mapping']);
            $mappingObj->delete();
            if (!$custLocationObj->isOrderExistsWithLocationId($addressMapping['id_location'])) {
                $custLocationObj = new WkSmlCustomerLocation($addressMapping['id_location']);
                $custLocationObj->delete();
            }
        }
    }

    /**
     * Get location for given id_address in GET/POST parameters
     *
     * @param boolean $isOrder: if order page(Checkout page)
     * @param boolean $isAdmin: if Admin Controller
     * @return void
     */
    private function getLocation($isOrder = false)
    {
        $custLocationObj = new WkSmlCustomerLocation();
        if ($isOrder) {
            $idAddress = (Tools::getValue('id_address') > 0)
            ? Tools::getValue('id_address')
            : ($this->context->cart->id_address_delivery
                ? $this->context->cart->id_address_delivery
                : 0
            );
        } else {
            $idAddress = Tools::getValue('id_address');
        }
        $defaultLocation = explode(',', Configuration::get('WK_SML_DEFAULT_MAP_LOCATION'));
        $delLocation = $custLocationObj->getLocationByIdAddress($idAddress);
        $delLocId = $delLocation ? $delLocation['id_location'] : 0;
        $delLat = $delLocation ? $delLocation['lat'] : $defaultLocation[0];
        $delLng = $delLocation ? $delLocation['lng'] : $defaultLocation[1];
        return array(
            'idAddress' => $idAddress,
            'delLocId' => $delLocId,
            'delLat' => $delLat,
            'delLng' => $delLng,
        );
    }

    /**
     * Add Map Javascript & Css to various pages
     *
     * @param boolean $isAdmin: (if Admin Controller)
     * @return void
     */
    private function addMapJsToController($isAdmin = false)
    {
        $loc = $this->getLocation(false);
        $idAddress = $loc['idAddress'];
        $delLocId = $loc['delLocId'];
        $delLat = $loc['delLat'];
        $delLng = $loc['delLng'];
        $paramArray = array(
            'iconUrl' => $this->getIconUrl(),
            'defaultZoom' => Configuration::get('WK_SML_DEFAULT_ZOOM'),
            'locationZoom' => Configuration::get('WK_SML_LOCATION_ZOOM'),
            'addressId' => $idAddress,
            'setLocationLink' => $this->context->link->getModuleLink($this->name, 'wkmaplocation'),
            'getDirections' => $this->l('Get Directions'),
            'locationId' => $delLocId,
            'lats'=> (float)$delLat,
            'lngs'=>(float)$delLng
        );
        if (!$isAdmin && ($this->context->controller->php_self == 'address')) {
            //Extra params for address page in front office
            $paramArray['shouldRedirect'] = Tools::getValue('orderRedirected');
            $paramArray['mapTpl'] = $this->context->smarty->fetch(
                'module:'.$this->name.'/views/templates/hook/wk-address-form-map.tpl'
            );
            $this->context->controller->registerJavascript(
                'wk_sml_addressmap_js',
                'modules/'.$this->name.'/views/js/front/wkaddressmap.js'
            );
        }
        Media::addJsDef($paramArray);
        if ($isAdmin) {
            $this->context->controller->addJS($this->_path.'views/js/front/wkadminmaplocation.js');
            $this->context->controller->addCSS($this->_path.'views/css/wk_sml_admin_map.css');
        } else {
            $this->context->controller->registerStylesheet(
                'wk_sml_maplocation_css',
                'modules/'.$this->name.'/views/css/wk_sml_map.css'
            );
            $this->context->controller->registerJavascript(
                'wk_sml_maplocation_js',
                'modules/'.$this->name.'/views/js/front/wkmaplocation.js'
            );
        }
        return ($delLocId != 0);
    }

    /**
     * Create tables on install to store location in database
     *
     * @return void
     */
    private function createTables()
    {
        if (!file_exists(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return false;
        } elseif (!$sql = Tools::file_get_contents(dirname(__FILE__).'/'.self::INSTALL_SQL_FILE)) {
            return false;
        }
        $sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
        $sql = preg_split("/;\s*[\r\n]+/", $sql);
        foreach ($sql as $query) {
            if ($query) {
                if (!Db::getInstance()->execute(trim($query))) {
                    return false;
                }
            }
        }
        return true;
    }
    /**
     * Uninstall override
     *
     * @return void
     */
    public function uninstall()
    {
        if (!parent::uninstall()
        || !$this->dropTables()
        || !$this->deleteConfigurations()) {
            return false;
        }
        return true;
    }

    /**
     * Delete configuration settings from table on uninstall
     *
     * @return void
     */
    private function deleteConfigurations()
    {
        $configurationData = array(
            'WK_SML_GOOGLE_API_KEY',
            'WK_SML_DEFAULT_MAP_LOCATION',
            'WK_SML_DEFAULT_ZOOM',
            'WK_SML_LOCATION_ZOOM',
            'WK_SML_MAP_ON_CONFIRMATION',
            'WK_SML_MAP_ON_DETAILS',
        );
        foreach ($configurationData as $configuration) {
            if (!Configuration::deleteByName($configuration)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Destroy tables from database on uninstall
     *
     * @return void
     */
    private function dropTables()
    {
        return Db::getInstance()->execute(
            'DROP TABLE IF EXISTS
            `'._DB_PREFIX_.'wk_sml_customer_location`,
            `'._DB_PREFIX_.'wk_sml_location_mapping`'
        );
    }

    /**
     * Configuration form submit action
     *
     * @return void
     */
    public function getContent()
    {
        $this->_html = '';
        if (Tools::isSubmit('submit'.$this->name)) {
            //getting space trimmed values
            $apiKey = preg_replace('/\s+/', '', Tools::getValue('WK_SML_GOOGLE_API_KEY'));
            $defaultLocation = preg_replace('/\s+/', '', Tools::getValue('WK_SML_DEFAULT_MAP_LOCATION'));
            $defaultZoom = preg_replace('/\s+/', '', Tools::getValue('WK_SML_DEFAULT_ZOOM'));
            $locationZoom = preg_replace('/\s+/', '', Tools::getValue('WK_SML_LOCATION_ZOOM'));
            $confirmationMap = Tools::getValue('WK_SML_MAP_ON_CONFIRMATION');
            $detailsMap = Tools::getValue('WK_SML_MAP_ON_DETAILS');
            //Validation
            if (!Tools::strlen(trim($apiKey))) {
                $this->errors[] = $this->l('Please enter the Google API key.');
            }
            if (!Tools::strlen($defaultLocation)) {
                $this->errors[] = $this->l('Please enter default location coordinates.');
            } else {
                $coordinates = explode(',', $defaultLocation);
                if (count($coordinates) < 2) {
                    $this->errors[] = $this->l('Please enter comma separated lattitude/longitude in default location.');
                } elseif (!Tools::strlen($coordinates[0])) {
                    $this->errors[] = $this->l('Please enter lattitude in default location.');
                } elseif (!Tools::strlen($coordinates[1])) {
                    $this->errors[] = $this->l('Please enter longitude in default location.');
                } elseif (!Validate::isFloat($coordinates[0])) {
                    $this->errors[] = $this->l('Please enter valid lattitude in default location.');
                } elseif (!Validate::isFloat($coordinates[1])) {
                    $this->errors[] = $this->l('Please enter valid longitude in default location.');
                }
            }

            if (!Tools::strlen($defaultZoom)) {
                $this->errors[] = $this->l('Please enter zoom level for map without location.');
            } elseif (!Validate::isInt($defaultZoom)) {
                $this->errors[] = $this->l('Please enter integer value for zoom level.');
            } elseif (($defaultZoom < 0) || ($defaultZoom > 19)) {
                $this->errors[] = $this->l('Please enter zoom level value between 0-19.');
            }

            if (!Tools::strlen($locationZoom)) {
                $this->errors[] = $this->l('Please enter zoom level for map with location.');
            } elseif (!Validate::isInt($locationZoom)) {
                $this->errors[] = $this->l('Please enter integer value for zoom level.');
            } elseif (($locationZoom < 0) || ($locationZoom > 19)) {
                $this->errors[] = $this->l('Please enter zoom level value between 0-19.');
            }
            if ($_FILES['WK_SML_MARKER_ICON']['size'] > 0) {
                if (!ImageManager::isCorrectImageFileExt($_FILES['WK_SML_MARKER_ICON']['name'])) {
                    $this->errors[] = $this->l('Image file extension is not valid.');
                }
            }
            if (!empty($this->errors)) {
                $this->_html = $this->displayError($this->errors);
            } else {
                Configuration::updateValue('WK_SML_GOOGLE_API_KEY', $apiKey);
                Configuration::updateValue('WK_SML_DEFAULT_MAP_LOCATION', $defaultLocation);
                Configuration::updateValue('WK_SML_DEFAULT_ZOOM', $defaultZoom);
                Configuration::updateValue('WK_SML_LOCATION_ZOOM', $locationZoom);
                Configuration::updateValue('WK_SML_MAP_ON_CONFIRMATION', $confirmationMap);
                Configuration::updateValue('WK_SML_MAP_ON_DETAILS', $detailsMap);
                if ($_FILES['WK_SML_MARKER_ICON']['size'] > 0) {
                    $iconNameParts = explode('.', $_FILES['WK_SML_MARKER_ICON']['name']);
                    $iconExt = end($iconNameParts);
                    $fileDir = _PS_MODULE_DIR_.$this->name.'/views/img/marker.';
                    $files = glob($fileDir.'*');
                    foreach ($files as $icon) {
                        unlink($icon);
                    }
                    ImageManager::resize($_FILES['WK_SML_MARKER_ICON']['tmp_name'], $fileDir.$iconExt);
                }
                $this->_html .= $this->displayConfirmation(
                    $this->l(
                        'Settings saved successfully.'
                    )
                );
            }
        }
        Media::addJsDef(
            array(
                'deleteIconLink' => $this->context->link->getModuleLink($this->name, 'wkmaplocation'),
                'iconUrl' => $this->getIconUrl()
            )
        );
        $this->context->controller->addJS($this->_path.'views/js/admin/wkadminconfigure.js');
        return $this->_html.$this->displayForm();
    }

    private function getIconUrl()
    {
        $iconPath = _PS_MODULE_DIR_.$this->name.'/views/img/marker.';
        $exts = array('gif', 'jpg', 'jpeg', 'jpe', 'png');
        $iconUrl = false;
        foreach ($exts as $ext) {
            if (file_exists($iconPath.$ext)) {
                $iconUrl = _MODULE_DIR_.$this->name.'/views/img/marker.'.$ext;
            }
        }
        return $iconUrl;
    }

    private function getPreviewImageBlock()
    {
        $iconUrl = $this->getIconUrl();
        if ($iconUrl) {
            $preview_loader = $this->context->smarty->fetch(
                _PS_MODULE_DIR_.$this->name.'/views/templates/admin/icon_preview.tpl',
                array(
                    'imgUrl' => $iconUrl,
                )
            );
            return $preview_loader;
        }
        return '';
    }

    /**
     * Creates and returns configuration form
     *
     * @return void
     */
    private function displayForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('General Settings')
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'name' => 'WK_SML_GOOGLE_API_KEY',
                    'label' => $this->l('Google maps API key'),
                ),
                array(
                    'type' => 'text',
                    'name' => 'WK_SML_DEFAULT_MAP_LOCATION',
                    'label' => $this->l('Default coordinates'),
                    'desc' => $this->l(
                        'Default coordinates to display location on the Google Maps'.
                        ' in case a customer has not set its location. The values must be comma separated.'
                    ),
                ),
                array(
                    'type' => 'text',
                    'name' => 'WK_SML_DEFAULT_ZOOM',
                    'col' => '2',
                    'label' => $this->l('Zoom level when location is not set'),
                ),
                array(
                    'type' => 'text',
                    'name' => 'WK_SML_LOCATION_ZOOM',
                    'col' => '2',
                    'label' => $this->l('Zoom level when location is set'),
                    'desc' => $this->l('Zoom level values should be between 0-19.'),
                ),
                array(
                    'type' => 'switch',
                    'name' => 'WK_SML_MAP_ON_CONFIRMATION',
                    'label' => $this->l('Display delivery location on order confirmation page'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'WK_SML_MAP_ON_CONFIRMATION_on',
                            'value' => true,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'WK_SML_MAP_ON_CONFIRMATION_off',
                            'value' => false,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'file',
                    'name' => 'WK_SML_MARKER_ICON',
                    'image' => $this->getPreviewImageBlock(),
                    'label' => $this->l('Marker icon'),
                    'desc' => $this->l('The default marker icon will show if no icon is uploaded here.')
                ),
                array(
                    'type' => 'switch',
                    'name' => 'WK_SML_MAP_ON_DETAILS',
                    'label' => $this->l('Display delivery location on order details page'),
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'WK_SML_MAP_ON_DETAILS_on',
                            'value' => true,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'WK_SML_MAP_ON_DETAILS_off',
                            'value' => false,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
            ),
            'submit' => array(
                'name' => 'submit'.$this->name,
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );
        //Generate for with HelperForm Class
        $helper = new HelperForm();
        //Set Module, controller name, token and current index
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        //set default and allowed form languages
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_language = $default_lang;

        //toolbar settings
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;

        //Page submit action
        $helper->submit_action = 'submit'.$this->name;
        $helper->fields_value['WK_SML_GOOGLE_API_KEY'] = Configuration::get('WK_SML_GOOGLE_API_KEY');
        $helper->fields_value['WK_SML_DEFAULT_MAP_LOCATION'] = Configuration::get('WK_SML_DEFAULT_MAP_LOCATION');
        $helper->fields_value['WK_SML_DEFAULT_ZOOM'] = Configuration::get('WK_SML_DEFAULT_ZOOM');
        $helper->fields_value['WK_SML_LOCATION_ZOOM'] = Configuration::get('WK_SML_LOCATION_ZOOM');
        $helper->fields_value['WK_SML_MAP_ON_CONFIRMATION'] = Configuration::get('WK_SML_MAP_ON_CONFIRMATION');
        $helper->fields_value['WK_SML_MAP_ON_DETAILS'] = Configuration::get('WK_SML_MAP_ON_DETAILS');
        return $helper->generateForm($fields_form);
    }
}
