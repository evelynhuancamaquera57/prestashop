<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkDeliveryLocationWkMapLocationModuleFrontController extends ModuleFrontController
{
    /**
     * Ajax method to set location from AdminOrders page
     *
     * @return void
     */
    public function displayAjaxSetMapLocationForOrder()
    {
        $locationId = Tools::getValue('id_location');
        $orderId = Tools::getValue('id_order');
        $response = array(
            'status' => 'failed',
            'msg' => $this->module->l('There was some technical error.')
        );
        $deliveryLocObj = new WkSmlCustomerLocation();

        if ($locationId != 0) {
            $isMappingExists = $deliveryLocObj->isAddressLocationMappingExists($locationId);
            if ($isMappingExists) {
                $deliveryLoc = new WkSmlCustomerLocation();
            } else {
                $deliveryLoc = new WkSmlCustomerLocation($locationId);
            }
        } else {
            die(json_encode($response));
        }

        $deliveryLoc->lat = (float)Tools::getValue('lat');
        $deliveryLoc->lng = (float)Tools::getValue('lng');
        $deliveryLoc->save();
        $locationId = $deliveryLoc->id;
        $orderMappingId = $deliveryLocObj->getOrderMappingIdByOrderId($orderId);
        $orderMappingObj = new WkSmlLocationMapping($orderMappingId);
        $orderMappingObj->id_element = $orderId;
        $orderMappingObj->id_location = $locationId;
        $orderMappingObj->element_type = "order";
        if ($orderMappingObj->save()) {
            $response['status'] = 'success';
            $response['msg'] = $this->module->l('Delivery location updated successfully');
        }
        die(json_encode($response));
    }

    /**
     * Ajax method to get location for address on Checkout page
     *
     * @return void
     */
    public function displayAjaxGetMapLocationForAddress()
    {
        $addressId = Tools::getValue('id_address');
        $response = array(
            'status' => 'failed',
            'lat' => 0,
            'lng' => 0,
            'addressLink' => $this->context->link->getPageLink(
                'address',
                true,
                $this->context->language->id,
                array('id_address'=>$addressId, 'back'=>'order.php')
            )
        );
        $custLocObj = new WkSmlCustomerLocation();
        $location = $custLocObj->getLocationByIdAddress($addressId);
        if ($location) {
            $response['status'] = 'success';
            $response['lat'] = $location['lat'];
            $response['lng'] = $location['lng'];
        }
        die(json_encode($response));
    }

    public function displayAjaxDeleteIcon()
    {
        $iconUrl = Tools::getValue('iconUrl');
        $imgNameParts = explode('/', $iconUrl);
        $realIconUrl = _PS_MODULE_DIR_.'wkdeliverylocation/views/img/'.end($imgNameParts);
        if (file_exists($realIconUrl)) {
            unlink($realIconUrl);
            die(json_encode(array('status' => 'success')));
        }
        die(json_encode(array('status' => 'failed')));
    }
}
