<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class AdminOrdersController extends AdminOrdersControllerCore
{
    public function setMedia()
    {
        if (Module::isInstalled('wkdeliverylocation')) {
            AdminController::setMedia();

            $this->addJqueryUI('ui.datepicker');
            $this->addJS(_PS_JS_DIR_.'vendor/d3.v3.min.js');
            $mapJsUrl = WkDeliveryLocation::getMapJsUrl('&libraries=places');
            $this->addJS($mapJsUrl);

            if ($this->access('edit') && $this->display == 'view') {
                $this->addJS(_PS_JS_DIR_.'admin/orders.js');
                $this->addJS(_PS_JS_DIR_.'tools.js');
                $this->addJqueryPlugin('autocomplete');
            }
        } else {
            parent::setMedia();
        }
    }
}
