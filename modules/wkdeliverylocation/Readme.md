### Delivery Location

- Module V5.0.0 compatible with PrestaShop V1.7.x.x

### Important Note (If Any):
- To use this module, you must have a Google Maps API key which can be create on Google API console.

### User Guide Documentation:
https://webkul.com/blog/prestashop-delivery-location/

### Support:
https://store.webkul.com/support.html/

### Refund:
https://store.webkul.com/refund-policy.html/