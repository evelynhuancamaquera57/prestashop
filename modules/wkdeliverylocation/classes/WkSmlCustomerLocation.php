<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkSmlCustomerLocation extends ObjectModel
{
    public $id_location;
    public $lat;
    public $lng;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'wk_sml_customer_location',
        'primary' => 'id_location',
        'fields' => array(
            'lat' => array('type'=> self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'lng' => array('type'=> self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false)
        )
    );

    /**
     * Get location from given idAddress
     *
     * @param [type] $idAddress
     * @return void
     */
    public function getLocationByIdAddress($idAddress)
    {
        $result = Db::getInstance()->executeS(
            'SELECT cl.*, lm.*
            FROM `'._DB_PREFIX_.'wk_sml_customer_location` cl
            JOIN `'._DB_PREFIX_.'wk_sml_location_mapping` lm
            ON (cl.`id_location` = lm.`id_location`)
            WHERE lm.`id_element` = '.(int) $idAddress.'
            AND lm.`element_type` = "address"'
        );
        if (count($result) > 0) {
            return $result[0];
        }
        return false;
    }

    /**
     * Check if mapping exists for given idAddress
     *
     * @param [type] $idAddress
     * @return boolean
     */
    public function isAddressLocationMappingExists($idLocation)
    {
        $result = Db::getInstance()->executeS(
            'SELECT `id_mapping`
            FROM `'._DB_PREFIX_.'wk_sml_location_mapping`
            WHERE `id_location` = '.(int)$idLocation.'
            AND `element_type` = "address"'
        );
        if (!$result) {
            return false;
        }
        return true;
    }

    /**
     * Get mapping id from given idAddress
     *
     * @param [type] $idAddress
     * @return void
     */
    public function getAddressLocationMappingId($idAddress)
    {
        $result = Db::getInstance()->executeS(
            'SELECT `id_mapping`
            FROM `'._DB_PREFIX_.'wk_sml_location_mapping`
            WHERE `id_element` = '.(int) $idAddress.'
            AND `element_type` = "address"'
        );
        if (!$result) {
            return false;
        }
        return $result[0]['id_mapping'];
    }

    /**
     * Check if mapping exist for given idOrder
     *
     * @param [type] $idOrder
     * @param [type] $id_location
     * @return boolean
     */
    public function isOrderLocationMappingExists($idOrder, $id_location)
    {
        $result = Db::getInstance()->executeS(
            'SELECT `id_mapping`
            FROM `'._DB_PREFIX_.'wk_sml_location_mapping`
            WHERE `id_element` = '.(int) $idOrder.'
            AND `id_location` = '.(int)$id_location.'
            AND `element_type` = "order"'
        );
        if (!$result) {
            return false;
        }
        return $result;
    }

    /**
     * Get location bt give idOrder
     *
     * @param [type] $idOrder
     * @return void
     */
    public function getLocationByIdOrder($idOrder)
    {
        $result = Db::getInstance()->executeS(
            'SELECT cl.*, lm.*
            FROM `'._DB_PREFIX_.'wk_sml_customer_location` cl
            JOIN `'._DB_PREFIX_.'wk_sml_location_mapping` lm
            ON (cl.`id_location` = lm.`id_location`)
            WHERE lm.`id_element` = '.(int) $idOrder.'
            AND lm.`element_type` = "order"'
        );
        if (count($result) > 0) {
            return $result[0];
        }
        return false;
    }

    /**
     * Get mapping id from given orderId
     *
     * @param [type] $orderId
     * @return void
     */
    public function getOrderMappingIdByOrderId($orderId)
    {
        $result = Db::getInstance()->executeS(
            'SELECT `id_mapping`
            FROM `'._DB_PREFIX_.'wk_sml_location_mapping`
            WHERE `id_element` = '.(int)$orderId.'
            AND `element_type` = "order"'
        );
        return $result[0]['id_mapping'];
    }

    /**
     * Check if order exist for given locationId
     *
     * @param [type] $locationId
     * @return boolean
     */
    public function isOrderExistsWithLocationId($locationId)
    {
        return Db::getInstance()->executeS(
            'SELECT *
            FROM `'._DB_PREFIX_.'wk_sml_location_mapping`
            WHERE `element_type` = "order"
            AND `id_location` = '.(int)$locationId
        );
    }
}
