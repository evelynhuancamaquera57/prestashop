<?php
/**
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

class WkSmlLocationMapping extends ObjectModel
{
    public $id_mapping;
    public $id_element;
    public $id_location;
    public $element_type;
    public $date_add;
    public $date_upd;

    public static $definition = array(
        'table' => 'wk_sml_location_mapping',
        'primary' => 'id_mapping',
        'fields' => array(
            'id_element' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'id_location' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true),
            'element_type' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => false)
        )
    );
}
