{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}
<div class="form-group row ">
  <label class="col-md-3 form-control-label">
    {l s='Delivery location' mod='wkdeliverylocation'}
  </label>
  <div class="col-md-6">
    {include file="module:wkdeliverylocation/views/templates/front/_partials/wk-sml-map.tpl" isOrder=false}
  </div>
  <div class="col-md-3 form-control-comment">
    {l s='Optional' mod='wkdeliverylocation'}
  </div>
</div>
