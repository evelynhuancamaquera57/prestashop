{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="form-group">
    <div class="control-label col-lg-3">{l s='Delivery location' mod='wkdeliverylocation'}</div>
    <input type="hidden" name="wk_sml_lats" id="wk_sml_lats" value="">
    <input type="hidden" name="wk_sml_lngs" id="wk_sml_lngs" value="">
    <div id="wk_sml_previous_latlng_div" class="col-lg-2">
        <span class="h3" id="wk_sml_previous_latlng"></span>
    </div>
    <div class="col-lg-6" style="margin-top:5px;">
        <button id="wk_sml_show_map_modal" type="button" class="btn btn-info btn-lg">{l s='Set delivery location on map' mod='wkdeliverylocation'}</button>
    </div>
</div>
<div id="wk_sml_map_modal"  class="container modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-body">
            <button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
            <h3 class="modal-title">{l s='Set location' mod='wkdeliverylocation'}</h3>
            <div style="padding:0;height:100%;">
                <input id="pac-input" class="controls" type="text" name="store_location_input" value="" placeholder="{l s='Enter location' mod='wkdeliverylocation'}">
                <div>
                    <select id="type-selector" name="type" class="controls">
                        <option id="changetype-all" value="" selected>{l s='All' mod='wkdeliverylocation'}</option>
                        <option id="changetype-establishment" value="establishment">{l s='Establishments' mod='wkdeliverylocation'}</option>
                        <option id="changetype-address" value="address">{l s='Addresses' mod='wkdeliverylocation'}</option>
                        <option id="changetype-geocode" value="geocode">{l s='Geocodes' mod='wkdeliverylocation'}</option>
                    </select>
                </div>
                <div id='wk_sml_map'></div>
            </div>
        </div>
    </div>
</div>
