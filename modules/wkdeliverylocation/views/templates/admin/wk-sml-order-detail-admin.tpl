{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<div class="row">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-truck"></i>
                {l s='Delivery location' mod='wkdeliverylocation'}
            </div>
            <div class="panel-content">
                {include file="{$module_dir}wkdeliverylocation/views/templates/front/_partials/wk-sml-map.tpl" isOrder=false idAddress=0}
            </div>
            <div class="panel-footer">
                <button id="wk_sml_admin_change_location" class="btn btn-default pull-right">
                    <i class="process-icon-save"></i> {l s='Save' mod='wkdeliverylocation'}
                </button>
            </div>
        </div>
    </div>
</div>