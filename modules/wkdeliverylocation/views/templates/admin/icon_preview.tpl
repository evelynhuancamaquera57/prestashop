{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

<img src="{$imgUrl}" alt="" class="imgm img-thumbnail" id="preview-img"/>
<span
    class="btn-primary"
    id="delete-custom-icon"
    style="padding:5px;margin-left:10px;border:1px solid lightgray; cursor:pointer;">
    {l s='Delete' mod='wkdeliverylocation'}
</span>
