{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{if $isConfirmation}
<section id="wk_sml_map_location" class="card">
{/if}
    <div class="{if $isConfirmation}card-block{else}box{/if}">
    <h4>Delivery address:</h4>
    {include file="module:wkdeliverylocation/views/templates/front/_partials/wk-sml-map.tpl" isOrder=$isOrder idAddress=$idAddress}
    <div>
{if $isConfirmation}
</section>
{/if}