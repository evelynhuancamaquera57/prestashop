{*
* 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{if !$isOrder}
    <input type="hidden" name="wk_sml_lats" id="wk_sml_lats" value="">
    <input type="hidden" name="wk_sml_lngs" id="wk_sml_lngs" value="">
    <div class="col-sm-12" style="padding:0;">
        <input id="pac-input" class="controls" type="text" name="store_location_input" value="" placeholder="{l s='Enter location' mod='wkdeliverylocation'}">
        <div>
            <select id="type-selector" name="type" class="controls">
                <option id="changetype-all" value="" selected>{l s='All' mod='wkdeliverylocation'}</option>
                <option id="changetype-establishment" value="establishment">{l s='Establishments' mod='wkdeliverylocation'}</option>
                <option id="changetype-address" value="address">{l s='Addresses' mod='wkdeliverylocation'}</option>
                <option id="changetype-geocode" value="geocode">{l s='Geocodes' mod='wkdeliverylocation'}</option>
            </select>
        </div>
        <div id='wk_sml_map'>
        </div>
    </div>
{else}
    {if isset($isEditing) AND $isEditing}
        <input type="hidden" name="wk_sml_lats" id="wk_sml_lats" value="">
        <input type="hidden" name="wk_sml_lngs" id="wk_sml_lngs" value="">
        <input id="pac-input" class="controls" type="text" name="store_location_input" value="" placeholder="{l s='Enter location' mod='wkdeliverylocation'}">
        <div>
            <select id="type-selector" name="type" class="controls">
                <option id="changetype-all" value="" selected>{l s='All' mod='wkdeliverylocation'}</option>
                <option id="changetype-establishment" value="establishment">{l s='Establishments' mod='wkdeliverylocation'}</option>
                <option id="changetype-address" value="address">{l s='Addresses' mod='wkdeliverylocation'}</option>
                <option id="changetype-geocode" value="geocode">{l s='Geocodes' mod='wkdeliverylocation'}</option>
            </select>
        </div>
    {/if}
    <div id='wk_sml_map' style="display:none;"></div>
    {if $idAddress != 0}
        <div id="wk_sml_maplink" style="display:none;margin-bottom:15px;">
            <a href="{url entity='address' params=['id_address' => {$idAddress}, 'back' => 'order.php']}">{l s="Set delivery location" mod="wkdeliverylocation"}</a>
        <div>
    {/if}
{/if}