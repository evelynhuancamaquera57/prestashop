/**
 * 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/

window.onload = function() {
    var marker;
    var geocoder = new google.maps.Geocoder();
    // Initialize and add the map
    if (locationId != 0) {
        $('#wk_sml_map').show();
        $('#wk_sml_maplink').hide();
        var mapOptions = {
            center: new google.maps.LatLng(lats, lngs),
            zoom: parseInt(locationZoom)
        };
    } else {
        $('#wk_sml_map').hide();
        $('#wk_sml_maplink').hide();
        var mapOptions = {
            center: new google.maps.LatLng(28, 77),
            zoom: parseInt(defaultZoom)
        };
    }

    // The map, centered at location
    var map = new google.maps.Map(document.getElementById('wk_sml_map'), mapOptions);
    var infowindow = new google.maps.InfoWindow();
    if (locationId != 0) {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center,
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center
            });
        }
        setAddressFronLatLong(mapOptions.center);
    } else {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
            });
        }
    }

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    function setAddressFronLatLong(position)
    {
        geocoder.geocode({
            'latLng': position
        }, function (results, status) {
            if (status ==
                google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var map_address = results[1].formatted_address;
                    infowindow.setContent(map_address + '<br><div class="view-link"><a href="http://maps.google.com/maps?daddr=('+position.lat()+','+position.lng()+')" target="_blank">'+getDirections+'</a></div>');
                    infowindow.open(map, marker);
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }
}