/**
 * 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/
var marker;
var geocoder;
$(document).ready(function() {
    if ((idAddress == 0) || isEditing) {
        $('#delivery-address section').after(mapTemplate);
    } else if (!isEditing) {
        $('#checkout-addresses-step .content form p.add-address:first').after(mapTemplate);
    }

    $('#wk_sml_map').addClass('m-y-1 my-1');
    $('#wk_sml_loc_save_success').addClass('mt-1');
    $('#wk_sml_loc_save_failed').addClass('mt-1');
    $('#wk_sml_map').on('click', function() {
        $('#checkout-addresses-step div.content').css('display', 'block');
    });
    $('#checkout-addresses-step div.content').on('change', function() {
        if ($('#checkout-addresses-step div.content').css('display') == 'none') {
            $('#wk_sml_map').css('display', 'none');
        }
    });
    $(document).ajaxComplete(function() {
        console.log(mapTemplate);
        if ($('#wk_sml_map').length == 0) {
            $('#checkout-addresses-step .content form .form-footer').before(mapTemplate);
            initializeMap();
        }
    });
});
window.onload = function() {
    initializeMap();
}

function initializeMap()
{
    geocoder = new google.maps.Geocoder();
    var mapOptions = {
        center: new google.maps.LatLng(lats, lngs),
        zoom: parseInt(locationZoom)
    };
    // Initialize and add the map
    if (locationId != 0) {
        $('#wk_sml_map').show();
        $('#wk_sml_maplink').hide();
    } else {
        $('#wk_sml_map').hide();
        $('#wk_sml_maplink').show();
    }
    if (isEditing || (locationId == 0) && (idAddress == 0)) {
        //new address form is open
        $('#wk_sml_map').show();
        $('#wk_sml_maplink').hide();
    }
    // The map, centered at location
    var map = new google.maps.Map(document.getElementById('wk_sml_map'), mapOptions);

    if (document.getElementById('pac-input') != null) {
        var input = (document.getElementById('pac-input'));
        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);
        //Place auto complete action
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(parseInt(locationZoom)); // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
            $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            //get lat, lng and address from map
            $("#latitude").val(place.geometry.location.lat());
            $("#longitude").val(place.geometry.location.lng());
            $("#map_address").val('<div><strong>' + place.name + '</strong><br>' + address);
            $("#map_address_text").val($("#pac-input").val());

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address + '<br><div class="view-link"><a href="http://maps.google.com/maps?daddr=('+place.geometry.location.lat()+','+place.geometry.location.lng()+')" target="_blank">'+getDirections+'</a></div>');
            infowindow.open(map, marker);
        });
        //Change map autocomplete preference from dropdown
        $('#type-selector').on('change', function() {
            var types = [];
            if ($('#type-selector').val().length > 0) {
                types.push($('#type-selector').val());
            }
            autocomplete.setTypes(types);
        });
    }

    var infowindow = new google.maps.InfoWindow();

    if ((locationId != 0) && !isNewAddress) {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center,
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center
            });
        }
        $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
        $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
        setAddressFronLatLong(mapOptions.center);
    } else {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
            });
        }
    }

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    //Change map on address click
    $('#delivery-addresses .address-item').on('mouseup', function(e) {
        id = $('#'+$(this).attr('id')+' input').val();
        $.ajax({
            type: "POST",
            url: getLocationLink,
            data: {
                ajax: true,
                action: "getMapLocationForAddress",
                id_address: id,
            },
            dataType: "json",
            success: function(result) {
                if (result.status == "success") {
                    $('#wk_sml_map').show();
                    $('#wk_sml_maplink').hide();
                    var pos = new google.maps.LatLng(result.lat, result.lng);
                    marker.setPosition(pos);
                    $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
                    $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
                    map.setCenter(pos);
                    setAddressFronLatLong(pos);
                    $('#wk_sml_maplink a').off("click");
                } else {
                    $('#wk_sml_map').hide();
                    $('#wk_sml_maplink').show();
                    $('#wk_sml_maplink a').attr('href', result.addressLink);
                }
            },
            error: function() {
                $('#wk_sml_map').hide();
                $('#wk_sml_maplink').show();
            }
        });
    });
    if ((idAddress == 0) || isEditing) {
        google.maps.event.addListener(map, 'click', function(event) {
            marker.setPosition({lat:event.latLng.lat(), lng:event.latLng.lng()});
            $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
            $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
            setAddressFronLatLong(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()));
        });
    }
    function setAddressFronLatLong(position)
    {
        geocoder.geocode({
            'latLng': position
        }, function (results, status) {
            if (status ==
                google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var map_address = results[1].formatted_address;
                    infowindow.setContent(map_address + '<br><div class="view-link"><a href="http://maps.google.com/maps?daddr=('+position.lat()+','+position.lng()+')" target="_blank">'+getDirections+'</a></div>');
                    infowindow.open(map, marker);
                    if (input != null) {
                        input.value = map_address;
                    }
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }
}
