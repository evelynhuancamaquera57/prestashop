/**
 * 2010-2018 Webkul.
*
* NOTICE OF LICENSE
*
* All right is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2018 Webkul IN
*  @license   https://store.webkul.com/license.html
*/
var map;
$(document).ready(function() {
    $('#fieldset_0 .form-wrapper').append(mapTemplate);
});
window.onresize = function() {
    resizeMap();
}
function resizeMap()
{
    var height = $('#wk_sml_map_modal div.modal-body').height() - 50;
    var mapHeight = height+'px';
    $('#wk_sml_map').css('height', mapHeight);
    google.maps.event.trigger(map, 'resize');
}
window.onload = function() {
    var marker;
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    // Initialize and add the map
    var mapOptions = {
        center: new google.maps.LatLng(lats, lngs),
        zoom: parseInt(locationZoom)
    };
    // The map, centered at location
    map = new google.maps.Map(document.getElementById('wk_sml_map'), mapOptions);
    var input = (document.getElementById('pac-input'));
    var types = document.getElementById('type-selector');
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
    google.maps.event.trigger(map, 'resize');
    map.setTilt(45);
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    input.onfocus = function() {
        $('.pac-container').css('z-index', '1050');
    }
    if (locationId != 0) {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center,
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                position: mapOptions.center
            });
        }
        $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
        $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
        $('#wk_sml_previous_latlng').text(lats+', '+lngs);
        $('#wk_sml_previous_latlng').parent().show();
        setAddressFronLatLong(mapOptions.center);
    } else {
        if (iconUrl) {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
                icon: iconUrl
            });
        } else {
            marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29),
            });
        }
    }

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(parseInt(locationZoom)); // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
        $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
        $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
        $('#wk_sml_previous_latlng').text(marker.position.lat().toFixed(4)+', '+marker.position.lng().toFixed(4));
        $('#wk_sml_previous_latlng').parent().show();
        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        //get lat, lng and address from map
        $("#latitude").val(place.geometry.location.lat());
        $("#longitude").val(place.geometry.location.lng());
        $("#map_address").val('<div><strong>' + place.name + '</strong><br>' + address);
        $("#map_address_text").val($("#pac-input").val());

        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address + '<br><div class="view-link"><a href="http://maps.google.com/maps?daddr=('+place.geometry.location.lat()+','+place.geometry.location.lng()+')" target="_blank">'+getDirections+'</a></div>');
        infowindow.open(map, marker);
    });

    $('#type-selector').on('change', function() {
        var types = [];
        if ($('#type-selector').val().length > 0) {
            types.push($('#type-selector').val());
        }
        autocomplete.setTypes(types);
    });

    // The marker, positioned at Uluru
    google.maps.event.addListener(map, 'click', function(event) {
        infowindow.close();
        marker.setPosition({lat: event.latLng.lat(), lng: event.latLng.lng()});
        $('#wk_sml_lats').val(marker.position.lat().toFixed(4));
        $('#wk_sml_lngs').val(marker.position.lng().toFixed(4));
        $('#wk_sml_previous_latlng').text(marker.position.lat().toFixed(4)+', '+marker.position.lng().toFixed(4));
        $('#wk_sml_previous_latlng').parent().show();
        setAddressFronLatLong(new google.maps.LatLng(event.latLng.lat(), event.latLng.lng()));
    });
    $('#wk_sml_show_map_modal').on('click', function(ev) {
        ev.preventDefault();
        map.setCenter(marker.position);
        $('#wk_sml_map_modal').modal("show");
    });
    $('#wk_sml_map_modal').on('shown.bs.modal', function (e) {
        setAddressFronLatLong(marker.position);
        resizeMap();
    });

    function setAddressFronLatLong(position)
    {
        geocoder.geocode({
            'latLng': position
        }, function (results, status) {
            if (status ==
                google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var map_address = results[1].formatted_address;
                    infowindow.setContent(map_address + '<br><div class="view-link"><a href="http://maps.google.com/maps?daddr=('+position.lat()+','+position.lng()+')" target="_blank">'+getDirections+'</a></div>');
                    infowindow.open(map, marker);
                    input.value = map_address;
                }
            } else {
                console.log('Geocoder failed due to: ' + status);
            }
        });
    }
}