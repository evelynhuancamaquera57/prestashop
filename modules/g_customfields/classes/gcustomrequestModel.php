<?php
/**
* The file is Model of module. Do not modify the file if you want to upgrade the module in future
*
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

class gcustomrequestModel extends ObjectModel
{
    public $id_g_customrequest;
    public $id_g_customfields;
    public $user_ip;
    public $id_customer;
    public $id_cart;
    public $jsonrequest;
    public $reloadcustomer = 0;
    public $date_add;
    public static $definition = array(
        'table' => 'g_customrequest',
        'primary' => 'id_g_customrequest',
        'multilang' => false,
        'fields' => array(
            //Fields
            'id_g_customfields' =>  array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'user_ip'  =>  array('type' => self::TYPE_STRING,'validate' => 'isString'),
            'id_customer'  =>  array('type' => self::TYPE_INT,'validate' => 'isUnsignedInt'),
            'id_cart'  =>  array('type' => self::TYPE_INT,'validate' => 'isUnsignedInt'),
            'jsonrequest' =>  array('type' => self::TYPE_HTML,'validate' => 'isString'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'reloadcustomer' =>  array('type' => self::TYPE_INT, 'validate' => 'isBool'),
        )
    );
    public function __construct($id_g_customrequest = null, $id_lang = null, $id_shop = null)
    {
        if(version_compare(_PS_VERSION_,'1.5.3') != -1)
            Shop::addTableAssociation('g_customrequest', array('type' => 'shop'));
        parent::__construct($id_g_customrequest, $id_lang, $id_shop);

    }
    public static function getCustomerAdditional($id_customer,$id_shop=0){
        if($id_shop == 0)
            $id_shop = (int)Context::getContext()->shop->id;
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT *
            FROM '._DB_PREFIX_.'g_customrequest as a,
                 '._DB_PREFIX_.'g_customrequest_shop as c
            WHERE a.id_g_customrequest = c.id_g_customrequest
            AND c.id_shop = '.(int)$id_shop.'
            AND a.id_customer = '.(int)$id_customer
        );
    }
    public static function getFormData($typeform,$id_form,$id_cart,$id_shop=0,$id_customer = 0){
        if($id_shop == 0)
            $id_shop = (int)Context::getContext()->shop->id;
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT *
            FROM '._DB_PREFIX_.'g_customrequest as a,
                 '._DB_PREFIX_.'g_customrequest_shop as c
            WHERE a.id_g_customrequest = c.id_g_customrequest
            AND c.id_shop = '.(int)$id_shop.
            ' AND a.id_g_customfields = '.(int)$id_form.
            (
                ($typeform == '1' || $typeform == '2')
                ?
                    ($id_customer > 0 ?
                    '
                        AND  a.id_customer = '.(int)$id_customer.'
                    '
                    :
                    '
                        AND a.id_customer =  0 AND a.id_cart = '.(int)$id_cart.' AND a.reloadcustomer = 1
                    ')
                : ''

            ).
            (($typeform == '3' || $typeform == '4' || $typeform == '5') ? ' AND a.id_cart = '.(int)$id_cart : '')
        );
    }
    public static function getRequestByCart($id_cart,$id_shop){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT a.id_g_customrequest,a.id_g_customfields,a.jsonrequest
            FROM '._DB_PREFIX_.'g_customrequest as a,
                 '._DB_PREFIX_.'g_customrequest_shop as c
            WHERE a.id_g_customrequest = c.id_g_customrequest
            AND c.id_shop = '.(int)$id_shop.
            '  AND a.id_cart = '.(int)$id_cart
        );
    }
    public static function getRequestById($id_g_customrequest,$id_shop){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
            SELECT a.id_g_customrequest,a.id_g_customfields,a.jsonrequest
            FROM '._DB_PREFIX_.'g_customrequest as a,
                 '._DB_PREFIX_.'g_customrequest_shop as c
            WHERE a.id_g_customrequest = c.id_g_customrequest
            AND c.id_shop = '.(int)$id_shop.
            '  AND a.id_g_customrequest = '.(int)$id_g_customrequest
        );
    }
    public static function removeRequestForm($id_form,$id_shop = 0){
        if($id_shop == 0)
            $id_shop = (int)Context::getContext()->shop->id;
        $res = true;
        $requests = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT a.id_g_customrequest
            FROM '._DB_PREFIX_.'g_customrequest as a,
                 '._DB_PREFIX_.'g_customrequest_shop as c
            WHERE a.id_g_customrequest = c.id_g_customrequest
            AND c.id_shop = '.(int)$id_shop.'
            AND a.id_g_customfields = '.(int)$id_form
        );
        $requestsId = array();
        if($requests){
            foreach($requests as $request){
                $requestsId[] = (int)$request['id_g_customrequest'];
            }
        }
        $requestsIdString = implode(',', array_map('intval', $requestsId));
        if($requestsIdString !='' && $res){
            $res &= Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
                DELETE FROM '._DB_PREFIX_.'g_customrequest
                    WHERE  id_g_customrequest IN ('.pSQL($requestsIdString).')'
            );
            $res &= Db::getInstance(_PS_USE_SQL_SLAVE_)->execute('
                DELETE FROM '._DB_PREFIX_.'g_customrequest_shop
                    WHERE  id_g_customrequest IN ('.pSQL($requestsIdString).')
                    AND id_shop = '.(int)$id_shop
            );
        }
        return $res;
    }

}
