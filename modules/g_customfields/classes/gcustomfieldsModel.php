<?php
/**
* The file is Model of module. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsfieldsModel.php');
include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomrequestModel.php');
class gcustomfieldsModel extends ObjectModel
{
    public $id_g_customfields;
    public $active = 1;
    public $show_title = 0;
    public $typeform;
    public $formtemplate;
    public $fields;
    public $title;
    public static $definition = array(
        'table' => 'g_customfields',
        'primary' => 'id_g_customfields',
        'multilang' => true,
        'fields' => array(
            //Fields
            'active'          =>  array('type' => self::TYPE_INT, 'validate' => 'isBool'),
            'show_title'  =>  array('type' => self::TYPE_INT, 'validate' => 'isBool'),
            'typeform'          =>  array('type' => self::TYPE_INT,'required' => true, 'validate' => 'isUnsignedInt'),
            'formtemplate'  =>  array('type' => self::TYPE_HTML,'validate' => 'isCleanHtml'),
            'fields'  =>  array('type' => self::TYPE_STRING,'validate' => 'isString'),
        
            //lang = true
            'title'       =>  array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 255),
        )
    );

    public function __construct($id_g_customfields = null, $id_lang = null, $id_shop = null)
    {
        if(version_compare(_PS_VERSION_,'1.5.3') != -1)
            Shop::addTableAssociation('g_customfields', array('type' => 'shop'));
        parent::__construct($id_g_customfields, $id_lang, $id_shop);

    }
    public static function getAllBlock(){
        $id_shop = (int)Context::getContext()->shop->id;
        $id_lang = (int)Context::getContext()->language->id;
        $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
            SELECT a.*,b.title
            FROM '._DB_PREFIX_.'g_customfields as a,
                 '._DB_PREFIX_.'g_customfields_lang as b,
                 '._DB_PREFIX_.'g_customfields_shop as c
            WHERE a.id_g_customfields = b.id_g_customfields
            AND a.id_g_customfields = c.id_g_customfields
            AND c.id_shop = '.(int)$id_shop.'
            AND b.id_lang = '.(int)$id_lang.'
            AND a.active = 1'
        );
        return $res;
    }
    public static function getFormType($formtype=0){
        $forms = array();
        $id_shop = (int)Context::getContext()->shop->id;
        $sql = 'SELECT a.id_g_customfields,a.typeform
                FROM '._DB_PREFIX_.'g_customfields as a,
                     '._DB_PREFIX_.'g_customfields_shop as c
                WHERE a.id_g_customfields = c.id_g_customfields
                AND c.id_shop = '.(int)$id_shop.'
                AND a.active = 1 ';
        $formok = false;
        if(is_array($formtype)){
            $formtype = array_map('intval',$formtype); // Validate all values in array to INT
            if(count($formtype) > 0){
                $sql .= ' AND a.typeform IN ('.pSQL(implode(',',$formtype)).') ';
                $formok = true;
            }
        }elseif($formtype > 0){
            $sql .= ' AND a.typeform = '.(int)$formtype;
            $formok = true;
        }
        if($formok){
            $res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
            if($res)
                foreach($res as $_res)
                    $forms[(int)$_res['id_g_customfields']] = array(
                                                                    'id_g_customfields'=>(int)$_res['id_g_customfields'],
                                                                    'typeform'=>(int)$_res['typeform']
                                                                );
        } 
        return $forms;
    }
    public function parseTpl($id_lang,$id_shop){
        $module_dir = _PS_MODULE_DIR_.'g_customfields/views/templates/front/formtemplates/';
        $id_form = $this->id_g_customfields;
        if (!is_dir($module_dir.$id_form.'/'))
           mkdir($module_dir.$id_form.'/', 0755);
        if(!file_exists($module_dir.$id_form.'/index.php'))
            @copy(_PS_MODULE_DIR_.'g_customfields/index.php',$module_dir.$id_form.'/index.php');
        if (!is_dir($module_dir.$id_form.'/'.$id_lang.'/'))
           mkdir($module_dir.$id_form.'/'.$id_lang.'/', 0755);
        if(!file_exists($module_dir.$id_form.'/'.$id_lang.'/index.php'))
            @copy(_PS_MODULE_DIR_.'g_customfields/index.php',$module_dir.$id_form.'/'.$id_lang.'/index.php');
        $html = preg_replace('/<div class="field_content">.+<\/div>/siU', '',$this->formtemplate);
        $html = preg_replace("/<span class=\"shortcode\">(.*?)<\/span>/", "$1", $html);
        preg_match_all('/\[(g_customfields:)(.*?)\]/', $html, $matches);
        $customShortCodes = array();
        $hasupload = false;
        $newContent = '';
        if(isset($matches[0]) && $matches[0]){
            foreach($matches[0] as $key=>$content)
            {
                $matchNoBrackets = str_replace(array('[',']'),'',$content);
                $shortCodeExploded = explode(':', $matchNoBrackets);
                $customShortCodes['g_customfields'][$key] = $shortCodeExploded[1];
            }
            $newContent = $html;
            
            foreach($customShortCodes as $shortCodeKey=>$shortCode)
            {
                if($shortCodeKey == 'g_customfields')
                {
                    foreach($shortCode as $show)
                    {
                        $testingReplacementText = gcustomfieldsfieldsModel::getField($show,$id_lang,$hasupload,$id_shop);
                        $originalShortCode = "[g_customfields:$show]";
                        $newContent = str_replace($originalShortCode,$testingReplacementText,$newContent);
                    }
                }
            }
        }
        $fields_value = array(
            'hasupload' =>(int)$hasupload,
            'htmlcontent'=>$newContent,
            'idform'=>(int)$id_form,
            'id_lang'=>(int)$id_lang,
            'id_shop'=>(int)$id_shop,
            'ajax'=>1,
            'typeform'=>(int)$this->typeform,
            'actionUrl'=>Context::getContext()->link->getModuleLink('g_customfields','additional',array()),
        );
        if (version_compare(_PS_VERSION_, '1.6.0.0', '<')){
            $fields_value['old_psversion_15'] = 1;
        }
        Context::getContext()->smarty->assign($fields_value);
        $tpl = _PS_MODULE_DIR_.'g_customfields/views/templates/front/templates.tpl';
        $newContent = Context::getContext()->smarty->fetch($tpl);
        $file_name = $id_shop.'_form.tpl';   
		$file = $module_dir.$id_form.'/'.$id_lang.'/'.$file_name;
		$handle = fopen($file, 'w+');
		fwrite($handle, self::minifyHtml($newContent));
		fclose($handle);
    }
    public function delete()
    {
        $fields = $this->fields;
        $res = true;
        // delete all field in form
        if($fields !=''){
            $all_fields = explode(',',$fields);
            foreach ($all_fields as $field)
    		{
				$fieldObj = new gcustomfieldsfieldsModel((int)$field);
                if(Validate::isLoadedObject($fieldObj))
                    $res&=$fieldObj->delete();
    		}
        }
        if($this->id_g_customfields){
            $dir = _PS_MODULE_DIR_.'g_customfields/views/templates/front/formtemplates/'.$this->id_g_customfields.'/';
            self::delTree($dir);
        }
        // delete all request from form
		$res &=gcustomrequestModel::removeRequestForm((int)$this->id_g_customfields);
        $res &= parent::delete();
        return $res;
    }
    public static function delTree($dir = ''){
        if (is_dir($dir) && $dir !='')
		{
			$objects = scandir($dir);
			foreach ($objects as $object)
				if ($object != '.' && $object != '..')
				{
					if (filetype($dir.'/'.$object) == 'dir')
						self::delTree($dir.'/'.$object);
					else
						unlink($dir.'/'.$object);
				}
			reset($objects);
			rmdir($dir);
		}
    }
    public static function  minifyHtml($input = ''){
        if(method_exists('Tools','minifyHTML')){
            return Tools::minifyHTML($input);
        }else{
            if(trim($input) === "") return $input;
            // Remove extra white-space(s) between HTML attribute(s)
            $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
                return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
            }, str_replace("\r", "", $input));
            // Minify inline CSS declaration(s)
            if(strpos($input, ' style=') !== false) {
                $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
                    return '<' . $matches[1] . ' style=' . $matches[2] . gcustomfieldsModel::minify_css($matches[3]) . $matches[2];
                }, $input);
            }
            return preg_replace(
                array(
                    // t = text
                    // o = tag open
                    // c = tag close
                    // Keep important white-space(s) after self-closing HTML tag(s)
                    '#<(img|input)(>| .*?>)#s',
                    // Remove a line break and two or more white-space(s) between tag(s)
                    '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
                    '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
                    '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
                    '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
                    '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
                    '#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
                    '#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
                    '#(?<=\>)(&nbsp;)(?=\<)#', // --ibid
                    // Remove HTML comment(s) except IE comment(s)
                    '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
                ),
                array(
                    '<$1$2</$1>',
                    '$1$2$3',
                    '$1$2$3',
                    '$1$2$3$4$5',
                    '$1$2$3$4$5$6$7',
                    '$1$2$3',
                    '<$1$2',
                    '$1 ',
                    '$1',
                    ""
                ),
            $input);
        }
    }
    public static function  minify_css($input) {
        if(method_exists('Tools','minifyCSS')){
            return Tools::minifyCSS($input);
        }else{
            if(trim($input) === "") return $input;
            return preg_replace(
                array(
                    // Remove comment(s)
                    '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
                    // Remove unused white-space(s)
                    '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~+]|\s*+-(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
                    // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
                    '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
                    // Replace `:0 0 0 0` with `:0`
                    '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
                    // Replace `background-position:0` with `background-position:0 0`
                    '#(background-position):0(?=[;\}])#si',
                    // Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
                    '#(?<=[\s:,\-])0+\.(\d+)#s',
                    // Minify string value
                    '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
                    '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
                    // Minify HEX color code
                    '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
                    // Replace `(border|outline):none` with `(border|outline):0`
                    '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
                    // Remove empty selector(s)
                    '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
                ),
                array(
                    '$1',
                    '$1$2$3$4$5$6$7',
                    '$1',
                    ':0',
                    '$1:0 0',
                    '.$1',
                    '$1$3',
                    '$1$2$4$5',
                    '$1$2$3',
                    '$1:0',
                    '$1$2'
                ),
            $input);
        }
    }
}