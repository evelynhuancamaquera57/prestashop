{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

<table width="100%">
    <thead>
        <tr class="column-headers">
            <th>{l s='Fields Name' mod='g_customfields'}</th>
            <th>{l s='Fields Value' mod='g_customfields'}</th>
        </tr>
    </thead>
    <tbody>
        {foreach from=$data item=item key=key name=name}
            <tr>
                <td>{$item.name_field|escape:'htmlall':'UTF-8'}</td>
                <td>{$item.value_field|escape:'htmlall':'UTF-8'}</td>
            </tr>
        {/foreach}
    </tbody>
</table>