{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2017 Globo JSC
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/
*}

<h1 class="panel-heading">{if isset($formLabel)}{$formLabel|escape:'html':'UTF-8'}{/if}</h1>
<table class="table">
    {foreach $fieldsData as $field}
        {if $field.type != 'captcha' && $field.type != 'html' && $field.type != 'googlemap'}
        {cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_class}
        <tr class="{$bgcolor_class|escape:'html':'UTF-8'}">
            <td><strong>{$field.label|escape:'html':'UTF-8'}</strong></td>
            <td>
                {if $field.type == 'imagethumb'}
                    {if isset($field.value)}
                        {foreach $field.value as $value}
                                <img class="image_view_form" src="{$link->getPageLink('index', true) nofilter}{* html content. No need to escape*}modules/g_customfields/views/img/thumbs/{$value|escape:'htmlall':'UTF-8'}" alt="" />
                        {/foreach}
                    {/if}
                {elseif $field.type == 'color'}
                    {if $field.value !=''}
                        <span class="color_box" style="background-color:{$field.value|escape:'html':'UTF-8'};"></span>{$field.value|escape:'html':'UTF-8'}
                    {/if}
                {elseif $field.type == 'colorchoose'}
                    {if $field.value}
                        {foreach $field.value as $value}
                            <span class="color_box" style="background-color:{$value|escape:'html':'UTF-8'};"></span>{$value|escape:'html':'UTF-8'};
                        {/foreach}
                    {/if}
                {elseif $field.type == 'product'}
                    {if $field.value}
                        {foreach $field.value as $value}
                            <a href="{$value.link|escape:'html':'UTF-8'}">#{$value.id|intval} : {$value.name|escape:'html':'UTF-8'}</a>;
                        {/foreach}
                    {/if}
                {elseif $field.type == 'fileupload'}
                    {if $field.value}
                        {foreach $field.value as $value}
                            <div class="panel panel_download">
                                <a href="{$requestdownload|escape:'html':'UTF-8'}{if isset($is_backend) && $is_backend}&{/if}download={$value.name|escape:'html':'UTF-8'}">
                                    <i class="icon-cloud-download"></i>
                                    {if $value.isImage}
                                        <img class="image_view_form" src="{$baseUri|escape:'html':'UTF-8'}upload/{$value.name|escape:'html':'UTF-8'}" alt="" />
                                    {else}
                                        {$value.name|escape:'html':'UTF-8'}
                                    {/if}
                                </a>
                            </div>
                        {/foreach}
                    {/if}
                {elseif $field.type == 'htmlinput'}
                    {$field.value nofilter}{* html content. No need to escape*}
                {else}
                    {if isset($field.value)}
                        {$field.value|escape:'html':'UTF-8'}
                    {/if}
                {/if}
            </td>
        </tr>{/if}
    {/foreach}
</table>