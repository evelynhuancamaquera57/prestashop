{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if isset($field) && $field}
    {if $field.type == 'imagethumb'}
        {if isset($field.value)}
            {foreach $field.value as $value}
                    <img class="image_view_form" src="{$link->getPageLink('index', true)|escape:'html':'UTF-8'}modules/g_customfields/views/img/thumbs/{$value|escape:'html':'UTF-8'}" alt="" />
            {/foreach}
        {/if}
    {elseif $field.type == 'color'}
        {if $field.value !=''}
            <span class="color_box" style="background-color:{$field.value|escape:'html':'UTF-8'};"></span>{$field.value|escape:'html':'UTF-8'}
        {/if}
    {elseif $field.type == 'colorchoose'}
        {if $field.value}
            {foreach $field.value as $value}
                <span class="color_box" style="width:20px;height:20px;display:inline-block;background-color:{$value|escape:'html':'UTF-8'};"></span>{$value|escape:'html':'UTF-8'};
            {/foreach}
        {/if}
    {elseif $field.type == 'product'}
        {if $field.value}
            {foreach $field.value as $value}
                <a href="{$value.link|escape:'html':'UTF-8'}">#{$value.id|intval} : {$value.name|escape:'html':'UTF-8'}</a>;
            {/foreach}
        {/if}
    {elseif $field.type == 'fileupload'}
        {if $field.value}
            {foreach $field.value as $value}
                <div class="panel panel_download">
                    <a href="{$requestdownload|escape:'html':'UTF-8'}{if isset($is_backend) && $is_backend}&{/if}download={$value.name|escape:'html':'UTF-8'}">
                        <i class="icon-cloud-download"></i>
                        {if $value.isImage}
                            <img class="image_view_form" src="{$baseUri|escape:'html':'UTF-8'}upload/{$value.name|escape:'html':'UTF-8'}" alt="" />
                        {else}
                            {$value.name|escape:'html':'UTF-8'}
                        {/if}
                    </a>
                </div>
            {/foreach}
        {/if}
    {elseif $field.type == 'htmlinput'}
        {$field.value nofilter}{* html content. no need to escape*}
    {else}
        {if $field.type != 'captcha' && $field.type != 'html' && $field.type != 'googlemap'}
            {if isset($field.value)}
                {$field.value|escape:'html':'UTF-8'}
            {/if}
        {/if}
    {/if}
{/if}