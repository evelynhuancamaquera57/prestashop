{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

<button type="submit" class="btn btn-default btn btn-default pull-left" name="cancelShortcode"><i class="process-icon-cancel"></i>{l s='Cancel' mod='g_customfields'}</button>