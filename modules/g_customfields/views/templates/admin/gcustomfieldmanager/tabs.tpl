{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}
<script type="text/javascript">
// <![CDATA[
    var psversion15 = {$psversion15|intval};
    var gdefault_language = {$gdefault_language|intval};
    var gtitleform = '{l s='Form Title' mod='g_customfields'}';
//]]>
</script>
<div class="g_customfields_admintab"></div>