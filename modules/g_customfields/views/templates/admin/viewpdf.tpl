{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $fieldsDatas}
    {foreach $fieldsDatas as $key=>$fieldsData}
        <table class="table" cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th class="header" colspan="2">
        				{$formLabel[$key]|escape:'html':'UTF-8'}
        			</th>
                </tr>
            </thead>
            <tbody>
            {foreach $fieldsData as $field}
                {if $field.type != 'captcha' && $field.type != 'html' && $field.type != 'googlemap'}
                {cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_class}
                <tr class="{$bgcolor_class|escape:'html':'UTF-8'}">
                    <td style="width:30%;"><strong>{$field.label|escape:'html':'UTF-8'}</strong></td>
                    <td>
                        {if $field.type == 'imagethumb'}
                            {if isset($field.value)}
                                {foreach $field.value as $value}
                                        <img style="width:75px;" src="{$link->getPageLink('index', true)|escape:'html':'UTF-8'}modules/g_customfields/views/img/thumbs/{$value|escape:'html':'UTF-8'}" alt="" />
                                {/foreach}
                            {/if}
                        {elseif $field.type == 'color'}
                            {if $field.value !=''}
                                <span class="color_box" style="background-color:{$field.value|escape:'html':'UTF-8'};"></span>{$field.value|escape:'html':'UTF-8'}
                            {/if}
                        {elseif $field.type == 'colorchoose'}
                            {if $field.value}
                                {foreach $field.value as $value}
                                    <span class="color_box" style="background-color:{$value|escape:'html':'UTF-8'};"></span>{$value|escape:'html':'UTF-8'};
                                {/foreach}
                            {/if}
                        {elseif $field.type == 'product'}
                            {if $field.value}
                                {foreach $field.value as $value}
                                    #{$value.id|intval} : {$value.name|escape:'html':'UTF-8'};
                                {/foreach}
                            {/if}
                        {elseif $field.type == 'fileupload'}
                            {if $field.value}
                                {foreach $field.value as $value}
                                        {if $value.isImage}
                                            <img style="
                                                {if isset($value.width) && isset($value.height) && $value.width > 0 && $value.height > 0}
                                                    {if $value.width > $value.height}
                                                        {if $value.width > 45} width:45px;{/if}
                                                    {else}
                                                        {if $value.width > 45} height:45px;{/if}
                                                    {/if}
                                                {/if}
                                            " src="{$baseUri|escape:'html':'UTF-8'}{$value.name|escape:'html':'UTF-8'}" alt="" />
                                        {else}{$value.name|escape:'html':'UTF-8'};{/if}
                                {/foreach}
                            {/if}
                        {elseif $field.type == 'htmlinput'}
                            {$field.value nofilter}{* html content. no need to escape*}
                        {else}
                            {if isset($field.value)}
                                {$field.value|escape:'html':'UTF-8'}
                            {/if}
                        {/if}
                    </td>
                </tr>
                {/if}
            {/foreach}
            </tbody>
        </table>
    {/foreach}
{/if}