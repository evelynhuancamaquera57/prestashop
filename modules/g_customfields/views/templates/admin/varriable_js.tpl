{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}
<script>
var ps_vs='{$ps_vs|escape:'html':'UTF-8'}';
var controller_admin='{$controller_admin|escape:'html':'UTF-8'}';
</script>