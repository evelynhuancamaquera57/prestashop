{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

<div class="card mt-2 col-lg-6 panel">
    <div id="view_additional"  class="card-body">
        <div class="row">
        {if $fieldsDatas}{$temp_index = 0}
        {foreach $fieldsDatas as $key=>$fieldsData}{$temp_index = $temp_index + 1}
            <div id="custom_field_box_{$key|intval}" class="panel col-lg-12">
                <h3 class="card-header">{$formLabel[$key]|escape:'html':'UTF-8'}</h3>
                <table class="table">
                    {foreach $fieldsData as $field}
                        {if $field.type != 'captcha' && $field.type != 'html' && $field.type != 'googlemap'}
                        {cycle values=["color_line_even", "color_line_odd"] assign=bgcolor_class}
                        <tr class="{$bgcolor_class|escape:'html':'UTF-8'}">
                            <td><strong>{$field.label|escape:'html':'UTF-8'}</strong></td>
                            <td>
                                {if $field.type == 'imagethumb'}
                                    {if isset($field.value)}
                                        {foreach $field.value as $value}
                                                <img class="image_view_form" src="{$link->getPageLink('index', true)|escape:'html':'UTF-8'}modules/g_customfields/views/img/thumbs/{$value|escape:'html':'UTF-8'}" alt="" />
                                        {/foreach}
                                    {/if}
                                {elseif $field.type == 'color'}
                                    {if $field.value !=''}
                                        <span class="color_box" style="background-color:{$field.value|escape:'html':'UTF-8'};"></span>{$field.value|escape:'html':'UTF-8'}
                                    {/if}
                                {elseif $field.type == 'colorchoose'}
                                    {if $field.value}
                                        {foreach $field.value as $value}
                                            <span class="color_box" style="background-color:{$value|escape:'html':'UTF-8'};"></span>{$value|escape:'html':'UTF-8'};
                                        {/foreach}
                                    {/if}
                                {elseif $field.type == 'product'}
                                    {if $field.value}
                                        {foreach $field.value as $value}
                                            <a href="{$value.link|escape:'html':'UTF-8'}">#{$value.id|intval} : {$value.name|escape:'html':'UTF-8'}</a>;
                                        {/foreach}
                                    {/if}
                                {elseif $field.type == 'fileupload'}
                                    {if $field.value}
                                        {foreach $field.value as $value}
                                            <div class="panel panel_download">
                                                <a href="{$requestdownload|escape:'html':'UTF-8'}{if isset($is_backend) && $is_backend}&{/if}download={$value.name|escape:'html':'UTF-8'}">
                                                    <i class="icon-cloud-download"></i>
                                                    {if $value.isImage}
                                                        <img class="image_view_form" src="{$baseUri|escape:'html':'UTF-8'}upload/{$value.name|escape:'html':'UTF-8'}" alt="" />
                                                    {else}
                                                        {$value.name|escape:'html':'UTF-8'}
                                                    {/if}
                                                    
                                                </a>
                                            </div>
                                        {/foreach}
                                    {/if}
                                {elseif $field.type == 'htmlinput'}
                                    {$field.value nofilter}{* html content. no need to escape*}
                                {else}
                                    {if isset($field.value)}
                                        {$field.value|escape:'html':'UTF-8'}
                                    {/if}
                                {/if}
                            </td>
                        </tr>
                        {/if}
                    {/foreach}
                </table>
            </div>{if $temp_index == 2}<div style="clear:both;"></div>{$temp_index = 0}{/if}
        {/foreach}
        {/if}
    </div>
    
    <div class="clear"></div>
    
	<div id="edit_additional" class="edit_gform">
		<hr />
		{if $forms}
			{foreach $forms as $form}
				{$form nofilter}{* html content. No need to escape*}
			{/foreach}
		{/if}
		<div class="g_customfields_action">
			<button type="button" name="submitCustomerAdditional" id="submitCustomerAdditional" class="btn btn-primary float-right"><span>{l s='Save' mod='g_customfields'}<i class=" right"></i></span></button>
		</div>
	</div>
    <a class="btn btn-outline-secondary btn btn-default edit_additional" href="edit_additional" title="{l s='Edit' mod='g_customfields'}"><i class="icon-edit left"></i><span>{l s='Edit' mod='g_customfields'}</span></a>
    
   </div>
    
    <div class="clear"></div>
</div>
{addJsDef baseUri=$baseUri}
