{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $ps_version=='ps16'}
<li>
<a class="" href="{$link->getModuleLink('g_customfields', 'additional')|escape:'html':'UTF-8'}" title="{l s='Additional information' mod='g_customfields'}">
    <span class="link-item">
      <i class="icon-user"></i>
      {l s='Additional information' mod='g_customfields'}
    </span>
</a></li>
{else}
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="{$link->getModuleLink('g_customfields', 'additional')|escape:'html':'UTF-8'}" title="{l s='Additional information' mod='g_customfields'}">
    <span class="link-item">
      <i class="material-icons">&#xE853;</i>
      {l s='Additional information' mod='g_customfields'}
    </span>
</a>
{/if}
