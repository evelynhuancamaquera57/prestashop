{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $labelpos == 0 || $labelpos == 3}
    <div class="form-group survey_box">
    	{if $labelpos == 0}
    	<label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
        {/if}
        {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
            <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
        {else}
        {/literal}
    	<table cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="ng-binding"></th>
                    {foreach $description as $colurm}
                        <th class="survey_colurm">{$colurm|escape:'html':'UTF-8'}</th>
                    {/foreach}
                </tr>
            </thead>
            <tbody>
                {foreach $value as $key=>$_value}
                <tr  class="{cycle values="odd,even"}">
                    <td class="ng-binding">{$_value|escape:'html':'UTF-8'}</td>
                    {foreach $description as $colurm}
                        <td class="surveyclass {$classatt|escape:'html':'UTF-8'}"><label><input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}:{$colurm|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} checked="checked" {/if}{/literal} type="radio" name="{$name|escape:'html':'UTF-8'}[{$key|escape:'html':'UTF-8'}]" value="{$colurm|escape:'html':'UTF-8'}"></label></td>{* $name is html content, no need to escape*}
                    {/foreach}
                </tr>
                {/foreach}
            </tbody>
        </table>
        {literal}
        {/if}
        {/literal}
     </div>
{else}
    <div class="form-group survey_box">
        <div class="row">
            {if $labelpos == 1}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
            <div class="col-xs-12 col-md-8">
                {literal}
                    {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                    <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
                {else}
                {/literal}
        	   <table cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th class="ng-binding"></th>
                            {foreach $description as $colurm}
                                <th class="survey_colurm">{$colurm|escape:'html':'UTF-8'}</th>
                            {/foreach}
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $value as $key=>$_value}
                        <tr  class="{cycle values="odd,even"}">
                            <td class="ng-binding">{$_value|escape:'html':'UTF-8'}</td>
                            {foreach $description as $colurm}
                                <td class="surveyclass {$classatt|escape:'html':'UTF-8'}"><label><input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}:{$colurm|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} checked="checked" {/if}{/literal} type="radio" name="{$name|escape:'html':'UTF-8'}[{$key|escape:'html':'UTF-8'}]" value="{$colurm|escape:'html':'UTF-8'}"></label></td>{* $name is html content, no need to escape*}
                            {/foreach}
                        </tr>
                        {/foreach}
                    </tbody>
                </table>
                {literal}
                {/if}
                {/literal}
            </div>
            {if $labelpos == 2}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
        </div>
    </div>
{/if}