{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if isset($multi) && $multi}
    {if $labelpos == 0 || $labelpos == 3}
        <div class="form-group multifileupload_box">
        	{if $labelpos == 0}
        	<label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            {/if}
            {literal}
            {if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
                <div class="file_box_wp">
                    {foreach $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'] as $file}
                    <div class="panel panel_download">
                        <p class="choose_old_files">
                            <input autocomplete="false" type="checkbox" name="{/literal}{$name|escape:'html':'UTF-8'}{literal}_old[]" value="{$file.name|escape:'html':'UTF-8'}" checked="checked" />
                        </p>
                        <a href="{$requestdownload}download={$file.name}" class="btn btn-default  form_download_link" title="{l s='Click to download' mod='g_customfields'}">
                            {if $file.isImage}
                                <img class="image_view_form" src="{$base_uri}upload/{$file.name}" alt="" />
                            {/if}
            					<i class="icon-cloud-download"></i>{$file.name}</a>
                        
                     </div>
                    {/foreach}
               </div>
            {/if}
            {/literal}
            {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
             {else}
            {/literal}
        	<input  data-no-uniform="true"  multiple type="file" name="{$name|escape:'html':'UTF-8'}[]" id="{$idatt|escape:'html':'UTF-8'}" class="form-control {$classatt|escape:'html':'UTF-8'} filestyle multifileupload" data-buttonName="btn-primary"  data-buttonText="{literal}{if isset($button_upload_text)}{$button_upload_text|escape:'html':'UTF-8'}{else}{l s='Choose file' mod='g_customfields'}{/if}{/literal}" />
            {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
            {literal}
                {/if}
                {/literal}
         </div>
    {else}
        <div class="form-group multifileupload_box">
            <div class="row">
                {if $labelpos == 1}
                <div class="col-xs-12 col-md-4">
            	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
                </div>  
                {/if} 
                <div class="col-xs-12 col-md-8">
                    {literal}
                    {if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
                        <div class="file_box_wp">
                            {foreach $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'] as $file}
                                <div class="panel panel_download">
                                    <p class="choose_old_files">
                                        <input autocomplete="false" type="checkbox" name="{/literal}{$name|escape:'html':'UTF-8'}{literal}_old[]" value="{$file.name|escape:'html':'UTF-8'}" checked="checked" />
                                    </p>
                                    <a href="{$requestdownload}download={$file.name}" class="btn btn-default form_download_link" title="{l s='Click to download' mod='g_customfields'}">
                                        {if $file.isImage}
                                            <img class="image_view_form" src="{$base_uri}upload/{$file.name}" alt="" />
                                        {/if}
                        					<i class="icon-cloud-download"></i>{$file.name}</a>
                                    
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                    {/literal}
                    {literal}
                    {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                     {else}
                    {/literal}
                    <input data-no-uniform="true" multiple type="file" name="{$name|escape:'html':'UTF-8'}[]" id="{$idatt|escape:'html':'UTF-8'}" class="form-control {$classatt|escape:'html':'UTF-8'} filestyle multifileupload" data-buttonName="btn-primary"  data-buttonText="{literal}{if isset($button_upload_text)}{$button_upload_text}{else}{l s='Choose file' mod='g_customfields'}{/if}{/literal}" />
        	       {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                   {literal}
                {/if}
                {/literal}
                </div>
                {if $labelpos == 2}
                <div class="col-xs-12 col-md-4">
            	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
                </div>  
                {/if}
            </div>
        </div>
    {/if}
{else}
    {if $labelpos == 0 || $labelpos == 3}
        <div class="form-group fileupload_box">
        	{if $labelpos == 0}
        	<label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            {/if}
            {literal}
            {if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
                <div class="file_box_wp">
                    {foreach $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'] as $file}
                        <div class="panel panel_download">
                            <p class="choose_old_files">
                                <input autocomplete="false" type="checkbox" name="{/literal}{$name|escape:'html':'UTF-8'}{literal}_old" value="{$file.name|escape:'html':'UTF-8'}" checked="checked" />
                            </p>
                            <a href="{$requestdownload|escape:'html':'UTF-8'}download={$file.name|escape:'html':'UTF-8'}" class="btn btn-default form_download_link" title="{l s='Click to download' mod='g_customfields'}">
                                {if $file.isImage}
                                    <img class="image_view_form" src="{$base_uri|escape:'html':'UTF-8'}upload/{$file.name|escape:'html':'UTF-8'}" alt="" />
                                {/if}
                					<i class="icon-cloud-download"></i>{$file.name|escape:'html':'UTF-8'}</a>
                            
                        </div>
                    {/foreach}
                </div>
            {/if}
            {/literal}
            {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
             {else}
            {/literal}
        	<input type="file" name="{$name|escape:'html':'UTF-8'}" id="{$idatt|escape:'html':'UTF-8'}" class="form-control {$classatt|escape:'html':'UTF-8'} filestyle" data-buttonName="btn-primary"  data-buttonText="{literal}{if isset($button_upload_text)}{$button_upload_text|escape:'html':'UTF-8'}{else}{l s='Choose file' mod='g_customfields'}{/if}{/literal}" />
            {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
            {literal}
            {/if}
            {/literal}
         </div>
    {else}
        <div class="form-group fileupload_box">
            <div class="row">
                {if $labelpos == 1}
                <div class="col-xs-12 col-md-4">
            	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
                </div>  
                {/if}
                <div class="col-xs-12 col-md-8">
                    {literal}
                        {if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
                            <div class="file_box_wp">
                            {foreach $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'] as $file}
                                <div class="panel panel_download">
                                    <p class="choose_old_files">
                                        <input autocomplete="false" type="checkbox" name="{/literal}{$name|escape:'html':'UTF-8'}{literal}_old" value="{$file.name|escape:'html':'UTF-8'}" checked="checked" />
                                    </p>
                                    <a href="{$requestdownload  nofilter}{* $description is html content, no need to escape*}download={$file.name|escape:'html':'UTF-8'}" class="btn btn-default form_download_link" title="{l s='Click to download' mod='g_customfields'}">
                                        {if $file.isImage}
                                            <img class="image_view_form" src="{$base_uri|escape:'html':'UTF-8'}upload/{$file.name|escape:'html':'UTF-8'}" alt="" />
                                        {/if}
                        					<i class="icon-cloud-download"></i>{$file.name|escape:'html':'UTF-8'}</a>
                                    
                                </div>
                            {/foreach}
                            </div>
                        {/if}
                        {/literal}
                        {literal}
                        {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                         {else}
                        {/literal}
                    <input type="file" name="{$name|escape:'html':'UTF-8'}" id="{$idatt|escape:'html':'UTF-8'}" class="form-control {$classatt|escape:'html':'UTF-8'} filestyle" data-buttonName="btn-primary"  data-buttonText="{literal}{if isset($button_upload_text)}{$button_upload_text|escape:'html':'UTF-8'}{else}{l s='Choose file' mod='g_customfields'}{/if}{/literal}" />
        	       {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                   {literal}
                {/if}
                {/literal}
                </div>
                {if $labelpos == 2}
                <div class="col-xs-12 col-md-4">
            	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
                </div>  
                {/if}
            </div>
        </div>
    {/if}
{/if}
{*}
{literal}
<script type="text/javascript">
    if($('.fileupload_box').length > 0)
        $('.fileupload_box').parents('form').attr('enctype','multipart/form-data');
    
    if($('.multifileupload_box').length > 0)
        $('.multifileupload_box').parents('form').attr('enctype','multipart/form-data');
    
</script>
{/literal}
{*}