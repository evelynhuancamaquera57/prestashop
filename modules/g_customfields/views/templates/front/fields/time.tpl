{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $labelpos == 0 || $labelpos == 3}
    <div class="form-group time_box">
    	{if $labelpos == 0}
    	<label for="{$idatt|escape:'html':'UTF-8'}"  class="{if $required} required_label{/if} toplabel">{$label|escape:'html':'UTF-8'}</label>
        {/if}
        {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
            <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
        {else}
        {/literal}
        <input type="hidden" name="{$name|escape:'html':'UTF-8'}" id="{$idatt|escape:'html':'UTF-8'}" class="time_input {$classatt|escape:'html':'UTF-8'}" />
    	<select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_hour" name="{$name|escape:'html':'UTF-8'}-hour">
            {if $extra}
                {for $i=0 to 12}
                    <option 
                    {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][0]) && {/literal}"{$i|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][0]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                    value="{$i|escape:'html':'UTF-8'}">{$i|escape:'html':'UTF-8'}</option>
                {/for}
            {else}
                {for $i=0 to 23}
                    <option 
                    {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][0]) && {/literal}"{$i|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][0]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                    value="{$i|escape:'html':'UTF-8'}">{$i|escape:'html':'UTF-8'}</option>
                {/for}
            {/if}
        </select>
        <select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_minute" name="{$name|escape:'html':'UTF-8'}-minute">
            {for $i=0 to 11}
                <option
                {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][1]) && {/literal}"{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][1]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                 value="{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}">{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}</option>
            {/for}
        </select>
        {if $extra}
            <select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_apm" name="{$name|escape:'html':'UTF-8'}-apm">
                <option 
                {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][2]) && {/literal}"{l s='AM' mod='g_customfields'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][2]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                value="{l s='AM' mod='g_customfields'}">{l s='AM' mod='g_customfields'}</option>
                <option
                {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][2]) && {/literal}"{l s='PM' mod='g_customfields'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][2]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                 value="{l s='PM' mod='g_customfields'}">{l s='PM' mod='g_customfields'}</option>
            </select>
        {/if}
        {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
        {literal}
        {/if}
        {/literal}
     </div>
{else}
    <div class="form-group time_box">
        <div class="row">
            {if $labelpos == 1}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
            <div class="col-xs-12 col-md-8">
            {literal}
                {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
            {else}
            {/literal}
                <input type="hidden" name="{$name|escape:'html':'UTF-8'}" id="{$idatt|escape:'html':'UTF-8'}" class="time_input {$classatt|escape:'html':'UTF-8'}" />
            	<select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_hour" name="{$name|escape:'html':'UTF-8'}-hour">
                    {if $extra}
                        {for $i=0 to 12}
                            <option 
                            {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][0]) && {/literal}"{$i|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][0]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                            value="{$i|escape:'html':'UTF-8'}">{$i|escape:'html':'UTF-8'}</option>
                        {/for}
                    {else}
                        {for $i=0 to 23}
                            <option 
                            {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][0]) && {/literal}"{$i|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][0]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                            value="{$i|escape:'html':'UTF-8'}">{$i|escape:'html':'UTF-8'}</option>
                        {/for}
                    {/if}
                </select>
                <select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_minute" name="{$name|escape:'html':'UTF-8'}-minute">
                    {for $i=0 to 11}
                        <option 
                        {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][1]) && {/literal}"{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][1]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                        value="{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}">{if $i < 2}0{/if}{$i*5|escape:'html':'UTF-8'}</option>
                    {/for}
                </select>
                {if $extra}
                    <select  autocomplete="off" rel="{$name|escape:'html':'UTF-8'}" class="form-control time_select {$name|escape:'html':'UTF-8'}_apm" name="{$name|escape:'html':'UTF-8'}-apm">
                        <option
                        {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][2]) && {/literal}"{l s='AM' mod='g_customfields'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][2]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                         value="{l s='AM' mod='g_customfields'}">{l s='AM' mod='g_customfields'}</option>
                        <option 
                        {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'][2]) && {/literal}"{l s='PM' mod='g_customfields'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}'][2]} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                        value="{l s='PM' mod='g_customfields'}">{l s='PM' mod='g_customfields'}</option>
                    </select>
                {/if}
                {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                {literal}
                {/if}
                {/literal}
    	    </div>
            {if $labelpos == 2}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
            
        </div>
    </div>
{/if}