{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $labelpos == 0 || $labelpos == 3}
    <div class="form-group yesno_box">
    	{if $labelpos == 0}
    	<label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
        {/if}
        {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
            <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}'])  && {/literal}"on"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
            {l s='YES' mod='g_customfields'}
            {else}{l s='NO' mod='g_customfields'}
            {/if}</p>
        {else}
        {/literal}
            <div class="onoffswitch">
                <input {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"on"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} checked="checked" {/if}{/literal} type="checkbox" name="{$name|escape:'html':'UTF-8'}" class="{$classatt|escape:'html':'UTF-8'} onoffswitch-checkbox" id="{$idatt|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                <label class="onoffswitch-label" for="{$idatt|escape:'html':'UTF-8'}">
                    <span class="onoffswitch-inner">
                        <span class="onoffswitch-inneryes">{l s='YES' mod='g_customfields'}</span>
                        <span class="onoffswitch-innerno">{l s='NO' mod='g_customfields'}</span>
                    </span>
                    <span class="onoffswitch-switch"></span>
                </label>
                
            </div>
            {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
        {literal}
        {/if}
        {/literal}
     </div>
{else}
    <div class="form-group yesno_box">
        <div class="row">
            {if $labelpos == 1}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}           
            <div class="col-xs-12 col-md-8">
                {literal}
                    {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                    <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}'])  && {/literal}"on"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']}{* $name is html content, no need to escape*}
                    {l s='YES' mod='g_customfields'}
                    {else}{l s='NO' mod='g_customfields'}
                    {/if}</p>
                {else}
                {/literal}
                <div class="onoffswitch">
                    <input {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"on"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} checked="checked" {/if}{/literal} type="checkbox" name="{$name|escape:'html':'UTF-8'}" class="{$classatt|escape:'html':'UTF-8'} onoffswitch-checkbox" id="{$idatt|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                    <label class="onoffswitch-label" for="{$idatt|escape:'html':'UTF-8'}">
                        <span class="onoffswitch-inner">
                            <span class="onoffswitch-inneryes">{l s='YES' mod='g_customfields'}</span>
                            <span class="onoffswitch-innerno">{l s='NO' mod='g_customfields'}</span>
                        </span>
                        <span class="onoffswitch-switch"></span>
                    </label>
                </div>
                {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                {literal}
                {/if}
                {/literal}
            </div>
            {if $labelpos == 2}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
        </div>
    </div>
{/if}