{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $labelpos == 0 || $labelpos == 3}
    <div class="form-group imagethumb__box">
    	{if $labelpos == 0}
    	<label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
        {/if}
        {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
            <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
        {else}
        {/literal}
        <div class="checkbox_item_wp">
            {if $extra}
                {foreach $extra as $key=>$_value}
                    <div class="imagethumb__item_wp">
                        <div class="imagethumb__item_content">
                            <label for="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}">
                                <img src="{literal}{$base_uri|escape:'html':'UTF-8'}{/literal}modules/g_customfields/views/img/thumbs/{$_value|escape:'html':'UTF-8'}" alt="{$label|escape:'html':'UTF-8'}" />
                            </label>
                            <div class="imagethumb_input_wp">
                                {if isset($multi) && $multi}
                                <input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']|escape:'html':'UTF-8'} checked="checked" {/if}{/literal}   id="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}" type="checkbox" name="{$name|escape:'html':'UTF-8'}[]" class="{$classatt|escape:'html':'UTF-8'}" value="{$_value|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                                {else}
                                <input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']|escape:'html':'UTF-8'} checked="checked" {/if}{/literal} id="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}" type="radio" name="{$name|escape:'html':'UTF-8'}" value="{$_value|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                                {/if}
                            </div>
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
        {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
        {literal}
        {/if}
        {/literal}
    </div>
{else}
    <div class="form-group imagethumb__box">
        <div class="row">
            {if $labelpos == 1}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div> 
            {/if}
            <div class="col-xs-12 col-md-8">
                {literal}
                    {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                    <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
                {else}
                {/literal}
                <div class="checkbox_item_wp">
                    {if $extra}
                        {foreach $extra as $key=>$_value}
                            <div class="imagethumb__item_wp">
                                <div class="imagethumb__item_content">
                                    <label for="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}">
                                        <img src="{literal}{$base_uri|escape:'html':'UTF-8'}{/literal}modules/g_customfields/views/img/thumbs/{$_value|escape:'html':'UTF-8'}" alt="{$label|escape:'html':'UTF-8'}" />
                                    </label>
                                    <div class="imagethumb_input_wp">
                                        {if isset($multi) && $multi}
                                        <input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']|escape:'html':'UTF-8'} checked="checked" {/if}{/literal}   id="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}" type="checkbox" name="{$name|escape:'html':'UTF-8'}[]" class="{$classatt|escape:'html':'UTF-8'}" value="{$_value|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                                        {else}
                                        <input autocomplete="off" {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']|escape:'html':'UTF-8'} checked="checked" {/if}{/literal} id="imagethumb__{$name|escape:'html':'UTF-8'}_{$key|escape:'html':'UTF-8'}" type="radio" name="{$name|escape:'html':'UTF-8'}" value="{$_value|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
                {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                {literal}
                {/if}
                {/literal}
            </div>
            {if $labelpos == 2}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div> 
            {/if}
        </div>
    </div>
{/if}