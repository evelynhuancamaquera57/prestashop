{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

<div class="hidden_box">
    <input type="hidden" value="{literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']|escape:'html':'UTF-8'}{else}{/literal}{if $extra !=''}{literal}{${/literal}{$extra|escape:'html':'UTF-8'}{literal}}{/literal}{else}{$value|escape:'html':'UTF-8'}{/if}{literal}{/if}{/literal}" name="{$name|escape:'html':'UTF-8'}" id="{$idatt|escape:'html':'UTF-8'}" class="{$classatt|escape:'html':'UTF-8'}" />{* $name is html content, no need to escape*}
</div>