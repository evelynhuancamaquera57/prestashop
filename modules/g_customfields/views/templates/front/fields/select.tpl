{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{if $labelpos == 0 || $labelpos == 3}
    <div class="form-group select_box">
    	{if $labelpos == 0}
    	<label for="{$idatt|escape:'html':'UTF-8'}" class="{if $required} required_label{/if} toplabel">{$label|escape:'html':'UTF-8'}</label>
        {/if}
        {literal}
            {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
            <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
        {else}
        {/literal}
    	<select  autocomplete="off" name="{$name|escape:'html':'UTF-8'}{if $multi}[]{/if}" id="{$idatt|escape:'html':'UTF-8'}" class="{$classatt|escape:'html':'UTF-8'} form-control select_chosen" {if $required} required="required" {/if} {if $multi} multiple {/if}>
            {if !$required}<option value=""></option>{/if}
            {if $value}
                {if $multi}
                    {foreach $value as $_value}
                        <option 
                          
                        {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                        value="{$_value|escape:'html':'UTF-8'}">{$_value|escape:'html':'UTF-8'}</option>
                    {/foreach}
                {else}
                    {foreach $value as $_value}
                        <option 
                          
                        {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                        value="{$_value|escape:'html':'UTF-8'}">{$_value|escape:'html':'UTF-8'}</option>
                    {/foreach}
                {/if}
            {/if}
        </select>
        {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
        {literal}
        {/if}
        {/literal}
     </div>
{else}
    <div class="form-group select_box">
        <div class="row">
            {if $labelpos == 1}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
            <div class="col-xs-12 col-md-8">
                {literal}
                    {if $is_backend != '1' && isset($id_g_customrequest) && $id_g_customrequest > 0 && (!isset($usercanedit) || $usercanedit[{/literal}"{$name|escape:'html':'UTF-8'}"{literal}] != '1')}
                    <p>{if isset($formval) && isset($formval['{/literal}{$name}{literal}_old'])}{$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}_old']}{/if}</p>{* $name is html content, no need to escape*}
                {else}
                {/literal}
                <select  autocomplete="off" name="{$name|escape:'html':'UTF-8'}{if $multi}[]{/if}" id="{$idatt|escape:'html':'UTF-8'}" class="{$classatt|escape:'html':'UTF-8'} form-control select_chosen" {if $required} required="required" {/if} {if $multi} multiple {/if}>
                    {if !$required}<option value=""></option>{/if}
                    {if $value}
                        {if $multi}
                            {foreach $value as $_value}
                                <option 
                                  
                                {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}|in_array:$formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                                value="{$_value|escape:'html':'UTF-8'}">{$_value|escape:'html':'UTF-8'}</option>
                            {/foreach}
                        {else}
                            {foreach $value as $_value}
                                <option 
                                  
                                {literal}{if isset($formval) && isset($formval['{/literal}{$name}{literal}']) && {/literal}"{$_value|escape:'html':'UTF-8'}"{literal}== $formval['{/literal}{$name|escape:'html':'UTF-8'}{literal}']} selected="selected" {/if}{/literal}{* $name is html content, no need to escape*}
                                value="{$_value|escape:'html':'UTF-8'}">{$_value|escape:'html':'UTF-8'}</option>
                            {/foreach}
                        {/if}
                    {/if}
                </select>
                {if $description!=''}<p class="help-block">{$description|escape:'html':'UTF-8'}</p>{/if}
                {literal}
                {/if}
                {/literal}
    	    </div>
            {if $labelpos == 2}
            <div class="col-xs-12 col-md-4">
        	   <label for="{$idatt|escape:'html':'UTF-8'}" {if $required} class="required_label"{/if}>{$label|escape:'html':'UTF-8'}</label>
            </div>  
            {/if}
        </div>
    </div>
{/if}