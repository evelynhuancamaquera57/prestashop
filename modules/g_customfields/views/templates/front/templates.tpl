{*
* Do not edit the file if you want to upgrade the module in future.
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright 2015 GreenWeb Team
* @link	     http://www.globosoftware.net
* @license   please read license in file license.txt
*/
*}

{literal}
    {if isset($show_title) && $show_title}
        <h1 class="page-subheading">{if isset($title_form) && $title_form !=''}{$title_form}{/if}</h1>
    {/if}
{/literal}
    <div id="g_customfields_content_box_{$idform|escape:'html':'UTF-8'}" class="g_customfields_content_box g_customfields_form_type_{$typeform|escape:'html':'UTF-8'}">
        <input type="hidden" name="id_g_customrequest" value="{literal}{$id_g_customrequest|intval}{/literal}" />
        <input type="hidden" class="actionUrl" value="{literal}{$actionUrl}{/literal}" />
        {if $ajax}
            <input type="hidden" name="usingajax" value="1" />
        {else}
            <input type="hidden" name="usingajax" value="0" />
        {/if}
        {literal}
            {if isset($id_customer)}
                <input type="hidden" name="id_customer" value="{$id_customer|intval}" />
            {/if}
            {if isset($id_cart)}
                <input type="hidden" name="id_cart" value="{$id_cart|intval}" />
            {/if}
            {if isset($id_order)}
                <input type="hidden" name="id_order" value="{$id_order|intval}" />
            {/if}
        {/literal}
        <input type="hidden" name="idform" value="{$idform|escape:'html':'UTF-8'}" />
        <input type="hidden" name="id_lang" value="{$id_lang|escape:'html':'UTF-8'}" />
        <input type="hidden" name="id_shop" value="{$id_shop|escape:'html':'UTF-8'}" />
        <input type="hidden" name="typeform" value="{$typeform|escape:'html':'UTF-8'}" />
        <input type="hidden" name="gSubmitForm" value="1" />
        <div class="g_customfields_content row">
            {$htmlcontent nofilter}{* $htmlcontent is html content, no need to escape*}
            {literal}
            {if isset($id_module_customfields) && $id_module_customfields > 0}
                <div class="g_customfields_content_gdpr">
                    {hook h='displayGDPRConsent' id_module=$id_module_customfields}
                </div>
            {/if}
            {/literal}
        </div>
    </div>
{literal}
{if isset($formajax) && $formajax}
<script type="text/javascript">
    reBuildForm();
</script>
{/if}
{/literal}
