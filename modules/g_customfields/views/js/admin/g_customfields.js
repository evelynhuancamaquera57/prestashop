/**
 * This is main js file. Don't edit the file if you want to update module in future.
 * 
 * @author    Globo Software Solution JSC <contact@globosoftware.net>
 * @copyright 2015 GreenWeb Team
 * @link	     http://www.globosoftware.net
 * @license   please read license in file license.txt
 */

if (typeof PS_ALLOW_ACCENTED_CHARS_URL == 'undefined') PS_ALLOW_ACCENTED_CHARS_URL = 0;

function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
    showSuccessMessage(copyToClipboard_success);
}

function changeSildeValue() {
    minval = parseInt($('#minval').val(), 10);
    if (isNaN(minval)) {
        minval = 0;
    }
    $('#minval').val(minval);
    maxval = parseInt($('#maxval').val(), 10);
    if (isNaN(maxval) || maxval <= minval) {
        maxval = minval + 1;
    }
    $('#maxval').val(maxval);
    rangeval = parseInt($('#rangeval').val(), 10);
    if (isNaN(rangeval) || rangeval < 1) {
        rangeval = 1;
    }
    $('#rangeval').val(rangeval);
    if ($('#multi_on').is(':checked')) {
        defaultval = $('#defaultval').val();
        defaultvals = defaultval.split(';');
        defaultmin = minval;
        defaultmax = maxval;
        if (defaultvals.length > 0) {
            defaultmin = parseInt(defaultvals[0], 10);
            if (isNaN(defaultmin) || defaultmin < minval || defaultmin > maxval) {
                defaultmin = Math.floor((minval + maxval) / 2);
            }
        }
        if (defaultvals.length > 1) {
            defaultmax = parseInt(defaultvals[1], 10);
            if (isNaN(defaultmax) || defaultmax < minval || defaultmax > maxval) {
                defaultmax = Math.ceil((minval + maxval) / 2);
            }
        }
        $('#defaultval').val(defaultmin + ';' + defaultmax);
        $('#slidervalue').val(minval + ';' + maxval + ';' + rangeval + ';' + defaultmin + ';' + defaultmax);
    } else {
        defaultval = parseInt($('#defaultval').val(), 10);
        if (isNaN(defaultval) || defaultval < minval || defaultval > maxval) {
            defaultval = Math.floor((minval + maxval) / 2);
        }
        $('#defaultval').val(defaultval);
        $('#slidervalue').val(minval + ';' + maxval + ';' + rangeval + ';' + defaultval);
    }
}

$(document).ready(function() {

    $('.edit_additional').click(function() {
        if (ps_vs == '16') {
            $('#edit_additional').toggleClass('active');
        } else {
            $('#edit_additional').toggleClass();
        }
        $(window).resize();
        return false;
    });

    if (controller_admin != 'AdminCustomers' && controller_admin != 'AdminOrders') {
        if ($(".datepicker").length > 0 && $('#customer_grid_panel').length <= 0) {
            $(".datepicker").datepicker({
                changeYear: true,
                changeMonth: true,
            });
        }
    }

});

$(document).on('click', '#submitCustomerAdditional',
    function(e) {
        if (typeof tinymce != "undefined") {
            tinymce.triggerSave();
        }
        if ($('.g_customfields_content_box').length > 0) {
            if ($('.formajaxresult').length > 0) $('.formajaxresult').html('');
            $('.g_customfields_content_box').each(function() {
                psgdpr_consent_checkbox = checkGdpr($(this));
                if (psgdpr_consent_checkbox == 0) {
                    submitok = false;
                    return false;
                }
                id = $(this).attr('id');
                formURL = $(this).find('.actionUrl').val();
                var formData = new FormData();
                if ($(this).find('input[type="file"]').length > 0) {
                    $('#' + id + ' input[type="file"]').each(function() {
                        var fileUpload = $(this).get(0);
                        var files = fileUpload.files;
                        var input_file_name = $(this).attr('name');
                        for (var i = 0; i < files.length; i++) {
                            formData.append(input_file_name, files[i]);
                        }
                    })
                }
                var other_data = $('#' + id).find("*").serializeArray();
                $.each(other_data, function(key, input) {
                    formData.append(input.name, input.value);
                });
                saveFieldData($(this), formURL, formData);

            });
            if ($('.g-recaptcha').length > 0)
                if (typeof grecaptcha != "undefined") {
                    grecaptcha.reset();

                }
            if ($('body#module-g_customfields-additional').length > 0) return false;
        }
    });

function checkdataField(param, url, id) {
    var val = 0;
    if (param == 'AdminCustomers')
        var formData = '&getfieldscustom=1&checkdataField=1&id_customer=' + id;
    if (param == 'AdminOrders')
        var formData = '&getfieldscustom=1&checkdataField=1&id_order=' + id;
    $.ajax({
        type: 'POST',
        url: url,
        data: formData,
        success: function(result) {
            val = result;
        }
    });

}

$(document).ready(function() {
    //update module dev Tung 2021

    if (ps_vs == '16') {
        //PS16
        if (controller_admin == 'AdminCustomers') {
            $('.btn-group-action .btn-group').each(function() {
                var id_customer = $(this).closest("tr").find(".noborder").val();
                var url = $('#subtab-AdminGcustomfieldconfig a').attr('href');
                var formData = '&getfieldscustom=1&checkdataField=1&id_customer=' + id_customer;
                var action_btn = $(this);
                action_btn.append('<span class="showcustomfield btn btn-default" data-href="' + url + '" data-customer="' + id_customer + '" data-toggle="modal" data-target="#templateModal"><i class="icon-external-link-sign"></i></span>');

            });
        } else {
            if (controller_admin == 'AdminOrders') {

                $('.btn-group.pull-right').each(function() {
                    var id_order = $(this).closest("tr").find(".noborder").val();
                    var url = $('#subtab-AdminGcustomfieldconfig a').attr('href');
                    var formData = '&getfieldscustom=1&checkdataField=1&id_order=' + id_order;
                    var action_btn = $(this);
                    action_btn.append('<span class="showcustomfield btn btn-default" data-href="' + url + '" data-id_order="' + id_order + '" data-toggle="modal" data-target="#templateModal"><i class="icon-external-link-sign"></i></span>');

                });
            }

        }
        $('body').append(`<div id="g_popup_customfields">
            <div id="templateModal" class="modal fade in" role="dialog" style="display: none;" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content" id="grid">
                <div class="modal-body" id="g_fieldscontent">
                </div>
                </div>
            </div>
            </div>
        </div>`);

    } else {
        //PS17
        if (controller_admin == 'AdminCustomers') {
            $('.action-type .btn-group').each(function() {
                var id_customer = $(this).find('.js-delete-customer-row-action').attr('data-customer-id');
                var url = $('#subtab-AdminGcustomfieldconfig a').attr('href');
                var formData = '&getfieldscustom=1&checkdataField=1&id_customer=' + id_customer;
                var action_btn = $(this);
                var style = '';
                // $.ajax({
                //     type: 'POST',
                //     url: url,
                //     data: formData,
                //     success: function(result) {
                //         if (result == '0') {
                //             style = 'style="color:#dbdbdb"';
                //         }
                //         action_btn.append('<a ' + style + ' class="showcustomfield" href="' + url + '" data-customer="' + id_customer + '" data-toggle="modal" data-target="#templateModal"><i class="material-icons">open_in_new</i></a>');

                //     }
                // });
                action_btn.append('<a ' + style + ' class="showcustomfield" href="' + url + '" data-customer="' + id_customer + '" data-toggle="modal" data-target="#templateModal"><i class="material-icons">open_in_new</i></a>');


            });
        } else {
            if (controller_admin == 'AdminOrders') {
                $('.action-type .btn-group').each(function() {
                    var id_order = $(this).closest("tr").find(".js-bulk-action-checkbox").val();
                    var url = $('#subtab-AdminGcustomfieldconfig a').attr('href');
                    var formData = '&getfieldscustom=1&checkdataField=1&id_order=' + id_order;
                    var action_btn = $(this);
                    var style = '';
                    action_btn.append('<a ' + style + ' class="showcustomfield" href="' + url + '" data-id_order="' + id_order + '" data-toggle="modal" data-target="#templateModal"><i class="material-icons">open_in_new</i></a>');

                    // $.ajax({
                    //     type: 'POST',
                    //     url: url,
                    //     data: formData,
                    //     success: function(result) {
                    //         if (result == '0') {
                    //             style = 'style="color:#dbdbdb"';
                    //         }
                    //         action_btn.append('<a ' + style + ' class="showcustomfield" href="' + url + '" data-id_order="' + id_order + '" data-toggle="modal" data-target="#templateModal"><i class="material-icons">open_in_new</i></a>');

                    //     }
                    // });
                });
            }
        }
        $('.content-div').append(`<div id="g_popup_customfields">
            <div id="templateModal" class="modal fade in" role="dialog" style="display: none;" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content" id="grid">
                <div class="modal-body" id="g_fieldscontent">
                </div>
                </div>
            </div>
            </div>
        </div>`);

    }

    //update module dev Tung 2021
    $(document).on('click', '.showcustomfield', function() {

        if (ps_vs == '16') {
            var url = $(this).attr('data-href');
        } else {
            var url = $(this).attr('href');
        }

        if (controller_admin == 'AdminCustomers') {
            var id_customer = $(this).attr('data-customer');
            var formData = '&getfieldscustom=1&id_customer=' + id_customer;
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function(result) {
                    $('#g_popup_customfields #g_fieldscontent').html(result);
                }
            });
            return false;
        } else {
            if (controller_admin == 'AdminOrders') {
                var id_order = $(this).attr('data-id_order');
                var formData = '&getfieldscustom=1&id_order=' + id_order;
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: formData,
                    success: function(result) {
                        $('#g_popup_customfields #g_fieldscontent').html(result);
                    }
                });
                return false;
            }
        }
    });
    //update module dev Tung 2021
    $(document).on('click', '.add_multival_newval', function() {
        rel = $(this).parents('.multival_box').attr('rel');
        default_val = $('#multival_' + rel + ' .multival_newval_' + default_language).val();
        if (default_val == '') {
            /* get other data val*/
            $.each(languages, function(key, val) {
                default_val = $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val();
                if (default_val != '') return false;
            })
        }

        if (default_val == '') {
            $('#multival_' + rel + ' .value_invalid').stop().slideDown(500);
        } else {
            $('#multival_' + rel + ' .value_invalid').slideUp(500);
            if ($(this).hasClass('updatebt')) {
                /*update*/
                $.each(languages, function(key, val) {
                    _val = $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val();
                    if (_val == '') _val = default_val;
                    $('#multival_' + rel + ' .multival.inedit .lang-' + val.id_lang).html(_val);
                });
                $('#multival_' + rel + ' .cancel_multival_newval').slideUp(500);
                $('#multival_' + rel + ' .updatelabel').css('display', 'none');
                $('#multival_' + rel + ' .addlabel').css('display', 'inline-block');
                $.each(languages, function(key, val) {
                    $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val('');
                });
                $('#multival_' + rel + ' .multival').removeClass('inedit');
                $('#multival_' + rel + ' .add_multival_newval').removeClass('updatebt');
            } else {
                /*add new*/
                new_val = '<div class="multival">';
                $.each(languages, function(key, val) {

                    new_val += '<div class="translatable-field lang-' + val.id_lang + '" ';
                    if (id_language != val.id_lang) new_val += ' style="display:none"';
                    new_val += '>';

                    newval = $('.multival_newval_' + val.id_lang).val();
                    if (newval == '') newval = default_val;
                    new_val += newval;
                    new_val += '</div>';
                });
                new_val += $('#multival_' + rel + ' .multival_action_wp').html();
                new_val += '</div>';
                $(new_val).appendTo($('#multival_' + rel + ' .multival_wp'));
                $.each(languages, function(key, val) {
                    $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val('');
                });
            }
        }
        return false;
    });
    $(document).on('click', '.multival_edit', function() {
        rel = $(this).parents('.multival_box').attr('rel');
        $('#multival_' + rel + ' .multival').removeClass('inedit');
        $(this).parents('.multival').addClass('inedit');
        $.each(languages, function(key, val) {
            $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val($('#multival_' + rel + ' .multival.inedit .lang-' + val.id_lang).text());
        });
        $('#multival_' + rel + ' .cancel_multival_newval').slideDown(500);
        $('#multival_' + rel + ' .updatelabel').css('display', 'inline-block');
        $('#multival_' + rel + ' .addlabel').css('display', 'none');
        $('#multival_' + rel + ' .add_multival_newval').addClass('updatebt');
        $('#multival_' + rel + ' .value_invalid').slideUp(500);
        return false;
    });

    $(document).on('click', '.cancel_multival_newval', function() {
        rel = $(this).parents('.multival_box').attr('rel');
        $('#multival_' + rel + ' .updatelabel').css('display', 'none');
        $('#multival_' + rel + ' .addlabel').css('display', 'inline-block');
        $.each(languages, function(key, val) {
            $('#multival_' + rel + ' .multival_newval_' + val.id_lang).val('');
        });
        $('#multival_' + rel + ' .multival').removeClass('inedit');
        $('#multival_' + rel + ' .add_multival_newval').removeClass('updatebt');
        $('#multival_' + rel + ' .cancel_multival_newval').slideUp(500);
        $('#multival_' + rel + ' .value_invalid').slideUp(500);
        return false;
    });

    $(document).on('click', '.multival_delete', function() {
        $(this).parents('.multival').remove();
        return false;
    });

    $(document).on('click', '.formbuilder_minify', function() {
        itemfield_wp = $(this).parents('.formbuilder_group');
        if (itemfield_wp.hasClass('has_minify')) {
            itemfield_wp.removeClass('has_minify');
        } else {
            itemfield_wp.addClass('has_minify');
        }
        return false;
    });
    $('#g_customfields_change_status').change(function() {
        val = $(this).val();
        id = $(this).attr('rel');
        $.ajax({
            url: currentIndex + '&token=' + token + '&changeStatus=1&id=' + id + '&val=' + val,
            type: 'POST',
            dataType: 'json',
            success: function(datas) {
                if (datas.success) {
                    showSuccessMessage(datas.warrning);
                } else {
                    showErrorMessage(datas.warrning);
                }
            }
        });

    });

    if (typeof psversion15 === 'undefined') {} else
    if (psversion15 == -1) {
        $('body').addClass('psversion15');
    }
    g_customfields_overlay = '<div id="g_customfields_overlay"><div class="container"><div class="content"><div class="circle"></div></div></div></div></div>';
    var selectedProduct;
    var beforeShow_call = 0;
    $("#popup_field_config_link").fancybox({
        'closeBtn': false,
        helpers: {
            overlay: { closeClick: false }
        },
        'beforeShow': function() {
            beforeShow_call = 1;
            default_config = {
                selector: ".textareatiny",
                plugins: "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor anchor",
                browser_spellcheck: true,
                toolbar1: "code,|,bold,italic,underline,strikethrough,|,alignleft,aligncenter,alignright,alignfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,anchor,|,media,image",
                toolbar2: "",
                external_filemanager_path: ad + "/filemanager/",
                filemanager_title: "File manager",
                external_plugins: { "filemanager": ad + "/filemanager/plugin.min.js" },
                language: iso,
                skin: "prestashop",
                statusbar: false,
                relative_urls: false,
                convert_urls: false,
                entity_encoding: "raw",
                extended_valid_elements: "em[class|name|id]",
                valid_children: "+*[*]",
                valid_elements: "*[*]",
                menu: {
                    edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
                    insert: { title: 'Insert', items: 'media image link | pagebreak' },
                    view: { title: 'View', items: 'visualaid' },
                    format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
                    table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
                    tools: { title: 'Tools', items: 'code' }
                }
            };
            tinyMCE.init(default_config);

            $('.mColorPickerInput').mColorPicker();

            if ($('#loadjqueryselect2').val() == '1') {
                $('#curPackItemName').select2({
                        placeholder: search_product_msg,
                        minimumInputLength: 2,
                        width: '100%',
                        dropdownCssClass: "bootstrap",
                        ajax: {
                            url: $('#ajaxaction').val(),
                            dataType: 'json',
                            data: function(term) {
                                return {
                                    q: term,
                                    gformgetproduct: true
                                };
                            },
                            results: function(data) {
                                var excludeIds = getSelectedIds();
                                var returnIds = new Array();
                                if (data) {
                                    for (var i = data.length - 1; i >= 0; i--) {
                                        var is_in = 0;
                                        for (var j = 0; j < excludeIds.length; j++) {
                                            if (data[i].id == excludeIds[j][0] && (typeof data[i].id_product_attribute == 'undefined' || data[i].id_product_attribute == excludeIds[j][1]))
                                                is_in = 1;
                                        }
                                        if (!is_in)
                                            returnIds.push(data[i]);
                                    }
                                    return {
                                        results: returnIds
                                    }
                                } else {
                                    return {
                                        results: []
                                    }
                                }
                            }
                        },
                        formatResult: productFormatResult,
                        formatSelection: productFormatSelection,
                    })
                    .on("select2-selecting", function(e) {
                        selectedProduct = e.object
                    });
                $('#add_pack_item').on('click', addPackItem);
            } else {
                $('#curPackItemName').autocomplete('ajax_products_list.php', {
                    delay: 100,
                    minChars: 1,
                    autoFill: true,
                    max: 20,
                    matchContains: true,
                    mustMatch: true,
                    scroll: false,
                    cacheLength: 0,
                    multipleSeparator: '||',
                    formatItem: function(item) {
                        return item[1] + ' - ' + item[0];
                    },
                    extraParams: {
                        excludeIds: getSelectedIds(),
                        excludeVirtuals: 1,
                        exclude_packs: 1
                    }
                }).result(function(event, item) {
                    $('#curPackItemId').val(item[1]);
                });
                $('#add_pack_item').on('click', addPackItem);
            }
        },
        'onComplete': function() {
            if (beforeShow_call != 1) {

                beforeShow_call = 0;
                default_config = {
                    selector: ".textareatiny",
                    plugins: "colorpicker link image paste pagebreak table contextmenu filemanager table code media autoresize textcolor anchor",
                    browser_spellcheck: true,
                    toolbar1: "code,|,bold,italic,underline,strikethrough,|,alignleft,aligncenter,alignright,alignfull,formatselect,|,blockquote,colorpicker,pasteword,|,bullist,numlist,|,outdent,indent,|,link,unlink,|,anchor,|,media,image",
                    toolbar2: "",
                    external_filemanager_path: ad + "/filemanager/",
                    filemanager_title: "File manager",
                    external_plugins: { "filemanager": ad + "/filemanager/plugin.min.js" },
                    language: iso,
                    skin: "prestashop",
                    statusbar: false,
                    relative_urls: false,
                    convert_urls: false,
                    entity_encoding: "raw",
                    extended_valid_elements: "em[class|name|id]",
                    valid_children: "+*[*]",
                    valid_elements: "*[*]",
                    menu: {
                        edit: { title: 'Edit', items: 'undo redo | cut copy paste | selectall' },
                        insert: { title: 'Insert', items: 'media image link | pagebreak' },
                        view: { title: 'View', items: 'visualaid' },
                        format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
                        table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
                        tools: { title: 'Tools', items: 'code' }
                    }
                };
                tinyMCE.init(default_config);

                $('.mColorPickerInput').mColorPicker();

                if ($('#loadjqueryselect2').val() == '1') {
                    $('#curPackItemName').select2({
                            placeholder: search_product_msg,
                            minimumInputLength: 2,
                            width: '100%',
                            dropdownCssClass: "bootstrap",
                            ajax: {
                                url: $('#ajaxaction').val(),
                                dataType: 'json',
                                data: function(term) {
                                    return {
                                        q: term,
                                        gformgetproduct: true
                                    };
                                },
                                results: function(data) {
                                    var excludeIds = getSelectedIds();
                                    var returnIds = new Array();
                                    if (data) {
                                        for (var i = data.length - 1; i >= 0; i--) {
                                            var is_in = 0;
                                            for (var j = 0; j < excludeIds.length; j++) {
                                                if (data[i].id == excludeIds[j][0] && (typeof data[i].id_product_attribute == 'undefined' || data[i].id_product_attribute == excludeIds[j][1]))
                                                    is_in = 1;
                                            }
                                            if (!is_in)
                                                returnIds.push(data[i]);
                                        }
                                        return {
                                            results: returnIds
                                        }
                                    } else {
                                        return {
                                            results: []
                                        }
                                    }
                                }
                            },
                            formatResult: productFormatResult,
                            formatSelection: productFormatSelection,
                        })
                        .on("select2-selecting", function(e) {
                            selectedProduct = e.object
                        });
                    $('#add_pack_item').on('click', addPackItem);
                } else {
                    $('#curPackItemName').autocomplete('ajax_products_list.php', {
                        delay: 100,
                        minChars: 1,
                        autoFill: true,
                        max: 20,
                        matchContains: true,
                        mustMatch: true,
                        scroll: false,
                        cacheLength: 0,
                        multipleSeparator: '||',
                        formatItem: function(item) {
                            return item[1] + ' - ' + item[0];
                        },
                        extraParams: {
                            excludeIds: getSelectedIds(),
                            excludeVirtuals: 1,
                            exclude_packs: 1
                        }
                    }).result(function(event, item) {
                        $('#curPackItemId').val(item[1]);
                    });
                    $('#add_pack_item').on('click', addPackItem);
                }
            }
        }
    });

    function productFormatResult(item) {
        itemTemplate = "<div class='media'>";
        itemTemplate += "<div class='pull-left'>";
        itemTemplate += "<img class='media-object' width='40' src='" + item.image + "' alt='" + item.name + "'>";
        itemTemplate += "</div>";
        itemTemplate += "<div class='media-body'>";
        itemTemplate += "<h4 class='media-heading'>" + item.name + "</h4>";
        itemTemplate += "</div>";
        itemTemplate += "</div>";
        return itemTemplate;
    }

    function productFormatSelection(item) {
        return item.name;
    }

    function delGformproductItem(id) {

        var reg = new RegExp(',', 'g');
        var input = $('#inputPackItems');
        var name = $('#namePackItems');

        var inputCut = input.val().split(reg);
        input.val(null);
        name.val(null);
        for (var i = 0; i < inputCut.length; ++i)
            if (inputCut[i]) {
                if (inputCut[i] != id) {
                    input.val(input.val() + inputCut[i] + ',');
                }
            }
        var elem = $('.product-pack-item[data-product-id="' + id + '"]');
        elem.remove();
    }

    function getSelectedIds() {
        var reg = new RegExp(',', 'g');
        var input = $('#inputPackItems');
        if (input.val() === undefined)
            return '';
        var inputCut = input.val().split(reg);
        if ($('#loadjqueryselect2').val() == '1')
            return inputCut;
        else {
            return inputCut.join(',');
        }
    }

    function addPackItem() {
        if ($('#loadjqueryselect2').val() == '1') {
            if (selectedProduct) {
                if (typeof selectedProduct.id_product_attribute === 'undefined')
                    selectedProduct.id_product_attribute = 0;

                var divContent = $('#divPackItems').html();
                divContent += '<div class="product-pack-item media-product-pack" data-product-name="' + selectedProduct.name + '" data-product-id="' + selectedProduct.id + '">';
                divContent += '<img class="media-product-pack-img" src="' + selectedProduct.image + '"/>';
                divContent += '<span class="media-product-pack-title">' + selectedProduct.name + '</span>';
                divContent += '<button type="button" class="btn btn-default delGformproductItem media-product-pack-action" data-delete="' + selectedProduct.id + '"><i class="icon-trash"></i></button>';
                divContent += '</div>';
                var line = selectedProduct.id;
                var lineDisplay = selectedProduct.name;

                $('#divPackItems').html(divContent);
                $('#inputPackItems').val($('#inputPackItems').val() + line + ',');
                $(document).on('click', '.delGformproductItem', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    delGformproductItem($(this).data('delete'));
                    return false;
                })
                selectedProduct = null;
                $('#curPackItemName').select2("val", "");
                $('.pack-empty-warning').hide();
            } else {
                return false;
            }
        } else {
            curPackItemId = $('#curPackItemId').val();
            curPackItemName = $('#curPackItemName').val();
            if (curPackItemId > 0) {
                var divContent = $('#divPackItems').html();
                divContent += '<div class="product-pack-item media-product-pack" data-product-name="' + curPackItemName + '" data-product-id="' + curPackItemId + '">';
                divContent += '<span class="media-product-pack-title">' + curPackItemName + '</span>';
                divContent += '<button type="button" class="btn btn-default delGformproductItem media-product-pack-action" data-delete="' + curPackItemId + '"><i class="icon-trash"></i></button>';
                divContent += '</div>';
                $('#divPackItems').html(divContent);
                $('#inputPackItems').val($('#inputPackItems').val() + curPackItemId + ',');
                $('#curPackItemId').val('');
                $('#curPackItemName').val('');
                $('#curPackItemName').setOptions({
                    extraParams: {
                        excludeIds: getSelectedIds()
                    }
                });
            }
        }
    }
    $(document).on('click', '.delGformproductItem', function() {
        delGformproductItem($(this).data('delete'));
        return false;
    })

    function clearBeforeSave() {
        content = $('#formbuilder').clone();
        content.find('.itemfield_wp').removeClass('ui-sortable');
        content.find('.itemfield').removeAttr('style').removeClass('ui-draggable');
        content.find('.control_box_wp').remove();
        $('#formbuilder_content').html(content.html());
        var myString = content.html();
        var returnIds = [];
        var pattern = /\[g_customfields:(\d+)\]/gi;
        var match;
        while (match = pattern.exec(myString)) {
            id_match = parseInt(match[1]);
            returnIds.push(id_match);
        }
        $('#fields').val(returnIds.join());
    }

    function addControlBt(field, width, widthsm, widthxs) {
        if (field.hasClass('formbuilder_group'))
            control = $('#control_group').clone();
        else
            control = $('#control_box').clone();
        control.find('.formbuilder_group_width_md option').filter(function() { return this.value == width; }).attr('selected', 'selected').end();
        control.find('.formbuilder_group_width_sm option').filter(function() { return this.value == widthsm; }).attr('selected', 'selected').end();
        control.find('.formbuilder_group_width_xs option').filter(function() { return this.value == widthxs; }).attr('selected', 'selected').end();
        field.append(control.html());
    }

    function removeField(id_field, multi, group) {
        if (id_field) {
            deletefields = $('#deletefields').val();
            if (deletefields != '')
                $('#deletefields').val(deletefields + '_' + id_field);
            else
                $('#deletefields').val(id_field);
            if (multi) {
                group.remove();
            } else {
                $("#g_customfields_" + id_field).remove();
            }
        } else {
            if (multi)
                group.remove();
        }
    }
    $(document).on('click', '#add_thumb_item_fromlist', function() {
        formURL = $(this).parents('form').attr('action');
        $.ajax({
            url: formURL + '&getThumb=true',
            type: 'POST',
            success: function(thumbs) {
                if (thumbs != '') {
                    divThumbItems = '';
                    thumbsdata = thumbs.split(',');
                    $.each(thumbsdata, function(index, value) {
                        divThumbItems += '<div class="gthumb_item">';
                        divThumbItems += '<img src="' + $('#thumb_url').val() + value + '" alt="">';
                        divThumbItems += '<input type="checkbox" name="item_fromlist[]" value="' + value + '" class="item_fromlist" />';
                        divThumbItems += '</div>';
                    });
                    $('#thumbs_fromlist').html(divThumbItems);
                }
            }
        });
        return false;
    });

    $(document).on('click', '#add_thumb_item', function() {
        thumbs = $('#thumbchoose').val();
        thumbsdata = [];
        if (thumbs != '') {
            thumbsdata = thumbs.split(',');
        }
        if ($('.item_fromlist').length > 0) {
            $('.item_fromlist').each(function() {
                if (this.checked) {

                    item_fromlist_val = $(this).val();
                    thumbsdata = $.grep(thumbsdata, function(val) {
                        return item_fromlist_val != val;
                    });
                    thumbsdata.push(item_fromlist_val);
                }
            })
        }
        var filedata = $("#imagethumbupload").prop("files");
        formURL = $(this).parents('form').attr('action');
        if (window.FormData !== undefined) {
            var formData = new FormData();
            len = filedata.length;
            if (len > 0) {
                for (var i = 0; i < len; i++) {
                    formData.append("file[]", filedata[i]);
                }
                $.ajax({
                    url: formURL + '&addThumb=true',
                    type: 'POST',
                    data: formData,
                    mimeType: "multipart/form-data",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data, textStatus, jqXHR) {
                        if (data != '') {
                            datas = data.split(',');

                            $.each(datas, function(index, value) {
                                thumbsdata = $.grep(thumbsdata, function(val) {
                                    return value != val;
                                });
                                thumbsdata.push(value);
                            });
                            _allfields = thumbsdata.join(',');
                            if (_allfields.charAt(0) == ',')
                                $('#thumbchoose').val(_allfields.slice(1));
                            else
                                $('#thumbchoose').val(_allfields);
                            divThumbItems = '';
                            $.each(thumbsdata, function(index, value) {
                                divThumbItems += '<div class="gthumb_item">';
                                divThumbItems += '<img src="' + $('#thumb_url').val() + value + '" alt="">';
                                divThumbItems += '<button type="button" class="btn btn-default delThumbItem" data-delete="' + value + '"><span><i class="icon-trash"></i></button>';
                                divThumbItems += '</div>';
                            });
                            $('#divThumbItems').html(divThumbItems);
                            $("#imagethumbupload").val('');
                            $('#thumbs_fromlist').html('');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        var err = eval("(" + jqXHR.responseText + ")");
                        alert(err.Message);

                    }
                });
            } else {
                _allfields = thumbsdata.join(',');
                if (_allfields.charAt(0) == ',')
                    $('#thumbchoose').val(_allfields.slice(1));
                else
                    $('#thumbchoose').val(_allfields);
                divThumbItems = '';
                $.each(thumbsdata, function(index, value) {
                    divThumbItems += '<div class="gthumb_item">';
                    divThumbItems += '<img src="' + $('#thumb_url').val() + value + '" alt="">';
                    divThumbItems += '<button type="button" class="btn btn-default delThumbItem" data-delete="' + value + '"><span><i class="icon-trash"></i></button>';
                    divThumbItems += '</div>';
                });
                $('#divThumbItems').html(divThumbItems);
                $("#imagethumbupload").val('');
                $('#thumbs_fromlist').html('');
            }
        }
        return false;
    });

    $(document).on('click', '.delThumbItem', function() {
        data = $(this).data('delete');
        thumbs = $('#thumbchoose').val();
        thumbsdata = [];
        if (thumbs != '') {
            thumbsdata = thumbs.split(',');
        }
        thumbsdata = $.grep(thumbsdata, function(val) {
            return data != val;
        });
        _allfields = thumbsdata.join(',');
        if (_allfields.charAt(0) == ',')
            $('#thumbchoose').val(_allfields.slice(1));
        else
            $('#thumbchoose').val(_allfields);
        divThumbItems = '';
        $.each(thumbsdata, function(index, value) {
            divThumbItems += '<div class="gthumb_item">';
            divThumbItems += '<img src="' + $('#thumb_url').val() + value + '" alt="">';
            divThumbItems += '<button type="button" class="btn btn-default delThumbItem" data-delete="' + value + '"><span><i class="icon-trash"></i></button>';
            divThumbItems += '</div>';
        });
        $('#divThumbItems').html(divThumbItems);
        return false;
    });

    $(document).on('click', '#add_color_item', function() {
        data = $('.mColorPickerinput').val();
        if (data != '' && /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(data)) {
            colors = $('#colorchoose').val();
            colorsdata = [];
            if (colors != '') {
                colorsdata = colors.split(',');
            }
            colorsdata = $.grep(colorsdata, function(value) {
                return value != data;
            });
            colorsdata.push(data);
            _allfields = colorsdata.join(',');
            if (_allfields.charAt(0) == ',')
                $('#colorchoose').val(_allfields.slice(1));
            else
                $('#colorchoose').val(_allfields);
            divColorItems = '';

            $.each(colorsdata, function(index, value) {
                divColorItems += '<div style="background-color: ' + value + ';" class="color_item">';
                divColorItems += '<button type="button" class="btn btn-default delColorItem" data-delete="' + value + '"><span><i class="icon-trash"></i> ' + value + '</button>';
                divColorItems += '</div>';
            });
            $('#divColorItems').html(divColorItems);
        }
        return false;
    });

    $(document).on('click', '.delColorItem', function() {
        data = $(this).data('delete');
        colors = $('#colorchoose').val();
        colorsdata = [];
        if (colors != '') {
            colorsdata = colors.split(',');
        }
        colorsdata = $.grep(colorsdata, function(value) {
            return value != data;
        });
        _allfields = colorsdata.join(',');
        if (_allfields.charAt(0) == ',')
            $('#colorchoose').val(_allfields.slice(1));
        else
            $('#colorchoose').val(_allfields);
        divColorItems = '';

        $.each(colorsdata, function(index, value) {
            divColorItems += '<div style="background-color: ' + value + ';" class="color_item">';
            divColorItems += '<button type="button" class="btn btn-default delColorItem" data-delete="' + value + '"><span><i class="icon-trash"></i> ' + value + '</button>';
            divColorItems += '</div>';
        });
        $('#divColorItems').html(divColorItems);
        return false;
    });
    $(document).on('change', '.grating',
        function() {
            rateval = $(this).val();
            ratename = $(this).attr('name');
            for (i = 1; i <= 5; i++) {
                if (i > rateval) {
                    $('.' + ratename + 'star' + i).removeClass('active');
                } else {
                    $('.' + ratename + 'star' + i).addClass('active');
                }
            }
        });
    $('#formbuilder .itemfield').each(function() {
        width = $(this).attr("class").match(/col-md-(\d*)/)[1];
        widthsm = $(this).attr("class").match(/col-sm-(\d*)/)[1];
        widthxs = $(this).attr("class").match(/col-xs-(\d*)/)[1];
        if (width < 1 || width > 12 || width == '') width = 12;
        if (widthsm < 1 || widthsm > 12 || widthsm == '') widthsm = 12;
        if (widthxs < 1 || widthxs > 12 || widthxs == '') widthxs = 12;
        addControlBt($(this), width, widthsm, widthxs);
    });
    $('#formbuilder .formbuilder_group').each(function() {
        width = $(this).attr("class").match(/col-md-(\d*)/)[1];
        widthsm = $(this).attr("class").match(/col-sm-(\d*)/)[1];
        widthxs = $(this).attr("class").match(/col-xs-(\d*)/)[1];
        if (width < 1 || width > 12 || width == '') width = 12;
        if (widthsm < 1 || widthsm > 12 || widthsm == '') widthsm = 12;
        if (widthxs < 1 || widthxs > 12 || widthxs == '') widthxs = 12;
        addControlBt($(this), width, widthsm, widthxs);
    });
    if ($("#formbuilder").length > 0) {
        $("#formbuilder").sortable({
            opacity: 0.5,
            cursor: 'move',
            handle: '.formbuilder_move',
            update: function() {
                clearBeforeSave();
            },
        });
        $(".itemfield_wp").sortable({
            connectWith: '.itemfield_wp',
            handle: '.formbuilder_move',
            opacity: 0.5,
            cursor: 'move',
            update: function(event, ui) {
                clearBeforeSave();
            },
            beforeStop: function(event, ui) {
                newItem = ui.item;
            },
            receive: function(event, ui) {
                type = newItem.data('type');
                newitem = ui.item.data('newitem');
                if (newitem == '1') {
                    width = newItem.attr("class").match(/col-md-(\d*)/)[1];
                    widthsm = newItem.attr("class").match(/col-sm-(\d*)/)[1];
                    widthxs = newItem.attr("class").match(/col-xs-(\d*)/)[1];
                    if (width < 1 || width > 12 || width == '') width = 12;
                    if (widthsm < 1 || widthsm > 12 || widthsm == '') widthsm = 12;
                    if (widthxs < 1 || widthxs > 12 || widthxs == '') widthxs = 12;
                    addControlBt(newItem, width, widthsm, widthxs);
                    newItem.removeAttr('data-newitem');
                    $(g_customfields_overlay).appendTo('body');
                    extra_url = '';
                    if ($('#typeform').length > 0) extra_url = '&typeform=' + $('#typeform').val();
                    $.ajax({
                            url: $('#ajaxurl').val(),
                            type: 'POST',
                            data: 'typefield=' + type + extra_url,
                        })
                        .done(function(data) {
                            $('#g_customfields_overlay').remove();
                            if (data != '') {
                                $("#popup_field_config #content").html(data);
                                $("#popup_field_config_link").click();
                                newItem.attr('id', 'newfield');
                            } else {
                                newItem.remove();
                            }
                        });
                }
            }
        });
    }


    $('#addnewgroup').click(function() {
        group_width_default = $('#group_width_default').val();
        if (group_width_default > 12 || group_width_default < 1 || group_width_default == '') group_width_default = 12;
        control = $('#control_group').clone();
        control.find('.formbuilder_group_width option')
            .filter(function() {
                return this.value == group_width_default;
            })
            .attr('selected', 'selected')
            .end();
        newgroup = '<div class="formbuilder_group col-md-' + group_width_default + ' col-sm-12 col-xs-12"><div class="itemfield_wp row"></div>' + control.html() + '</div>';
        $('#formbuilder').append(newgroup);
        $('.itemfield_wp').each(function() {
            if (!$(this).hasClass('ui-sortable')) {
                $(this).sortable({
                    connectWith: '.itemfield_wp',
                    handle: '.formbuilder_move',
                    opacity: 0.5,
                    cursor: 'move',
                    update: function(event, ui) {
                        clearBeforeSave();
                    },
                    beforeStop: function(event, ui) {
                        newItem = ui.item;
                    },
                    receive: function(event, ui) {
                        type = newItem.data('type');
                        newitem = ui.item.data('newitem');
                        if (newitem) {
                            width = newItem.attr("class").match(/col-md-(\d*)/)[1];
                            widthsm = newItem.attr("class").match(/col-sm-(\d*)/)[1];
                            widthxs = newItem.attr("class").match(/col-xs-(\d*)/)[1];
                            if (width < 1 || width > 12 || width == '') width = 12;
                            if (widthsm < 1 || widthsm > 12 || widthsm == '') widthsm = 12;
                            if (widthxs < 1 || widthxs > 12 || widthxs == '') widthxs = 12;
                            addControlBt(newItem, width, widthsm, widthxs);
                            newItem.removeAttr('data-newitem');
                            $(g_customfields_overlay).appendTo('body');
                            $.ajax({
                                    url: $('#ajaxurl').val(),
                                    type: 'POST',
                                    data: 'typefield=' + type,
                                })
                                .done(function(data) {
                                    $('#g_customfields_overlay').remove();
                                    if (data != '') {
                                        $("#popup_field_config #content").html(data);
                                        $("#popup_field_config_link").click();
                                        newItem.attr('id', 'newfield');
                                    } else {
                                        newItem.remove();
                                    }
                                });
                        }
                    }
                });
            }
        })
        $("#formbuilder").sortable("refresh");
        $(".itemfield_wp").sortable("refresh");
    });
    $('#addnewgroupbreak').click(function() {
        newgroup = '<div class="formbuilder_group formbuilder_group_break col-md-12 col-sm-12 col-xs-12">' + control.html() + '</div>';
        $('#formbuilder').append(newgroup);
        $("#formbuilder").sortable("refresh");
    });
    if ($("#itemfieldparent").length > 0)
        $("#itemfieldparent .itemfield").draggable({
            connectToSortable: '.itemfield_wp',
            helper: 'clone',
            revert: 'invalid'
        });

    $(document).on('click', '.formbuilder_edit', function() {
        $(g_customfields_overlay).appendTo('body');
        id = $(this).parents('.itemfield').attr("id").match(/g_customfields_(\d*)/)[1];
        if (id) {
            extra_url = '';
            if ($('#typeform').length > 0) extra_url = '&typeform=' + $('#typeform').val();
            $.ajax({
                    url: $('#ajaxurl').val(),
                    type: 'POST',
                    data: 'typefield=true&id_g_customfieldsfields=' + id + extra_url,
                })
                .done(function(data) {
                    $('#g_customfields_overlay').remove();
                    if (data != '') {
                        $("#popup_field_config #content").html(data);
                        $("#popup_field_config_link").click();
                    }
                });
        }
        return false;
    });


    $(document).on('click', '.formbuilder_delete', function() {
        if ($(this).hasClass('formbuilder_delete_group')) {
            group = $(this).parents('.formbuilder_group');
            itemfields = group.find(".itemfield");
            ids = [];
            allfields = $('#fields').val().split(',');
            if (itemfields)
                itemfields.each(function() {
                    id = $(this).attr("id").match(/g_customfields_(\d*)/)[1];
                    if (id > 0) {
                        ids.push(id);
                        allfields = $.grep(allfields, function(value) {
                            return value != id;
                        });
                        $('#fields').val(allfields);
                    }
                });
            removeField(ids.join('_'), 1, group);
        } else {
            id = $(this).parents('.itemfield').attr("id").match(/g_customfields_(\d*)/)[1];
            if (id) {
                removeField(id, 0, null);
                allfields = $('#fields').val().split(',');
                allfields = $.grep(allfields, function(value) {
                    return value != id;
                });
                $('#fields').val(allfields);
            }
        }
        return false;
    });
    $('.label_lang').attr('style', '');
    $('.label_lang_' + $('#idlang_default').val()).css('display', 'block');

    $(document).on('click', 'button[name="addShortcode"],input[name="addShortcode"]',
        function() {
            if ($('.slidervalue').length > 0) changeSildeValue();
            form = $(this).parents('form');
            if (form.find('.textareatiny').length > 0) tinyMCE.triggerSave();
            form.find('.alert-danger').remove();
            formok = true;
            form.find('.tagify').each(function() {
                $(this).val($(this).tagify('serialize'));
            });
            if ($('.multival_wp').length > 0) {
                $.each(languages, function(key, val) {
                    if ($('#multival_value').length > 0) {
                        multival_wp = [];
                        $('#multival_value .multival_wp .multival').each(function() {
                            multival_wp.push($(this).find('.lang-' + val.id_lang).html());
                        });
                        $('#multival_value #value_' + val.id_lang).val(multival_wp.join());
                    }
                    if ($('#multival_description').length > 0) {
                        multival_wp = [];
                        $('#multival_description .multival_wp .multival').each(function() {
                            multival_wp.push($(this).find('.lang-' + val.id_lang).html());
                        });
                        $('#multival_description #description_' + val.id_lang).val(multival_wp.join());
                    }
                });
            }
            form.find('.gvalidate_isRequired').each(function() {
                if ($(this).val() == '') {
                    namenolang = $(this).attr('name').split('_');
                    if (namenolang.length > 1) {
                        if ($('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').length > 0 && $('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').val() != '') {
                            $(this).val($('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').val());
                        } else {
                            langerror = '';
                            $.each(languages, function(index, value) {
                                if (value.id_lang == namenolang[1]) {
                                    langerror = '(' + value.name + ')';
                                }
                            });

                            if (typeof psversion15 != 'undefined' && psversion15 != -1) {
                                form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + langerror + empty_danger + '</div>');
                            } else
                                form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + langerror + empty_danger + '</div>');
                            formok = false;
                        }
                    } else {
                        if (typeof psversion15 != 'undefined' && psversion15 != -1)
                            form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                        else
                            form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                        formok = false;
                    }
                }
            });
            form.find('.gvalidate_isRequired2').each(function() {
                if ($(this).val() == '') {
                    namenolang = $(this).attr('name').split('_');
                    if (namenolang.length > 1) {
                        if ($('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').length > 0 && $('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').val() != '') {
                            $(this).val($('input[name="' + namenolang[0] + '_' + gdefault_language + '"]').val());
                        } else
                        if ($('#extra').val() == '') {
                            langerror = '';
                            $.each(languages, function(index, value) {
                                if (value.id_lang == namenolang[1]) {
                                    langerror = '(' + value.name + ')';
                                }
                            });
                            if (typeof psversion15 != 'undefined' && psversion15 != -1)
                                form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + langerror + empty_danger + '</div>');
                            else
                                form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + langerror + empty_danger + '</div>');
                            formok = false;
                        }
                    } else {
                        if ($('#extra').val() == '') {
                            if (typeof psversion15 != 'undefined' && psversion15 != -1)
                                form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                            else
                                form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                            formok = false;
                        }
                    }

                }
            });
            form.find('.gvalidate_isRequired3').each(function() {
                val = $(this).val();
                if ($(this).val() == '') {
                    if (typeof psversion15 != 'undefined' && psversion15 != -1)
                        form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                    else form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                    formok = false;
                } else {
                    vals = val.split(',');
                    if (vals[0] == "undefined") {
                        if (typeof psversion15 != 'undefined' && psversion15 != -1)
                            form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                        else form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                        formok = false;
                    } else
                    if (vals[1] == "undefined") {
                        if (typeof psversion15 != 'undefined' && psversion15 != -1)
                            form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                        else form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                        formok = false;
                    } else
                    if (vals[2] == "undefined") {
                        if (typeof psversion15 != 'undefined' && psversion15 != -1)
                            form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                        else form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                        formok = false;
                    } else
                    if (vals[3] == "undefined") {
                        if (typeof psversion15 != 'undefined' && psversion15 != -1)
                            form.find('.form-wrapper').append('<div class="alert alert-danger">' + $(this).parents('.form-group').find('.control-label').html() + empty_danger + '</div>');
                        else form.find('fieldset').append('<div class="alert alert-danger">' + $(this).parents('.margin-form').prev('label').html() + empty_danger + '</div>');
                        formok = false;
                    }
                }
            });
            if (!formok) {
                return false;
            }
            btform = $(this);
            btform.attr('disable', 'disable');
            action = form.attr('action');
            serializedForm = form.serialize();
            name = form.find('#name').val();
            labels = '';
            $.each(languages, function(index, value) {
                labels += '<span class="label_lang label_lang_' + value.id_lang + '">' + form.find('#label_' + value.id_lang).val() + '</span>';
            });
            $(g_customfields_overlay).appendTo('body');
            $.ajax({
                type: 'POST',
                url: action,
                data: serializedForm,
                async: false,
                success: function(data, textStatus, request) {
                    btform.removeAttr('disable');
                    $('#g_customfields_overlay').remove();
                    data = data.replace(/\s/g, '');
                    if (data > 0) {
                        $('#newfield .shortcode').html('[g_customfields:' + data + ']');
                        $('#newfield').removeAttr('id').attr('id', 'g_customfields_' + data);
                        $('#g_customfields_' + data).find('.feildname').html('{' + name + '}');
                        $('#g_customfields_' + data).find('.feildlabel').html(labels);
                        $('.label_lang').attr('style', '');
                        $('.label_lang_' + $('#idlang_default').val()).css('display', 'block');
                        fields_val = $('#fields').val() + '';
                        fields = fields_val.split(',');
                        fields = $.grep(fields, function(value) {
                            return value != data;
                        });
                        fields.push(data);
                        _allfields = fields.join(',');
                        if (_allfields.charAt(0) == ',')
                            $('#fields').val(_allfields.slice(1));
                        else
                            $('#fields').val(_allfields);
                        $.fancybox.close();
                    } else {
                        alert & ("Error occurred!");
                    }
                },
                error: function(req, status, error) {
                    $('#g_customfields_overlay').remove();
                    alert & ("Error occurred!");
                }
            });
            return false;
        });

    $(document).on('click', 'button[name="cancelShortcode"],input[name="cancelShortcode"]',
        function() {
            $('#newfield').remove();
            $.fancybox.close();
            return false;
        });


    $('.formbuilder_group_width').on('change', function() {
        rel = $(this).attr('rel');
        width = $(this).val();
        if ($(this).hasClass('control_box_select')) {
            if (rel == 'sm') {
                $(this).parents('.itemfield').attr('class', $(this).parents('.itemfield').attr('class').replace(/(^|\s)col-sm-\S+/g, ' col-sm-' + width + ' '));
            } else if (rel == 'xs') {
                $(this).parents('.itemfield').attr('class', $(this).parents('.itemfield').attr('class').replace(/(^|\s)col-xs-\S+/g, ' col-xs-' + width + ' '));
            } else {
                $(this).parents('.itemfield').attr('class', $(this).parents('.itemfield').attr('class').replace(/(^|\s)col-md-\S+/g, ' col-md-' + width + ' '));
            }
        } else {
            if (rel == 'sm') {
                $(this).parents('.formbuilder_group').attr('class', $(this).parents('.formbuilder_group').attr('class').replace(/(^|\s)col-sm-\S+/g, ' col-sm-' + width + ' '));
            } else if (rel == 'xs') {
                $(this).parents('.formbuilder_group').attr('class', $(this).parents('.formbuilder_group').attr('class').replace(/(^|\s)col-xs-\S+/g, ' col-xs-' + width + ' '));
            } else {
                $(this).parents('.formbuilder_group').attr('class', $(this).parents('.formbuilder_group').attr('class').replace(/(^|\s)col-md-\S+/g, ' col-md-' + width + ' '));
            }
        }
        clearBeforeSave();
    });

    $('#g_customfields_form').submit(function() {
        clearBeforeSave();
        $(".g_customfields_admintab").html();
        if ($('#title_' + gdefault_language).val() == '') {
            alertdanger = '<div class="alert alert-danger">' + gtitleform + ' ' + empty_danger + '</div>';
            $(".g_customfields_admintab").html(alertdanger);
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        } else {
            $.each(languages, function(index, value) {
                if ($('#title_' + value.id_lang).val() == '') {
                    $('#title_' + value.id_lang).val($('#title_' + gdefault_language).val());
                }
            });
        }
        $.each(languages, function(index, value) {
            if ($('#rewrite_' + value.id_lang).val() == '') {
                val = $('#title_' + value.id_lang).val();
                val = val.replace(/[^0-9a-zA-Z:._-]/g, '').replace(/^[^a-zA-Z]+/, '');
                $('#rewrite_' + value.id_lang).val(val.toLowerCase());
            }
        });
        return true;
    });

    $('.g_customfields_admintab .tab-page').click(function() {
        if (!$(this).parent('.tab-row').hasClass('active')) {
            $('.g_customfields_admintab .tab-row').removeClass('active');
            $(this).parent('.tab-row').addClass('active');
            $('.formbuilder_tab').removeClass('activetab');
            idtabactive = $(this).attr('href');
            if ($(idtabactive).length > 0) $(idtabactive).addClass('activetab');
        }
        return false;
    });

    $('select[name="groupaccessbox[]"]').change(function() {
        $('#groupaccess').val($(this).val().join(','));
    });
    $('#gfromloaddefault').click(function() {
        tinyMCE.triggerSave();
        form = $(this).parents('form');
        action = form.attr('action');
        serializedForm = form.serialize();
        $(g_customfields_overlay).appendTo('body');
        $.ajax({
            type: 'POST',
            url: action,
            data: serializedForm + '&gfromloaddefault=true',
            async: false,
            success: function(data, textStatus, request) {
                $('#g_customfields_overlay').remove();
                var result = $.parseJSON(data);
                if (result.errors == '0') {
                    $.each(result.datas, function(index, value) {
                        if ($('iframe[id^="emailtemplate_' + index + '_ifr"]').length > 0)
                            $('iframe[id^="emailtemplate_' + index + '_ifr"]').contents().find('body').html(value);
                        if ($('#emailtemplate_' + index + ' .mceIframeContainer iframe').length > 0)
                            $('#emailtemplate_' + index + ' .mceIframeContainer iframe').contents().find('body').html(value);
                    });
                    $.each(result.datassender, function(index, value) {
                        if ($('iframe[id^="emailtemplatesender_' + index + '_ifr"]').length > 0)
                            $('iframe[id^="emailtemplatesender_' + index + '_ifr"]').contents().find('body').html(value);
                        if ($('#emailtemplatesender_' + index + ' .mceIframeContainer iframe').length > 0)
                            $('#emailtemplatesender_' + index + ' .mceIframeContainer iframe').contents().find('body').html(value);
                    });
                    $.each(result.datassendersubject, function(index, value) {
                        if ($('textarea[name="subjectsender_' + index + '"]').length > 0)
                            $('textarea[name="subjectsender_' + index + '"]').html(value);
                        if ($('#subjectsender_' + index + ' .mceIframeContainer iframe').length > 0)
                            $('#subjectsender_' + index + ' .mceIframeContainer iframe').contents().find('body').html(value);
                    });
                    $.each(result.subject, function(index, value) {
                        if ($('textarea[name="subject_' + index + '"]').length > 0)
                            $('textarea[name="subject_' + index + '"]').html(value);
                        if ($('#subject_' + index + ' .mceIframeContainer iframe').length > 0)
                            $('#subject_' + index + ' .mceIframeContainer iframe').contents().find('body').html(value);
                    });
                } else
                    alert & ("Error occurred!");

            },
            error: function(req, status, error) {
                $('#g_customfields_overlay').remove();
                alert & ("Error occurred!");
            }
        });
        return false;
    });
    $('#gfromloadshortcode').click(function() {
        tinyMCE.triggerSave();
        form = $(this).parents('form');
        action = form.attr('action');
        serializedForm = form.serialize();
        $(g_customfields_overlay).appendTo('body');
        $.ajax({
            type: 'POST',
            url: action,
            data: serializedForm + '&gfromloadshortcode=true',
            async: false,
            success: function(data, textStatus, request) {
                $('#g_customfields_overlay').remove();
                var result = $.parseJSON(data);
                if (result.errors == '0') {
                    $.each(result.datas, function(index, value) {
                        htmlshortcode = '<table>';
                        $.each(value, function(_index, _value) {
                            if (parseInt(_index % 2) == 1) {
                                htmlshortcode += '<tr class="odd"><td class="label">' + _value.label + '</td><td>' + _value.shortcode + '</td></tr>';
                            } else {
                                htmlshortcode += '<tr  class="even"><td class="label">' + _value.label + '</td><td>' + _value.shortcode + '</td></tr>';
                            }
                        });
                        htmlshortcode += '</table>';
                        if ($('.emailshortcode_wp .translatable-field.lang-' + index).length > 0) {
                            $('.emailshortcode_wp .translatable-field.lang-' + index + ' .emailshortcode').html(htmlshortcode);
                        } else {
                            $('.emailshortcode_wp .emailshortcode').html(htmlshortcode);
                        }
                    });

                } else
                    alert & ("Error occurred!");

            },
            error: function(req, status, error) {
                $('#g_customfields_overlay').remove();
                alert & ("Error occurred!");
            }
        });
        return false;
    });

    $('.emailshortcode_panel .panel-heading').click(function() {
        if ($(this).next('.emailshortcode').css('display') != 'none') {
            $(this).removeClass('active');
            $(this).next('.emailshortcode').stop(true, true).slideUp(300);
        } else {
            $(this).addClass('active');
            $(this).next('.emailshortcode').stop(true, true).slideDown(300);
        }
    });
    if (typeof psversion15 === 'undefined') {} else
    if (psversion15 != -1) {
        $('#itemfieldparent .itemfield').each(function() {
            if ($(this).data('image') != '')
                $(this).tooltip({
                    title: '<img style="display:block;clear:both;" src="' + $(this).data('image') + '" />',
                    html: true
                });
        });
    } else {

    }


    $('#multi_on').on('change', function() {
        if ($('.slidervalue').length > 0) changeSildeValue();
    });
    $('#multi_off').on('change', function() {
        if ($('.slidervalue').length > 0) changeSildeValue();
    });
    $(document).on('click', 'click_to_copy', function() {
        content_copy = $(this).html();
        copyToClipboard(content_copy);
        return false;
    });
    $('input[name="autoredirect"]').change(function() {
        if ($('input[name="autoredirect"]:checked').val() == '1') {
            $('.autoredirect_config').stop(true, true).slideDown(500);
        } else {
            $('.autoredirect_config').stop(true, true).slideUp(500);
        }
    });
});
$(document).on('focusout', '.rewrite_url', function() {
    val = $(this).val();
    val = val.replace(/[^0-9a-zA-Z:._-]/g, '').replace(/^[^a-zA-Z]+/, '');
    $(this).val(val.toLowerCase());
});
$(document).on('focusout', '.slidervalue', function() {
    changeSildeValue();
});

function saveFieldData(formsubmit, formURL, formData) {
    resultsave = false;
    $.ajax({
        url: formURL,
        type: 'POST',
        data: formData,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        async: false,
        processData: false,
        success: function(data, textStatus, jqXHR) {
            formsubmit.find('#g_customfields_overlay').remove();
            var result = $.parseJSON(data);
            if (formsubmit.find('.formajaxresult').length > 0) {
                if (result.errors == '0' || result.errors == false || result.errors == 'false') {
                    formsubmit.find('.formajaxresult').html('<div class="success_box alert alert-success">' + result.thankyou + '</div>');
                    if (typeof result.formval != "undefined" && typeof result.id_g_customrequest != "undefined") {
                        if ($('#custom_field_box_' + result.id_g_customrequest).length > 0) {
                            $('#custom_field_box_' + result.id_g_customrequest).html(result.formval);
                        } else if ($('#view_additional').length > 0) {
                            $('#view_additional .row').append('<div id="custom_field_box_' + result.id_g_customrequest + '" class="panel col-lg-6">' + result.formval + '</div>');
                        }
                    }
                } else
                    formsubmit.find('.formajaxresult').html('<div class="alert alert-danger alert-drop">' + result.thankyou + '</div>');
            } else {
                resulthtml = '';
                if (result.errors == '0' || result.errors == false || result.errors == 'false') {
                    resulthtml = '<div class="formajaxresult"><div class="success_box alert alert-success">' + result.thankyou + '</div></div>';
                    if (typeof result.formval != "undefined" && typeof result.id_g_customrequest != "undefined") {
                        if ($('#custom_field_box_' + result.id_g_customrequest).length > 0) {
                            $('#custom_field_box_' + result.id_g_customrequest).html(result.formval);
                        } else if ($('#view_additional').length > 0) {
                            $('#view_additional .row').append('<div id="custom_field_box_' + result.id_g_customrequest + '" class="panel col-lg-6">' + result.formval + '</div>');
                        }
                    }
                } else
                    resulthtml = '<div class="formajaxresult"><div class="alert alert-danger alert-drop">' + result.thankyou + '</div></div>';
                $(resulthtml).insertBefore(formsubmit.find('.g_customfields_content'));
            }
            if (typeof result.id_g_customrequest != "undefined") {
                formsubmit.find('input[name="id_g_customrequest"]').val(result.id_g_customrequest);
            }
            $('html,body').animate({
                    scrollTop: formsubmit.find('.formajaxresult').offset().top
                },
                'slow');
            if (result.autoredirect == true) {
                setTimeout(function() {
                    window.location.href = result.redirect_link;
                }, result.timedelay);
            }
            if (result.errors == '0' || !result.errors) {
                resultsave = true;
            }


        },
        error: function(jqXHR, textStatus, errorThrown) {
            if ($('.g-recaptcha').length > 0)
                if (typeof grecaptcha != "undefined") {
                    grecaptcha.reset();
                }
            var err = eval("(" + jqXHR.responseText + ")");
            alert(err.Message);

        }
    });
    return resultsave;
}

function checkGdpr(item_check) {
    psgdpr_consent_checkbox = 1;
    if (item_check.find('input[name="psgdpr_consent_checkbox"]').length > 0) {
        if (item_check.find('input[name="psgdpr_consent_checkbox"]').is(":checked")) {} else {
            item_check.find('input[name="psgdpr_consent_checkbox"]').focus();
            if (item_check.find('input[name="psgdpr_consent_checkbox"]').parents('.g_customfields_content_gdpr').length > 0) {
                item_check.find('input[name="psgdpr_consent_checkbox"]').parents('.g_customfields_content_gdpr').addClass('gdpr_no_check');
            }
            psgdpr_consent_checkbox = 0;
            return 0;
        }
    } else if (item_check.find('input.gdprcompliancy_consent_checkbox').length > 0) {
        if (item_check.find('input.gdprcompliancy_consent_checkbox').is(":checked")) {} else {
            item_check.find('input.gdprcompliancy_consent_checkbox').focus();
            if (item_check.find('input.gdprcompliancy_consent_checkbox').parents('.g_customfields_content_gdpr').length > 0) {
                item_check.find('input.gdprcompliancy_consent_checkbox').parents('.g_customfields_content_gdpr').addClass('gdpr_no_check');
            }
            psgdpr_consent_checkbox = 0;
            return 0;
        }
    }
    return psgdpr_consent_checkbox;
}