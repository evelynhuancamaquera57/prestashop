<?php
/**
* This is main class of module.
*
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

if (!defined("_PS_VERSION_"))
    exit;
include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsModel.php');
include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsfieldsModel.php');
class G_customfields extends Module
{
    public function __construct()
    {
        $this->name = "g_customfields";
        $this->tab = "administration";
        $this->version = "1.1.0";
        $this->author = "Globo Jsc";
        $this->need_instance = 1;
        $this->bootstrap = 1;
        $this->module_key = '22680154270a262fa1bc247a14aa239a';
        if(version_compare(_PS_VERSION_, '1.7.0.0', '>='))
            parent::__construct();
        $this->displayName = $this->l('Custom fields - registration form and checkout');
        $this->description = $this->l('The module allows you to add extra fields to registration form, order page.');
        if(version_compare(_PS_VERSION_, '1.7.0.0', '<'))
            parent::__construct();
        $this->ps_versions_compliancy = array('min' => '1.6.0.0', 'max' => _PS_VERSION_);
    }
    public function install()
    {
        if (Shop::isFeatureActive()){
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        $res = true;
        if(version_compare(_PS_VERSION_, '1.7.0.1', '<='))  
            $res = Configuration::updateValue('GF_PRODUCT_TYPE',ImageType::getFormatedName('home'));
        else    
            $res =  Configuration::updateValue('GF_PRODUCT_TYPE',ImageType::getFormattedName('home'));
            
        return $res 
            && parent::install()
            && $this->_createTables()
            && $this->_createTab()
            && $this->installConfigData()
            && $this->registerHook('displayBackOfficeHeader')
            && $this->registerHook('displayHeader')
            && $this->registerHook('moduleRoutes')
            && $this->registerHook('actionbeforesubmitaccount')
            && $this->registerHook('actionSubmitAccountBefore')
            && $this->registerHook('actionCustomerAccountAdd')
            && $this->registerHook('displayCustomerAccount')
            && $this->registerHook('displayBeforeCarrier')
            && $this->registerHook('displayPaymentTop')
            && $this->registerHook('displayCustomerAccountForm')
            && $this->registerHook('displayCustomerAccountFormTop')
            && $this->registerHook('displayAdminCustomers')
            && $this->registerHook('displayAdminOrder')
            && $this->registerHook('displayPDFInvoice')
            && $this->registerHook('displayPDFDeliverySlip')
            && $this->registerHook('displayOrderDetail')
            && $this->registerHook('displayShoppingCartFooter')

            /* update GDPR Compliance */
            && $this->registerHook('actionDeleteGDPRCustomer')
            && $this->registerHook('actionExportGDPRData');
            /* #update GDPR Compliance */
    }
    public function uninstall()
    {
        return parent::uninstall()
            && $this->_deleteTables()
            && $this->_deleteTab()
            && $this->unregisterHook("displayBackOfficeHeader")
            && $this->unregisterHook("displayHeader")
            && $this->unregisterHook("moduleRoutes")
            && $this->unregisterHook("actionSubmitAccountBefore")
            && $this->unregisterHook("actionCustomerAccountAdd")
            && $this->unregisterHook("displayCustomerAccount")
            && $this->unregisterHook("displayBeforeCarrier")
            && $this->unregisterHook("displayPaymentTop")
            && $this->unregisterHook("displayCustomerAccountForm")
            && $this->unregisterHook("displayCustomerAccountFormTop")
            && $this->unregisterHook("displayAdminCustomers")
            && $this->unregisterHook("displayAdminOrder")
            && $this->unregisterHook("displayPDFInvoice")
            && $this->unregisterHook("displayPDFDeliverySlip")
            && $this->unregisterHook("displayOrderDetail")
            && $this->unregisterHook("displayShoppingCartFooter")

            /* update GDPR Compliance */
            && $this->unregisterHook("actionDeleteGDPRCustomer")
            && $this->unregisterHook("actionExportGDPRData")
            /* #update GDPR Compliance */
            ;
    }
    private function _createTables()
    {
        $res = (bool) Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfields` (
                `id_g_customfields` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `active` tinyint(1) unsigned NOT NULL,
                `show_title` tinyint(1) unsigned NOT NULL,
                `typeform` int(2) unsigned NOT NULL,
                `formtemplate` MEDIUMTEXT NULL,
                `fields` text NULL,
                PRIMARY KEY (`id_g_customfields`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= (bool) Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfields_lang` (
                    `id_g_customfields` int(10) unsigned NOT NULL,
                    `id_lang` int(10) unsigned NOT NULL,
                    `title` varchar(255) NOT NULL,
                    PRIMARY KEY (`id_g_customfields`,`id_lang`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
            ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfields_shop` (
                `id_g_customfields` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_g_customfields`,`id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= (bool) Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfieldsfields` (
                `id_g_customfieldsfields` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `labelpos` tinyint(2) unsigned NOT NULL,
                `type` varchar(255) NOT NULL,
                `name` varchar(255) NOT NULL,
                `idatt` varchar(255) NULL,
                `classatt` varchar(255) NULL,
                `required` tinyint(1) unsigned NOT NULL,
                `validate` varchar(255) NULL,
                `extra` varchar(255) NULL,
                `multi` tinyint(1) unsigned NOT NULL,
                `showinlistadmin` tinyint(1) unsigned NULL,
                `usercanedit` tinyint(1) unsigned NULL,
                PRIMARY KEY (`id_g_customfieldsfields`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= (bool) Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfieldsfields_lang` (
                    `id_g_customfieldsfields` int(10) unsigned NOT NULL,
                    `id_lang` int(10) unsigned NOT NULL,
                    `label` varchar(255) NOT NULL,
                    `value` text  NULL,
                    `placeholder` text  NULL,
                    `description` text  NULL,
                    PRIMARY KEY (`id_g_customfieldsfields`,`id_lang`)
                ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
            ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customfieldsfields_shop` (
                `id_g_customfieldsfields` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_g_customfieldsfields`,`id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= (bool) Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customrequest` (
                `id_g_customrequest` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `id_g_customfields` int(10) unsigned NOT NULL,
                `user_ip` varchar(255) NULL,
                `id_customer` text NULL,
                `id_cart` text NULL,
                `jsonrequest` MEDIUMTEXT  NULL,
                `date_add` datetime DEFAULT NULL,
                `status` int(10) NULL DEFAULT  "0",
                `reloadcustomer` tinyint(1) unsigned NOT NULL,
                PRIMARY KEY (`id_g_customrequest`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        $res &= Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'g_customrequest_shop` (
                `id_g_customrequest` int(10) unsigned NOT NULL,
                `id_shop` int(10) unsigned NOT NULL,
                PRIMARY KEY (`id_g_customrequest`,`id_shop`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=UTF8;
        ');
        return $res;
    }
    private function _deleteTables()
    {
        return Db::getInstance()->execute('
                DROP TABLE IF EXISTS    `' . _DB_PREFIX_ . 'g_customfields`,
                                        `' . _DB_PREFIX_ . 'g_customfields_lang`,
                                        `' . _DB_PREFIX_ . 'g_customfields_shop`,
                                        `' . _DB_PREFIX_ . 'g_customfieldsfields`,
                                        `' . _DB_PREFIX_ . 'g_customfieldsfields_lang`,
                                        `' . _DB_PREFIX_ . 'g_customfieldsfields_shop`,
                                        `' . _DB_PREFIX_ . 'g_customrequest`,
                                        `' . _DB_PREFIX_ . 'g_customrequest_shop`;
        ');
    }
    private function _createTab()
    {
        $res = true;
        $tabparent = "AdminGcustomfieldparent";
        $id_parent = Tab::getIdFromClassName($tabparent);
        if(!$id_parent){
            $tab = new Tab();
            $tab->active = 1;
            $tab->class_name = "AdminGcustomfieldparent";
            $tab->name = array();
            foreach (Language::getLanguages() as $lang){
                $tab->name[$lang["id_lang"]] = "Custom fields";
            }
            $tab->id_parent = 0;
            $tab->module = $this->name;
            $res &= $tab->add();
            $id_parent = $tab->id;
        }
        $subtabs = array(
            array(
                'class'=>'AdminGcustomfieldconfig',
                'name'=>'General Settings'
            ),
            array(
                'class'=>'AdminGcustomfieldmanager',
                'name'=>'Fields'
            ),
            array(
                'class'=>'AdminGcustomfieldrequest',
                'name'=>'Received Data',
                'parent'=>-1
            )
        );
        foreach($subtabs as $subtab){
            $idtab = Tab::getIdFromClassName($subtab['class']);
            if(!$idtab){
                $tab = new Tab();
                $tab->active = 1;
                $tab->class_name = $subtab['class'];
                $tab->name = array();
                foreach (Language::getLanguages() as $lang){
                    $tab->name[$lang["id_lang"]] = $subtab['name'];
                }
                $tab->id_parent = (isset($subtab['parent'])) ? (int)$subtab['parent'] : $id_parent;
                $tab->module = $this->name;
                $res &= $tab->add();
            }
        }
        return $res;
    }
    private function _deleteTab()
    {
        $id_tabs = array('AdminGcustomfieldconfig','AdminGcustomfieldmanager' ,'AdminGcustomfieldrequest');
        foreach($id_tabs as $id_tab){
            $idtab = Tab::getIdFromClassName($id_tab);
            $tab = new Tab((int)$idtab);
            $parentTabID = $tab->id_parent;
            $tab->delete();
            $tabCount = Tab::getNbTabs((int)$parentTabID);
            if ($tabCount == 0){
                $parentTab = new Tab((int)$parentTabID);
                $parentTab->delete();
            }
        }
        return true;
    }
    public function installConfigData(){
    
        $res = true;
        $shop_groups_list = array();
		$shops = Shop::getContextListShopID();
        $shop_context = Shop::getContext();
        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', 12);
        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', 12);
        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', 12);
		$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', 12);
        
		foreach ($shops as $shop_id)
		{
			$shop_group_id = (int)Shop::getGroupFromShop((int)$shop_id, true);
			if (!in_array($shop_group_id, $shop_groups_list))
				$shop_groups_list[] = (int)$shop_group_id;
			$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', 12, false, (int)$shop_group_id, (int)$shop_id);
            $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', 12, false, (int)$shop_group_id, (int)$shop_id);
            $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', 12, false, (int)$shop_group_id, (int)$shop_id);
			$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', 12, false, (int)$shop_group_id, (int)$shop_id);
        }
		/* Update global shop context if needed*/
		switch ($shop_context)
		{
			case Shop::CONTEXT_ALL:
				$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', 12);
                $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', 12);
                $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', 12);
				$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', 12);
                if (count($shop_groups_list))
				{
					foreach ($shop_groups_list as $shop_group_id)
					{
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', 12, false, (int)$shop_group_id);
                        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', 12, false, (int)$shop_group_id);
                        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', 12, false, (int)$shop_group_id);
						$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', 12, false, (int)$shop_group_id);
                    }
				}
				break;
			case Shop::CONTEXT_GROUP:
				if (count($shop_groups_list))
				{
					foreach ($shop_groups_list as $shop_group_id)
					{
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', 12, false, (int)$shop_group_id);
                        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', 12, false, (int)$shop_group_id);
                        $res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', 12, false, (int)$shop_group_id);
						$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', 12, false, (int)$shop_group_id);
                    }
				}
				break;
		}
     
        return $res;
    }
    public function getContent()
	{
	   if(Tools::getValue('reset_hook')){
    	   $this->registerHook('actionbeforesubmitaccount');
           $this->registerHook('actionSubmitAccountBefore');
       }
	   if (Tools::isSubmit('getThumb')){
	        $extension = array('png','gif','jpg','jpeg','bmp','svg');
	        $listthumbs = array();
            $thumbsdir = opendir(_PS_MODULE_DIR_.'g_customfields/views/img/thumbs/');
    		while (($file = readdir($thumbsdir)) !== false) {
    			if(in_array(Tools::strtolower(Tools::substr($file, -3)), $extension) || in_array(Tools::strtolower(Tools::substr($file, -4)), $extension)){
    			     $listthumbs[] = $file;
    			}
    		}
    		closedir($thumbsdir);
            die(implode(',',$listthumbs));
	   }
	   elseif (Tools::isSubmit('addThumb')){
            $thumbs = array();
           $extension = array('png','gif','jpg','jpeg','bmp','svg');
            if (isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) && !empty($_FILES['file']['tmp_name']))
            {
                foreach(array_keys($_FILES['file']['name']) as $key){
                    if($_FILES['file']['name'][$key]){
                        if(in_array(Tools::strtolower(Tools::substr($_FILES['file']['name'][$key], -3)), $extension) || in_array(Tools::strtolower(Tools::substr($_FILES['file']['name'][$key], -4)), $extension)){
                	        $file_attachment = array();
                			$file_attachment['rename'] = uniqid(). Tools::strtolower(Tools::substr($_FILES['file']['name'][$key], -5));
                			$file_attachment['tmp_name'] = $_FILES['file']['tmp_name'][$key];
                			$file_attachment['name'] = $_FILES['file']['name'][$key];
                            if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) && rename($file_attachment['tmp_name'], _PS_MODULE_DIR_.'g_customfields/views/img/thumbs/'.basename($file_attachment['rename']))) {
                                @chmod(_PS_MODULE_DIR_.'g_customfields/views/img/thumbs/'.basename($file_attachment['rename']), 0664);
                                $thumbs[] = $file_attachment['rename'];
                            }
                        }
                    }
                }
            }
           die(implode(',',$thumbs));
	   }elseif (Tools::isSubmit('getFormTypeConfig')){
	       $typefield = Tools::getValue('typefield');
           $id_g_customfieldsfields = (int)Tools::getValue('id_g_customfieldsfields',0);
           echo $this->hookConfigFieldAjax(array('typefield' => $typefield,'id'=>$id_g_customfieldsfields));
	       die();
       }elseif (Tools::isSubmit('addShortcode')){
           $id_field = (int)Tools::getValue('id_g_customfieldsfields',0);
           if($id_field){
                $fieldObj = new gcustomfieldsfieldsModel($id_field);
           }else{
                $fieldObj = new gcustomfieldsfieldsModel();
           }
           $fieldObj->name = Tools::getValue('name','');
           $fieldObj->required = (int)Tools::getValue('required',0);
           $fieldObj->labelpos = (int)Tools::getValue('labelpos',1);
           $fieldObj->idatt = Tools::getValue('idatt','');
           $fieldObj->classatt = Tools::getValue('classatt','');
           $fieldObj->validate = Tools::getValue('validate','');
           $fieldObj->type = Tools::getValue('type','');
           $fieldObj->extra = Tools::getValue('extra','');
           $fieldObj->multi = (bool)Tools::getValue('multi','0');
           $fieldObj->showinlistadmin = (bool)Tools::getValue('showinlistadmin','0');
           $fieldObj->usercanedit = (bool)Tools::getValue('usercanedit','1');
           $languages = Language::getLanguages(false);
           foreach ($languages as $lang)
           {
                $fieldObj->label[(int)$lang['id_lang']] = Tools::getValue('label_'.(int)$lang['id_lang'],'');
                $fieldObj->value[(int)$lang['id_lang']] = Tools::getValue('value_'.(int)$lang['id_lang'],'');
                $fieldObj->description[(int)$lang['id_lang']] = Tools::getValue('description_'.(int)$lang['id_lang'],'');
                $fieldObj->placeholder[(int)$lang['id_lang']] = Tools::getValue('placeholder_'.(int)$lang['id_lang'],'');
           }
           if($id_field){
                if($fieldObj->update()){
                    echo (int)$fieldObj->id;die();
                }
           }else{
                if($fieldObj->save()){
                    echo (int)$fieldObj->id;die();
                }
           }
           echo '0';die();
       }else
		  Tools::redirectAdmin($this->context->link->getAdminLink('AdminGcustomfieldmanager'));
	}
    public function hookConfigFieldAjax($params){
        $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https://' : 'http://';
        $base_uri = $protocol_content.Tools::getHttpHost().__PS_BASE_URI__;
        $result = '';
        $id_lang = $this->context->language->id;
        $id_shop = $this->context->shop->id;
        if($params['typefield']){
            $typefield = basename($params['typefield'], '.php');
            if(isset($params['id']) && $params['id']){
                $fieldObj = new gcustomfieldsfieldsModel((int)$params['id']);
                $typefield = $fieldObj->type;
            }
            $shortcode_dir = _PS_MODULE_DIR_.'g_customfields/classes/fields/';
            if(file_exists($shortcode_dir.$typefield.'.php')){
                $fields_value = array();
                if(version_compare(_PS_VERSION_,'1.6') == -1){
                    $fields_value['psoldversion15'] = -1;
                }else $fields_value['psoldversion15'] = 0;
                $fields_value['base_uri'] = $base_uri;
                require_once($shortcode_dir.$typefield.'.php');
                $Objname = Tools::ucfirst($typefield.'field');
	            $obj = new $Objname;
                $inputs = $obj->getConfig();
                $inputs[]= array(
        				'type' => 'hidden',
        				'name' => 'id_g_customfieldsfields'
        			);
                $inputs[]= array(
        				'type' => 'hidden',
        				'name' => 'type'
        			);


                $fields_form = array(
        			'form' => array(
        				'legend' => array(
        					'title' => $this->l('Settings'),
        					'icon' => 'icon-cogs'
        				),
        				'input' => $inputs,
        				'submit' => array(
        					'title' => $this->l('Add'),
        				),
                        'buttons' => array(
                            'cancel' => array(
            					'name' => 'cancelShortcode',
            					'type' => 'submit',
            					'title' => $this->l('Cancel'),
            					'class' => 'btn btn-default pull-left',
            					'icon' => 'process-icon-cancel'
            				),
            			)
        			),
        		);
                $fields_value['type'] = $typefield;
                $languages = Language::getLanguages(false);
                foreach ($languages as $lang)
        		{
        		      $fields_value['placeholder'][(int)$lang['id_lang']] = '';
                      $fields_value['description'][(int)$lang['id_lang']] = '';
                      $fields_value['value'][(int)$lang['id_lang']]  = '';
                }
                $fields_value['labelpos'] = 0;
                $fields_value['validate'] = '';
                $fields_value['id_g_customfieldsfields'] = '';
                $fields_value['multi'] = false;
                $fields_value['required'] = false;
                $fields_value['showinlistadmin'] = false;
                $fields_value['usercanedit'] = true;
                $typeform = (int)Tools::getValue('typeform');
                if($typeform ==3 || $typeform ==4 || $typeform ==5) $fields_value['usercanedit'] = false;

                if(isset($params['id']) && $params['id']){
                    $fields_value['id_g_customfieldsfields'] = $fieldObj->id;
                    $fields_value['labelpos'] = $fieldObj->labelpos;
                    $fields_value['name'] = $fieldObj->name;
                    $fields_value['idatt'] = $fieldObj->idatt;
                    $fields_value['classatt'] = $fieldObj->classatt;
                    $fields_value['required'] = $fieldObj->required;
                    $fields_value['validate'] = $fieldObj->validate;
                    $fields_value['multi'] = (bool)$fieldObj->multi;
                    $fields_value['showinlistadmin'] = (bool)$fieldObj->showinlistadmin;
                    if($typeform !=3 && $typeform !=4 && $typeform !=5)
                        $fields_value['usercanedit'] = (bool)$fieldObj->usercanedit;

                    $fields_value['extra'] = '';
                    if($typefield == 'product'){
                        $extra = $fieldObj->extra;
                        $producthtml = array();
                        if($extra !=''){
                            $products = explode(',',$extra);
                            foreach($products as $productid){
                                if($productid !=''){
                                    $cover = Product::getCover((int)$productid);
                                    $id_image = 0;
                                    if(isset($cover['id_image'])) $id_image = (int)$cover['id_image'];
                                    $productObj = new Product((int)$productid,false,(int)$id_lang,(int)$id_shop);
                                    $producthtml[(int)$productid] =array(
                                        'id'=>(int)$productid,
                                        'name'=>Product::getProductName((int)$productid,null,(int)$id_lang),
                                        'image_link' =>$this->context->link->getImageLink($productObj->link_rewrite,$id_image,Configuration::get('GF_PRODUCT_TYPE'))
                                    );
                                }
                            }
                        }

                        $fields_value['extra']['products'] = $extra;
                        $fields_value['extra']['html'] = $producthtml;
                    }elseif($typefield == 'colorchoose'){
                        $extra = $fieldObj->extra;
                        $colors = explode(',',$extra);
                        $fields_value['extra'] = array('value'=>$extra,'colors'=>$colors);
                    }elseif($typefield == 'slider' || $typefield == 'spinner'){
                        $extra = $fieldObj->extra;
                        $colors = explode(';',$extra);
                        $fields_value['extra'] = array('value'=>$extra,'extraval'=>$colors);
                    }
                    elseif($typefield == 'imagethumb'){
                        $extra = $fieldObj->extra;
                        $thumbs = explode(',',$extra);
                        $_thumbs = array();
                        if($thumbs)
                            foreach($thumbs as $thumb)
                                if(file_exists(_PS_MODULE_DIR_.'g_customfields/views/img/thumbs/'.$thumb))
                                    $_thumbs[] = $thumb;
                        $fields_value['extra'] = array('value'=>$extra,'thumbs'=>$_thumbs);
                    }else
                        $fields_value['extra'] = $fieldObj->extra;
            		foreach ($languages as $lang)
            		{
            		      $fields_value['label'][(int)$lang['id_lang']] = isset($fieldObj->label[(int)$lang['id_lang']]) ? $fieldObj->label[(int)$lang['id_lang']] : Tools::ucfirst($typefield);
            		      if($typefield == 'checkbox' || $typefield == 'select' || $typefield == 'radio' || $typefield == 'survey'){
            		          $fields_value['value'][(int)$lang['id_lang']] = (isset($fieldObj->value[(int)$lang['id_lang']]) && $fieldObj->value[(int)$lang['id_lang']] !='') ? explode(',',$fieldObj->value[(int)$lang['id_lang']]) : array();
            		      }else
                            $fields_value['value'][(int)$lang['id_lang']] = isset($fieldObj->value[(int)$lang['id_lang']]) ? $fieldObj->value[(int)$lang['id_lang']] : '';

                          $fields_value['placeholder'][(int)$lang['id_lang']] = isset($fieldObj->placeholder[(int)$lang['id_lang']]) ? $fieldObj->placeholder[(int)$lang['id_lang']] : '';

                          if($typefield == 'survey'){
                            $fields_value['description'][(int)$lang['id_lang']] = (isset($fieldObj->description[(int)$lang['id_lang']]) && $fieldObj->description[(int)$lang['id_lang']] !='') ? explode(',',$fieldObj->description[(int)$lang['id_lang']]) : array();
                          }else
                            $fields_value['description'][(int)$lang['id_lang']] = isset($fieldObj->description[(int)$lang['id_lang']]) ? $fieldObj->description[(int)$lang['id_lang']] : '';
                    }
                }else{
                    $fields_value['extra'] = '';
                    if($typefield == 'product'){
                        $fields_value['extra']['products'] = '';
                        $fields_value['extra']['html'] = array();
                    }elseif($typefield == 'colorchoose'){
                        $fields_value['extra'] = array('value'=>'','colors'=>array());
                    }elseif($typefield == 'slider' || $typefield == 'spinner'){
                        $fields_value['extra'] = array('value'=>'','extraval'=>array());
                    }
                    elseif($typefield == 'imagethumb'){
                        $fields_value['extra'] = array('value'=>'','thumbs'=>array());
                    }
                    $fields_value['name'] = $typefield.'_'.time();
                    $fields_value['idatt'] = $typefield.'_'.time();
                    $fields_value['classatt'] = $typefield.'_'.time();
                    foreach ($languages as $lang)
            		{
            		      $fields_value['label'][(int)$lang['id_lang']] = Tools::ucfirst($typefield);
            		}
                }
                $fields_value['ajaxaction'] = $this->context->link->getAdminLink('AdminGcustomfieldmanager');
                $fields_value['loadjqueryselect2'] = 1;
                if(version_compare(_PS_VERSION_,'1.6.0.7') == -1){
                    $fields_value['loadjqueryselect2'] = 0;
                }
        		$helper = new HelperForm();
                $helper->module = new $this->name();
        		$helper->submit_action = 'addShortcode';
                $helper->show_toolbar = false;
        		$helper->table = $this->table;
        		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        		$helper->default_form_language = $lang->id;
        		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        		$this->fields_form = array();

        		$helper->identifier = $this->identifier;
        		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        		$helper->token = Tools::getAdminTokenLite('AdminModules');
        		$helper->tpl_vars = array(
                    'fields_value' => $fields_value,
        			'languages' => $this->context->controller->getLanguages(),
        			'id_language' => $this->context->language->id
        		);
                $html_extra='';
                if(version_compare(_PS_VERSION_,'1.6') == -1){
                    $html_extra_tpl = _PS_MODULE_DIR_.'g_customfields/views/templates/admin/button15.tpl';
                    $html_extra = Context::getContext()->smarty->fetch($html_extra_tpl);
                }
                return $helper->generateForm(array($fields_form)).$html_extra;
            }
        }
        return $result;
    }
    public function hookDisplayBeforeCarrier($params){
        $html = '';
        $forms = gcustomfieldsModel::getFormType(3);
        if($forms)
            foreach($forms as $form){
                $id_g_customrequest = 0;
                $requestData = gcustomrequestModel::getFormData(3,(int)$form['id_g_customfields'],(int)$this->context->cart->id,(int)$this->context->shop->id);
                if($requestData && isset($requestData['id_g_customrequest']))
                    $id_g_customrequest = (int)$requestData['id_g_customrequest'];
                $html.=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',$id_g_customrequest);
            }
        return $html;
    }
    public function hookDisplayPaymentTop($params){
        $html = '';
        $forms = gcustomfieldsModel::getFormType(4);
        if($forms)
            foreach($forms as $form){
                $id_g_customrequest = 0;
                $requestData = gcustomrequestModel::getFormData(4,(int)$form['id_g_customfields'],(int)$this->context->cart->id,(int)$this->context->shop->id);
                if($requestData && isset($requestData['id_g_customrequest']))
                    $id_g_customrequest = (int)$requestData['id_g_customrequest'];
                $html.=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',$id_g_customrequest);
            }
        return $html;
    }

    public function hookDisplayCustomerAccountFormTop($params){
        if(Tools::getValue('controller') == 'identity') return;
        $html = '';
        $forms = gcustomfieldsModel::getFormType(1);
        if($forms)
            foreach($forms as $form){
                $id_g_customrequest = 0;
                $requestData = gcustomrequestModel::getFormData(1,(int)$form['id_g_customfields'],(int)$this->context->cart->id,(int)$this->context->shop->id,(isset($this->context->customer) ? $this->context->customer->id : 0));
                if($requestData && isset($requestData['id_g_customrequest']))
                    $id_g_customrequest = (int)$requestData['id_g_customrequest'];
                $html.=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',$id_g_customrequest);
            }
        return $html;
    }
    public function hookDisplayCustomerAccountForm($params){
        if(Tools::getValue('controller') == 'identity') return;
        $html = '';
        $forms = gcustomfieldsModel::getFormType(2);
        if($forms){
            /* fix 28/02/2018 */
            if(!isset($this->context->customer) || $this->context->customer->id ==0 || $this->context->customer->id == ''){
                $id_g_customfields = array();
                foreach($forms as $form){
                    $id_g_customfields[(int)$form['id_g_customfields']] = (int)$form['id_g_customfields'];
                }
                if($id_g_customfields){
                    $sql = 'DELETE FROM '._DB_PREFIX_.'g_customrequest
                        WHERE id_customer = 0
                        AND id_cart = '.(int)$this->context->cart->id.' 
                        AND id_g_customfields IN('.pSql(implode(',',$id_g_customfields)).')';
                    ;
                    Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
                }
            }
            /* #fix 28/02/2018 */
            foreach($forms as $form){
                $id_g_customrequest = 0;
                $requestData = gcustomrequestModel::getFormData(2,(int)$form['id_g_customfields'],(int)$this->context->cart->id,(int)$this->context->shop->id,(isset($this->context->customer) ? $this->context->customer->id : 0));
                if($requestData && isset($requestData['id_g_customrequest']))
                    $id_g_customrequest = (int)$requestData['id_g_customrequest'];
                $html.=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',$id_g_customrequest);
            }
            if(Tools::getValue('controller') == 'order'){
                $tpl = _PS_MODULE_DIR_.'g_customfields/views/templates/front/order_opc.tpl';
                $html.=Context::getContext()->smarty->fetch($tpl);
            }
        }
        return $html;
    }
    public function hookDisplayShoppingCartFooter($params){
        $html = '';
        $forms = gcustomfieldsModel::getFormType(5);
        if($forms){
            foreach($forms as $form){
                $id_g_customrequest = 0;
                $requestData = gcustomrequestModel::getFormData(5,(int)$form['id_g_customfields'],(int)$this->context->cart->id,(int)$this->context->shop->id);
                if($requestData && isset($requestData['id_g_customrequest']))
                    $id_g_customrequest = (int)$requestData['id_g_customrequest'];
                $html.=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',$id_g_customrequest);
            }
        }
        return $html;
    }
    public function hookDisplayPDFInvoice($params){
        $html = '';
        $object = $params['object'];
        if(get_class($object) == 'OrderInvoice'){
            $id_order = (int)$object->id_order;
            $orderObj = new Order((int)$id_order);
            if(Validate::isLoadedObject($orderObj)){
                $id_cart = (int)$orderObj->id_cart;
                $requests = gcustomrequestModel::getRequestByCart((int)$id_cart,(int)$orderObj->id_shop);
                $fieldsDatas = array();$formLabel = array();
                if($requests){
                    foreach($requests as $request){
                        $fieldsData = array();
                        $formObj = new gcustomfieldsModel((int)$request['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                        $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                        $dataadditional = (isset($request['jsonrequest'])) ? json_decode($request['jsonrequest'],true) : array();
                        $fieldsDatas[(int)$request['id_g_customrequest']] = $this->getFormDataVal($fieldsData,$dataadditional);
                        $formLabel[(int)$request['id_g_customrequest']] = $formObj->title;
                    }
                    $this->context->smarty->assign(
                        array(
                            'baseUri'=>_PS_UPLOAD_DIR_,
                            'fieldsDatas'=>$fieldsDatas,
                            'formLabel'=>$formLabel
                        )
                    );
                    $viewpdf =  $this->display(__FILE__, 'views/templates/admin/viewpdf.tpl');
                    return $viewpdf ;
                }
            }

        }

        return $html;
    }
    public function hookDisplayOrderDetail($params){

        $html = '';
        $orderObj = $params['order'];
        if(Validate::isLoadedObject($orderObj)){
            $id_cart = (int)$orderObj->id_cart;
            $requests = gcustomrequestModel::getRequestByCart((int)$id_cart,(int)$orderObj->id_shop);
            $fieldsDatas = array();$formLabel = array();
            if($requests){
                foreach($requests as $request){
                    $fieldsData = array();
                    $formObj = new gcustomfieldsModel((int)$request['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                    $dataadditional = (isset($request['jsonrequest'])) ? json_decode($request['jsonrequest'],true) : array();
                    $fieldsDatas[(int)$request['id_g_customrequest']] = $this->getFormDataVal($fieldsData,$dataadditional);
                    $formLabel[(int)$request['id_g_customrequest']] = $formObj->title;
                }
                $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
                $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
                $this->context->smarty->assign(
                    array(
                        'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__.'upload/',
                        'fieldsDatas'=>$fieldsDatas,
                        'formLabel'=>$formLabel
                    )
                );
                $viewpdf =  $this->display(__FILE__, 'views/templates/front/vieworderdetail.tpl');
                return $viewpdf ;
            }
        }

        return $html;
    }
    public function hookDisplayPDFDeliverySlip($params){
        $html = '';
        $object = $params['object'];
        if(get_class($object) == 'OrderInvoice'){
            $id_order = (int)$object->id_order;
            $orderObj = new Order((int)$id_order);
            if(Validate::isLoadedObject($orderObj)){
                $id_cart = (int)$orderObj->id_cart;
                $requests = gcustomrequestModel::getRequestByCart((int)$id_cart,(int)$orderObj->id_shop);
                $fieldsDatas = array();$formLabel = array();
                if($requests){
                    foreach($requests as $request){
                        $fieldsData = array();
                        $formObj = new gcustomfieldsModel((int)$request['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                        $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                        $dataadditional = (isset($request['jsonrequest'])) ? json_decode($request['jsonrequest'],true) : array();
                        $fieldsDatas[(int)$request['id_g_customrequest']] = $this->getFormDataVal($fieldsData,$dataadditional);
                        $formLabel[(int)$request['id_g_customrequest']] = $formObj->title;
                    }
                    $this->context->smarty->assign(
                        array(
                            'baseUri'=>_PS_UPLOAD_DIR_,
                            'fieldsDatas'=>$fieldsDatas,
                            'formLabel'=>$formLabel
                        )
                    );
                    $viewpdf =  $this->display(__FILE__, 'views/templates/admin/viewpdf.tpl');
                    return $viewpdf ;
                }
            }

        }

        return $html;
    }

    public function hookDisplayAdminOrder($params){
        
        $this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/views/css/front/jquery.minicolors.css');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/tinymce/tinymce.min.js');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/jquery.minicolors.js');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/g_customfields.js');
        $id_order = (int)$params['id_order'];
        $orderObj = new Order((int)$id_order);
        
        $forms = array();$fieldsDatas = array();$formLabel = array();
        if(Validate::isLoadedObject($orderObj)){
            $_forms = gcustomfieldsModel::getFormType(array(3,4,5));
            $requests = gcustomrequestModel::getRequestByCart((int)$orderObj->id_cart,(int)$orderObj->id_shop);
            $_g_orderrequests = array();
            if($requests)
                foreach($requests as $request)
                    $_g_orderrequests[(int)$request['id_g_customfields']] = $request;
            if($_forms)
                foreach($_forms as $form){
                    $id_g_orderrequest = 0;
                    if(isset($_g_orderrequests[(int)$form['id_g_customfields']])) $id_g_orderrequest = $_g_orderrequests[(int)$form['id_g_customfields']]['id_g_customrequest'];
                    $forms[(int)$form['id_g_customfields']]=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',(int)$id_g_orderrequest);
                    $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                    $formLabel[(int)$id_g_orderrequest] = $formObj->title;
                    if($id_g_orderrequest > 0){
                        $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                        $dataadditional = (isset($_g_orderrequests[(int)$form['id_g_customfields']]['jsonrequest'])) ? json_decode($_g_orderrequests[(int)$form['id_g_customfields']]['jsonrequest'],true) : array();
                        $fieldsDatas[(int)$id_g_orderrequest] = $this->getFormDataVal($fieldsData,$dataadditional);
                    }
                }
        }


        $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';

        $this->context->smarty->assign(
            array(
                'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__,
                'forms'=>$forms,
                'fieldsDatas'=>$fieldsDatas,
                'formLabel'=>$formLabel
            )
        );
        return $this->display(__FILE__, 'views/templates/admin/vieworder.tpl');
    }
    public function hookDisplayCustomerAccount($params){
         //update module dev Tung 2021
        if(version_compare(_PS_VERSION_, '1.7.0.0', '<')){
            $this->context->smarty->assign(
                array(
                    'ps_version'=> 'ps16',
                )
            );
        }else{
            $this->context->smarty->assign(
                array(
                    'ps_version'=> 'ps17',
                )
            );
        }
       
        return $this->display(__FILE__, 'views/templates/front/myacount.tpl');
    }
    public function getFormDataVal($fieldsData,$dataadditional){
        
        if($fieldsData)
                foreach($fieldsData as &$data){
                    if(isset($dataadditional['{'.$data['name'].'}'])) {
                        $data['value'] = $dataadditional['{'.$data['name'].'}'];
                        switch($data['type']){
                            case 'product':
                            {
                                $product_ids = array();
                                
                                if($data['multi'] == 1){
                                    $product_ids = explode(',',$data['value']);
                                }
                                else{
                                    if($data['value'])
                                        $product_ids = array($data['value']);
                                } 
                                if($product_ids!=null){
                                    $data['value'] = array();
                                    foreach($product_ids as $product_id){
                                        $product_name=Product::getProductName((int)$product_id,null,(int)(int)$this->context->language->id);
                                        
                                        $product_link=$this->context->link->getProductLink((int)$product_id,null,null,null,(int)(int)$this->context->language->id,(int)(int)$this->context->shop->id);
                                        $data['value'][]= array('name'=>$product_name,'link'=>$product_link,'id'=>$product_id);
                                    }
                                }
                                break;
                            }
                            case 'fileupload':
                            {
                                $extension = array('jpg','jpeg','gif','png');
                                $fileuploads = array();
                                if($data['multi'] == 1)
                                    $fileuploads = explode(',',$data['value']);
                                else $fileuploads = array($data['value']);
                                $data['value'] = array();
                                foreach($fileuploads as $file){
                                    if($file !='' && file_exists(_PS_UPLOAD_DIR_.$file)){
                                        if(in_array(Tools::strtolower(Tools::substr($file, -3)), $extension) ||
                                            in_array(Tools::strtolower(Tools::substr($file, -4)), $extension)){
                                            $size = getimagesize(_PS_UPLOAD_DIR_.$file);
                                             $data['value'][] = array(
                                                'isImage'=>true,
                                                'name'=>$file,
                                                'width' =>(isset($size[0]) ? (int)$size[0] : -1),
                                                'height' =>(isset($size[1]) ? (int)$size[1] : -1)
                                             );
                                        }else{
                                            $data['value'][] = array('isImage'=>false,'name'=>$file);
                                        }
                                    }

                                }
                                break;
                            }
                            case 'imagethumb':
                            case 'colorchoose':
                            {
                                $data['value'] = explode(',',$data['value']);
                                break;
                            }
                            case 'htmlinput' :{
                                $data['value'] = htmlspecialchars_decode($data['value']);
                                break;
                            }

                        }
                    }

                }
        return $fieldsData;
    }
    public function hookDisplayAdminCustomers($params){
        $id_customer = (int)$params['id_customer'];
        $id_g_customrequest = 0;
        $additional = gcustomrequestModel::getCustomerAdditional((int)$id_customer,(int)$this->context->shop->id);

        if(isset($additional['id_g_customrequest']))
            $id_g_customrequest = (int)$additional['id_g_customrequest'];

            
        $this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/views/css/front/jquery.minicolors.css');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/tinymce/tinymce.min.js');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/jquery.minicolors.js');
        $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/g_customfields.js');


        $forms = array();$fieldsDatas = array();$formLabel = array();
        $_forms = gcustomfieldsModel::getFormType(array(1,2));
        $this->context->smarty->assign(array('id_customer'=>$id_customer));
        $additional = gcustomrequestModel::getCustomerAdditional((int)$id_customer,(int)$this->context->shop->id);
        $_g_customrequests = array();
        if($additional)
            foreach($additional as $request)
                $_g_customrequests[(int)$request['id_g_customfields']] = $request;
        if($_forms)
            foreach($_forms as $form){
                $id_g_customrequest = 0;
                if(isset($_g_customrequests[(int)$form['id_g_customfields']])) $id_g_customrequest = $_g_customrequests[(int)$form['id_g_customfields']]['id_g_customrequest'];
                $forms[(int)$form['id_g_customfields']]=$this->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',(int)$id_g_customrequest);
                $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                $formLabel[(int)$id_g_customrequest] = $formObj->title;
                if($id_g_customrequest > 0){
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                    $dataadditional = (isset($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'])) ? json_decode($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'],true) : array();
                    $fieldsDatas[(int)$id_g_customrequest] = $this->getFormDataVal($fieldsData,$dataadditional);
                }
            }
        $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
        $this->context->smarty->assign(
            array(
                'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__,
                'forms'=>$forms,
                'fieldsDatas'=>$fieldsDatas,
                'formLabel'=>$formLabel,
                'id_customer'=>$id_customer,
            )
        );
        return $this->display(__FILE__, 'views/templates/admin/viewcustomer.tpl');
    }

    public function getAdminFormVal($id_g_customrequest){
        $requests = gcustomrequestModel::getRequestById((int)$id_g_customrequest,(int)$this->context->shop->id);
        if($requests){
            $formObj = new gcustomfieldsModel((int)$requests['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
            $formLabel= $formObj->title;
            $dataadditional = array();
            $fieldsDatas = array();
            $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
            $dataadditional = json_decode($requests['jsonrequest'],true);
        
            $fieldsDatas = $this->getFormDataVal($fieldsData,$dataadditional);
            $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
            $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
            $link=$this->context->link;
            $this->context->smarty->assign(
                array(
                    'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__,
                    'fieldsData'=>$fieldsDatas,
                    'formLabel'=>$formLabel,
                    'is_backend'=>1,
                    'link'=>$link,
                )
            );
            return $this->display(__FILE__, 'views/templates/admin/viewformval.tpl');
        }
        else return '';
    }
    public function getCustomerFormVal($id_g_customrequest){
        $requests = gcustomrequestModel::getRequestById((int)$id_g_customrequest,(int)$this->context->shop->id);
        
        if($requests){
            $formObj = new gcustomfieldsModel((int)$requests['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
            $formLabel= $formObj->title;
            $dataadditional = array();
            $fieldsDatas = array();
            $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
            $dataadditional = json_decode($requests['jsonrequest'],true);
            $fieldsDatas = $this->getFormDataVal($fieldsData,$dataadditional);
            $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
            $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
            
            //fixx bug file upload dev Tung
            $url_rewrite = Context::getContext()->link->getModuleLink('g_customfields','additional',array(),$useSSL);
            if (!strpos($url_rewrite, 'index.php')){
                // fix in version 1.5
                $url_rewrite = str_replace('?module=g_customfields&controller=additional','',$url_rewrite);
            }
            //fixx bug file upload dev Tung
            $this->context->smarty->assign(
                array(
                    'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__,
                    'fieldsData'=>$fieldsDatas,
                    'formLabel'=>$formLabel,
                    'is_backend'=>0,
                    'requestdownload'=>$url_rewrite,
                )
            );
            return $this->display(__FILE__, 'views/templates/front/viewformval.tpl');
        }
        else return '';
    }
    public function hookActionbeforesubmitaccount($params) {
        return  $this->hookActionSubmitAccountBefore($params);
    }
    public function hookActionSubmitAccountBefore($params){
        $res = true;
        if(!Tools::isSubmit('gOpcFormSubmit'))
            if (Tools::isSubmit('submitCreate') && Tools::isSubmit('gSubmitForm')) {
                $id_shop = (int)$this->context->shop->id;
                $id_lang = (int)$this->context->language->id;
                $forms = gcustomfieldsModel::getFormType(array(1,2));

                if($forms)
                    foreach($forms as $form){
                        $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$id_lang,(int)$id_shop);
                        if(Validate::isLoadedObject($formObj) && $formObj->active){
                            $fields = $formObj->fields;
                            if($fields !=''){
                                $errors =  $this->checkData($formObj,$id_lang,$id_shop);

                                if(count($errors) > 0){
                                    foreach($errors as $error){
                                        $this->context->controller->errors[] = $error;
                                        $res = false;
                                    }
                                }   
                            }
                        }
                    }
            }
        return $res;
    }
    public function hookActionCustomerAccountAdd($_params){
        $customer = null;
        if(isset($_params['newCustomer'])) $customer = $_params['newCustomer'];
        elseif(isset($_params['customer'])) $customer = $_params['customer'];
        
        if(!Tools::isSubmit('gOpcFormSubmit')){
            if($customer !=null){
                $id_shop = (int)$this->context->shop->id;
                $id_lang = (int)$this->context->language->id;
                $forms = gcustomfieldsModel::getFormType(array(1,2));
               
                if($forms)
                    foreach($forms as $form){
                        $id_g_customrequest = 0;
                        $this->submitFormData((int)$form['typeform'],(int)$form['id_g_customfields'],$id_g_customrequest,(int)$id_lang,(int)$id_shop,$customer,null,false,false);
                    }
            }
        }else{
            if($customer !=null){
                $sql = 'UPDATE '._DB_PREFIX_.'g_customrequest
                    SET id_customer = '.(int)$customer->id.'
                    WHERE id_cart = '.(int)$this->context->cart->id
                ;
                Db::getInstance(_PS_USE_SQL_SLAVE_)->execute($sql);
            }
        }
    }
    public function submitFormData($typeform='1',$idform,&$id_g_customrequest=0,$id_lang,$id_shop,$customer=null,$cart=null,$ajax=false,$checkcaptcha = true,$in_backend = false){

        $_errors = array();$params = array();$formok = true;$is_update = false;
        $success = '';
        $need_load_cart = false;
        if($typeform == '1' || $typeform == '2')
        {
                if(!Validate::isLoadedObject($customer)){
                    if(!Tools::getValue('gOpcFormSubmit')){
                        $formok = false;
                        $_errors[] = $this->l(' An error occurred during load customer object.');
                    }else{
                        $need_load_cart = true;
                    }
                };

        }
        if($need_load_cart || $typeform == '3' || $typeform == '4' || $typeform == '5'){
            if(!Validate::isLoadedObject($cart)){
                $formok = false;
                $_errors[] = $this->l(' An error occurred during load cart object.');
            };
        }
        //if ($formok && Tools::isSubmit('gSubmitForm') || Tools::isSubmit('submitAccount')) {
        if ($formok && (Tools::isSubmit('submitCreate') || Tools::isSubmit('gSubmitForm'))) {

            $formObj = new gcustomfieldsModel((int)$idform,(int)$id_lang,(int)$id_shop);
            $formval = array();
            $requestObj = null;
            if(Validate::isLoadedObject($formObj) && $formObj->active){
                $fields = $formObj->fields;
                if($fields !=''){
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($fields,$id_lang,$id_shop);
                 
                    if($id_g_customrequest > 0){
                    
                        $requestObj = new gcustomrequestModel((int)$id_g_customrequest,null,$id_shop);
                        $is_update = true;
                    }  
                    if(Validate::isLoadedObject($requestObj)){
                    
                            if($requestObj->id_g_customfields !=$idform){
                                $formok = false;
                                $_errors[] = $this->l(' An error occurred during load form data.');
                            }
                            $need_load_cart = false;
                            if($formok && ($typeform == '1' || $typeform == '2')){
                                if($requestObj->id_customer != $customer->id){
                                    if(!$requestObj->reloadcustomer){
                                        $formok = false;
                                        $_errors[] = $this->l(' An error occurred during load customer data.');
                                    }else
                                        $need_load_cart = true;
                                }
                            }
                            if($formok && ($need_load_cart || $typeform == '3' || $typeform == '4' || $typeform == '5')){
                                if($requestObj->id_cart != $cart->id){
                                    $formok = false;
                                    $_errors[] = $this->l(' An error occurred during load cart data.');
                                }
                            }
                            if($formok){
                                $jsonrequest = $requestObj->jsonrequest;
                                $datas = json_decode($jsonrequest);
                                if($datas)
                                    foreach($datas as $key=>$data){
                                        $formval[rtrim(ltrim($key, '{'),'}')] = $data;
                                    }
                            }
                        }
                    else
                        $requestObj = new gcustomrequestModel(null,null,$id_shop);
                    // check captcha first

                    if($fieldsData && $formok && $checkcaptcha){
                        foreach($fieldsData as $fieldData){
                            if($fieldData['type'] =='captcha' && $checkcaptcha){
                                $grecaptcharesponse = Tools::getValue('g-recaptcha-response');
                                $formok &=$this->checkCaptcha($grecaptcharesponse,$_errors,$id_shop);
                            }
                        }
                    }
                    if($fieldsData && $formok){
                        foreach($fieldsData as $fieldData){
                            if($fieldData['type'] !='captcha' && $fieldData['type'] !='html'){
                                $field_data = Tools::getValue($fieldData['name']);
                                $old_val = '';
                                if($is_update && isset($formval[$fieldData['name']])) {
                                    $old_val = $formval[$fieldData['name']];
                                    if($fieldData['usercanedit'] != '1' && !$in_backend){ // missing permission to edit
                                        $params['{'.$fieldData['name'].'}'] = $old_val;
                                        continue; // next field
                                    }
                                }
                                $formok &= (bool)$this->validateForm($fieldData,$field_data,$_errors,$id_lang,$id_shop,$old_val);
                                if($formok){
                                    if($fieldData['type'] =='select' || $fieldData['type'] =='colorchoose' || $fieldData['type'] =='checkbox')
                                    {
                                        $params['{'.$fieldData['name'].'}'] = '';
                                        if(is_array($field_data) && !empty($field_data)){
                                            $params['{'.$fieldData['name'].'}'] = implode(',',$field_data);
                                        }else{
                                            $params['{'.$fieldData['name'].'}'] = $field_data;
                                        }

                                    }
                                    elseif($fieldData['type'] =='imagethumb'){
                                        $params['{'.$fieldData['name'].'}'] = '';
                                        if(is_array($field_data) && !empty($field_data)){
                                            $thumbs = array();
                                            foreach($field_data as $thumb_data){
                                                if($thumb_data !=''){
                                                    $thumb = $thumb_data;
                                                    $thumbs[] = $thumb;
                                                }
                                            }
                                            $params['{'.$fieldData['name'].'}'] = implode(',',$thumbs);
                                        }elseif($field_data !=''){

                                            $params['{'.$fieldData['name'].'}'] = $field_data;
                                        }

                                    }
                                    elseif($fieldData['type'] =='htmlinput')
                                    {

                                        $params['{'.$fieldData['name'].'}'] = htmlspecialchars($field_data);
                                    }
                                    elseif($fieldData['type'] =='product')
                                    {

                                        $params['{'.$fieldData['name'].'}'] = '';
                                        if(is_array($field_data) && !empty($field_data)){
                                            $product_chooses = array();
                                            foreach($field_data as $productid){
                                                if($productid > 0){
                                                    $product_chooses[] = (int)$productid;
                                                }
                                            }
                                            $params['{'.$fieldData['name'].'}'] = implode(',',$product_chooses);
                                        }else{
                                            if($field_data > 0){
                                                $product_choose = (int)$field_data;
                                                $params['{'.$fieldData['name'].'}'] = $product_choose;
                                            }
                                        }

                                    }elseif($fieldData['type'] == 'survey')
                                    {
                                        $surceyObj = new gcustomfieldsfieldsModel($fieldData['id_g_customfieldsfields'],$id_lang,$id_shop);
                                        $surcey_vals =   explode(',',$surceyObj->value);
                                        $surcey_data = array();
                                        foreach($surcey_vals as $key=>$surcey)
                                            $surcey_data[] = $surcey.':'.(isset($field_data[$key]) ? $field_data[$key] : '');
                                        $params['{'.$fieldData['name'].'}'] = implode(',',$surcey_data);
                                    }
                                    elseif($fieldData['type'] == 'input'){
                                        // register newletter
                                        if($fieldData['extra'] == 1){

                                            if(Module::isInstalled('blocknewsletter') && Module::isEnabled('blocknewsletter'))
                                            {
                                                if(Validate::isEmail($field_data)){

                                                    $blocknewsletterObj = Module::getInstanceByName('blocknewsletter');
                                                    $register_status = $blocknewsletterObj->isNewsletterRegistered($field_data);

                                                    if ($register_status <= 0){
                                                        if (!in_array(
                                                			$register_status,
                                                			array($blocknewsletterObj::GUEST_REGISTERED, $blocknewsletterObj::CUSTOMER_REGISTERED)
                                                		))
                                            			{

                                            				if (Configuration::get('NW_VERIFICATION_EMAIL'))
                                            				{
                                            					if ($register_status == Blocknewsletter::GUEST_NOT_REGISTERED){

                                                                   $sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
                                                        				VALUES
                                                        				('.(int)$this->context->shop->id.',
                                                        				'.(int)$this->context->shop->id_shop_group.',
                                                        				\''.pSQL($field_data).'\',
                                                        				NOW(),
                                                        				\''.pSQL(Tools::getRemoteAddr()).'\',
                                                        				(
                                                        					SELECT c.http_referer
                                                        					FROM '._DB_PREFIX_.'connections c
                                                        					WHERE c.id_guest = '.(int)$this->context->customer->id.'
                                                        					ORDER BY c.date_add DESC LIMIT 1
                                                        				),
                                                        				0
                                                        				)';
                                                		              Db::getInstance()->execute($sql);

                                            					}
                                                                $sql = '';
                                                                if (in_array($register_status, array(Blocknewsletter::GUEST_NOT_REGISTERED, Blocknewsletter::GUEST_REGISTERED)))
                                                        		{
                                                        			$sql = 'SELECT MD5(CONCAT( `email` , `newsletter_date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\')) as token
                                                        					FROM `'._DB_PREFIX_.'newsletter`
                                                        					WHERE `active` = 0
                                                        					AND `email` = \''.pSQL($field_data).'\'';
                                                        		}
                                                        		else if ($register_status == Blocknewsletter::CUSTOMER_NOT_REGISTERED)
                                                        		{
                                                        			$sql = 'SELECT MD5(CONCAT( `email` , `date_add`, \''.pSQL(Configuration::get('NW_SALT')).'\' )) as token
                                                        					FROM `'._DB_PREFIX_.'customer`
                                                        					WHERE `newsletter` = 0
                                                        					AND `email` = \''.pSQL($field_data).'\'';
                                                        		}
                                                                $token = false;
                                                                if($sql !='')
                                                        		  $token =  Db::getInstance()->getValue($sql);



                                            					if ($token){
                                            					   $verif_url = Context::getContext()->link->getModuleLink(
                                                            			'blocknewsletter', 'verification', array(
                                                            				'token' => $token,
                                                            			)
                                                            		);

                                                            		Mail::Send($this->context->language->id, 'newsletter_verif', Mail::l('Email verification', $this->context->language->id), array('{verif_url}' => $verif_url), $field_data, null, null, null, null, null, _PS_MODULE_DIR_.'blocknewsletter/mails/', false, $this->context->shop->id);
                                            					}
                                            				}
                                            				else
                                            				{
                                            				    $register_ok = false;
                                                                if ($register_status == Blocknewsletter::GUEST_NOT_REGISTERED){
                                                                    $sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active)
                                                        				VALUES
                                                        				('.(int)$this->context->shop->id.',
                                                        				'.(int)$this->context->shop->id_shop_group.',
                                                        				\''.pSQL($field_data).'\',
                                                        				NOW(),
                                                        				\''.pSQL(Tools::getRemoteAddr()).'\',
                                                        				(
                                                        					SELECT c.http_referer
                                                        					FROM '._DB_PREFIX_.'connections c
                                                        					WHERE c.id_guest = '.(int)$this->context->customer->id.'
                                                        					ORDER BY c.date_add DESC LIMIT 1
                                                        				),
                                                        				0
                                                        				)';
                                                		              $register_ok = Db::getInstance()->execute($sql);
                                                                }
                                                        		if ($register_status == Blocknewsletter::CUSTOMER_NOT_REGISTERED){
                                                        		  $sql = 'UPDATE '._DB_PREFIX_.'customer
                                                            				SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL(Tools::getRemoteAddr()).'\'
                                                            				WHERE `email` = \''.pSQL($field_data).'\'
                                                            				AND id_shop = '.(int)$this->context->shop->id;

                                                            		$register_ok =  Db::getInstance()->execute($sql);
                                                        		}


                                            					if ($register_ok){
                                            					   if ($code = Configuration::get('NW_VOUCHER_CODE'))
                                                                        Mail::Send($this->context->language->id, 'newsletter_voucher', Mail::l('Newsletter voucher', $this->context->language->id), array('{discount}' => $code), pSQL($field_data), null, null, null, null, null, _PS_MODULE_DIR_.'blocknewsletter/mails/', false, $this->context->shop->id);

                                           					       if (Configuration::get('NW_CONFIRMATION_EMAIL'))
                                                                        Mail::Send($this->context->language->id, 'newsletter_conf', Mail::l('Newsletter confirmation', $this->context->language->id), array(), pSQL($field_data), null, null, null, null, null, _PS_MODULE_DIR_.'blocknewsletter/mails/', false, $this->context->shop->id);
                                            					}
                                            				}
                                            			}
                                                    }
                                                }
                                            }
                                        }
                                        // #register newletter
                                        $params['{'.$fieldData['name'].'}'] = $field_data;
                                    }
                                    elseif($fieldData['type'] != 'fileupload') $params['{'.$fieldData['name'].'}'] = $field_data;
                                }
                            }
                        }
                    }
                    $params['{date_add}'] = date("Y-m-d H:i:s");
                    $params['{user_ip}'] = Tools::getRemoteAddr();
                    
                    if($formok){
                        
                        //upload file after form is validate ok
                        foreach($fieldsData as $fieldData){
                            if($formok && $fieldData['type'] =='fileupload'){
                                $file_attachments = array();
                        		$params['{'.$fieldData['name'].'}'] = '';
                                $multi = (bool)$fieldData['multi'];
                                if($multi){
                                    if (isset($_FILES[$fieldData['name']]['name']) && !empty($_FILES[$fieldData['name']]['name']) && !empty($_FILES[$fieldData['name']]['tmp_name']))
                            		{
                            		    foreach(array_keys($_FILES[$fieldData['name']]['name']) as $key){
                          		            if($_FILES[$fieldData['name']]['name'][$key]){
                                		        $file_attachment = array();
                                    			$file_attachment['rename'] = uniqid(). Tools::strtolower(Tools::substr($_FILES[$fieldData['name']]['name'][$key], -5));
                                    			$file_attachment['content'] = Tools::file_get_contents($_FILES[$fieldData['name']]['tmp_name'][$key]);
                                    			$file_attachment['tmp_name'] = $_FILES[$fieldData['name']]['tmp_name'][$key];
                                    			$file_attachment['name'] = $_FILES[$fieldData['name']]['name'][$key];
                                    			$file_attachment['mime'] = $_FILES[$fieldData['name']]['type'][$key];
                                    			$file_attachment['error'] = $_FILES[$fieldData['name']]['error'][$key];
                                                if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) && rename($file_attachment['tmp_name'], _PS_UPLOAD_DIR_.basename($file_attachment['rename']))) {
                                                    $file_attachments[]= $file_attachment['rename'];
                                                    @chmod(_PS_UPLOAD_DIR_.basename($file_attachment['rename']), 0664);
                                                }else{
                                                    $_errors[] = $fieldData['label'].' '.$this->l(' An error occurred during the file-upload process.');
                                                    $formok = false;
                                                    break;
                                                }
                                            }
                                        }
                            		}
                                    if(isset($formval[$fieldData['name']])){
                                        $file_attachments_olds = explode(',',$formval[$fieldData['name']]);
                                        if($file_attachments_olds){
                                            $file_old_choose = Tools::getValue($fieldData['name'].'_old');
                                            if($file_old_choose)
                                                foreach($file_old_choose as $file_old){
                                                    if(in_array($file_old,$file_attachments_olds)){
                                                        $file_attachments[] = $file_old;
                                                    }
                                                }

                                            //remove old image
                                            foreach($file_attachments_olds as $file_attachments_old){
                                                if(!in_array($file_attachments_old,$file_attachments)){
                                                    if(basename($file_attachments_old) !='' && file_exists(_PS_UPLOAD_DIR_.basename($file_attachments_old)))
                                                        @unlink(_PS_UPLOAD_DIR_.basename($file_attachments_old));
                                                }
                                            }
                                        }
                                    }
                                    if($file_attachments)
                                        $params['{'.$fieldData['name'].'}'] = implode(',',$file_attachments);
                                    else{
                                        if((bool)$fieldData['required']){
                                            $_errors[] = $fieldData['label'].' '.$this->l(' required.');
                                            $formok = false;
                                        }
                                    }

                                }else{
                                    if (isset($_FILES[$fieldData['name']]['name']) && !empty($_FILES[$fieldData['name']]['name']) && !empty($_FILES[$fieldData['name']]['tmp_name']))
                            		{
                            		    $file_attachment = array();
                            			$file_attachment['rename'] = uniqid(). Tools::strtolower(Tools::substr($_FILES[$fieldData['name']]['name'], -5));
                            			$file_attachment['content'] = Tools::file_get_contents($_FILES[$fieldData['name']]['tmp_name']);
                            			$file_attachment['tmp_name'] = $_FILES[$fieldData['name']]['tmp_name'];
                            			$file_attachment['name'] = $_FILES[$fieldData['name']]['name'];
                            			$file_attachment['mime'] = $_FILES[$fieldData['name']]['type'];
                            			$file_attachment['error'] = $_FILES[$fieldData['name']]['error'];
                                        if (isset($file_attachment['rename']) && !empty($file_attachment['rename']) &&
                                            rename($file_attachment['tmp_name'], _PS_UPLOAD_DIR_.basename($file_attachment['rename']))) {
                                                //fix bug uploadfile dev tung
                                              
                                            $old_file = isset($formval[$fieldData['name']]) ? $formval[$fieldData['name']] : '';
                                            $params['{'.$fieldData['name'].'}'] = $file_attachment['rename'];
                                            @chmod(_PS_UPLOAD_DIR_.basename($file_attachment['rename']), 0664);
                                            if(basename($old_file) !='' && file_exists(_PS_UPLOAD_DIR_.basename($old_file)))
                                                @unlink(_PS_UPLOAD_DIR_.basename($old_file));
                                        }else{
                                            $_errors[] = $fieldData['label'].' '.$this->l(' An error occurred during the file-upload process.');
                                            $formok = false;
                                            break;
                                        }
                                    }else{
                                        if(isset($formval[$fieldData['name']])) {
                                            $file_old_choose = Tools::getValue($fieldData['name'].'_old');
                                            if($file_old_choose !=''){
                                                $params['{'.$fieldData['name'].'}'] = $formval[$fieldData['name']];
                                            }else{
                                                
                                                $params['{'.$fieldData['name'].'}'] = '';
                                                if((bool)$fieldData['required']){
                                                    $_errors[] = $fieldData['label'].' '.$this->l(' required.');
                                                    $formok = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //#upload file after is validate ok
                    }
                    if($formok){
                        $requestObj->id_g_customfields = (int)$formObj->id_g_customfields;
                        $requestObj->user_ip = $params['{user_ip}'];
                        $requestObj->id_customer = (isset($customer) && $customer !=null && Validate::isLoadedObject($customer)) ? (int)$customer->id : 0;
                        $requestObj->id_cart = (isset($cart) && $cart !=null && Validate::isLoadedObject($cart)) ? (int)$cart->id : 0;
                        $requestObj->jsonrequest = json_encode($params);
                        $requestObj->date_add = $params['{date_add}'];

                        if($typeform == '1' || $typeform == '2'){
                            if(!isset($customer) || $customer ==null || !Validate::isLoadedObject($customer))
                                $requestObj->reloadcustomer = 1;
                        }
                        if($id_g_customrequest == 0)
                            $requestObj->status = 0;
                        $requestObj->save();
                        $id_g_customrequest = $requestObj->id;
                        
                        if($ajax)
                            
                            $success = $this->l('Data update successfull.');
                    }else{

                        if(count($_errors) > 0){
                            if(!$ajax)
                                foreach($_errors as $_error){
                                    $this->context->controller->errors[] = $_error;
                                }
                        }
                    }
                }
            }else{
                $_errors[] = $this->l(' An error occurred during load form object or form disabled.');
                $this->context->controller->errors[] = $this->l(' An error occurred during load form object or form disabled.');;
            }
        }
        if($ajax){
           
            $results = array(
                        'id_g_customrequest'=>(int)$id_g_customrequest,
                        'errors'=>!$formok,
                        'thankyou'=>$formok ? $success : $_errors,
                        'formval'=>''
                    );
                   
            if($in_backend)
                $results['formval'] = $this->getAdminFormVal((int)$id_g_customrequest);
            else if(Tools::getValue('id_customer')){
                $results['formval'] = $this->getCustomerFormVal((int)$id_g_customrequest);
            }
          
            die(json_encode($results));
        }
    }
    public function checkCaptcha($grecaptcharesponse,&$_errors,$id_shop){
        $formok = true;
        if(!empty($grecaptcharesponse)){
            $id_shop_group = (int)Shop::getContextShopGroupID();
            $secret = Configuration::get('GF_CUSTOMFIELD_SECRET_KEY', null, $id_shop_group, $id_shop);
            $verifyResponse = Tools::file_get_contents('https//www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$grecaptcharesponse);
            $responseData = json_decode($verifyResponse);
            if(is_object($responseData)){
                if(!$responseData->success){
                    $formok = false;
                    $_errors[] = $this->l('Robot verification failed, please try again.');
                }
            }elseif(is_array($responseData))
                if(!$responseData["success"]){
                    $formok = false;
                    $_errors[] = $this->l('Robot verification failed, please try again.');
                }
        }else{
            $formok = false;
            $_errors[] = $this->l('Please click on the reCAPTCHA box.');
        }
        return $formok;
    }
    public function checkData($formObj,$id_lang,$id_shop){
        $_errors = array();
        //if(Tools::isSubmit('submitAccount')){
        if (Tools::isSubmit('submitCreate') && Tools::isSubmit('gSubmitForm')) {
            $formok = true;
            if($formObj->id_g_customfields > 0 && $formObj->active){
                $fields = $formObj->fields;
                if($fields !=''){
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($fields,$id_lang,$id_shop);
                    
                    if($fieldsData){
                        foreach($fieldsData as $fieldData){
                            if($fieldData['type'] =='captcha' && $formok){
                                $grecaptcharesponse = Tools::getValue('g-recaptcha-response');
                                $formok &=$this->checkCaptcha($grecaptcharesponse,$_errors,$id_shop);
                            }
                        }
                    }
                    if($fieldsData && $formok){
                        foreach($fieldsData as $fieldData){
                            if($fieldData['type'] !='captcha' && $fieldData['type'] !='html'){
                                $field_data = Tools::getValue($fieldData['name']);
                                $this->validateForm($fieldData,$field_data,$_errors,$id_lang,$id_shop);
                            }
                        }
                    }

                }
            }
        }
        return $_errors;
    }
    public function validateForm($field='',$field_data,&$_errors,$id_lang,$id_shop,$old_val=''){
        

        if($field != ''){
            if((bool)$field['required'] && empty($field_data) && $field['type'] !='fileupload'){
                $_errors[] = $this->l('Field ').$field['label'].$this->l(' is empty.');
                return false;
            }
            if($field['validate'] !=''){
                if($field['type'] == 'slider' && (bool)$field['multi']){
                    $field_data = explode('-',$field_data);
                    if(isset($field_data[0])){
                        $isok = gcustomfieldsfieldsModel::gValidateField($field_data[0],$field['validate']);
                    }else{
                        $isok = false;
                    }
                    if($isok && isset($field_data[1])){
                        $isok = gcustomfieldsfieldsModel::gValidateField($field_data[1],$field['validate']);
                    }else{
                        $isok = false;
                    }
                    if($isok && isset($field_data[2])){
                        $isok = false;
                    }
                }else
                    $isok = gcustomfieldsfieldsModel::gValidateField($field_data,$field['validate']);
                if(!$isok) {
                    $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                    return false;
                }
            }
            switch ($field['type']) {
                case 'survey':
                    $surceyObj = new gcustomfieldsfieldsModel($field['id_g_customfieldsfields'],$id_lang,$id_shop);
                    $surcey_vals =   explode(',',$surceyObj->value);
                    $surcey_colurms =   explode(',',$surceyObj->description);
                    foreach($surcey_vals as $key=>$surcey){
                        if((bool)$field['required']){
                            if(!isset($field_data[$key]) || $field_data[$key] == ''){
                                $_errors[] =  sprintf($this->l('Field %s is empty.'), $surcey);
                                return false;
                            }
                        }
                        elseif(isset($field_data[$key]) && $field_data[$key] !='' && !in_array($field_data[$key],$surcey_colurms)){
                            $_errors[] = sprintf($this->l('Field %s is invalid.'), $surcey);
                            return false;
                        }
                    }
                    break;
                case 'checkbox':
                case 'radio':
                case 'select':
                case 'checkbox':
                    $fieldObj = new gcustomfieldsfieldsModel($field['id_g_customfieldsfields'],$id_lang,$id_shop);
                    $option_vals =   explode(',',$fieldObj->value);
                    if((bool)$field['required']){
                        if(!isset($field_data) || empty($field_data)){
                            $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                            return false;
                        }
                    }elseif(isset($field_data)){

                        if(is_array($field_data)){
                            foreach($field_data as $data)
                                if(!in_array($data,$option_vals) && $data !=''){

                                    $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                    return false;
                                }
                        }elseif(!in_array($field_data,$option_vals) && $field_data !=''){
                                $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                return false;
                        }
                    }
                    break;
                case 'colorchoose':
                case 'imagethumb':
                    $fieldObj = new gcustomfieldsfieldsModel($field['id_g_customfieldsfields'],$id_lang,$id_shop);
                    $option_vals =   explode(',',$fieldObj->extra);
                    if((bool)$field['required']){
                        if(!isset($field_data) || empty($field_data)){
                            $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                            return false;
                        }
                    }elseif(isset($field_data)){
                        if(is_array($field_data)){
                            foreach($field_data as $data)
                                if(!in_array($data,$option_vals) && $data !=''){
                                    $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                    return false;
                                }
                        }elseif(!in_array($field_data,$option_vals) && $field_data !=''){
                                $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                return false;
                        }
                    }
                    break;
                case 'survey':
                    $fieldObj = new gcustomfieldsfieldsModel($field['id_g_customfieldsfields'],$id_lang,$id_shop);
                    $option_vals =   explode(',',$fieldObj->extra);
                    if((bool)$field['required']){
                        if(!isset($field_data) || empty($field_data)){
                            $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                            return false;
                        }
                    }elseif(isset($field_data)){
                        if(is_array($field_data)){
                            foreach($field_data as $data)
                                if(!in_array($data,$option_vals) && $data !=''){
                                    $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                    return false;
                                }
                        }elseif(!in_array($field_data,$option_vals) && $field_data !=''){
                                $_errors[] = sprintf($this->l('Field %s is invalid.'), $field['label']);
                                return false;
                        }
                    }
                    break;
                case 'fileupload':
                   
                    $extension = explode(',',$field['extra']);
                    $multi = (bool)$field['multi'];
                    
                    if($multi){
                        if (isset($_FILES[$field['name']]['name']) && !empty($_FILES[$field['name']]['name']) && !empty($_FILES[$field['name']]['tmp_name']))
                    	{
                    	    foreach(array_keys($_FILES[$field['name']]['name']) as $key){
                                if($_FILES[$field['name']]['name'][$key]){
                    		        $file_attachment = array();
                        			$file_attachment['tmp_name'] = $_FILES[$field['name']]['tmp_name'][$key];
                        			$file_attachment['name'] = $_FILES[$field['name']]['name'][$key];
                        			$file_attachment['error'] = $_FILES[$field['name']]['error'][$key];
                                    if (!empty($file_attachment['name']) && $file_attachment['error'] != 0) {
                                        $_errors[] = $field['label'].' '.$this->l(' An error occurred during the file-upload process.');
                                        return false;
                                    } elseif (!empty($file_attachment['name']) &&
                                        !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -3)), $extension) &&
                                        !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -4)), $extension)
                                        ) {
                                        $_errors[] = $field['label'].' '.$this->l('Bad file extension');
                                        return false;
                                    }
                                }
                            }
                    	}
                        if($field['required'] && (!isset($_FILES[$field['name']]['name']) || empty($_FILES[$field['name']]['name']) || empty($_FILES[$field['name']]['tmp_name'])))
                        {
                            if($old_val ==''){
                        	    $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                        	    return false;
                            }
                        }
                        else if($field['required']){
                            $isempty = true;
                            foreach(array_keys($_FILES[$field['name']]['name']) as $key){
                                if($_FILES[$field['name']]['name'][$key]){
                                    $isempty = false;
                                }
                            }
                            if($old_val =='' && $isempty){
                               $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                    	       return false;
                            }
                        }
                    }else{
                        $file_attachment = array();
                        if (isset($_FILES[$field['name']]['name']) && !empty($_FILES[$field['name']]['name']) && !empty($_FILES[$field['name']]['tmp_name']))
                    	{
                    		$file_attachment['tmp_name'] = $_FILES[$field['name']]['tmp_name'];
                    		$file_attachment['name'] = $_FILES[$field['name']]['name'];
                    		$file_attachment['error'] = $_FILES[$field['name']]['error'];
                    	}
                        if($field['required'] && empty($file_attachment)){
                            if($old_val ==''){
                        	    $_errors[] = sprintf($this->l('Field %s is empty.'), $field['label']);
                        	    return false;
                             }
                        }elseif (!empty($file_attachment['name']) && $file_attachment['error'] != 0) {
                            $_errors[] = $field['label'].' '.$this->l(' An error occurred during the file-upload process.');
                            return false;
                        } elseif (!empty($file_attachment['name']) &&
                            !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -3)), $extension) &&
                            !in_array(Tools::strtolower(Tools::substr($file_attachment['name'], -4)), $extension)
                            ) {
                            $_errors[] = $field['label'].' '.$this->l('Bad file extension');
                            return false;
                        }
                    }
                    break;
            }
        }
        return true;
    }
    public function hookModuleRoutes($route = '', $detail = array())
	{
		$routes = array();
        $routes['module-g_customfields-additional'] = array(
			'controller' => 'additional',
			'rule' => 'my-account/additional.html',
			'keywords' => array(),
			'params' => array(
				'fc' => 'module',
				'module' => 'g_customfields',
			)
		);
		return $routes;
	}
    public function hookDisplayHeader($params){
            $controller = Tools::strtolower(Tools::getValue('controller'));
        if (version_compare(_PS_VERSION_, '8.0.0', '>=')){
            if($controller =='registration' || $controller == 'order'         
                || $controller == 'order-opc' || $controller == 'orderopc' 
                || $controller == 'authentication' || $controller == 'cart' 
                || (Tools::getValue('module') == 'g_customfields' && $controller == 'additional'))
            {
                $this->context->controller->addCSS(_MODULE_DIR_.$this->name.'/views/css/front/style_ps8.css');
               
            }

        }
       
        if($controller == 'order' || $controller == 'order-opc' || $controller == 'orderopc' ||
            $controller == 'authentication' || $controller == 'cart' ||
            (Tools::getValue('module') == 'g_customfields' && $controller == 'additional')){
            $this->context->controller->addJqueryUI('ui.datepicker');
            $this->context->controller->addJqueryUI('ui.slider');
            $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
            $protocol_content = ($useSSL) ? 'https://' : 'http://';
            $this->context->controller->addJS($protocol_content.'maps.googleapis.com/maps/api/js?v=3.exp');
            if (version_compare(_PS_VERSION_, '1.6.0.0', '<')){
                $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/g_customfields_oldversion.js');
            }
            
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/tinymce/tinymce.min.js');
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/jquery.minicolors.js');
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/front/g_customfields.js');
            $this->context->controller->addJS($protocol_content.'www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit');
            $this->context->controller->addCss(_MODULE_DIR_.$this->name.'/views/css/front/jquery.minicolors.css');
            $this->context->controller->addCss(_MODULE_DIR_.$this->name.'/views/css/front/g_customfields.css');


            // fix bug baseUri update ps1.7 03/2021
            $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
            $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
            $baseUri = $protocol_content.Tools::getHttpHost().__PS_BASE_URI__;
            return '<script>var baseUri="'.$baseUri.'"</script>';
        }
    }
    public function hookActionAdminControllerSetMedia()
    {
        $controller_admin = Tools::getValue('controller');
        if(
            Tools::strtolower($controller_admin)  == 'admincustomers' ||
            Tools::strtolower($controller_admin)  == 'adminorders'
        ){
            $this->context->controller->addCss($this->_path.'/views/css/front/'.$this->name.'.css');
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/admin/g_customfields.js');
        }
    }

    public function hookDisplayBackOfficeHeader($params){
        
        $controller_admin = Tools::getValue('controller');
        if(Tools::strtolower($controller_admin)  == 'admingcustomfieldmanager'){
            $this->context->controller->addCss($this->_path.'/views/css/admin/'.$this->name.'.css');
            if (version_compare(_PS_VERSION_, '1.7.8.0', '>=') === true){
            $this->context->controller->addCss(_MODULE_DIR_.$this->name.'/views/css/admin/cssStyle1780.css');
            }
        }
        if(
            Tools::strtolower($controller_admin)  == 'admincustomers' ||
            Tools::strtolower($controller_admin)  == 'adminorders'
        ){
            if(version_compare(_PS_VERSION_, '1.7.0.0', '<')){
                $this->context->controller->addJquery();
            }
            if(version_compare(_PS_VERSION_, '8.0.0', '>=')){
                $this->context->controller->addCss(_MODULE_DIR_.$this->name.'/views/css/admin/style_ps8.css');
            }
            $this->context->controller->addCss($this->_path.'/views/css/front/'.$this->name.'.css');
            $this->context->controller->addJS(_MODULE_DIR_.$this->name.'/views/js/admin/g_customfields.js');
            
        }
        $ps_vs=17;
            if(version_compare(_PS_VERSION_, '1.7.5.1', '<')){
                $ps_vs=16;
            }
        $this->context->smarty->assign(array(
            'ps_vs'=>$ps_vs,
            'controller_admin'=>$controller_admin,
        ));
        $this->context->controller->addCss($this->_path.'/views/css/admin/icon.css');
        return $this->display(__FILE__, 'views/templates/admin/varriable_js.tpl');
    }
    public function getForm($id_form,$id_lang,$id_shop,$hookname='',$id_data = 0){
        $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https://' : 'http://';
        $base_uri = $protocol_content.Tools::getHttpHost().__PS_BASE_URI__;
         $formObj = new gcustomfieldsModel((int)$id_form,(int)$id_lang,(int)$id_shop);
         if(Validate::isLoadedObject($formObj) && (bool)$formObj->active && $formObj->id_g_customfields == (int)$id_form){
            $module_dir = _PS_MODULE_DIR_.'g_customfields/views/templates/front/formtemplates/';
            $id_shop_group = (int)Shop::getContextShopGroupID();
            $url_rewrite = Context::getContext()->link->getModuleLink('g_customfields','additional',array(),$useSSL);
            if (!strpos($url_rewrite, 'index.php')){
                // fix in version 1.5
                $url_rewrite = str_replace('?module=g_customfields&controller=additional','',$url_rewrite);
            }
            if($hookname !=''){}
            $formval = array();
            if($id_data > 0){
                $dataObj = new gcustomrequestModel((int)$id_data);
                if(Validate::isLoadedObject($dataObj)){
                    $this->context->smarty->assign(array(
                        'id_customer'=>(int)$dataObj->id_customer,
                        'id_cart'=>(int)$dataObj->id_cart,
                    ));
                    $jsonrequest = $dataObj->jsonrequest;
                    $datas = json_decode($jsonrequest);
                    if($datas)
                        foreach($datas as $key=>$data){
                            $formval[rtrim(ltrim($key, '{'),'}')] = $data;
                        }
                }
            }
            $is_backend = 0;
            if(Tools::getValue('controller') == 'AdminCustomers' || Tools::getValue('controller') == 'AdminOrders'){
                $url_rewrite=$this->context->link->getAdminLink('AdminGcustomfieldrequest');
                $is_backend = 1;
            }
             //update module dev Tung 2021
            $_customerid=0;
            $_customername='';
            $_customeremail='';
            if($is_backend==1){
                if(Tools::getValue('controller') == 'AdminCustomers'){
                    $_customerid=(int)Tools::getValue('id_customer');
                    $_customer=new Customer($_customerid);
                    $_customerid=$_customer->id;
                    $_customername= $_customer->firstname.' '.$_customer->lastname;
                    $_customeremail=$_customer->email;
                }else{
                    if(Tools::getValue('controller') == 'AdminOrders'){
                        $id_order=(int)Tools::getValue('id_order');
                        if($id_order>0){
                            $Obj_order= new Order($id_order);
                            $Obj_customer=new Customer($Obj_order->id_customer);
                            $_customerid=$Obj_customer->id;
                            $_customername=$Obj_customer->firstname.' '.$Obj_customer->lastname;
                            $_customeremail=$Obj_customer->email;
                        }
                    }
                }
            }else{
                $_customerid=($this->context->customer->isLogged()) ? $this->context->customer->id : '0';
                $_customername=($this->context->customer->isLogged()) ? $this->context->customer->firstname.' '.$this->context->customer->lastname : '';
                $_customeremail=($this->context->customer->isLogged()) ? $this->context->customer->email : '';
           
            }
            
            $this->context->smarty->assign(array(
                'sitekey'=>Configuration::get('GF_CUSTOMFIELD_RECAPTCHA', null, $id_shop_group, $id_shop),
                'gmap_key'=>Configuration::get('GF_GMAP_CUSTOMFIELD_API_KEY', null, $id_shop_group, $id_shop),
                'customerid'=>$_customerid,
                'customername'=>$_customername,
                'customeremail'=>$_customeremail,
                'productid'=>(Tools::getValue('id_product')) ? (int)Tools::getValue('id_product') : '0',
                'productname'=>(Tools::getValue('id_product')) ? Product::getProductName((int)Tools::getValue('id_product'),null,$this->context->language->id) : '',
                'shopname'=>$this->context->shop->name,
                'currencyname'=>$this->context->currency->name,
                'languagename'=>$this->context->language->name,
                'base_uri'=>$base_uri,
                'actionUrl'=>$url_rewrite,
                'button_upload_text'=>$this->l('Choose File'),
                'id_module_customfields'=> ($is_backend ? 0 :(int)Module::getModuleIdByName('g_customfields'))
            ));
            //update module dev Tung 2021

            //get product data
            $fields = $formObj->fields;
            $fieldsData = gcustomfieldsfieldsModel::getAllFields($fields,$id_lang,$id_shop);
            $usercanedit = array();
            if($fieldsData){
                foreach($fieldsData as $field){
                    $usercanedit[$field['name']] = (bool)$field['usercanedit'];
                    if($field['type'] == 'product'){
                        $productids = explode(',',$field['extra']);
                        if($productids){
                            $productData = array();
                            foreach($productids as $productid){
                                if((int)$productid){
                                    $cover = Product::getCover((int)$productid);
                                    $id_image = 0;
                                    if(isset($cover['id_image'])) $id_image = (int)$cover['id_image'];
                                    $productObj = new Product((int)$productid,false,(int)$id_lang,(int)$id_shop);
                                    $productData[(int)$productid] =array(
                                        'id'=>(int)$productid,
                                        'name'=>Product::getProductName((int)$productid,null,(int)$id_lang),
                                        'link'=>$this->context->link->getProductLink($productid,null,null,null,(int)$id_lang,(int)$id_shop),
                                        'image_link' =>$this->context->link->getImageLink($productObj->link_rewrite,$id_image,Configuration::get('GF_PRODUCT_TYPE'))
                                    );
                                }
                            }
                            $this->context->smarty->assign(array(
                                    $field['name'].'product'=>$productData
                                )
                            );
                        }
                    }
                    if(isset($formval[$field['name']])){
                        $formval[$field['name'].'_old'] = $formval[$field['name']];
                        switch ($field['type']) {
                            case 'checkbox':
                            case 'survey':
                                $formval[$field['name']] = explode(',',$formval[$field['name']]);
                                break;
                            case 'slider':
                                if($field['multi']) $formval[$field['name']] = explode('-',$formval[$field['name']]);
                                break;
                            case 'imagethumb':
                            case 'select':
                            case 'product':
                                if($field['multi']) $formval[$field['name']] = explode(',',$formval[$field['name']]);
                                break;
                            case 'time':
                                if($field['extra'] == 1){
                                    $formval[$field['name']] = explode(':',$formval[$field['name']]);
                                    if(isset($formval[$field['name']][1])){
                                        $m_ampm = explode(' ',$formval[$field['name']][1]);
                                        $formval[$field['name']][1] = $m_ampm[0];
                                        if(isset($m_ampm[1]))
                                            $formval[$field['name']][2] = $m_ampm[1];
                                    }
                                }else
                                    $formval[$field['name']] = explode(':',$formval[$field['name']]);
                                break;
                            case 'htmlinput':
                                $formval[$field['name']] = htmlspecialchars_decode($formval[$field['name']]);
                                break;
                            case 'fileupload':
                                $attachfiles_array = array();
                                $extension = array('jpg','jpeg','gif','png');
                                $attachfiles = explode(',',$formval[$field['name']]);
                                foreach($attachfiles as $file){
                                    if($file !='' && file_exists(_PS_UPLOAD_DIR_.$file)){
                                        if(in_array(Tools::strtolower(Tools::substr($file, -3)), $extension) ||
                                            in_array(Tools::strtolower(Tools::substr($file, -4)), $extension)){
                                             $attachfiles_array[] = array('isImage'=>true,'name'=>$file);
                                        }else{
                                            $attachfiles_array[] = array('isImage'=>false,'name'=>$file);
                                        }
                                    }
                                }
                                $formval[$field['name']] = $attachfiles_array;
                                break;
                        }
                    }
                }
            }
            $requestdownload = '';

            if(Tools::getValue('controller') == 'AdminCustomers' || Tools::getValue('controller') == 'AdminOrders'){
                $requestdownload=$this->context->link->getAdminLink('AdminGcustomfieldrequest');
                $is_backend = 1;
            }else{
                $requestdownload = $url_rewrite;
                if (!strpos($url_rewrite, 'index.php')) $requestdownload.='?'; else $requestdownload.='?';
            }
            if(Tools::getValue('controller') == 'AdminCustomers'){
                $this->context->smarty->assign(array('id_customer'=>(int)Tools::getValue('id_customer')));
            }elseif(Tools::getValue('controller') == 'AdminOrders'){
                $this->context->smarty->assign(array('id_order'=>(int)Tools::getValue('id_order')));
            }
            $this->context->smarty->assign(array(
                'formval'=>$formval,
                'requestdownload'=>$requestdownload,
                'is_backend'=>$is_backend,
                'id_g_customrequest'=>(int)$id_data,
                'show_title'=>(bool)$formObj->show_title,
                'title_form'=>$formObj->title,
                'formajax'=>(bool)Tools::getValue('ajax'),
                'usercanedit'=>$usercanedit
            ));
            if(file_exists($module_dir.(int)$formObj->id_g_customfields.'/'.(int)$id_lang.'/'.(int)$id_shop.'_form.tpl')){
                return $this->display(__FILE__, 'views/templates/front/formtemplates/'.(int)$formObj->id_g_customfields.'/'.(int)$id_lang.'/'.(int)$id_shop.'_form.tpl');
            }else{
                $formObj->parseTpl((int)$id_lang,(int)$id_shop);
                return $this->display(__FILE__, 'views/templates/front/formtemplates/'.(int)$formObj->id_g_customfields.'/'.(int)$id_lang.'/'.(int)$id_shop.'_form.tpl');
            }
         }else
            return '';

    }
   public function hookActionDeleteGDPRCustomer ($customer)
   {
       if (!empty($customer['email']) && Validate::isEmail($customer['email'])) {
            $res = true;
            $id_customer = (int)$customer['id'];
            if($id_customer > 0){
                $additional = gcustomrequestModel::getCustomerAdditional((int)$id_customer,(int)$this->context->shop->id);
                if($additional)
                    foreach($additional as $request){
                        $requestObj = new gcustomrequestModel((int)$request['id_g_customrequest']);
                        if(Validate::isLoadedObject($requestObj)) $res &=$requestObj->delete();
                    }
            }
            if ($res) {
                return json_encode(true);
            }
            return json_encode($this->l('Unable to delete customer datas using email.'));
        }
   }
   public function hookActionExportGDPRData ($customer)
   {
       if (!Tools::isEmpty($customer['email']) && Validate::isEmail($customer['email'])) {
            $id_customer = (int)$customer['id'];
            $requests = array();
            if($id_customer > 0){
                $_forms = gcustomfieldsModel::getFormType(array(1,2));
                $additional = gcustomrequestModel::getCustomerAdditional((int)$id_customer,(int)$this->context->shop->id);

                $_g_customrequests = array();
                if($additional)
                    foreach($additional as $request)
                        $_g_customrequests[(int)$request['id_g_customfields']] = $request;
                if($_forms){
                    foreach($_forms as $form){
                        $_data = array();
                        $id_g_customrequest = 0;
                        if(isset($_g_customrequests[(int)$form['id_g_customfields']])) $id_g_customrequest = $_g_customrequests[(int)$form['id_g_customfields']]['id_g_customrequest'];
                        $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                        if($id_g_customrequest > 0){
                            $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                            $dataadditional = (isset($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'])) ? json_decode($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'],true) : array();
                            $_fieldsData = $this->getFormDataVal($fieldsData,$dataadditional);
                            if($_fieldsData){
                                foreach($_fieldsData as $customrequest){
                                    $_data[$customrequest['label']] = $customrequest['value'];
                                }
                                $requests[] = $_data;
                            }
                        }

                    }

               }
               $orderids = Order::getCustomerOrders((int)$id_customer);
               if($orderids){
                    $_forms = gcustomfieldsModel::getFormType(array(3,4,5));
                    foreach($orderids as $order){
                        $orderObj = new Order((int)$order['id_order']);
                        if(Validate::isLoadedObject($orderObj)){
                            $_requests = gcustomrequestModel::getRequestByCart((int)$orderObj->id_cart,(int)$orderObj->id_shop);
                            $_g_orderrequests = array();
                            if($_requests)
                                foreach($_requests as $request)
                                    $_g_orderrequests[(int)$request['id_g_customfields']] = $request;
                            if($_forms)
                                foreach($_forms as $form){
                                    $_data = array();
                                    $id_g_orderrequest = 0;
                                    if(isset($_g_orderrequests[(int)$form['id_g_customfields']])) $id_g_orderrequest = $_g_orderrequests[(int)$form['id_g_customfields']]['id_g_customrequest'];
                                    $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                                    if($id_g_orderrequest > 0){

                                        $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                                        $dataadditional = (isset($_g_orderrequests[(int)$form['id_g_customfields']]['jsonrequest'])) ? json_decode($_g_orderrequests[(int)$form['id_g_customfields']]['jsonrequest'],true) : array();
                                        $_fieldsData =   $this->getFormDataVal($fieldsData,$dataadditional);
                                        if($_fieldsData){
                                            $_data[$this->l('Id Order')] = (int)$order['id_order'];
                                            foreach($_fieldsData as $customrequest){
                                                $_data[$customrequest['label']] = $customrequest['value'];
                                            }
                                            $requests[] = $_data;
                                        }

                                    }
                                }
                        }

                    }
               }
               if($requests)
                    return json_encode($requests);
               else
                    return json_encode($this->l('No datas.'));
            }
       }
   }
}
