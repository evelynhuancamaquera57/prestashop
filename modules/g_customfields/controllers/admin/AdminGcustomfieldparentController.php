<?php
/**
* The file is controller. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2018 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

class AdminGcustomfieldparentController extends ModuleAdminController
{
	public function __construct()
	{
	   Tools::redirectAdmin($this->context->link->getAdminLink('AdminGcustomfieldmanager',true));
	}
}