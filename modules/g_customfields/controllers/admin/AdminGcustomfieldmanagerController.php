<?php
/**
* The file is controller. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsModel.php');
include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsfieldsModel.php');
class AdminGcustomfieldmanagerController extends ModuleAdminController
{
    public $typeform;
    public function __construct()
    {
        $this->className = 'gcustomfieldsModel';
        $this->table = 'g_customfields';
        parent::__construct();
        $this->meta_title = $this->l('Custom fields');
        $this->deleted = false;
        $this->explicitSelect = true;
        $this->context = Context::getContext();
        $this->lang = true;
        $this->bootstrap = true;
        $this->_defaultOrderBy = 'id_g_customfields';
        $this->filter = true;
        if (Shop::isFeatureActive()) {
            Shop::addTableAssociation($this->table, array('type' => 'shop'));
        }
        $this->bulk_actions = array(
			'delete' => array(
				'text' => $this->l('Delete selected'),
				'confirm' => $this->l('Delete selected items?'),
				'icon' => 'icon-trash'
			)
		);
        $this->position_identifier = 'id_g_customfields';
        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->typeform = array(
            '1'=>$this->l('Top of Registration Form'),
            '2'=>$this->l('Registration Form'),
            '5'=>$this->l('Order Page: Summary step'),
            '3'=>$this->l('Order Page: Shipping step'),
            '4'=>$this->l('Order Page: Payment step')
        );        
        $this->fields_list = array(
            'id_g_customfields' => array(
                'title' => $this->l('ID'),
                'type' => 'int',
                'width' => 'auto',
                'orderby' => false),
            'title' => array(
                'title' => $this->l('Title'),
                'width' => 'auto',
                'orderby' => false),
            'typeform' => array(
                'title' => $this->l('Position'),
                'type' => 'select',
                'list' => $this->typeform,
                'filter_key' => 'a!typeform',
                'filter_type' => 'int',
                'order_key' => 'typeform',
                'callback' => 'printTypeform',
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'width' => 'auto',
                'active' => 'status',
                'type' => 'bool',
                'orderby' => false),
            );
        
    }
    public function printTypeform($val,$row){
        $row;
        if(isset($this->typeform[(int)$val])) return $this->typeform[(int)$val];
        return '';
    }
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        $this->addJqueryUI('ui.sortable');
        $this->addJqueryUI('ui.draggable');
        $this->addJqueryUI('ui.droppable');
        $this->addJqueryPlugin('tagify');
        $this->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
        //fix version ps < 1.6.0.7 mising jquery plugin select2
        if(version_compare(_PS_VERSION_,'1.6.0.7') == -1){
            $this->addJqueryPlugin('autocomplete');
        }else
            $this->addJqueryPlugin('select2');
        $this->addJqueryPlugin('colorpicker');
        $this->addJqueryPlugin('fancybox');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/views/js/admin/g_customfields.js');
        $this->addJS(_MODULE_DIR_.$this->module->name.'/views/js/admin/validate.js');
        return true;
    }
    public function initToolBarTitle()
    {
        $this->toolbar_title[] = $this->l('Custom fields');
        $this->toolbar_title[] = $this->l('Fields');
    }
    public function postProcess()
	{
	   if (Tools::isSubmit('gformgetproduct')){
	       $query = Tools::getValue('q', false);
            if (!$query or $query == '' or Tools::strlen($query) < 1) {
                die();
            }
            if ($pos = strpos($query, ' (ref:')) {
                $query = Tools::substr($query, 0, $pos);
            }
            $excludeIds = Tools::getValue('excludeIds', false);
            if ($excludeIds && $excludeIds != 'NaN') {
                $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds))); // Validate all values in array to INT
            } else {
                $excludeIds = '';
            }
            $context = Context::getContext();
            $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
            		FROM `'._DB_PREFIX_.'product` p
            		'.Shop::addSqlAssociation('product', 'p').'
            		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
            		LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
            			ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
            		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$context->language->id.')
            		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\') 
                    '.(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
                    ' GROUP BY p.id_product';
            if(version_compare(_PS_VERSION_,'1.6.0.12') == -1){
                $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`
        		FROM `'._DB_PREFIX_.'product` p
        		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
        		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
        		Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
        		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$context->language->id.')
        		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
        		(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
        		' GROUP BY p.id_product';
            }elseif(version_compare(_PS_VERSION_,'1.6.1.0') == -1){
                $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`, p.`cache_default_attribute`
        		FROM `'._DB_PREFIX_.'product` p
        		'.Shop::addSqlAssociation('product', 'p').'
        		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
        		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
        		Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
        		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$context->language->id.')
        		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
        		(!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
        		' GROUP BY p.id_product';
            }
            $items = Db::getInstance()->executeS($sql);
            $results = array();
            if ($items) {
                foreach ($items as $item) {
                    $product = array(
                        'id' => (int)($item['id_product']),
                        'name' => $item['name'],
                        'ref' => (!empty($item['reference']) ? $item['reference'] : ''),
                        'image' => str_replace('http://', Tools::getShopProtocol(), $context->link->getImageLink($item['link_rewrite'], $item['id_image'], Configuration::get('GF_PRODUCT_TYPE'))),
                    );
                array_push($results, $product);
                }
            }
            $results = array_values($results);
            echo json_encode($results);
            die();
       }else{
    	   if (Tools::isSubmit('deletefields')){
    	       $deletefields = Tools::getValue('deletefields');
               if($deletefields !=''){
                    $deletefields_array = explode('_',$deletefields);
                    foreach($deletefields_array as $deletefield){
                        $fieldObj = new gcustomfieldsfieldsModel((int)$deletefield);
                        if($fieldObj->id_g_customfieldsfields == (int)$deletefield && $fieldObj->id_g_customfieldsfields){
                            $fieldObj->delete();
                        }
                    }
               }
    	   }
           $id_g_customfields = (int)Tools::getValue('id_g_customfields');
           //minify html code before save to database (remove space in HTML file)
           //I can't use Tools:getValue here.
           if(Tools::getIsset('formtemplate')){
                $_POST['formtemplate'] = gcustomfieldsModel::minifyHtml(Tools::getValue('formtemplate'));
           }
           //#end minify
           $this->clearcache();
           $return = parent::postProcess(true);
           if (Tools::isSubmit('submitAddg_customfields')){
               if(is_object($return) && get_class($return) == 'gcustomfieldsModel'){
                    $id_g_customfields = (int)Tools::getValue('id_g_customfields');
                    if($id_g_customfields<=0){ $id_g_customfields = $return->id;}
                    $shopsactive = Tools::getValue('checkBoxShopAsso_g_customfields');
                    foreach (Language::getLanguages() as $lang){
                        if($shopsactive){
                            $this->parseEmailAndTpl((int)$id_g_customfields,$lang,$shopsactive);
                        }else{
                            $id_shop = $this->context->shop->id;
                            $this->parseEmailAndTpl((int)$id_g_customfields,$lang,$id_shop);
                        }
                    }
               }
           }
       }
	}
    public function clearcache()
    {
        $clearSmartyCache = Tools::clearSmartyCache();
        $clearXMLCache = Tools::clearXMLCache();
        $clearXMLCache;
        $clearSmartyCache;
    }
    public function renderForm()
    {
        $id_g_customfields = (int)Tools::getValue('id_g_customfields');
        $allfieldstype = gcustomfieldsfieldsModel::getAllFieldType();
        if($id_g_customfields>0){
            $formObj = new gcustomfieldsModel((int)$id_g_customfields);
            $datas = array();
            $fields = $formObj->fields;
            if($fields){
                $languages = Language::getLanguages(false);
                $id_shop = $this->context->shop->id;
                foreach($languages as $language){
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($fields,(int)$language['id_lang'],$id_shop);
                    foreach($fieldsData as $field)
                        if($field['type'] !='html' && $field['type'] !='captcha' && $field['type'] !='googlemap')
                            $datas[$language['id_lang']][] = array(
                                'label'=>$field['label'],
                                'shortcode'=>' {'.$field['name'].'}'
                            );
                    $datas[$language['id_lang']][] = array(
                            'label'=>$this->l('Ip Address'),
                            'shortcode'=>' {user_ip}'
                        );
                    $datas[$language['id_lang']][] = array(
                                'label'=>$this->l('Date add'),
                                'shortcode'=>'{date_add}'
                            );
                }
           }
           $this->fields_value = array(
                'formtemplate'=>Tools::getValue('formtemplate',$formObj->formtemplate),
                'allfieldstype'=>$allfieldstype,
                'shortcodes'=>$datas,
            );
        }else{
            $this->fields_value = array(
                'formtemplate'=>Tools::getValue('formtemplate',''),
                'allfieldstype'=>$allfieldstype,
            );
        }
        $id_shop_group = Shop::getContextShopGroupID();
		$id_shop = Shop::getContextShopID();
        $this->fields_value['idlang_default'] = (int)$this->context->language->id;
        $this->fields_value['field_width_default'] = (int)Configuration::get('GF_CUSTOMFIELD_WIDTH_DEFAULT', null, $id_shop_group, $id_shop);
        $this->fields_value['field_width_tablet_default'] = (int)Configuration::get('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', null, $id_shop_group, $id_shop);
        $this->fields_value['group_width_mobile_default'] = (int)Configuration::get('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', null, $id_shop_group, $id_shop);
        $this->fields_value['group_width_default'] = (int)Configuration::get('GF_CUSTOMGROUP_WIDTH_DEFAULT', null, $id_shop_group, $id_shop);
        if($this->fields_value['field_width_default'] == 0){$this->fields_value['field_width_default'] = 12;}
        if($this->fields_value['field_width_tablet_default'] == 0){$this->fields_value['field_width_tablet_default'] = 12;}
        if($this->fields_value['group_width_mobile_default'] == 0){$this->fields_value['group_width_mobile_default'] = 12;}
        if($this->fields_value['group_width_default'] == 0){
            $this->fields_value['group_width_default'] = 12;
        }
        $this->fields_value['loadjqueryselect2'] = 1;
        if(version_compare(_PS_VERSION_,'1.6.0.7') == -1){
            $this->fields_value['loadjqueryselect2'] = 0;
        }
        if(version_compare(_PS_VERSION_,'1.6') == -1){
            $this->fields_value['psoldversion15'] = -1;
        }else $this->fields_value['psoldversion15'] = 0;
        $typeform = array(
            array('value'=>'2','name'=>$this->l('Registration Form')),
            array('value'=>'5','name'=>$this->l('Order Page: Summary step')),
            array('value'=>'3','name'=>$this->l('Order Page: Shipping step')),
            array('value'=>'4','name'=>$this->l('Order Page: Payment step')),
        );
        $input = array();
        $input[] = array(
            'type' => 'text',
            'label' => $this->l('Form Title'),
            'hint' => $this->l('Invalid characters') . ' &lt;&gt;;=#{}',
            'name' => 'title',
            'size' => 255,
            'required' => true,
            'lang' => true);    
        $input[] = array(
            'type' => 'select',
            'label' => $this->l('Position'),
            'name' => 'typeform',
            'required' => false,
            'lang' => false,
            'class' => 'typeform',
            'options' => array(
                'query' => $typeform,
                'id' => 'value',
                'name' => 'name'
            ));
        $input[] = array(
            'type' => (version_compare(_PS_VERSION_,'1.6') == -1) ? 'radio' : 'switch',
            'label' => $this->l('Status'),
            'name' => 'active',
            'required' => false,
            'is_bool' => true,
            'class'=>'switch_radio',
            'values' => array(array(
                    'id' => 'active_on',
                    'value' => 1,
                    'label' => $this->l('Active')), 
                    array(
                    'id' => 'active_off',
                    'value' => 0,
                    'label' => $this->l('Inactive'))));
        $input[] = array(
            'type' => (version_compare(_PS_VERSION_,'1.6') == -1) ? 'radio' : 'switch',
            'label' => $this->l('Show title'),
            'name' => 'show_title',
            'required' => false,
            'is_bool' => true,
            'class'=>'switch_radio',
            'values' => array(array(
                    'id' => 'show_title_on',
                    'value' => 1,
                    'label' => $this->l('Active')), 
                    array(
                    'id' => 'show_title_off',
                    'value' => 0,
                    'label' => $this->l('Inactive'))));     
        if (Shop::isFeatureActive()) {
            $input[] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
                );
        }
        $input[] = array('type' => 'formbuilder', 'name' => 'formbuilder');
        $input[] = array(
            'type' => 'textarea',
            'name' => 'fields',
            'class' => 'hidden',
            'cols'=>50,
            'rows'=>5
            );
        $this->fields_form = array(
                'legend' => array('title' => $this->l('Form Config'), 'icon' => 'icon-cogs'),
                'input' => $input,
                'submit' => array(
                                'title' => $this->l('Save'), 
                                'name' =>'submitAddg_customfields'
                                ),
                'buttons' => array(
                    'save_and_stay' => array(
    					'name' => 'submitAddg_customfieldsAndStay',
    					'type' => 'submit',
    					'title' => $this->l('Save and Stay'),
    					'class' => 'btn btn-default pull-right',
    					'icon' => 'process-icon-save'
    				),
    			)
            );
        Context::getContext()->smarty->assign(array(
            'psversion15'=>version_compare(_PS_VERSION_,'1.6'),
            'gdefault_language'=>(int)Context::getContext()->language->id
        ));
        $tpl = _PS_MODULE_DIR_.'g_customfields/views/templates/admin/gcustomfieldmanager/tabs.tpl';
        return Context::getContext()->smarty->fetch($tpl).parent::renderForm();
    }
    public function parseEmailAndTpl($id_form,$lang,$id_shops=null){
        $formObj = new gcustomfieldsModel((int)$id_form);
        if($formObj->id_g_customfields == (int)$id_form){
            if($id_shops && is_array($id_shops)){
                foreach($id_shops as $id_shop){
                    $formObj->parseTpl($lang["id_lang"],$id_shop);
                }
            }else{
                $id_shop = $id_shops;
                if($id_shop == null)
                    $id_shop = $this->context->shop->id;
                $formObj->parseTpl($lang["id_lang"],$id_shop);
            }
        }
    }
}
?>