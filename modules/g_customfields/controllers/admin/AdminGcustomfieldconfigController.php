<?php
/**
* The file is controller. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

class AdminGcustomfieldconfigController extends ModuleAdminController
{
    public $_html = '';
	public function __construct()
	{
		$this->bootstrap = true;
		$this->display = 'edit';
        parent::__construct();
		$this->meta_title = $this->l('Custom fields');
		
		if (!$this->module->active)
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
	}
    public function initContent()
	{
		$this->display = 'edit';
		$this->initTabModuleList();
		$this->initToolbar();
		$this->initPageHeaderToolbar();
		$this->content = $this->_html.$this->renderForm();
        $this->context->smarty->assign(array(
    			'content' => $this->content,
    			'url_post' => self::$currentIndex.'&token='.$this->token,			
    		));
        if(version_compare(_PS_VERSION_,'1.6') == 1){
    		$this->context->smarty->assign(array(		
    			'show_page_header_toolbar' => $this->show_page_header_toolbar,
    			'page_header_toolbar_title' => $this->page_header_toolbar_title,
    			'page_header_toolbar_btn' => $this->page_header_toolbar_btn
    		));
        }
	}
    public function setMedia($isNewTheme = false)
    {
        parent::setMedia($isNewTheme);
        if(version_compare(_PS_VERSION_,'1.6') == -1)
            $this->addJqueryUI('ui.mouse');
        $this->addJqueryPlugin('tagify');
        return true;
    }
    public function initTabModuleList(){
        if(version_compare(_PS_VERSION_,'1.5.4.0') == -1) 
            return true;
        if (version_compare(_PS_VERSION_, '1.7.8.8', '<='))
            return parent::initTabModuleList();
		else return true;
    }
    public function initToolBarTitle()
	{
		$this->toolbar_title[] = $this->l('Custom fields');
		$this->toolbar_title[] = $this->l('General Settings');
	}
    public function initPageHeaderToolbar()
	{
        $this->page_header_toolbar_btn = array(
            'new' => array(
                'href' => $this->context->link->getAdminLink('AdminGcustomfieldmanager'),
                'desc' => $this->l('Custom field', null, null, false),
                'icon' => 'process-icon-cogs'
            )
        );
        if(version_compare(_PS_VERSION_,'1.6') == 1){
		  parent::initPageHeaderToolbar();
        }
	}
	
    public function postProcess()
	{
		//update module dev Tung 2021
		if (Tools::isSubmit('getfieldscustom') && ((Tools::getValue('id_customer') && Tools::getValue('id_customer') > 0) || (Tools::getValue('id_order') && Tools::getValue('id_order') > 0))) {
			
			if(Tools::getValue('id_customer')){
				$id_customer=(int)Tools::getValue('id_customer');
				$sql = 'SELECT * FROM `'._DB_PREFIX_.'g_customrequest` WHERE id_customer ='.(int)$id_customer;
			}else{
				if(Tools::getValue('id_order')){
					$id_order=(int)Tools::getValue('id_order');
					$obj_order=new Order($id_order);
					if(Validate::isLoadedObject($obj_order)){
						$id_customer = $obj_order->id_customer;
						$sql = 'SELECT * FROM `'._DB_PREFIX_.'g_customrequest` WHERE id_cart='.(int)$obj_order->id_cart;
					}
				}else{
					die();
				}
			}
            $res = Db::getInstance()->executeS($sql);
            $id_lang = (int)Context::getContext()->language->id;
            $id_shop = (int)Context::getContext()->shop->id;
            $getfield = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
                SELECT a.name,b.label,a.type,a.multi
                FROM '._DB_PREFIX_.'g_customfieldsfields as a,
                     '._DB_PREFIX_.'g_customfieldsfields_lang as b,
                     '._DB_PREFIX_.'g_customfieldsfields_shop as c
                WHERE a.id_g_customfieldsfields = b.id_g_customfieldsfields
                AND a.id_g_customfieldsfields = c.id_g_customfieldsfields
                AND c.id_shop = '.(int)$id_shop.'
                AND b.id_lang = '.(int)$id_lang.'
                AND a.showinlistadmin = 1'
            );
			
            $data = array();
            if ($res) {
                foreach ($res as $value) {
                    $jsonrequest = json_decode($value['jsonrequest'],true);
                    if ($getfield) {
                        foreach($getfield as $key => $label){
                            if (isset($jsonrequest['{'.$label['name'].'}']) && $jsonrequest['{'.$label['name'].'}']) {
                                $data[$key]['name_field'] = $label['label'];
                                $data[$key]['value_field'] = $jsonrequest['{'.$label['name'].'}'];
                            }
                        }
                    }
                }
            }
			
			if(Tools::getValue('checkdataField')=='1'){
				if($data!=null){
					echo ('1');
					die();
				}else{
					echo ('0');
					die();
				}
			}
            $this->context->smarty->assign('data',$data);
            $tpl = $this->context->smarty->fetch(_PS_MODULE_DIR_.'g_customfields/views/templates/admin/fields_customer.tpl');
            echo $tpl;
            die();
			//update module dev Tung 2021
        } else{
			if (Tools::isSubmit('saveConfig'))
			{
				$shop_groups_list = array();
				$shops = Shop::getContextListShopID();
				$shop_context = Shop::getContext();
				foreach ($shops as $shop_id)
				{
					$shop_group_id = (int)Shop::getGroupFromShop((int)$shop_id, true);
					if (!in_array($shop_group_id, $shop_groups_list))
						$shop_groups_list[] = (int)$shop_group_id;
					$res = Configuration::updateValue('GF_CUSTOMFIELD_RECAPTCHA', Tools::getValue('GF_CUSTOMFIELD_RECAPTCHA'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_CUSTOMFIELD_SECRET_KEY', Tools::getValue('GF_CUSTOMFIELD_SECRET_KEY'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_DEFAULT'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMGROUP_WIDTH_DEFAULT'), false, (int)$shop_group_id, (int)$shop_id);
					$res &= Configuration::updateValue('GF_GMAP_CUSTOMFIELD_API_KEY', Tools::getValue('GF_GMAP_CUSTOMFIELD_API_KEY'), false, (int)$shop_group_id, (int)$shop_id);
				}
				/* Update global shop context if needed*/
				switch ($shop_context)
				{
					case Shop::CONTEXT_ALL:
						$res = Configuration::updateValue('GF_CUSTOMFIELD_RECAPTCHA', Tools::getValue('GF_CUSTOMFIELD_RECAPTCHA'));
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_SECRET_KEY', Tools::getValue('GF_CUSTOMFIELD_SECRET_KEY'));
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_DEFAULT'));
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT'));
						$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT'));
						$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMGROUP_WIDTH_DEFAULT'));
						$res &= Configuration::updateValue('GF_GMAP_CUSTOMFIELD_API_KEY', Tools::getValue('GF_GMAP_CUSTOMFIELD_API_KEY'));
						if (count($shop_groups_list))
						{
							foreach ($shop_groups_list as $shop_group_id)
							{
								$res = Configuration::updateValue('GF_CUSTOMFIELD_RECAPTCHA', Tools::getValue('GF_CUSTOMFIELD_RECAPTCHA'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_SECRET_KEY', Tools::getValue('GF_CUSTOMFIELD_SECRET_KEY'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMGROUP_WIDTH_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_GMAP_CUSTOMFIELD_API_KEY', Tools::getValue('GF_GMAP_CUSTOMFIELD_API_KEY'), false, (int)$shop_group_id);
							}
						}
						break;
					case Shop::CONTEXT_GROUP:
						if (count($shop_groups_list))
						{
							foreach ($shop_groups_list as $shop_group_id)
							{
								$res = Configuration::updateValue('GF_CUSTOMFIELD_RECAPTCHA', Tools::getValue('GF_CUSTOMFIELD_RECAPTCHA'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_SECRET_KEY', Tools::getValue('GF_CUSTOMFIELD_SECRET_KEY'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', (int)Tools::getValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', (int)Tools::getValue('GF_CUSTOMGROUP_WIDTH_DEFAULT'), false, (int)$shop_group_id);
								$res &= Configuration::updateValue('GF_GMAP_CUSTOMFIELD_API_KEY', Tools::getValue('GF_GMAP_CUSTOMFIELD_API_KEY'), false, (int)$shop_group_id);
							 }
						}
						break;
				}  
				if (!$res)
					$this->_html .= $this->module->displayError($this->l('The configuration could not be updated.'));
				else
					Tools::redirectAdmin($this->context->link->getAdminLink('AdminGcustomfieldconfig', true).'&conf=4');
			}
        }
    }
    public function renderForm() {
        $widths = array(
            array('value' => '1','name' => $this->l('1/12')),
            array('value' => '2','name' => $this->l('2/12')),
            array('value' => '3','name' => $this->l('3/12')),
            array('value' => '4','name' => $this->l('4/12')),
            array('value' => '5','name' => $this->l('5/12')),
            array('value' => '6','name' => $this->l('6/12')),
            array('value' => '7','name' => $this->l('7/12')),
            array('value' => '8','name' => $this->l('8/12')),
            array('value' => '9','name' => $this->l('9/12')),
            array('value' => '10','name' => $this->l('10/12')),
            array('value' => '11','name' => $this->l('11/12')),
            array('value' => '12','name' => $this->l('12/12')),
        );
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('General'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
					'type' => 'select',
					'label' => $this->l('Default width value of a input field (PC)'),
                    'name' => 'GF_CUSTOMFIELD_WIDTH_DEFAULT',
					'required' => true,
                    'lang' => false,
					'class' => 'GF_CUSTOMFIELD_WIDTH_DEFAULT',
					'options' => array(
						'query' => $widths,
						'id' => 'value',
						'name' => 'name'
					)
                ),
                array(
					'type' => 'select',
					'label' => $this->l('Default width value of a input field (Tablet)'),
                    'name' => 'GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT',
					'required' => true,
                    'lang' => false,
					'class' => 'GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT',
					'options' => array(
						'query' => $widths,
						'id' => 'value',
						'name' => 'name'
					)
                ),
                array(
					'type' => 'select',
					'label' => $this->l('Default width value of a input field (Mobile)'),
                    'name' => 'GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT',
					'required' => true,
                    'lang' => false,
					'class' => 'GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT',
					'options' => array(
						'query' => $widths,
						'id' => 'value',
						'name' => 'name'
					)
                ),
                array(
					'type' => 'select',
					'label' => $this->l('Default width value of a group'),
                    'name' => 'GF_CUSTOMGROUP_WIDTH_DEFAULT',
					'required' => true,
                    'lang' => false,
					'class' => 'GF_CUSTOMGROUP_WIDTH_DEFAULT',
					'options' => array(
						'query' => $widths,
						'id' => 'value',
						'name' => 'name'
					)
                ),
                array(
					'type' => 'text',
					'label' => $this->l('reCAPTCHA Site Key'),
                    'desc' => $this->l('Required if you want use Captcha for your form.'),
					'name' => 'GF_CUSTOMFIELD_RECAPTCHA'
				),
                array(
					'type' => 'text',
					'label' => $this->l('reCAPTCHA Secret Key'),
					'name' => 'GF_CUSTOMFIELD_SECRET_KEY'
				),
                array(
					'type' => 'text',
					'label' => $this->l('Google map api'),
					'name' => 'GF_GMAP_CUSTOMFIELD_API_KEY'
				)
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'saveConfig'
            )
        );
        $this->fields_value = $this->getConfigFieldsValues();
        return parent::renderForm();
    }
    public function getConfigFieldsValues()
	{
		$id_shop_group = Shop::getContextShopGroupID();
		$id_shop = Shop::getContextShopID();
		return array(
			'GF_GMAP_CUSTOMFIELD_API_KEY' => Tools::getValue('GF_GMAP_CUSTOMFIELD_API_KEY', Configuration::get('GF_GMAP_CUSTOMFIELD_API_KEY', null, $id_shop_group, $id_shop)),
            'GF_CUSTOMFIELD_RECAPTCHA' => Tools::getValue('GF_CUSTOMFIELD_RECAPTCHA', Configuration::get('GF_CUSTOMFIELD_RECAPTCHA', null, $id_shop_group, $id_shop)),
			'GF_CUSTOMFIELD_SECRET_KEY' => Tools::getValue('GF_CUSTOMFIELD_SECRET_KEY', Configuration::get('GF_CUSTOMFIELD_SECRET_KEY', null, $id_shop_group, $id_shop)),
            'GF_CUSTOMFIELD_WIDTH_DEFAULT' => Tools::getValue('GF_CUSTOMFIELD_WIDTH_DEFAULT', Configuration::get('GF_CUSTOMFIELD_WIDTH_DEFAULT', null, $id_shop_group, $id_shop)),
            'GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT' => Tools::getValue('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', Configuration::get('GF_CUSTOMFIELD_WIDTH_TABLET_DEFAULT', null, $id_shop_group, $id_shop)),
            'GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT' => Tools::getValue('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', Configuration::get('GF_CUSTOMFIELD_WIDTH_MOBILE_DEFAULT', null, $id_shop_group, $id_shop)),
            'GF_CUSTOMGROUP_WIDTH_DEFAULT' => Tools::getValue('GF_CUSTOMGROUP_WIDTH_DEFAULT', Configuration::get('GF_CUSTOMGROUP_WIDTH_DEFAULT', null, $id_shop_group, $id_shop)),
        );
	}
 }
?>