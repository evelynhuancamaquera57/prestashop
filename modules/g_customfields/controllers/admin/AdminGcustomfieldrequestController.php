<?php
/**
* The file is controller. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomfieldsModel.php');
include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomrequestModel.php');
class AdminGcustomfieldrequestController extends ModuleAdminController
{
    public $statuses_array = array();
    public function __construct()
    {
        $download = Tools::getValue('download');
        if($download!=''){
            if(file_exists(_PS_UPLOAD_DIR_.$download)){
                header('Content-Transfer-Encoding: binary');
                header('Content-Type: '.$download);
                header('Content-Length: '.filesize(_PS_UPLOAD_DIR_.$download));
                header('Content-Disposition: attachment; filename="'.utf8_decode($download).'"');
                @set_time_limit(0);
                readfile(_PS_UPLOAD_DIR_.$download);
            }
            exit;
        }
        parent::__construct();
        if(Tools::isSubmit('gSubmitForm')){
            $id_form = (int)Tools::getValue('idform');
            $id_g_customrequest = (int)Tools::getValue('id_g_customrequest');
            $id_customer = (int)Tools::getValue('id_customer');
            $customer = new Customer((int)$id_customer);
            $typeform = (int)Tools::getValue('typeform'); 
            $id_cart = (int)Tools::getValue('id_cart'); ; 
            $cartObj = null;$customerObj = null;$gcustomrequestModel = null;
            if($id_g_customrequest > 0)
                $gcustomrequestModel = new gcustomrequestModel($id_g_customrequest);
                
            if($typeform == '3' || $typeform == '4' || $typeform == '5'){
                if($gcustomrequestModel != null)
                    $id_cart = (int)$gcustomrequestModel->id_cart;
                else{
                    $id_order = (int)Tools::getValue('id_order');
                    $orderObj = new Order((int)$id_order);
                    if(Validate::isLoadedObject($orderObj))
                        $id_cart = (int)$orderObj->id_cart;
                }
                $cartObj = new Cart($id_cart);
            }
                
            if($typeform == '1' || $typeform == '2'){
                if($gcustomrequestModel != null)
                    $id_customer = $gcustomrequestModel->id_customer;
                $customerObj = new Customer((int)$id_customer);
            }
                                      
            if((Validate::isLoadedObject($customer) && ($typeform == '1' || $typeform == '2')) || 
                (Validate::isLoadedObject($cartObj) && ($typeform == '3' || $typeform == '4' || $typeform == '5'))){
                if(
                    ($gcustomrequestModel != null && Validate::isLoadedObject($gcustomrequestModel) && $gcustomrequestModel->id_g_customfields == $id_form) ||
                    ($gcustomrequestModel == null && $id_g_customrequest == 0 //&& ($typeform == '1' || $typeform == '2')
                    )
                ){
                    $this->module->submitFormData($typeform,$id_form,$id_g_customrequest,(int)$this->context->language->id,(int)$this->context->shop->id,$customerObj,$cartObj,true,false,true);
            
                }else{
                    $results = array(
                        'errors'=>1,
                        'thankyou'=>$this->l(' An error occurred during load data.Please try again.')
                    );
                    die(json_encode($results));
                }
            }else{
                $results = array(
                        'errors'=>1,
                        'thankyou'=>$this->l(' An error occurred during load object.')
                    );
                die(json_encode($results));
            }    
        }
        $changeStatus = Tools::getValue('changeStatus');
        if($changeStatus == '1'){
            $result = array(
                'success'=>0,
                'warrning'=>$this->l('Change status fail, please try again')
            );
            $idrequest = (int)Tools::getValue('id');
            $requestObj = new gcustomrequestModel($idrequest);
            if(Validate::isLoadedObject($requestObj)){
                $requestObj->status = (int)Tools::getValue('val');
                $requestObj->update();
                $result = array(
                    'success'=>1,
                    'warrning'=>$this->l('Change status successfull')
                );
            }
            echo json_encode($result);
            die();
        }
    }
}
?>