<?php
/**
* The file is controller. Do not modify the file if you want to upgrade the module in future
* 
* @author    Globo Software Solution JSC <contact@globosoftware.net>
* @copyright  2017 Globo., Jsc
* @license   please read license in file license.txt
* @link	     http://www.globosoftware.net
*/

include_once(_PS_MODULE_DIR_ . 'g_customfields/classes/gcustomrequestModel.php');
class G_customfieldsAdditionalModuleFrontController extends ModuleFrontController{
    public function __construct()
	{
		parent::__construct();
        if(version_compare(_PS_VERSION_,'1.6') ==-1){
            $this->display_column_left = false;
            $this->display_column_right = false;
        }
		$this->context = Context::getContext();
        $this->context->smarty->assign(array(
            'path'=>$this->l('Additional'),
            'meta_title'=>$this->l('Additional'),
            'meta_keywords' =>$this->l('Additional'),
            'meta_description' => '',
        ));
	}
    public function initContent()
    {
        parent::initContent();
        $download = Tools::getValue('download');
        if($download!=''){
            if(file_exists(_PS_UPLOAD_DIR_.$download)){
                header('Content-Transfer-Encoding: binary');
                header('Content-Type: '.$download);
                header('Content-Length: '.filesize(_PS_UPLOAD_DIR_.$download));
                header('Content-Disposition: attachment; filename="'.utf8_decode($download).'"');
                @set_time_limit(0);
                readfile(_PS_UPLOAD_DIR_.$download);
            }
            exit;
        }
        //ajax call
        if(Tools::isSubmit('gSubmitForm')){
            $id_form = (int)Tools::getValue('idform');
            $id_g_customrequest = (int)Tools::getValue('id_g_customrequest');
            $typeform = (int)Tools::getValue('typeform');
            $this->module->submitFormData($typeform,(int)$id_form,$id_g_customrequest,(int)$this->context->language->id,(int)$this->context->shop->id,$this->context->customer,$this->context->cart,true);
            die();
        }
        //#ajax call

        if(!$this->context->customer->isLogged())
        {
            Tools::redirect('index.php?controller=authentication?back=my-account');
            exit;
        }
        // load form customer
        $forms = array();$fieldsDatas = array();$formLabel = array();
        $_forms = gcustomfieldsModel::getFormType(array(1,2));
        $id_customer = (int)$this->context->customer->id;
        $this->context->smarty->assign(array('id_customer'=>$id_customer));
        $additional = gcustomrequestModel::getCustomerAdditional((int)$id_customer,(int)$this->context->shop->id);
        $_g_customrequests = array();
        if($additional)
            foreach($additional as $request)
                $_g_customrequests[(int)$request['id_g_customfields']] = $request;
        if($_forms)   
            foreach($_forms as $form){
                $id_g_customrequest = 0;
                if(isset($_g_customrequests[(int)$form['id_g_customfields']])) $id_g_customrequest = $_g_customrequests[(int)$form['id_g_customfields']]['id_g_customrequest'];
                $forms[(int)$form['id_g_customfields']]=$this->module->getForm((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id,'',(int)$id_g_customrequest);
                $formObj = new gcustomfieldsModel((int)$form['id_g_customfields'],(int)$this->context->language->id,(int)$this->context->shop->id);
                $formLabel[(int)$id_g_customrequest] = $formObj->title;
                if($id_g_customrequest > 0){
                    $fieldsData = gcustomfieldsfieldsModel::getAllFields($formObj->fields,(int)$this->context->language->id,(int)$this->context->shop->id);
                    $dataadditional = (isset($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'])) ? json_decode($_g_customrequests[(int)$form['id_g_customfields']]['jsonrequest'],true) : array();
                    $fieldsDatas[(int)$id_g_customrequest] = $this->module->getFormDataVal($fieldsData,$dataadditional);
                }
            }
        $useSSL = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https:/'.'/' : 'http:/'.'/';
        $this->context->smarty->assign(
            array(
                'baseUri'=>$protocol_content.Tools::getHttpHost().__PS_BASE_URI__,
                'forms'=>$forms,
                'fieldsDatas'=>$fieldsDatas,
                'formLabel'=>$formLabel,
                'id_customer'=>$id_customer,
            )
        );
        $tpl = 'module:'.$this->module->name.'/views/templates/front/additional17.tpl';
        if(version_compare(_PS_VERSION_, '1.7.0.0', '<')){
            $this->setTemplate('additional.tpl');
        }else{
            $this->setTemplate($tpl);
        }
        
    }
    
    /* fix missing function l() : translate in prestashop version 1.6 */
    protected function l($string, $specific = false, $class = null, $addslashes = false, $htmlentities = true)
    {
        $class;$addslashes;$htmlentities;
        if (isset($this->module) && is_a($this->module, 'Module')) {

            return $this->module->l($string, $specific);
        } else {
            return $string;
        }
    }
    
}