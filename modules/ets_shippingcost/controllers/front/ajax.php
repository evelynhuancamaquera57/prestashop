<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }
class Ets_shippingcostAjaxModuleFrontController extends ModuleFrontController
{
    public function __construct()
	{
		parent::__construct();
	}
    public function init()
    {
        parent::init();
    }
    public function postProcess()
    {
        if(Tools::isSubmit('submitEtsScClosePopup') && ($id_promote = (int)Tools::getValue('submitEtsScClosePopup')))
        {
            if($this->context->cookie->sc_closed_popups)
            {
                $sc_closed_popups = explode(',',$this->context->cookie->sc_closed_popups);
            }
            else
                $sc_closed_popups = array();
            $sc_closed_popups[] = $id_promote;
            $this->context->cookie->sc_closed_popups = implode(',',$sc_closed_popups);
            $this->context->cookie->write();
            die('Closed');
        }
        if(Tools::isSubmit('submitEtsScCloseHighlightBar') && ($id_promote = (int)Tools::getValue('submitEtsScCloseHighlightBar')))
        {
            if($this->context->cookie->sc_closed_highlightbars)
            {
                $sc_closed_highlightbars = explode(',',$this->context->cookie->sc_closed_highlightbars);
            }
            else
                $sc_closed_highlightbars = array();
            $sc_closed_highlightbars[] = $id_promote;
            $this->context->cookie->sc_closed_highlightbars = implode(',',$sc_closed_highlightbars);
            $this->context->cookie->write();
            die('Closed');
        }
    }
 }