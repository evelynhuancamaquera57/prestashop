<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }
require_once(dirname(__FILE__) . '/classes/ets_sc_defines.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_obj.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_paggination_class.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_shipping_rule.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_condition_rule.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_action_rule.php');
require_once(dirname(__FILE__) . '/classes/ets_sc_promote_rule.php');
if (!function_exists('ets_sc_execute_php'))
    include_once(_PS_MODULE_DIR_ . 'ets_shippingcost/classes/ext/temp');
if (!defined('_PS_ETS_SC_IMG_DIR_')) {
    define('_PS_ETS_SC_IMG_DIR_', _PS_IMG_DIR_.'ets_shippingcost/');
}
if (!defined('_PS_ETS_SC_IMG_')) {
    define('_PS_ETS_SC_IMG_', _PS_IMG_.'ets_shippingcost/');
}
class Ets_shippingcost extends Module
{
    public $is17 = false;
    public $hooks = array(
        'displayBackOfficeHeader',
        'displayHeader',
        'displayBanner',
        'actionValidateOrder',
        'displayShoppingCartFooter',
    );
    public $html;
    public static $trans;
    public $_errors = array();
    public $check_shipping_discount;
    public $module_dir;
    public function __construct()
    {
        $this->name = 'ets_shippingcost';
        $this->tab = 'shipping_logistics';
        $this->version = '1.1.4';
        $this->author = 'PrestaHero';
        $this->need_instance = 0;
        $this->bootstrap = true;
        parent::__construct();
		$this->module_key = 'b0d78c79c5910af8d2442d5693e84eaa';
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
        $this->module_dir = $this->_path;
        $this->displayName = $this->l('Shipping Cost Pro');
        $this->description = $this->l('Create shipping cost rules by zone, country, zip code, customer group, size, weight, time periods, etc. Calculate shipping costs flexibly to encourage customers to buy more!');
$this->refs = 'https://prestahero.com/';
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true;
    }
    public function getTabs()
    {
        $tabs = array(
            array(
                'class' => 'AdminShippingCostRule',
                'icon' => 'icon-rule',
                'active' => 1,
                'name' => $this->l('Shipping cost'),
                'name_lang' =>'Shipping cost'
            ),
        );
        return $tabs;
    }
    public function _installTab()
    {
        
        if ($parentId = Tab::getIdFromClassName('AdminParentShipping')) {
            
            $languages = Language::getLanguages(false);
            $tabs = $this->getTabs();
            foreach($tabs as $tab)
            {
                $tabObj = new Tab();
                $tabObj->id_parent = (int)$parentId;
                $tabObj->class_name = $tab['class'];
                $tabObj->icon = $tab['icon'];
                $tabObj->module = $this->name;
                $tabObj->active=(int)$tab['active'];
                $tabObj->position = 6;
                foreach ($languages as $l) {
                    $tabObj->name[$l['id_lang']] = $this->getTextLang($tab['name_lang'],$l) ?: $tab['name'];
                }
                if (!Tab::getIdFromClassName($tabObj->class_name))
                    $tabObj->add();
            } 
        }
        return true;
    }
    public function _unInstallTab()
    {
        $tabs = $this->getTabs();
        foreach($tabs as $tab)
        {
            if ($id = Tab::getIdFromClassName($tab['class'])) {
                $tabobj = new Tab((int)$id);
                $tabobj->delete();
            }
        }
        return true;
    }
    public function install()
    {
        return parent::install() && $this->installHooks() && $this->installDefaultConfig() && $this->installDb()&& $this->_installTab();
    }
    public function unInstall()
    {
        return parent::unInstall() && $this->unInstallHooks()&& $this->unInstallDefaultConfig()&& $this->unInstallDb() && $this->_unInstallTab() && $this->rrmdir(_PS_ETS_SC_IMG_DIR_);
    }
    public function installDb()
    {
        return Ets_sc_defines::getInstance()->installDb();
    }
    public function unInstallDb()
    {
        return Ets_sc_defines::getInstance()->unInstallDb();
    }
    public function installHooks()
    {
        foreach($this->hooks as $hook)
        {
            $this->registerHook($hook);
        }
        $position_hooks = Ets_sc_promote_rule::getInstance()->getPositions();
        foreach(array_keys($position_hooks) as $position_hook)
        {
            $this->registerHook($position_hook);
        }
        return true;
    }
    public function unInstallHooks()
    {
        foreach($this->hooks as $hook)
        {
            $this->unRegisterHook($hook);
        }
        $position_hooks = Ets_sc_promote_rule::getInstance()->getPositions();
        foreach(array_keys($position_hooks) as $position_hook)
        {
            $this->unRegisterHook($position_hook);
        }
        return true;
    }
    public function installDefaultConfig(){
        $inputs = Ets_sc_defines::getInstance()->getConfigInputs();
        $languages = Language::getLanguages(false);
        if($inputs)
        {
            foreach($inputs as $input)
            {
                if(isset($input['default']) && $input['default'])
                {
                    if(isset($input['lang']) && $input['lang'])
                    {
                        $values = array();
                        foreach($languages as $language)
                        {
                            $values[$language['id_lang']] = isset($input['default_lang']) && $input['default_lang'] ? $this->getTextLang($input['default_lang'],$language) : $input['default'];
                        }
                        Configuration::updateGlobalValue($input['name'],$values);
                    }
                    else
                        Configuration::updateGlobalValue($input['name'],$input['default']);
                }
            }
        }
        $this->context->cookie->sc_closed_popups ='';
        $this->context->cookie->sc_closed_highlightbars ='';
        $this->context->cookie->write();
        return true;
    }
    public function unInstallDefaultConfig(){
        $inputs = Ets_sc_defines::getInstance()->getConfigInputs();
        if($inputs)
        {
            foreach($inputs as $input)
            {
                Configuration::deleteByName($input['name']);
            }
        }
        $this->context->cookie->sc_closed_popups ='';
        $this->context->cookie->sc_closed_highlightbars ='';
        $this->context->cookie->write();
        return true; 
    }
    public function getTextLang($text, $lang,$file_name='')
    {
        if(is_array($lang))
            $iso_code = $lang['iso_code'];
        elseif(is_object($lang))
            $iso_code = $lang->iso_code;
        else
        {
            $language = new Language($lang);
            $iso_code = $language->iso_code;
        }
		$modulePath = rtrim(_PS_MODULE_DIR_, '/').'/'.$this->name;
        $fileTransDir = $modulePath.'/translations/'.$iso_code.'.'.'php';
        if(!@file_exists($fileTransDir)){
            return $text;
        }
        $fileContent = Tools::file_get_contents($fileTransDir);
        $text_tras = preg_replace("/\\\*'/", "\'", $text);
        $strMd5 = md5($text_tras);
        $keyMd5 = '<{' . $this->name . '}prestashop>' . ($file_name ? : $this->name) . '_' . $strMd5;
        preg_match('/(\$_MODULE\[\'' . preg_quote($keyMd5) . '\'\]\s*=\s*\')(.*)(\';)/', $fileContent, $matches);
        if($matches && isset($matches[2])){
           return  $matches[2];
        }
        return $text;
    }
    public function displayText($content=null,$tag='',$class=null,$id=null,$href=null,$blank=false,$src = null,$alt = null, $name = null,$value = null,$type = null,$data_id_product = null,$rel = null,$attr_datas=null, $title = null)
    {
    	return Ets_sc_defines::displayText($content, $tag, $class, $id, $href, $blank, $src, $alt, $name, $value, $type, $data_id_product, $rel, $attr_datas, $title);
    }
    public function displayPaggination($limit,$name)
    {
        $this->context->smarty->assign(
            array(
                'limit' => $limit,
                'pageName' => $name,
            )
        );
        return $this->display(__FILE__,'limit.tpl');
    }
    public function renderList($listData)
    { 
        if(isset($listData['fields_list']) && $listData['fields_list'])
        {
            foreach($listData['fields_list'] as $key => &$val)
            {
                $value_key = (string)Tools::getValue($key);
                $value_key_max = (string)Tools::getValue($key.'_max');
                $value_key_min = (string)Tools::getValue($key.'_min');
                if(isset($val['filter']) && $val['filter'] && ($val['type']=='int' || $val['type']=='date'))
                {
                    if(Tools::isSubmit('ets_sc_submit_'.$listData['name']))
                    {
                        $val['active']['max'] =  trim($value_key_max);   
                        $val['active']['min'] =  trim($value_key_min); 
                    }
                    else
                    {
                        $val['active']['max']='';
                        $val['active']['min']='';
                    }  
                }  
                elseif(!Tools::isSubmit('del') && Tools::isSubmit('ets_sc_submit_'.$listData['name']))               
                    $val['active'] = trim($value_key);
                else
                    $val['active']='';
            }
        }  
        if(!isset($listData['class']))
            $listData['class']='';  
        $this->smarty->assign($listData);
        return $this->display(__FILE__, 'list_helper.tpl');
    }
    public function getFilterParams($field_list,$table='')
    {
        $params = '';        
        if($field_list)
        {
            if(Tools::isSubmit('ets_sc_submit_'.$table))
                $params .='&ets_sc_submit_'.$table.='=1';
            foreach($field_list as $key => $val)
            {
                $value_key = (string)Tools::getValue($key);
                $value_key_max = (string)Tools::getValue($key.'_max');
                $value_key_min = (string)Tools::getValue($key.'_min');
                if($value_key!='')
                {
                    $params .= '&'.$key.'='.urlencode($value_key);
                }
                if($value_key_max!='')
                {
                    $params .= '&'.$key.'_max='.urlencode($value_key_max);
                }
                if($value_key_min!='')
                {
                    $params .= '&'.$key.'_min='.urlencode($value_key_min);
                } 
            }
            unset($val);
        }
        return $params;
    }
    public function addJquery()
    {
        if (version_compare(_PS_VERSION_, '1.7.6.0', '>=') && version_compare(_PS_VERSION_, '1.7.7.0', '<'))
            $this->context->controller->addJS(_PS_JS_DIR_ . 'jquery/jquery-'._PS_JQUERY_VERSION_.'.min.js');
        else
            $this->context->controller->addJquery();
    }
    public function hookDisplayBackOfficeHeader()
    {
        $controller = Tools::getValue('controller');
        if($controller=='AdminShippingCostRule')
        {
            $this->context->controller->addCSS($this->_path.'views/css/admin.css');
            $this->addJquery();
            $this->context->controller->addJqueryPlugin('autocomplete');
            $this->context->controller->addJS($this->_path.'views/js/admin.js');
        }
    }
    public function displayListProducts($productIds)
    {
        if($productIds)
        {
            $products = Ets_sc_defines::getInstance()->getProductsByIds($productIds);
            $this->smarty->assign('products', $products);
            return $this->display(__FILE__, 'product-item.tpl');
        }
    }
    public static function validateArray($array,$validate='isCleanHtml')
    {
        if(!is_array($array))
        {
            if(method_exists('Validate',$validate))
            {
                return Validate::$validate($array);
            }
            else
                return true;
        }
        if(method_exists('Validate',$validate))
        {
            if($array && is_array($array))
            {
                $ok= true;
                foreach($array as $val)
                {
                    if(!is_array($val))
                    {
                        if($val && !Validate::$validate($val))
                        {
                            $ok= false;
                            break;
                        }
                    }
                    else
                        $ok = self::validateArray($val,$validate);
                }
                return $ok;
            }
        }
        return true;
    }
    public static function isColor($color)
    {
        return preg_match('/^(#[0-9a-fA-F]{6})$/', $color);
    }
    public static function isLink($link)
    {
        $link_validation = '/(http|https)\:\/\/[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/';
        if($link =='#' || preg_match($link_validation, $link)){
            return  true;
        }
        return false;
    }
    public function getContent()
    {
        if(!$this->active)
            return $this->displayWarning($this->l('You must enable Shipping Cost Pro module to configure its features'));
        if(Tools::isSubmit('search_product'))
        {
            Ets_sc_defines::getInstance()->searchProduct();
        }
        if(Tools::isSubmit('search_customer'))
        {
            Ets_sc_defines::getInstance()->searchCustomer();
        }
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminShippingCostRule'));
    }
    public function renderFormShippingRule()
    {
        $html = '';
        if(Tools::isSubmit('addNewShippingRule') || (Tools::isSubmit('editshipping_rule') && ($id_ets_sc_shipping_rule = (int)Tools::getValue('id_ets_sc_shipping_rule'))) )
        {
            $current_tab = Tools::getValue('current_tab','information');
            if($current_tab=='information')
            {
                if(isset($id_ets_sc_shipping_rule) && $id_ets_sc_shipping_rule)
                    $shippingrule = new Ets_sc_shipping_rule($id_ets_sc_shipping_rule);
                else
                    $shippingrule = new Ets_sc_shipping_rule();
                $html .= $shippingrule->renderForm();
            }
            elseif($current_tab=='condition')
            {
                $html .= $this->rederFormCombine();
                $html .= $this->displayCoditionRuleForm();
            }
            elseif($current_tab=='action' && isset($id_ets_sc_shipping_rule) && $id_ets_sc_shipping_rule)
            {
                $action = Ets_sc_action_rule::getActionByIdRule($id_ets_sc_shipping_rule);
                $html .= $action->renderForm();
            }
            elseif($current_tab=='discount')
                $html .=  $this->displayPromoteRuleForm();
            
        }
        $this->context->smarty->assign(
            array(
                'sc_link_search_product' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&search_product=1',
                'sc_link_search_customer' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&search_customer=1',
                'tab_rule' => $this->displayTabRule(),
                'link' => $this->context->link,
                'form_html' => $html,
                'current_tab' => isset($current_tab) ? $current_tab :'information',
                'id_ets_sc_shipping_rule' => isset($id_ets_sc_shipping_rule) && $id_ets_sc_shipping_rule,
                'shippingrule' => isset($id_ets_sc_shipping_rule) ? new Ets_sc_shipping_rule($id_ets_sc_shipping_rule,$this->context->language->id):false,
            )
        );
        return $this->display(__FILE__, 'form_rule.tpl');
    }
    public function displayPromoteRuleForm()
    {
        $id_ets_sc_shipping_rule = (int)Tools::getValue('id_ets_sc_shipping_rule');
        if($id_ets_sc_shipping_rule)
        {
	        $id_ets_sc_promote_rule = (int)Tools::getValue('id_ets_sc_promote_rule');
            if($id_ets_sc_promote_rule)
                $promoteRule = new Ets_sc_promote_rule($id_ets_sc_promote_rule);
            else
            {
                $promoteRule = new Ets_sc_promote_rule();
                $promoteRule->id_ets_sc_shipping_rule = $id_ets_sc_shipping_rule;
            }
            $this->smarty->assign(
                array(
                    'link' => $this->context->link,
                    'promotes' => $promoteRule->getListPromoteByRule($promoteRule->id_ets_sc_shipping_rule),
                    'promote_form' => $promoteRule->renderForm(),
                    'id_ets_sc_shipping_rule' => $id_ets_sc_shipping_rule,
                )
            );
            return $this->display(__FILE__,'promote.tpl');
        }
    }
    public function displayTabRule()
    {
        $id_ets_sc_shipping_rule = (int)Tools::getValue('id_ets_sc_shipping_rule');
        $current_tab = Tools::getValue('current_tab','information');
	    $cache_id = $this->_getCacheId(['rule_tab', $id_ets_sc_shipping_rule, $current_tab]);
	    if (!$this->isCached('rule_tab.tpl', $cache_id)) {
		    $this->context->smarty->assign(
			    array(
				    'link' => $this->context->link,
				    'id_ets_sc_shipping_rule' => $id_ets_sc_shipping_rule,
				    'current_tab' => Validate::isCleanHtml($current_tab) ? $current_tab : 'current_tab',
			    )
		    );
	    }
        return $this->display(__FILE__, 'rule_tab.tpl', $cache_id);
    }
    public function rederFormCombine()
    {
        $id_ets_sc_shipping_rule = (int)Tools::getValue('id_ets_sc_shipping_rule');
        if($id_ets_sc_shipping_rule)
        {
            $shippingRule = new Ets_sc_shipping_rule($id_ets_sc_shipping_rule);
                $fields_form = array(
                'form' => array(
                    'legend' => array(
                        'title' => $this->l('Combine condition'),
                        'icon' => ''
                    ),
                    'input' => array(
                        array(
                            'type' => 'radio',
                            'name' => 'type_combine_condition',
                            'validate' => 'isCleanHtml',
                            'default' => 'and',
                            'label' => $this->l('How to combine shipping rule conditions'),
                            'values' => array(
                                array(
                                    'id'=> 'type_combine_condition_and',
                                    'label' => $this->l('AND'),
                                    'value'=>'and',
                                    'p' => $this->l('Customer order must satisfy ALL conditions listed below to apply shipping rule')
                                ),
                                array(
                                    'id'=> 'type_combine_condition_or',
                                    'label' => $this->l('OR'),
                                    'value'=>'or',
                                    'p' => $this->l('Customer order just needs to satisfy ONE among the conditions listed below to apply shipping rule')
                                ),
                            ),
                        ),
                    ),
                    'submit' => array(
                        'title' => $this->l('Save'),
                    ),
                ),
            );
            $helper = new HelperForm();
            $helper->show_toolbar = false;
            $helper->id = $this->id;
            $helper->module = $this;
            $helper->identifier = $this->identifier;
            $helper->submit_action = 'btnSubmitCombine';
            $helper->currentIndex = $this->context->link->getAdminLink('AdminShippingCostRule', false).'&editshipping_rule=1&id_ets_sc_shipping_rule='.$id_ets_sc_shipping_rule.'&current_tab=condition';
            $helper->token = Tools::getAdminTokenLite('AdminShippingCostRule');
            $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
            $helper->default_form_language = $language->id;
            $helper->override_folder ='/';
            $helper->tpl_vars = array(
                'base_url' => $this->context->shop->getBaseURL(),
    			'language' => array(
    				'id_lang' => $language->id,
    				'iso_code' => $language->iso_code
    			),
                'PS_ALLOW_ACCENTED_CHARS_URL', (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL'),
                'fields_value' => array('type_combine_condition' => $shippingRule->type_combine_condition),
                'languages' => $this->context->controller->getLanguages(),
    			'id_language' => $this->context->language->id,
                'link' => $this->context->link,
            );
            return $helper->generateForm(array($fields_form));
        }
    }
    public function displayCoditionRuleForm(){
        $id_ets_sc_shipping_rule = (int)Tools::getValue('id_ets_sc_shipping_rule');
        if($id_ets_sc_shipping_rule)
        {
            $id_ets_sc_condition_rule = (int)Tools::getValue('id_ets_sc_condition_rule');
            $cache_id = $this->_getCacheId(['condition', $id_ets_sc_condition_rule, $id_ets_sc_shipping_rule]);
            if (!$this->isCached('condition.tpl', $cache_id)) {
	            if($id_ets_sc_condition_rule)
		            $conditionRule = new Ets_sc_condition_rule($id_ets_sc_condition_rule);
	            else
	            {
		            $conditionRule = new Ets_sc_condition_rule();
		            $conditionRule->id_ets_sc_shipping_rule = $id_ets_sc_shipping_rule;
	            }
	            $this->smarty->assign(
		            array(
			            'link' => $this->context->link,
			            'conditions' => $conditionRule->getListConditionsByRule($conditionRule->id_ets_sc_shipping_rule),
			            'condition_form' => $conditionRule->renderForm(),
			            'id_ets_sc_shipping_rule' => $id_ets_sc_shipping_rule,
		            )
	            );
            }
            return $this->display(__FILE__,'condition.tpl', $cache_id);
        }
    }
    public function displayListCustomers($id_customers)
    {
        if($id_customers)
        {
	        $cache_id = $this->_getCacheId(is_array($id_customers) ? array_merge(['customers'], $id_customers) : ['customers', $id_customers]);
	        if (!$this->isCached('customers.tpl', $cache_id)) {
		        $this->smarty->assign(
			        array(
				        'customers' => Ets_sc_condition_rule::getCustomersByIds($id_customers),
			        )
		        );
	        }
	        return $this->display(__FILE__,'customers.tpl', $cache_id);
        }
    }
    public function getPackageShippingCost($id_carrier,$use_tax,&$fees_shipping,$shipping_default=false)
    {
        if(!$shipping_default)
            $shipping_default = $fees_shipping;
        $shippingRules =Ets_sc_shipping_rule::getShippingRulesActive();
        if($shippingRules)
        {
            foreach($shippingRules as $shippingRule)
            {
                $rule = new Ets_sc_shipping_rule($shippingRule['id_ets_sc_shipping_rule']);
                if($rule->getPackageShippingCost($id_carrier,$use_tax,$fees_shipping,$shipping_default))
                {
                    break;
                }
            }
        }
    }
    public static function isPostCode($postcode)
    {
        return empty($postcode) || preg_match('/^[a-zA-Z 0-9-*]+$/', $postcode);
    }
    public function rrmdir($dir) {
        $dir = rtrim($dir,'/');
        if ($dir && is_dir($dir)) {
             if($objects = scandir($dir))
             {
                 foreach ($objects as $object) {
                       if ($object != "." && $object != "..") {
                         if (is_dir($dir."/".$object) && !is_link($dir."/".$object))
                           $this->rrmdir($dir."/".$object);
                         else
                           @unlink($dir."/".$object);
                       }
                 }
             }
             rmdir($dir);
       }
       return true;
    }
    public function displayPromoteDiscountRule($position='',$way_to_promote='')
    {
        $promoteDiscountRules = Ets_sc_shipping_rule::getPromoteDiscountRulesByPosition($position,$way_to_promote);
        $html = '';
        if($this->context->cookie->sc_closed_highlightbars)
        {
            $sc_closed_highlightbars = explode(',',$this->context->cookie->sc_closed_highlightbars);
        }
        else
            $sc_closed_highlightbars = array();
        if($this->context->cookie->sc_closed_popups)
        {
            $sc_closed_popups = explode(',',$this->context->cookie->sc_closed_popups);
        }
        else
            $sc_closed_popups = array();
        $this->smarty->assign(
            array(
                'sc_closed_highlightbars' => $sc_closed_highlightbars,
                'sc_closed_popups' => $sc_closed_popups,
            )
        );
        if($promoteDiscountRules)
        {
            foreach($promoteDiscountRules as $promoteDiscountRule)
            {
                $this->smarty->assign(
                    array(
                        'promoteDiscountRule' => $promoteDiscountRule,
                    )
                );
                $html .= $this->display(__FILE__,'promote_discount_rule.tpl');
            }
        }
        return $html;
    }
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/front.css');
        $this->context->controller->addJS($this->_path.'views/js/front.js');
        return $this->_header();    
    }
    public function _header()
    {
        if ($this->context->controller->php_self == 'category') {
            $this->smarty->assign(array(
                'hookDisplayProductListHeaderAfter' => $this->hookDisplayProductListHeaderAfter(),
                'hookDisplayProductListHeaderBefore' => $this->hookDisplayProductListHeaderBefore(),
            ));
        }
        if ($this->context->controller->php_self == 'product') {
            $this->smarty->assign(array(
                'hookDisplayProductVariantsBefore' => $this->hookDisplayProductVariantsBefore(),
                'hookDisplayProductVariantsAfter' => $this->hookDisplayProductVariantsAfter(),
                'hookDisplayProductCommentsListHeaderBefore' => $this->hookDisplayProductCommentsListHeaderBefore(),
            ));
        }
        if ($this->context->controller->php_self == 'cart') {
            $this->smarty->assign(array(
                'hookDisplayCartGridBodyBefore1' => $this->hookDisplayCartGridBodyBefore1(),
            ));
        }
        if ($this->context->controller->php_self == 'order') {
            $this->smarty->assign(array(
                'hookDisplayCartGridBodyBefore1' => $this->hookDisplayCartGridBodyBefore1(),
                'hookDisplayCartGridBodyBefore2' => $this->hookDisplayCartGridBodyBefore2(),
                'hookDisplayCartGridBodyAfter' => $this->hookDisplayCartGridBodyAfter(),
            ));
        }
        $this->smarty->assign(array(
            'hookDisplayLeftColumnBefore' => $this->hookDisplayLeftColumnBefore(),
            'hookDisplayRightColumnBefore' => $this->hookDisplayRightColumnBefore(),
        ));
        return $this->display(__FILE__, 'render-js.tpl');
    }
    public function hookDisplayShoppingCartFooter()
    {
        return $this->displayPromoteDiscountRule('displayShoppingCartFooter');
    }
    public function hookDisplayProductAdditionalInfo()
    {
        return $this->displayPromoteDiscountRule('displayProductAdditionalInfo');
    }
    public function hookDisplayBanner()
    {
        return $this->displayPromoteDiscountRule(false,'display_highlight_bar').$this->displayPromoteDiscountRule(false,'display_popup');
    }
    public function hookDisplayHome()
    {
        return $this->displayPromoteDiscountRule('displayHome');
    }
    public function hookDisplayLeftColumn()
    {
        return $this->displayPromoteDiscountRule('displayLeftColumn');
    }
    public function hookDisplayLeftColumnBefore()
    {
        return $this->displayPromoteDiscountRule('displayBeforeLeftColumn');
    }
    public function hookDisplayRightColumnBefore()
    {
        return $this->displayPromoteDiscountRule('displayBeforeRightColumn');
    }
    public function hookDisplayRightColumn()
    {
        return $this->displayPromoteDiscountRule('displayRightColumn');
    }
    public function hookDisplayCartGridBodyBefore2()
    {
        //custom hook
        return $this->displayPromoteDiscountRule('displayCartGridBodyBefore2');
    }
    public function hookDisplayCartGridBodyAfter()
    {
        //custom hook
        return $this->displayPromoteDiscountRule('displayCartGridBodyAfter');
    }
    public function hookDisplayProductListHeaderBefore()
    {
        return $this->displayPromoteDiscountRule('displayProductListHeaderBefore');
    }
    public function hookDisplayProductListHeaderAfter()
    {
        return $this->displayPromoteDiscountRule('displayProductListHeaderAfter');
    }
    public function hookDisplayFooterCategory()
    {
        return $this->displayPromoteDiscountRule('displayFooterCategory');
    } 
    public function hookDisplayNav1()
    {
        return $this->displayPromoteDiscountRule('displayNav1');
    }  
    public function hookDisplayFooterBefore()
    {
        return $this->displayPromoteDiscountRule('displayFooterBefore');
    }
    public function hookDisplayFooterAfter()
    {
        return $this->displayPromoteDiscountRule('displayFooterAfter');
    }
    public function hookDisplayCartGridBodyBefore1()
    {
        return $this->displayPromoteDiscountRule('displayCartGridBodyBefore1');
    }
    public function hookDisplayFooterProduct()
    {
        return $this->displayPromoteDiscountRule('displayFooterProduct');
    }
    public function hookDisplayProductVariantsBefore()
    {
        return $this->displayPromoteDiscountRule('displayProductVariantsBefore');
    }
    public function hookDisplayProductVariantsAfter()
    {
        return $this->displayPromoteDiscountRule('displayProductVariantsAfter');
    }
    public function hookDisplayReassurance()
    {
        $controller = Tools::getValue('controller');
        if($controller=='product')
            return $this->displayPromoteDiscountRule('displayReassurance');
    }
    public function hookDisplayAfterProductThumbs()
    {
        return $this->displayPromoteDiscountRule('displayAfterProductThumbs');
    }
    public function hookDisplayProductActions()
    {
        return $this->displayPromoteDiscountRule('displayProductActions');
    }
    public function hookDisplayProductCommentsListHeaderBefore()
    {
        return $this->displayPromoteDiscountRule('displayProductCommentsListHeaderBefore');
    }
    public function hookActionValidateOrder($params)
    {
        if(isset($params['cart']) && Validate::isLoadedObject($params['cart']) && isset($params['order']) && Validate::isLoadedObject($params['order']))
        {
            Ets_sc_shipping_rule::addToOrder($params['order']);
        }
    }
    public static function getLinkCustomerAdmin($id_customer)
    {
        if(version_compare(_PS_VERSION_, '1.7.6', '>='))
        {
            $link_customer = self::getLinkAdminController('admin_customers_view',array('customerId' => $id_customer));
        }
        else
            $link_customer = Context::getContext()->link->getAdminLink('AdminCustomers').'&id_customer='.(int)$id_customer.'&viewcustomer';
        return $link_customer;
    }
    public static function getLinkOrderAdmin($id_order)
    {
        if(version_compare(_PS_VERSION_, '1.7.7.0', '>='))
        {
            $link_order = self::getLinkAdminController('admin_orders_view',array('orderId' => $id_order));
        }
        else
            $link_order = Context::getContext()->link->getAdminLink('AdminOrders').'&id_order='.(int)$id_order.'&vieworder';
        return $link_order;
    }
    public static function getLinkAdminController($entiny,$params=array())
    {
        $sfContainer = call_user_func(array('\PrestaShop\PrestaShop\Adapter\SymfonyContainer','getInstance'));
    	if (null !== $sfContainer) {
    		$sfRouter = $sfContainer->get('router');
    		return $sfRouter->generate(
    			$entiny,
    			$params
    		);
    	}
    }
    /**
     * @param string $path
     * @param int $permission
     *
     * @return bool
     *
     * @throws \PrestaShopException
     */
    private function safeMkDir($path, $permission = 0755)
    {
        if (!@mkdir($concurrentDirectory = $path, $permission) && !is_dir($concurrentDirectory)) {
            throw new \PrestaShopException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        return true;
    }
    private function checkOverrideDir()
    {
        if (defined('_PS_OVERRIDE_DIR_')) {
            $psOverride = @realpath(_PS_OVERRIDE_DIR_) . DIRECTORY_SEPARATOR;
            if (!is_dir($psOverride)) {
                $this->safeMkDir($psOverride);
            }
            $base = str_replace('/', DIRECTORY_SEPARATOR, $this->getLocalPath() . 'override');
            $iterator = new RecursiveDirectoryIterator($base, FilesystemIterator::SKIP_DOTS);
            /** @var RecursiveIteratorIterator|\SplFileInfo[] $iterator */
            $iterator = new RecursiveIteratorIterator($iterator, RecursiveIteratorIterator::SELF_FIRST);
            $iterator->setMaxDepth(4);
            foreach ($iterator as $k => $item) {
                if (!$item->isDir()) {
                    continue;
                }
                $path = str_replace($base . DIRECTORY_SEPARATOR, '', $item->getPathname());
                if (!@file_exists($psOverride . $path)) {
                    $this->safeMkDir($psOverride . $path);
                    @touch($psOverride . $path . DIRECTORY_SEPARATOR . '_do_not_remove');
                }
            }
            if (!file_exists($psOverride . 'index.php')) {
                Tools::copy($this->getLocalPath() . 'index.php', $psOverride . 'index.php');
            }
        }
    }
    public function replaceOverridesBeforeInstall()
    {
        if(version_compare(_PS_VERSION_,'1.7.7.0','<'))
        {
            $file_cart_content = Tools::file_get_contents(dirname(__FILE__).'/override/classes/Cart.php');
            $search = array(
                'public function getPackageShippingCost($id_carrier = null, $use_tax = true, Country $default_country = null, $product_list = null, $id_zone = null, bool $keepOrderPrices = false, $default = false)'
            );
            $replace = array(
                'public function getPackageShippingCost($id_carrier = null, $use_tax = true, Country $default_country = null, $product_list = null, $id_zone = null, $keepOrderPrices = false, $default = false)'
            );
            $file_cart_content = str_replace($search,$replace,$file_cart_content);
            file_put_contents(dirname(__FILE__).'/override/classes/Cart.php',$file_cart_content);
        }
    }
    public function replaceOverridesAfterInstall()
    {
        if(version_compare(_PS_VERSION_,'1.7.7.0','<'))
        {
            $file_cart_content = Tools::file_get_contents(dirname(__FILE__).'/override/classes/Cart.php');
            $search = array(
                'public function getPackageShippingCost($id_carrier = null, $use_tax = true, Country $default_country = null, $product_list = null, $id_zone = null, $keepOrderPrices = false, $default = false)'
            );
            $replace= array(
                'public function getPackageShippingCost($id_carrier = null, $use_tax = true, Country $default_country = null, $product_list = null, $id_zone = null, bool $keepOrderPrices = false, $default = false)'
            );
            $file_cart_content = str_replace($search,$replace,$file_cart_content);
            file_put_contents(dirname(__FILE__).'/override/classes/Cart.php',$file_cart_content);
        }
    }
    public function replaceOverridesOtherModuleAfterInstall()
    {
        if(Module::isInstalled('ets_marketplace') && ($ets_marketplace = Module::getInstanceByName('ets_marketplace')) && method_exists($ets_marketplace,'replaceOverridesAfterInstall'))
        {
            $ets_marketplace->replaceOverridesAfterInstall();
        }
        if(Module::isInstalled('ets_onepagecheckout') && ($ets_onepagecheckout = Module::getInstanceByName('ets_onepagecheckout')) && method_exists($ets_onepagecheckout,'replaceOverridesAfterInstall'))
        {
            $ets_onepagecheckout->replaceOverridesAfterInstall();
        }
    }
    public function replaceOverridesOtherModuleBeforeInstall()
    {
        if(Module::isInstalled('ets_marketplace') && ($ets_marketplace = Module::getInstanceByName('ets_marketplace')) && method_exists($ets_marketplace,'replaceOverridesBeforeInstall'))
        {
            $ets_marketplace->replaceOverridesBeforeInstall();
        }
        if(Module::isInstalled('ets_onepagecheckout') && ($ets_onepagecheckout = Module::getInstanceByName('ets_onepagecheckout')) && method_exists($ets_onepagecheckout,'replaceOverridesBeforeInstall'))
        {
            $ets_onepagecheckout->replaceOverridesBeforeInstall();
        }
    }
    public function uninstallOverrides(){
        $this->replaceOverridesBeforeInstall();
        $this->replaceOverridesOtherModuleBeforeInstall();
        if(parent::uninstallOverrides())
        {
            require_once(dirname(__FILE__) . '/classes/OverrideUtil');
            $class= 'Ets_sc_overrideUtil';
            $method = 'restoreReplacedMethod';
            call_user_func_array(array($class, $method),array($this));
            $this->replaceOverridesAfterInstall();
            $this->replaceOverridesOtherModuleAfterInstall();
            return true;
        }
        $this->replaceOverridesAfterInstall();
        $this->replaceOverridesOtherModuleAfterInstall();
        return false;
    }
    public function installOverrides()
    {
        $this->replaceOverridesBeforeInstall();
        $this->replaceOverridesOtherModuleBeforeInstall();
        require_once(dirname(__FILE__) . '/classes/OverrideUtil');
        $class= 'Ets_sc_overrideUtil';
        $method = 'resolveConflict';
        call_user_func_array(array($class, $method),array($this));
        if(parent::installOverrides())
        {
            call_user_func_array(array($class, 'onModuleEnabled'),array($this));
            $this->replaceOverridesAfterInstall();
            $this->replaceOverridesOtherModuleAfterInstall();
            return true;
        }
        $this->replaceOverridesAfterInstall();
        $this->replaceOverridesOtherModuleAfterInstall();
        return false;
    }
    public function enable($force_all = false)
    {
        if(!$force_all && Ets_sc_defines::checkEnableOtherShop($this->id) && $this->getOverrides() != null)
        {
            try {
                $this->uninstallOverrides();
            }
            catch (Exception $e)
            {
                if($e)
                {

                }
            }
        }
        $this->checkOverrideDir();
        return parent::enable($force_all);
    }
    public function disable($force_all = false)
    {
        if(parent::disable($force_all))
        {
            if(!$force_all && Ets_sc_defines::checkEnableOtherShop($this->id))
            {
                if(property_exists('Tab','enabled') && method_exists($this, 'get') && $dispatcher = $this->get('event_dispatcher')){
                    /** @var \Symfony\Component\EventDispatcher\Debug\TraceableEventDispatcher|\Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher */
                    $dispatcher->addListener(\PrestaShopBundle\Event\ModuleManagementEvent::DISABLE, function (\PrestaShopBundle\Event\ModuleManagementEvent $event) {
                        Ets_sc_defines::activeTab($this->name);
                    });
                }
                if($this->getOverrides() != null)
                {
                    try {
                        $this->installOverrides();
                    }
                    catch (Exception $e)
                    {
                        if($e)
                        {

                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
	public function _getCacheId($params = null,$parentID = true)
	{
		$cacheId = $this->getCacheId($this->name);
		$cacheId = str_replace($this->name, '', $cacheId);
		$suffix ='';
		if($params)
		{
			if(is_array($params))
				$suffix .= '|'.implode('|',$params);
			else
				$suffix .= '|'.$params;
		}
        if($parentID && defined('_PS_ADMIN_DIR_') && isset($this->context->employee) && isset($this->context->employee->id))
        {
            $cacheId .='|'.$this->context->employee->id;
        }
		return $this->name . $suffix .($parentID ? $cacheId:'');
	}


	public function _clearSmartyCache($template,$cache_id = null, $compile_id = null)
	{
		if($cache_id===null)
			$cache_id = $this->name;
		if($template=='*')
		{
			Tools::clearCache(Context::getContext()->smarty,null, $cache_id, $compile_id);
		}
		else
		{
			Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath($template), $cache_id, $compile_id);
		}
	}

	public function display($file, $template, $cache_id = null, $compile_id = null)
	{
		if (($overloaded = Module::_isTemplateOverloadedStatic(basename($file, '.php'), $template)) === null) {
			return $this->l('No template found for module').' '.basename($file, '.php').(_PS_MODE_DEV_ ? ' (' . $template . ')' : '');
		} else {
			$this->smarty->assign([
				'module_dir' => __PS_BASE_URI__ . 'modules/' . basename($file, '.php') . '/',
				'module_template_dir' => ($overloaded ? _THEME_DIR_ : __PS_BASE_URI__) . 'modules/' . basename($file, '.php') . '/',
				'allow_push' => isset($this->allow_push) ? $this->allow_push : false ,
			]);
			if ($cache_id !== null) {
				Tools::enableCache();
			}
			if ($compile_id === null && method_exists($this,'getDefaultCompileId')) {
				$compile_id = $this->getDefaultCompileId();
			}
			$result = $this->getCurrentSubTemplate($template, $cache_id, $compile_id);
			if ($cache_id !== null) {
				Tools::restoreCacheSettings();
			}
			$result = $result->fetch();
			$this->resetCurrentSubTemplate($template, $cache_id, $compile_id);
			return $result;
		}
	}


	public function splitProductID($id_product) {
		return array_map('intval', str_split($id_product));
	}

	public function _clearSmartyCacheWhenUpdateConfig() {
		$this->_clearSmartyCache('*', $this->_getCacheId(['promote'], false));
	}
	public function _clearSmartyCacheWhenUpdatePromoteRule($id_ets_sc_promote_rule = null) {
    	if ($id_ets_sc_promote_rule) {
		    $this->_clearSmartyCache('*', $this->_getCacheId(['promote', $id_ets_sc_promote_rule], false));
	    } else {
		    $this->_clearSmartyCache('*', $this->_getCacheId(['promote'], false));
	    }

	}

	public function _clearSmartyCacheWhenUpdateShippingRule($id_ets_sc_shipping_rule = null) {
		if ($id_ets_sc_shipping_rule) {
			$this->_clearSmartyCache('*', $this->_getCacheId(['rule_tab', $id_ets_sc_shipping_rule], false));
		} else {
			$this->_clearSmartyCache('*', $this->_getCacheId(['rule_tab'], false));
		}
		$this->_clearSmartyCache('*', $this->_getCacheId(['condition'], false));
	}
	public function _clearSmartyCacheWhenUpdateConditionRule($id_ets_sc_condition_rule = null) {
		if ($id_ets_sc_condition_rule) {
			$this->_clearSmartyCache('*', $this->_getCacheId(['condition', $id_ets_sc_condition_rule], false));
		} else {
			$this->_clearSmartyCache('*', $this->_getCacheId(['condition'], false));
		}
		$this->_clearSmartyCache('*', $this->_getCacheId(['rule_tab'], false));
	}
    public static function registerPlugins(){
        if(version_compare(_PS_VERSION_, '8.0.4', '>='))
        {
            $smarty = Context::getContext()->smarty->_getSmartyObj();
            if(!isset($smarty->registered_plugins[ 'modifier' ][ 'implode' ]))
                Context::getContext()->smarty->registerPlugin('modifier', 'implode', 'implode');
            if(!isset($smarty->registered_plugins[ 'modifier' ][ 'strpos' ]))
                Context::getContext()->smarty->registerPlugin('modifier', 'strpos', 'strpos');
        }
    }
}