{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{extends file="helpers/form/form.tpl"}
{block name="label"}
	{if isset($input.label)}
		<label class="control-label col-lg-3 {if (isset($input.required) && $input.required && $input.type != 'radio') || (isset($input.showRequired) && $input.showRequired)} required{/if}">
			{if isset($input.hint)}
			<span class="label-tooltip" data-toggle="tooltip" data-html="true" title="{if is_array($input.hint)}
						{foreach $input.hint as $hint}
							{if is_array($hint)}
								{$hint.text|escape:'html':'UTF-8'}
							{else}
								{$hint|escape:'html':'UTF-8'}
							{/if}
						{/foreach}
					{else}
						{$input.hint|escape:'html':'UTF-8'}
					{/if}">
			{/if}
			{$input.label|escape:'html':'UTF-8'}
			{if isset($input.hint)}
			</span>
			{/if}
		</label>
	{/if}
{/block}
{block name="legend"}
    {$smarty.block.parent}
    {if isset($configTabs) && $configTabs}
        <ul class="ets_rule_tabs_configs">
            {foreach from=$configTabs key='key' item='tab'}
                <li class="rule_tab rule_tab_{$tab.tab|escape:'html':'UTF-8'}{if $key==0} active{/if}" data-tab-id="{$tab.tab|escape:'html':'UTF-8'}">{$tab.name|escape:'html':'UTF-8'}</li>
            {/foreach}
        </ul>
    {/if}
{/block}
{block name="input_row"}
    {if $input.name=='applicable_product_categories' && $table!='ets_sc_promote_rule'}
        <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
                <input type="hidden" name="display_advanced_setting" value="{$display_advanced_setting|intval}" />
                <a  href="#" class="btn-advanced-settings"{if $display_advanced_setting} style="display:none{/if}">
                <i class="ets_icon lh_16">
                    <svg width="14" height="14" viewBox="0 0 2048 1792" xmlns="http://www.w3.org/2000/svg"><path d="M960 896q0-106-75-181t-181-75-181 75-75 181 75 181 181 75 181-75 75-181zm768 512q0-52-38-90t-90-38-90 38-38 90q0 53 37.5 90.5t90.5 37.5 90.5-37.5 37.5-90.5zm0-1024q0-52-38-90t-90-38-90 38-38 90q0 53 37.5 90.5t90.5 37.5 90.5-37.5 37.5-90.5zm-384 421v185q0 10-7 19.5t-16 10.5l-155 24q-11 35-32 76 34 48 90 115 7 11 7 20 0 12-7 19-23 30-82.5 89.5t-78.5 59.5q-11 0-21-7l-115-90q-37 19-77 31-11 108-23 155-7 24-30 24h-186q-11 0-20-7.5t-10-17.5l-23-153q-34-10-75-31l-118 89q-7 7-20 7-11 0-21-8-144-133-144-160 0-9 7-19 10-14 41-53t47-61q-23-44-35-82l-152-24q-10-1-17-9.5t-7-19.5v-185q0-10 7-19.5t16-10.5l155-24q11-35 32-76-34-48-90-115-7-11-7-20 0-12 7-20 22-30 82-89t79-59q11 0 21 7l115 90q34-18 77-32 11-108 23-154 7-24 30-24h186q11 0 20 7.5t10 17.5l23 153q34 10 75 31l118-89q8-7 20-7 11 0 21 8 144 133 144 160 0 8-7 19-12 16-42 54t-45 60q23 48 34 82l152 23q10 2 17 10.5t7 19.5zm640 533v140q0 16-149 31-12 27-30 52 51 113 51 138 0 4-4 7-122 71-124 71-8 0-46-47t-52-68q-20 2-30 2t-30-2q-14 21-52 68t-46 47q-2 0-124-71-4-3-4-7 0-25 51-138-18-25-30-52-149-15-149-31v-140q0-16 149-31 13-29 30-52-51-113-51-138 0-4 4-7 4-2 35-20t59-34 30-16q8 0 46 46.5t52 67.5q20-2 30-2t30 2q51-71 92-112l6-2q4 0 124 70 4 3 4 7 0 25-51 138 17 23 30 52 149 15 149 31zm0-1024v140q0 16-149 31-12 27-30 52 51 113 51 138 0 4-4 7-122 71-124 71-8 0-46-47t-52-68q-20 2-30 2t-30-2q-14 21-52 68t-46 47q-2 0-124-71-4-3-4-7 0-25 51-138-18-25-30-52-149-15-149-31v-140q0-16 149-31 13-29 30-52-51-113-51-138 0-4 4-7 4-2 35-20t59-34 30-16q8 0 46 46.5t52 67.5q20-2 30-2t30 2q51-71 92-112l6-2q4 0 124 70 4 3 4 7 0 25-51 138 17 23 30 52 149 15 149 31z"/></svg>
                </i> {l s='Advanced settings' mod='ets_shippingcost'}</a>
            </div>
        </div>
        <div class="wapper-advanced-settings{if $display_advanced_setting} active{/if}">
        <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
            <h2 class="title_advanced_settings">{l s='Advanced settings' mod='ets_shippingcost'}</h2>
        </div>

    {/if} 
    {if isset($input.tab) && $input.tab}
        <div class="ets_rule_tab_content rule_tab_content_{$input.tab|escape:'html':'UTF-8'} ets_sc_row_{$input.name|escape:'html':'UTF-8'}">
    {else}
        <div class="ets_sc_row_{$input.name|escape:'html':'UTF-8'}">
    {/if}
    {if $input.name=='customer_signed_up_from' || $input.name=='customer_signed_up_to'}
        {if $input.name=='customer_signed_up_from'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Registration date' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-group">
                            <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                                <input id="customer_signed_up_from" class="datepicker input-medium" name="customer_signed_up_from" value="{$fields_value['customer_signed_up_from']|escape:'html':'UTF-8'}" type="text" />
                                <span class="input-group-addon">
                                    <i class="icon-calendar-empty"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                            <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                                <input id="customer_signed_up_to" class="datepicker input-medium" name="customer_signed_up_to" value="{$fields_value['customer_signed_up_to']|escape:'html':'UTF-8'}" type="text" />
                                <span class="input-group-addon">
                                    <i class="icon-calendar-empty"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='days_since_signed_up_cal' || $input.name=='days_since_singed_up_day'}
        {if $input.name=='days_since_signed_up_cal'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Days since registration' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6">
                            <select id="days_since_signed_up_cal" name="days_since_signed_up_cal">
                                <option value=">="{if $fields_value['days_since_signed_up_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="=" {if $fields_value['days_since_signed_up_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<=" {if $fields_value['days_since_signed_up_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="days_since_singed_up_day" class="input-medium" name="days_since_singed_up_day" value="{$fields_value['days_since_singed_up_day']|escape:'html':'UTF-8'}" type="text" />
                                <span class="input-group-addon">
                                    {l s='Days' mod='ets_shippingcost'}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='cart_amount_cal' || $input.name=='cart_amount'}
        {if $input.name=='cart_amount'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3 required"> {l s='Cart amount' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="cart_amount_cal" name="cart_amount_cal">
                                <option value=">="{if $fields_value['cart_amount_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="=" {if $fields_value['cart_amount_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<=" {if $fields_value['cart_amount_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="cart_amount" class="input-medium" name="cart_amount" value="{if $fields_value['cart_amount'] >0}{$fields_value['cart_amount']|escape:'html':'UTF-8'}{/if}" type="text" />
                                <span class="input-group-addon">
                                    {$input.suffix|escape:'html':'UTF-8'}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='total_weight_cal' || $input.name=='total_weight'}
        {if $input.name=='total_weight'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {$input.label|escape:'html':'UTF-8'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="total_weight_cal" name="total_weight_cal">
                                <option value=">="{if $fields_value['total_weight_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['total_weight_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['total_weight_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="total_weight" class="input-medium" name="total_weight" value="{$fields_value['total_weight']|escape:'html':'UTF-8'}" type="text" />
                                <span class="input-group-addon">
                                    {$input.suffix|escape:'html':'UTF-8'}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='total_product_quantity_cal' || $input.name=='total_product_quantity'}
        {if $input.name=='total_product_quantity'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Total product quantity' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="total_product_quantity_cal" name="total_product_quantity_cal">
                                <option value=">="{if $fields_value['total_product_quantity_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['total_product_quantity_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['total_product_quantity_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="total_product_quantity" class="input-medium" name="total_product_quantity" value="{$fields_value['total_product_quantity']|escape:'html':'UTF-8'}" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='quantity_of_same_product_cal' || $input.name=='quantity_of_same_product'}
        {if $input.name=='quantity_of_same_product'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Purchased quantity of the same product' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="quantity_of_same_product_cal" name="quantity_of_same_product_cal">
                                <option value=">="{if $fields_value['quantity_of_same_product_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['quantity_of_same_product_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['quantity_of_same_product_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="quantity_of_same_product" class="input-medium" name="quantity_of_same_product" value="{$fields_value['quantity_of_same_product']|escape:'html':'UTF-8'}" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='number_of_different_product_cal' || $input.name=='number_of_different_product'}
        {if $input.name=='number_of_different_product'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Number of different products' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="number_of_different_product_cal" name="number_of_different_product_cal">
                                <option value=">="{if $fields_value['number_of_different_product_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['number_of_different_product_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['number_of_different_product_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="number_of_different_product" class="input-medium" name="number_of_different_product" value="{$fields_value['number_of_different_product']|escape:'html':'UTF-8'}" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='number_of_product_in_same_category_cal' || $input.name=='number_of_product_in_same_category'}
        {if $input.name=='number_of_product_in_same_category'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Number of products in the same category' mod='ets_shippingcost'}<br />({l s='Only apply for the default product category' mod='ets_shippingcost'}) </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="number_of_product_in_same_category_cal" name="number_of_product_in_same_category_cal">
                                <option value=">="{if $fields_value['number_of_product_in_same_category_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['number_of_product_in_same_category_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['number_of_product_in_same_category_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="number_of_product_in_same_category" class="input-medium" name="number_of_product_in_same_category" value="{$fields_value['number_of_product_in_same_category']|escape:'html':'UTF-8'}" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='apply_for_product_price_cal' || $input.name=='apply_for_product_price'}
        {if $input.name=='apply_for_product_price'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Apply for product price' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="apply_for_product_price_cal" name="apply_for_product_price_cal">
                                <option value=">="{if $fields_value['apply_for_product_price_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['apply_for_product_price_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['apply_for_product_price_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="apply_for_product_price" class="input-medium" name="apply_for_product_price" value="{$fields_value['apply_for_product_price']|escape:'html':'UTF-8'}" type="text" />
                                <span class="input-group-addon">
                                    {$input.suffix|escape:'html':'UTF-8'}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='apply_for_availabled_quantity_stock_cal' || $input.name=='apply_for_availabled_quantity_stock'}
        {if $input.name=='apply_for_availabled_quantity_stock'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3"> {l s='Apply for available stock quantity' mod='ets_shippingcost'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-6 w_100">
                            <select id="apply_for_availabled_quantity_stock_cal" name="apply_for_availabled_quantity_stock_cal">
                                <option value=">="{if $fields_value['apply_for_availabled_quantity_stock_cal']=='>='} selected="slected"{/if} > >= </option>
                                <option value="="{if $fields_value['apply_for_availabled_quantity_stock_cal']=='='} selected="slected"{/if}> = </option>
                                <option value="<="{if $fields_value['apply_for_availabled_quantity_stock_cal']=='<='} selected="slected"{/if}> <= </option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group">
                                <input id="apply_for_availabled_quantity_stock" class="input-medium" name="apply_for_availabled_quantity_stock" value="{$fields_value['apply_for_availabled_quantity_stock']|escape:'html':'UTF-8'}" type="text" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.name=='formular' || $input.name=='id_currency'}
        {if $input.name=='id_currency'}
            <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
                <label class="control-label col-lg-3 required"> {$input.label|escape:'html':'UTF-8'} </label>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="row">
                            <div class="col-lg-5">
                                <input class="form-control" id="formular" name="formular" value="{$fields_value['formular']|escape:'html':'UTF-8'}" />
                            </div>
                            <div class="col-lg-5">
                                <select name="formular_defined">
                                    <option value="">{l s='Self defined formula' mod='ets_shippingcost'}</option>
                                    <option value="5+3*(totalquantity-1)">{l s='$5 for the first product and $3 for each next products' mod='ets_shippingcost'}</option>
                                    <option value="(10/100)*defaultshipping + (5/100)*totalproductamounttaxincl">{l s='10% of default shipping cost and 5% of total order amount' mod='ets_shippingcost'}</option>
                                    <option value="(5/100)*defaultshipping + 2*totalweight">{l s='5% of default shipping cost and $2 for each product weight (1kg)' mod='ets_shippingcost'}</option>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select name="{$input.name|escape:'html':'UTF-8'}">
                                    {foreach from=$input.options.query item='option'}
                                        <option value="{$option.id|escape:'html':'UTF-8'}"{if $option.id = $fields_value[$input.name]} selected="selected"{/if}>{$option.sign|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <p class="help-block">
                            {l s='Enter the formula to calculate shipping cost for this rule.' mod='ets_shippingcost'}<br />
                            {$input.desc|replace:'[highlight]':'<code>'|replace:'[end_highlight]':'</code>' nofilter}
                        </p>
                    </div>
                </div>
            </div>
        {/if}
    {elseif $input.type=='html'}
        {$input.html_content nofilter}
    {else}
        {$smarty.block.parent}
    {/if}
    </div>
    {if $input.name=='apply_for_availabled_quantity_stock'}
        <div class="form-group {if isset($input.form_group_class)}{$input.form_group_class|escape:'html':'UTF-8'}{/if}">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
                <a  href="#" class="btn-hide-advanced-settings" {if $display_advanced_setting} style="display:block"{/if}>
                    <i class="ets_icon lh_16">
                        <svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1277 493q-9 19-29 19h-224v1248q0 14-9 23t-23 9h-192q-14 0-23-9t-9-23v-1248h-224q-21 0-29-19t5-35l350-384q10-10 23-10 14 0 24 10l355 384q13 16 5 35z"/></svg>
                    </i> {l s='Hide advanced settings' mod='ets_shippingcost'}
                </a>
            </div>
        </div>
        </div>
    {/if} 
{/block}
{block name="input"}
    {if $input.type == 'checkbox'}
            {if isset($input.values.query) && $input.values.query}
                {assign var=id_checkbox value=$input.name|cat:'_'|cat:'all'}
                {assign var=checkall value=true}
				{if !(isset($fields_value[$input.name]) && is_array($fields_value[$input.name]) && $fields_value[$input.name] && in_array('all',$fields_value[$input.name]))} 
                    {assign var=checkall value=false}
                {/if}
                <div class="checkbox_all checkbox">
					{strip}
						<label for="{$id_checkbox|escape:'html':'UTF-8'}">                                
							<input type="checkbox" name="{$input.name|escape:'html':'UTF-8'}[]" id="{$id_checkbox|escape:'html':'UTF-8'}" value="all" {if $checkall} checked="checked"{/if} />
							{l s='Select/Unselect all' mod='ets_shippingcost'}
						</label>
					{/strip}
				</div>
                {foreach $input.values.query as $value}
    				{assign var=id_checkbox value=$input.name|cat:'_'|cat:$value[$input.values.id]|escape:'html':'UTF-8'}
    				<div class="checkbox{if isset($input.expand) && strtolower($input.expand.default) == 'show'} hidden{/if}">
    					{strip}
    						<label for="{$id_checkbox|escape:'html':'UTF-8'}">                                
    							<input type="checkbox" name="{$input.name|escape:'html':'UTF-8'}[]" id="{$id_checkbox|escape:'html':'UTF-8'}" {if isset($value[$input.values.id])} value="{$value[$input.values.id]|escape:'html':'UTF-8'}"{/if}{if isset($fields_value[$input.name]) && is_array($fields_value[$input.name]) && $fields_value[$input.name] && (in_array($value[$input.values.id],$fields_value[$input.name]) || in_array('all',$fields_value[$input.name])) } checked="checked"{/if} {if isset($value.class) && $value.class} class="{$value.class|escape:'html':'UTF-8'}"{/if}/>
    							{$value[$input.values.name]|replace:'[highlight]':'<strong>'|replace:'[end_highlight]':'</strong>' nofilter}
    						</label>
    					{/strip}
    				</div>
    			{/foreach} 
            {/if}
    {elseif $input.type == 'file_lang'}
		{if $languages|count > 1}
		  <div class="form-group">
		{/if}
			{foreach from=$languages item=language}
				{if $languages|count > 1}
					<div class="translatable-field lang-{$language.id_lang|intval}" {if $language.id_lang != $defaultFormLanguage}style="display:none"{/if}>
				{/if}
					<div class="col-lg-9">
						<div class="dummyfile input-group sass">
							<input id="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}" type="file" name="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}" class="hide-file-upload" />
							<span class="input-group-addon"><i class="icon-file"></i></span>
							<input id="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}-name" type="text" class="disabled" name="filename" readonly />
							<span class="input-group-btn">
								<button id="{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}-selectbutton" type="button" name="submitAddAttachments" class="btn btn-default">
									<i class="icon-folder-open"></i> {l s='Choose a file' mod='ets_shippingcost'}
								</button>
							</span>
						</div>
                        {if isset($fields_value[$input.name]) && $fields_value[$input.name] && $fields_value[$input.name][$language.id_lang]}
                            <label class="control-label col-lg-3 uploaded_image_label" style="font-style: italic;">{l s='Uploaded image: ' mod='ets_shippingcost'}</label>
                            <div class="col-lg-9 uploaded_img_wrapper">
                        		<img title="" style="display: inline-block; max-width: 200px;" src="{$image_baseurl|escape:'html':'UTF-8'}{$fields_value[$input.name][$language.id_lang]|escape:'html':'UTF-8'}" /></a>
                            </div>
						{/if}
					</div>
				{if $languages|count > 1}
					<div class="col-lg-2">
						<button type="button" class="btn btn-default dropdown-toggle" tabindex="-1" data-toggle="dropdown">
							{$language.iso_code|escape:'html':'UTF-8'}
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							{foreach from=$languages item=lang}
							<li><a href="javascript:hideOtherLanguage({$lang.id_lang|intval});" tabindex="-1">{$lang.name|escape:'html':'UTF-8'}</a></li>
							{/foreach}
						</ul>
					</div>
				{/if}
				{if $languages|count > 1}
					</div>
				{/if}
				<script>
				$(document).ready(function(){
					$("#{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}-selectbutton,#{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}-name").click(function(e){
						$("#{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}").trigger('click');
					});
					$("#{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}").change(function(e){
						var val = $(this).val();
						var file = val.split(/[\\/]/);
						$("#{$input.name|escape:'html':'UTF-8'}_{$language.id_lang|intval}-name").val(file[file.length-1]);
					});
				});
			</script>
			{/foreach}
		{if $languages|count > 1}
		  </div>
		{/if}
    {elseif $input.type=='range'}
        <div class="range_custom">
            <input name="{$input.name|escape:'html':'UTF-8'}" min="{$input.min|intval}" max="{$input.max|intval}" value="{if $fields_value[$input.name]}{$fields_value[$input.name]|escape:'html':'UTF-8'}{else}1{/if}"  forever="1" type="range" data-suffix="{$input.data_suffix|escape:'html':'UTF-8'}" data-suffixs="{$input.data_suffixs|escape:'html':'UTF-8'}" />
            <div class="range_new">
                <span class="range_new_bar"></span>
                    <span class="range_new_run" style="">
                    <span class="range_new_button"></span>
                </span>
            </div>
            <span class="input-group-unit">{if $fields_value[$input.name] >1}{$fields_value[$input.name]|escape:'html':'UTF-8'}{else}1{/if}</span>
            <span class="range_suffixs">{$input.data_suffixs|escape:'html':'UTF-8'}</span>
            <span class="range_min">{$input.min|escape:'html':'UTF-8'}</span>
            <span class="range_max">{$input.max|escape:'html':'UTF-8'}</span>
        </div>
    {elseif $input.type == 'search_customer'}
        <div class="sc_search_customer_form">
            <div class="input-group ">
                <input class="sc_search_customer" name="sc_search_customer" {if isset($input.placeholder)}placeholder="{$input.placeholder|escape:'html':'UTF-8'}"{/if} autocomplete="off" type="text" />
                <span class="input-group-addon"> <i class="icon-search"></i> </span>
            </div>
            <input class="sc_ids_customer" name="{$input.name|escape:'html':'UTF-8'}" value="{$fields_value[$input.name]|escape:'html':'UTF-8'}" type="hidden" />
            <ul class="sc_customers" id="block_search_{$input.name|escape:'html':'UTF-8'}">
                {Module::getInstanceByName('ets_shippingcost')->displayListCustomers($fields_value[$input.name]) nofilter}
                <li class="sc_customer_loading"></li>
            </ul>
        </div>
    {elseif $input.type == 'search_product'}
        <div class="sc_search_product_form">
            <div class="input-group ">
                <input class="sc_search_product" name="sc_search_product" {if isset($input.placeholder)}placeholder="{$input.placeholder|escape:'html':'UTF-8'}"{/if} autocomplete="off" type="text" data-name="{$input.name|escape:'html':'UTF-8'}" />
                <span class="input-group-addon"> <i class="icon-search"></i> </span>
            </div>
            <input class="sc_ids_product" name="{$input.name|escape:'html':'UTF-8'}" value="{$fields_value[$input.name]|escape:'html':'UTF-8'}" type="hidden" />
            <ul class="sc_products" id="block_search_{$input.name|escape:'html':'UTF-8'}">
                {Module::getInstanceByName('ets_shippingcost')->displayListProducts($fields_value[$input.name]) nofilter}
                <li class="sc_product_loading"></li>
            </ul>
        </div>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}
{block name='description'}
    {if $input.type == 'file' && isset($input.is_image) && $input.is_image}
        {$smarty.block.parent}
        <p class="help-block">{l s='Available image type: jpg, png, gif, jpeg'  mod='ets_shippingcost'}. {l s='Limit'  mod='ets_shippingcost'} {Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE')|escape:'html':'UTF-8'}Mb</p>

    {elseif isset($input.desc) && !is_array($input.desc)}
        <p class="help-block">{$input.desc|replace:'[highlight]':'<code>'|replace:'[end_highlight]':'</code>' nofilter}</p>
    {else}
        {$smarty.block.parent}
    {/if}
{/block}