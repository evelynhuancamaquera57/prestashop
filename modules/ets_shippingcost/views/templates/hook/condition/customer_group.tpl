{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $customer_groups=='all'}
    <p>{l s='All group' mod='ets_shippingcost'}</p>
{else}
    <p>
        {l s='Customer groups:' mod='ets_shippingcost'}
        <ul class="pl_10">
            {foreach from =$customer_groups item='customer_group'}
                <li>- {$customer_group.name|escape:'html':'UTF-8'}</li>
            {/foreach}
        </ul>
    </p>
    <p>
        {l s='Only apply on default group' mod='ets_shippingcost'}: {if $only_apply_on_default_group}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
    </p>
{/if}