{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{assign var='customer_membership' value=false}
{if ($customer_signed_up_from && $customer_signed_up_from!='0000-00-00 00:00:00') || ($customer_signed_up_to && $customer_signed_up_to!='0000-00-00 00:00:00')}
    <p>{l s='Registration date' mod='ets_shippingcost'}: {if $customer_signed_up_from && $customer_signed_up_from!='0000-00-00 00:00:00'}{l s='from' mod='ets_shippingcost'} {$customer_signed_up_from|escape:'html':'UTF-8'}{/if} {if $customer_signed_up_to && $customer_signed_up_to!='0000-00-00 00:00:00'}{l s='to' mod='ets_shippingcost'} {$customer_signed_up_to|escape:'html':'UTF-8'}{/if}</p>
    {assign var='customer_membership' value=true}
{/if}
{if $days_since_singed_up_day}
    <p>{l s='Days since registration' mod='ets_shippingcost'}: {$days_since_signed_up_cal|escape:'html':'UTF-8'}{$days_since_singed_up_day|escape:'html':'UTF-8'}</p>
    {assign var='customer_membership' value=true}
{/if}
{if !$customer_membership}--{/if}