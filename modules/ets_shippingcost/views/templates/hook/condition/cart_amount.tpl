{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<p>{l s='Cart amount' mod='ets_shippingcost'}: {$cart_amount_cal|escape:'html':'UTF-8'}{$cart_amount|escape:'html':'UTF-8'}</p>
<p>
    {l s='Tax included' mod='ets_shippingcost'}: {if $cart_amount_tax_incl}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
</p>
<p>
    {l s='Shipping included' mod='ets_shippingcost'}: {if $cart_amount_shipping_incl}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
</p>
<p>
    {l s='Discount included' mod='ets_shippingcost'}: {if $cart_amount_discount_incl}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
</p>