{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<div class="form-group parent_codition delivery_zipcode">
    <div class="alert alert-info">
    {l s='When checkout, if customer enters the delivery address with ZIP code/postal code that satisfied the condition, the shipping rule will be applied to customer shopping cart.' mod='ets_shippingcost'}
<br />- {l s='From - to: ZIP code/postal code in customer delivery address is in the code range you enter below.' mod='ets_shippingcost'}
<br />- {l s='Start from/end at: ZIP code/postal code in customer delivery address start from/end at the code you enter below.' mod='ets_shippingcost'}
<br />- {l s='Is exactly: ZIP code/postal code in customer delivery address must be the same with the code you enter below.' mod='ets_shippingcost'}
<br />- {l s='Different: ZIP code/postal code in customer delivery address must be different with the code you enter below.' mod='ets_shippingcost'}
</div>

</div>
{if $delivery_zipcode_type}
    {foreach from=$delivery_zipcode_type key='index' item='zipcode_type'}
        <div class="form-group parent_codition delivery_zipcode">
            <label class="control-label col-lg-3 required">{l s='ZIP code/Postal code' mod='ets_shippingcost'} </label>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-3"> 
                        <select class="select_delivery_zipcode_type" name="delivery_zipcode_type[]">
                            <option value="from_to"{if $zipcode_type=='from_to'} selected="selected"{/if}>{l s='From - To' mod='ets_shippingcost'}</option>
                            <option value="start_from"{if $zipcode_type=='start_from'} selected="selected"{/if}>{l s='Start from' mod='ets_shippingcost'}</option>
                            <option value="end_at"{if $zipcode_type=='end_at'} selected="selected"{/if}>{l s='End at' mod='ets_shippingcost'}</option>
                            <option value="is_exactly"{if $zipcode_type=='is_exactly'} selected="selected"{/if}>{l s='Is exactly' mod='ets_shippingcost'}</option>
                            <option value="different"{if $zipcode_type=='different'} selected="selected"{/if}>{l s='Different' mod='ets_shippingcost'}</option>
                        </select>
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to"{if $zipcode_type!='from_to'} style="display:none"{/if}>
                                <div class="input-group ">
                                    <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                                    <input name="delivery_zipcode_from[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_from[$index])}{$delivery_zipcode_from[$index]|escape:'html':'UTF-8'}{/if}" />
                                </div>
                            </div>
                            <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to"{if $zipcode_type!='from_to'} style="display:none"{/if}>
                                <div class="input-group">
                                    <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                                    <input name="delivery_zipcode_to[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_to[$index])}{$delivery_zipcode_to[$index]|escape:'html':'UTF-8'}{/if}"  />
                                </div>
                            </div>
                        </div>
                        <div class="delivery_zipcode_type delivery_zipcode_type_start_from"{if $zipcode_type!='start_from'} style="display:none"{/if}>
                            <div class="input-group ">
                                <input name="delivery_zipcode_start_from[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_start_from[$index])}{$delivery_zipcode_start_from[$index]|escape:'html':'UTF-8'}{/if}"  />
                            </div>
                        </div>
                        <div class="delivery_zipcode_type delivery_zipcode_type_end_at"{if $zipcode_type!='end_at'} style="display:none"{/if}>
                            <div class="input-group ">
                                <input name="delivery_zipcode_end_at[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_end_at[$index])}{$delivery_zipcode_end_at[$index]|escape:'html':'UTF-8'}{/if}" />
                            </div>
                        </div>
                        <div class="delivery_zipcode_type delivery_zipcode_type_is_exactly"{if $zipcode_type!='is_exactly'} style="display:none"{/if}>
                            <div class="input-group ">
                                <input name="delivery_zipcode_is_exactly[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_is_exactly[$index])}{$delivery_zipcode_is_exactly[$index]|escape:'html':'UTF-8'}{/if}" />
                            </div>
                        </div>
                        <div class="delivery_zipcode_type delivery_zipcode_type_different"{if $zipcode_type!='different'} style="display:none"{/if}>
                            <div class="input-group">
                                <input name="delivery_zipcode_different[]" autocomplete="off" type="text" class="input-medium" value="{if isset($delivery_zipcode_different[$index])}{$delivery_zipcode_different[$index]|escape:'html':'UTF-8'}{/if}"/>
                            </div>
                        </div>
                    </div>
                    {if $index >0}
                        <div class="col-lg-1">
                            <button class="btn btn-default btn_delete_row_delivery_zipcode" title="{l s='Delete' mod='ets_shippingcost'}"><i class="fa fa-trash icon icon-trash"></i></button>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    {/foreach}
{else}
    <div class="form-group parent_codition delivery_zipcode">
        <label class="control-label col-lg-3 required">{l s='ZIP code/Postal code' mod='ets_shippingcost'} </label>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3"> 
                    <select class="select_delivery_zipcode_type" name="delivery_zipcode_type[]">
                        <option value="from_to">{l s='From - To' mod='ets_shippingcost'}</option>
                        <option value="start_from">{l s='Start from' mod='ets_shippingcost'}</option>
                        <option value="end_at">{l s='End at' mod='ets_shippingcost'}</option>
                        <option value="is_exactly">{l s='Is exactly' mod='ets_shippingcost'}</option>
                        <option value="different">{l s='Different' mod='ets_shippingcost'}</option>
                    </select>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to">
                            <div class="input-group ">
                                <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                                <input name="delivery_zipcode_from[]" autocomplete="off" type="text" class="input-medium" />
                            </div>
                        </div>
                        <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to">
                            <div class="input-group">
                                <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                                <input name="delivery_zipcode_to[]" autocomplete="off" type="text" class="input-medium" />
                            </div>
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_start_from" style="display:none">
                        <div class="input-group ">
                            <input name="delivery_zipcode_start_from[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_end_at" style="display:none">
                        <div class="input-group ">
                            <input name="delivery_zipcode_end_at[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_is_exactly" style="display:none">
                        <div class="input-group ">
                            <input name="delivery_zipcode_is_exactly[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_different" style="display:none">
                        <div class="input-group">
                            <input name="delivery_zipcode_different[]" autocomplete="off" type="text" class="input-medium"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
<div class="form-group parent_codition delivery_zipcode">
    <label class="control-label col-lg-3"></label>
    <div class="col-lg-9">
        <button class="btn btn-default btn_add_new_row_delivery_zipcode">
            <i class="icon-plus-circle"></i>&nbsp;{l s='Add' mod='ets_shippingcost'}
        </button>
    </div>
</div>
<div class="new_row_delivery_zipcode" style="display:none;">
    <div class="form-group parent_codition delivery_zipcode">
        <label class="control-label col-lg-3 required">{l s='ZIP code/Postal code' mod='ets_shippingcost'} </label>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3"> 
                    <select class="select_delivery_zipcode_type" data-name="delivery_zipcode_type[]">
                        <option value="from_to">{l s='From - To' mod='ets_shippingcost'}</option>
                        <option value="start_from">{l s='Start from' mod='ets_shippingcost'}</option>
                        <option value="end_at">{l s='End at' mod='ets_shippingcost'}</option>
                        <option value="is_exactly">{l s='Is exactly' mod='ets_shippingcost'}</option>
                        <option value="different">{l s='Different' mod='ets_shippingcost'}</option>
                    </select>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to">
                            <div class="input-group ">
                                <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                                <input data-name="delivery_zipcode_from[]" autocomplete="off" type="text" class="input-medium" />
                            </div>
                        </div>
                        <div class="col-lg-6 delivery_zipcode_type delivery_zipcode_type_from_to">
                            <div class="input-group">
                                <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                                <input data-name="delivery_zipcode_to[]" autocomplete="off" type="text" class="input-medium" />
                            </div>
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_start_from" style="display:none">
                        <div class="input-group ">
                            <input data-name="delivery_zipcode_start_from[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_end_at" style="display:none">
                        <div class="input-group ">
                            <input data-name="delivery_zipcode_end_at[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_is_exactly" style="display:none">
                        <div class="input-group ">
                            <input data-name="delivery_zipcode_is_exactly[]" autocomplete="off" type="text" class="input-medium" />
                        </div>
                    </div>
                    <div class="delivery_zipcode_type delivery_zipcode_type_different" style="display:none">
                        <div class="input-group">
                            <input data-name="delivery_zipcode_different[]" autocomplete="off" type="text" class="input-medium"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1">
                    <button class="btn btn-default btn_delete_row_delivery_zipcode" title="{l s='Delete' mod='ets_shippingcost'}"><i class="fa fa-trash icon icon-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>