{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $delivery_zipcode_type}
    <ul>
        {foreach from =$delivery_zipcode_type key='index' item='zipcode_type'}
            <li>
                {if $zipcode_type=='from_to'}
                    {if isset($delivery_zipcode_from[$index]) && $delivery_zipcode_from[$index]}{l s='From' mod='ets_shippingcost'}: {$delivery_zipcode_from[$index]|escape:'html':'UTF-8'}{/if}
                    {if isset($delivery_zipcode_to[$index]) && $delivery_zipcode_to[$index]}{l s='To' mod='ets_shippingcost'}: {$delivery_zipcode_to[$index]|escape:'html':'UTF-8'}{/if}
                {elseif $zipcode_type=='start_from'}
                    {if isset($delivery_zipcode_start_from[$index]) && $delivery_zipcode_start_from[$index]}{l s='Start from' mod='ets_shippingcost'}: {$delivery_zipcode_start_from[$index]|escape:'html':'UTF-8'}{/if}
                {elseif $zipcode_type=='end_at'}
                    {if isset($delivery_zipcode_end_at[$index]) && $delivery_zipcode_end_at[$index]}{l s='End at' mod='ets_shippingcost'}: {$delivery_zipcode_end_at[$index]|escape:'html':'UTF-8'}{/if}
                {elseif $zipcode_type=='is_exactly'}
                    {if isset($delivery_zipcode_is_exactly[$index]) && $delivery_zipcode_is_exactly[$index]}{l s='Is exactly' mod='ets_shippingcost'}: {$delivery_zipcode_is_exactly[$index]|escape:'html':'UTF-8'}{/if}
                {elseif $zipcode_type=='different'}
                    {if isset($delivery_zipcode_different[$index]) && $delivery_zipcode_different[$index]}{l s='Different' mod='ets_shippingcost'}: {$delivery_zipcode_different[$index]|escape:'html':'UTF-8'}{/if}
                {/if}
            </li>
        {/foreach}
    </ul>
{/if}