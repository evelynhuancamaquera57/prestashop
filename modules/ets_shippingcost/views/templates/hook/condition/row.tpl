{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<tr class="row-condtion-{$id_ets_sc_condition_rule|intval}">
    <td>{$parent_codition|escape:'html':'UTF-8'}</td>
    <td>{$detail_condition nofilter}</td>
    <td class="text-right">
        <div class="btn-group-action">
            <div class="btn-group pull-right">
                <a class="btn btn-default link_edit_condition" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editconditionrule=1&id_ets_sc_condition_rule={$id_ets_sc_condition_rule|intval}">
                    <i class="icon icon-pencil fa fa-pencil"></i>
                    {l s='Edit' mod='ets_shippingcost'}
                </a>
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-caret-down"></i>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a class="btn btn-default link_delete_condition" data-confirm="{l s='Do you want to delete this item?' mod='ets_shippingcost' js=1}" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&id_ets_sc_condition_rule={$id_ets_sc_condition_rule|intval}&delCondition=yes">
                            <i class="fa fa-trash icon icon-trash"></i>
                            {l s='Delete' mod='ets_shippingcost'}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </td>
</tr>