{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $specific_occasion_day_of_week}
    {assign var ='dem' value= 1}
    {foreach from =$specific_occasion_day_of_week key='index' item='day_week'}
        <div class="form-group parent_codition specific_occasion">
            <label class="control-label col-lg-3">{l s='Day of week' mod='ets_shippingcost'} </label>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-3"> 
                        <select name="specific_occasion_day_of_week[]">
                            {foreach from =$days key='key' item='day'}
                                <option value="{$key|escape:'html':'UTF-8'}" {if $day_week==$key} selected="selected"{/if}>{$day|escape:'html':'UTF-8'}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                            <select name="specific_occasion_day_of_week_from[]">
                                <option value="">--</option>
                                {if $hours}
                                    {foreach from= $hours key='h' item='hour'}
                                        <option value="{$h|escape:'html':'UTF-8'}"{if $specific_occasion_day_of_week_from[$index]!='' && $specific_occasion_day_of_week_from[$index]== $h} selected="selected"{/if}>{$hour|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="input-group">
                            <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                            <select name="specific_occasion_day_of_week_to[]">
                                <option value="">--</option>
                                {if $hours}
                                    {foreach from= $hours key='h' item='hour'}
                                        <option value="{$h|escape:'html':'UTF-8'}"{if $specific_occasion_day_of_week_to[$index]!='' && $specific_occasion_day_of_week_to[$index]== $h} selected="selected"{/if}>{$hour|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    </div>
                    {if $dem > 1}
                        <div class="col-lg-1">
                            <button class="btn btn-default btn_delete_row_specific_occasion" title="{l s='Delete' mod='ets_shippingcost'}"><i class="fa fa-trash icon icon-trash"></i></button>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
        {assign var ='dem' value= $dem+1}
    {/foreach}
{else}
    <div class="form-group parent_codition specific_occasion">
        <label class="control-label col-lg-3">{l s='Day of week' mod='ets_shippingcost'} </label>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3"> 
                    <select name="specific_occasion_day_of_week[]">
                        {foreach from =$days key='key' item='day'}
                            <option value="{$key|escape:'html':'UTF-8'}">{$day|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                        <select name="specific_occasion_day_of_week_from[]">
                            <option value="">--</option>
                            {if $hours}
                                {foreach from= $hours key='h' item='hour'}
                                    <option value="{$h|escape:'html':'UTF-8'}">{$hour|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                        <select name="specific_occasion_day_of_week_to[]">
                            <option value="">--</option>
                            {if $hours}
                                {foreach from= $hours key='h' item='hour'}
                                    <option value="{$h|escape:'html':'UTF-8'}">{$hour|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}
<div class="form-group parent_codition specific_occasion">
    <label class="control-label col-lg-3"></label>
    <div class="col-lg-9">
        <button class="btn btn-default btn_add_new_row_specific_occasion">
            <i class="icon-plus-circle"></i>&nbsp;{l s='Add day' mod='ets_shippingcost'}
        </button>
    </div>
</div>
<div class="new_row_specific_occasion" style="display:none;">
    <div class="form-group parent_codition specific_occasion">
        <label class="control-label col-lg-3">{l s='Day of week' mod='ets_shippingcost'} </label>
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-3"> 
                    <select data-name="specific_occasion_day_of_week[]">
                        {foreach from =$days key='key' item='day'}
                            <option value="{$key|escape:'html':'UTF-8'}">{$day|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">{l s='From' mod='ets_shippingcost'}</span>
                        <select name="specific_occasion_day_of_week_from[]">
                            <option value="">--</option>
                            {if $hours}
                                {foreach from= $hours key='h' item='hour'}
                                    <option value="{$h|escape:'html':'UTF-8'}">{$hour|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="input-group">
                        <span class="input-group-addon">{l s='To' mod='ets_shippingcost'}</span>
                        <select name="specific_occasion_day_of_week_to[]">
                            <option value="">--</option>
                            {if $hours}
                                {foreach from= $hours key='h' item='hour'}
                                    <option value="{$h|escape:'html':'UTF-8'}">{$hour|escape:'html':'UTF-8'}</option>
                                {/foreach}
                            {/if}
                        </select>
                    </div>
                </div>
                <div class="col-lg-1">
                    <button class="btn btn-default btn_delete_row_specific_occasion" title="{l s='Delete' mod='ets_shippingcost'}"><i class="fa fa-trash icon icon-trash"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>