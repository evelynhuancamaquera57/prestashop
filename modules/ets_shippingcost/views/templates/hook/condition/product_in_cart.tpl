{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $total_weight >0}
    <p>{l s='Total weight' mod='ets_shippingcost'}: {$total_weight_cal|escape:'html':'UTF-8'}{$total_weight|floatval} (kg)</p>
{/if}
{if $total_product_quantity > 0}
    <p>{l s='Total product quantity' mod='ets_shippingcost'}: {$total_product_quantity_cal|escape:'html':'UTF-8'}{$total_product_quantity|intval}</p>
{/if}
{if $quantity_of_same_product > 0}
    <p>{l s='Quantity of the same product' mod='ets_shippingcost'}: {$quantity_of_same_product_cal|escape:'html':'UTF-8'}{$quantity_of_same_product|intval}</p>
{/if}
{if $number_of_different_product > 0}
    <p>{l s='Number of different products' mod='ets_shippingcost'}: {$number_of_different_product_cal|escape:'html':'UTF-8'}{$number_of_different_product|intval}</p>
{/if}
{if $number_of_product_in_same_category > 0}
    <p>{l s='Number of product in the same category' mod='ets_shippingcost'}: {$number_of_product_in_same_category_cal|escape:'html':'UTF-8'}{$number_of_product_in_same_category|intval}</p>
{/if}
<p>
    {l s='Apply for discounted products' mod='ets_shippingcost'}: {if $apply_for_discounted_products}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
</p>
<p>
    {l s='Product with different attributes are counted as different products' mod='ets_shippingcost'}: {if $products_with_different_attribute}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
</p>
<p><h2 class="adv_title">{l s='Advanced settings' mod='ets_shippingcost'}</h2></p>
<p>
    {l s='Applicable product categories' mod='ets_shippingcost'} :
    {if $applicable_product_categories =='all_product' || (!$applicable_categories && !$include_specific_products) }
        {l s='All categories' mod='ets_shippingcost'}<br />
    {else}
          {if $applicable_categories}  
              <ul class="pl_10">
                  {foreach from =$applicable_categories item='category'}
                        <li>- {$category.name|escape:'html':'UTF-8'}</li>
                  {/foreach}  
              </ul>
          {/if}  
          <p>
            {l s='Include products in subcategories' mod='ets_shippingcost'}: {if $include_sub_categories}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
          </p>
        {if $include_specific_products}
            {l s='Include specific products' mod='ets_shippingcost'}:
            <ul class="pl_10">
                {foreach from=$include_specific_products item='product'}
                    <li class="prm_products">
                        <img src="{$product.image|escape:'html':'UTF-8'}" />
                        <div class="product-info">
                            <div class="name">{$product.name|escape:'html':'UTF-8'}{if isset($product.attributes)} - {$product.attributes|escape:'html':'UTF-8'}{/if}</div>
                            <div class="price">{$product.price|escape:'html':'UTF-8'}</div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        {/if}
    {/if}
    {if $exclude_products}
        {l s='Exclude products' mod='ets_shippingcost'}:
        <ul class="pl_10">
            {foreach from=$exclude_products item='product'}
                <li class="prm_products">
                    <img src="{$product.image|escape:'html':'UTF-8'}" />
                    <div class="product-info">
                        <div class="name">{$product.name|escape:'html':'UTF-8'}{if isset($product.attributes)} - {$product.attributes|escape:'html':'UTF-8'}{/if}</div>
                        <div class="price">{$product.price|escape:'html':'UTF-8'}</div>
                    </div>
                </li>
            {/foreach}
        </ul>
    {/if}
    
</p>
<p>
{l s='Apply attributes' mod='ets_shippingcost'}:
{if $apply_all_attribute || $select_attributes=='all' || !$select_attributes}
    {l s='All' mod='ets_shippingcost'}
{else}
    <ul class="pl_10">
        {foreach from =$select_attributes item='attribute'}
            <li>- {$attribute.name|escape:'html':'UTF-8'}</li>
        {/foreach}
    </ul>
{/if}
</p>
<p>
{l s='Apply features' mod='ets_shippingcost'}:
{if $apply_all_features || $select_features=='all' || !$select_features}
    {l s='All' mod='ets_shippingcost'}
{else}
    <ul class="pl_10">
        {foreach from =$select_features item='feature'}
            <li>- {$feature.name|escape:'html':'UTF-8'}</li>
        {/foreach}
    </ul>
{/if}
</p>
<p>
{l s='Apply suppliers' mod='ets_shippingcost'}:
{if $apply_all_supplier || $select_suppliers=='all' || !$select_suppliers}
    {l s='All' mod='ets_shippingcost'}
{else}
    <ul class="pl_10">
        {foreach from =$select_suppliers item='supplier'}
            <li>- {$supplier.name|escape:'html':'UTF-8'}</li>
        {/foreach}
    </ul>
{/if}
</p>
<p>
{l s='Apply manufacturers' mod='ets_shippingcost'}:
{if $apply_all_manufacturer || $select_manufacturers=='all' || !$select_manufacturers}
    {l s='All' mod='ets_shippingcost'}
{else}
    <ul class="pl_10">
        {foreach from =$select_manufacturers item='manufacturer'}
            <li>- {$manufacturer.name|escape:'html':'UTF-8'}</li>
        {/foreach}
    </ul>
{/if}
</p>
{if $apply_for_product_price}
    <p>{l s='Apply for product price' mod='ets_shippingcost'}: {$apply_for_product_price_cal|escape:'html':'UTF-8'}{$apply_for_product_price|escape:'html':'UTF-8'}</p>
{/if}
{if $apply_for_availabled_quantity_stock >0}
    <p>{l s='Apply for availabled quanitty stock' mod='ets_shippingcost'}: {$apply_for_availabled_quantity_stock_cal|escape:'html':'UTF-8'}{$apply_for_availabled_quantity_stock|intval}</p>
{/if}

