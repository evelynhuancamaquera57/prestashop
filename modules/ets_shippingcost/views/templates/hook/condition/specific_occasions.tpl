{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $specific_occasion=='hour_of_day'}
    <p>{l s='Specific occasion: Hour of day' mod='ets_shippingcost'}</p>
    {if $specific_occasion_hour_of_day_from}
        <ul  class="pl_10">
            {foreach from=$specific_occasion_hour_of_day_from key='index' item ='day_from'}
                {if $day_from || $specific_occasion_hour_of_day_to[$index]}
                    <li>- {if $day_from!==''}{l s='From' mod='ets_shippingcost'}: {$day_from|escape:'html':'UTF-8'}h{/if} {if $specific_occasion_hour_of_day_to[$index]}{l s='To' mod='ets_shippingcost'}: {$specific_occasion_hour_of_day_to[$index]|escape:'html':'UTF-8'}h{/if}</li>
                {/if}
            {/foreach}
        </ul>
    {/if}
{elseif $specific_occasion=='day_of_week'}
    <p>{l s='Specific occasion: Day of week' mod='ets_shippingcost'}</p>
    {if $specific_occasion_day_of_week}
        <ul class="pl_10">
            {foreach from=$specific_occasion_day_of_week key='index' item ='day'}
                <li>- {$days[$day]|escape:'html':'UTF-8'}
                {if $specific_occasion_day_of_week_from[$index]!=='' || $specific_occasion_day_of_week_to[$index]!==''}
                    {if $specific_occasion_day_of_week_from[$index]!==''}{l s='From' mod='ets_shippingcost'}: {$specific_occasion_day_of_week_from[$index]|escape:'html':'UTF-8'}h{/if} {if $specific_occasion_day_of_week_to[$index]!==''}{l s='To' mod='ets_shippingcost'}: {$specific_occasion_day_of_week_to[$index]|escape:'html':'UTF-8'}h{/if}
                {/if}
                </li>
            {/foreach}
        </ul>
    {/if}
{elseif $specific_occasion=='month_of_year'}
    <p>{l s='Specific occasion: Month of year' mod='ets_shippingcost'}</p>
    {if $specific_occasion_month_of_year}
        <ul  class="pl_10">
            {foreach from=$specific_occasion_month_of_year key='index' item ='month'}
                <li>
                    - {$months[$month]|escape:'html':'UTF-8'}
                {if $specific_occasion_month_of_year_from[$index] || $specific_occasion_month_of_year_to[$index]}
                    {if $specific_occasion_month_of_year_from[$index]}{l s='From' mod='ets_shippingcost'}: {dateFormat date=$specific_occasion_month_of_year_from[$index]}{/if} {if $specific_occasion_month_of_year_to[$index]}{l s='To' mod='ets_shippingcost'}: {dateFormat date=$specific_occasion_month_of_year_to[$index]}{/if}
                {/if}
                </li>
            {/foreach}
        </ul>
    {/if}
{elseif $specific_occasion=='from_to'}
    <p>{l s='Specific occasion : From - To' mod='ets_shippingcost'}</p>
    {if $specific_occasion_date_from}
        <ul class="pl_10">
            {foreach from=$specific_occasion_date_from key='index' item ='date_from'}
                {if $date_from || $specific_occasion_date_to[$index]}
                    <li>- {if $date_from}{l s='From' mod='ets_shippingcost'}: {dateFormat date=$date_from full=1}{/if} {if $specific_occasion_date_to[$index]}{l s='To' mod='ets_shippingcost'}: {dateFormat date=$specific_occasion_date_to[$index] full=1}{/if}</li>
                {/if}
                
            {/foreach}
        </ul>
    {/if}
{/if}