{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<div class="panel">
    <div class="panel-heading">
        {l s='Conditions' mod='ets_shippingcost'}
        <span class="panel-heading-action">
            <a class="btn btn-default btn-new-condition" href="#">
                    <svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"/></svg>
                 {l s='Add new condition' mod='ets_shippingcost'}
            </a>
        </span>
    </div>
    <div class="form-wrapper">
        <table class="table configuration list-rule">
            <thead>
                <tr class="nodrag nodrop">
                    <th>{l s='Condition' mod='ets_shippingcost'}</th>
                    <th>{l s='Detail' mod='ets_shippingcost'}</th>
                    <th class="text-right">{l s='Action' mod='ets_shippingcost'}</th>
                </tr>
            </thead>
            <tbody>
                {if $conditions}
                    {foreach from=$conditions item='condition'}
                        <tr class="row-condtion-{$condition.id_ets_sc_condition_rule|intval}">
                            <td>{$condition.condition|escape:'html':'UTF-8'}</td>
                            <td>{$condition.detail nofilter}</td>
                            <td class="text-right">
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="btn btn-default link_edit_condition" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editconditionrule=1&id_ets_sc_condition_rule={$condition.id_ets_sc_condition_rule|intval}">
                                            <i class="icon icon-pencil fa fa-pencil"></i>
                                            {l s='Edit' mod='ets_shippingcost'}
                                        </a>
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="btn btn-default link_delete_condition" data-confirm="{l s='Do you want to delete this item?' mod='ets_shippingcost' js=1}" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&id_ets_sc_condition_rule={$condition.id_ets_sc_condition_rule|intval}&delCondition=yes">
                                                    <i class="fa fa-trash icon icon-trash"></i>
                                                    {l s='Delete' mod='ets_shippingcost'}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    {/foreach}
                    <tr class="no-data" style="display:none">
                        <td colspan="100%">{l s='No data found' mod='ets_shippingcost'}</td>
                    </tr>
                {else}
                    <tr class="no-data">
                        <td colspan="100%">{l s='No data found' mod='ets_shippingcost'}</td>
                    </tr>
                {/if}
                
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <a class="btn btn-default pull-left" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=information">
            <i class="process-icon-back"></i>
            {l s='Back' mod='ets_shippingcost'}
        </a>
        <a class="btn btn-default pull-right" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=action">
            <i class="process-icon-next"></i>
            {l s='Next' mod='ets_shippingcost'}
        </a>
    </div>
</div>
<div class="ets_condition_popup">
    <div class="popup_content table">
        <div class="popup_content_tablecell">
            <div class="popup_content_wrap" style="position: relative">
                <span class="close_popup" title="{l s='Close' mod='ets_shippingcost'}">+</span>
                <div id="block-form-popup-condition" class="form_codition">
                    {$condition_form nofilter}
                </div>
            </div>
        </div>
    </div>
</div>
<div id="new_from_condition_rule" style="display:none;">
    {$condition_form nofilter}
</div>