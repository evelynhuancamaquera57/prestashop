{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<p>
    {l s='Applicable product categories' mod='ets_shippingcost'} :
    {if $specific_category =='all_product' || (!$list_categories && !$include_specific_products) }
        {l s='All categories' mod='ets_shippingcost'}<br />
    {else}
          {if $list_categories}  
              <ul class="pl_10">
                  {foreach from =$list_categories item='category'}
                        <li>- {$category.name|escape:'html':'UTF-8'}</li>
                  {/foreach}  
              </ul>
          {/if}  
          <p>
            {l s='Include products in subcategories' mod='ets_shippingcost'}: {if $include_sub_category}<span class="status_yes">{l s='Yes' mod='ets_shippingcost'}</span>{else}<span class="status_no">{l s='No' mod='ets_shippingcost'}</span>{/if}
          </p>
        {if $include_specific_products}
            {l s='Include specific products' mod='ets_shippingcost'}:
            <ul class="pl_10">
                {foreach from=$include_specific_products item='product'}
                    <li class="prm_products">
                        <img src="{$product.image|escape:'html':'UTF-8'}" />
                        <div class="product-info">
                            <div class="name">{$product.name|escape:'html':'UTF-8'}{if isset($product.attributes)} - {$product.attributes|escape:'html':'UTF-8'}{/if}</div>
                            <div class="price">{$product.price|escape:'html':'UTF-8'}</div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        {/if}
    {/if}
    {if $exclude_products}
        {l s='Exclude products' mod='ets_shippingcost'}:
        <ul class="pl_10">
            {foreach from=$exclude_products item='product'}
                <li class="prm_products">
                    <img src="{$product.image|escape:'html':'UTF-8'}" />
                    <div class="product-info">
                        <div class="name">{$product.name|escape:'html':'UTF-8'}{if isset($product.attributes)} - {$product.attributes|escape:'html':'UTF-8'}{/if}</div>
                        <div class="price">{$product.price|escape:'html':'UTF-8'}</div>
                    </div>
                </li>
            {/foreach}
        </ul>
    {/if}
    
</p>