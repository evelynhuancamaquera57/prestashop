{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<div class="panel">
    <div class="panel-heading">
        {l s='Promote shipping rule' mod='ets_shippingcost'}
        <span class="panel-heading-action">
            <a class="btn btn-default btn-new-promote" href="#">
                    <svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1600 736v192q0 40-28 68t-68 28h-416v416q0 40-28 68t-68 28h-192q-40 0-68-28t-28-68v-416h-416q-40 0-68-28t-28-68v-192q0-40 28-68t68-28h416v-416q0-40 28-68t68-28h192q40 0 68 28t28 68v416h416q40 0 68 28t28 68z"/></svg>
              {l s='Add new way to promote shipping rule' mod='ets_shippingcost'}
            </a>
        </span>
    </div>
    <div class="form-wrapper">
        <table class="table configuration list-rule">
            <thead>
                <tr class="nodrag nodrop">
                    <th>{l s='Way to promote' mod='ets_shippingcost'}</th>
                    <th>{l s='Position to display' mod='ets_shippingcost'}</th>
                    <th>{l s='Detail' mod='ets_shippingcost'}</th>
                    <th>{l s='Active' mod='ets_shippingcost'}</th>
                    <th class="text-right">{l s='Action' mod='ets_shippingcost'}</th>
                </tr>
            </thead>
            <tbody>
                {if $promotes}
                    {foreach from=$promotes item='promote'}
                         <tr class="row-promote-{$promote.id_ets_sc_promote_rule|intval}">
                            <td>{$promote.way_to_promote|escape:'html':'UTF-8'}</td>
                            <td class="position_display">{$promote.position_to_display nofilter}</td>
                            <td>{$promote.detail nofilter}</td>
                            <td>
                                {if $promote.enabled}
                                    <a class="list-action field-active list-action-enable action-enabled list-item-{$promote.id_ets_sc_promote_rule|intval}" name="promotion_rule" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$promote.id_ets_sc_shipping_rule|intval}&id_ets_sc_promote_rule={$promote.id_ets_sc_promote_rule|intval}&change_enabled=0&field=enabled" data-id="{$promote.id_ets_sc_promote_rule|intval}" title="{l s='Click to disable' mod='ets_shippingcost'}">
                                        <i class="icon icon-check fa fa-check"></i>
                                    </a>
                                {else}
                                    <a class="list-action field-active list-action-enable action-disabled list-item-{$promote.id_ets_sc_promote_rule|intval}" name="promotion_rule" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$promote.id_ets_sc_shipping_rule|intval}&id_ets_sc_promote_rule={$promote.id_ets_sc_promote_rule|intval}&change_enabled=1&field=enabled" data-id="{$promote.id_ets_sc_promote_rule|intval}" title="{l s='Click to enable' mod='ets_shippingcost'}">
                                        <i class="icon icon-remove fa fa-remove"></i>
                                    </a>
                                {/if}
                            </td>
                            <td class="text-right">
                                <div class="btn-group-action">
                                    <div class="btn-group pull-right">
                                        <a class="btn btn-default link_edit_condition" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editpromoterule=1&id_ets_sc_promote_rule={$promote.id_ets_sc_promote_rule|intval}">
                                            <i class="icon icon-pencil fa fa-pencil"></i>
                                            {l s='Edit' mod='ets_shippingcost'}
                                        </a>
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-caret-down"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a class="btn btn-default link_delete_condition" data-confirm="{l s='Do you want to delete this item?' mod='ets_shippingcost' js=1}" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&id_ets_sc_promote_rule={$promote.id_ets_sc_promote_rule|intval}&delPromote=yes">
                                                    <i class="fa fa-trash icon icon-trash"></i>
                                                    {l s='Delete' mod='ets_shippingcost'}
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                         </tr>
                    {/foreach}
                    <tr class="no-data" style="display:none">
                        <td colspan="100%">{l s='No data found' mod='ets_shippingcost'}</td>
                    </tr>
                {else}
                    <tr class="no-data">
                        <td colspan="100%">{l s='No data found' mod='ets_shippingcost'}</td>
                    </tr>
                {/if}
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        <a class="btn btn-default pull-left" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=action">
            <i class="process-icon-back"></i>
            {l s='Back' mod='ets_shippingcost'}
        </a>
        <a class="btn btn-default pull-right" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}">
            <i class="process-icon-save"></i>
            {l s='Finish' mod='ets_shippingcost'}
        </a>
    </div>
</div>
<div class="ets_condition_popup">
    <div class="popup_content table">
        <div class="popup_content_tablecell">
            <div class="popup_content_wrap" style="position: relative">
                <span class="close_popup" title="{l s='Close' mod='ets_shippingcost'}">+</span>
                <div id="block-form-popup-condition" class="form_promote form_codition">
                </div>
            </div>
        </div>
    </div>
</div>
<div id="new_from_promote_rule" style="display:none;" data-form="{$promote_form|escape:'html':'UTF-8'}">
</div>