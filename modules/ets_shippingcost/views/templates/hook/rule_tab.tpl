{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}

<ul class="ets_rule_tabs_left">
    <li class="rule_tab rule_tab_information{if $current_tab=='information'} active{/if}{if $current_tab=='condition' || $current_tab=='action' || $current_tab=='discount'} active done{/if}" data-tab-id="information" data-step="1">
        {if $id_ets_sc_shipping_rule!=0}
            <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=information">
        {/if}
                <span>{l s='Information' mod='ets_shippingcost'}</span>
        {if $id_ets_sc_shipping_rule!=0}
            </a>
        {/if}
    </li>
    <li class="rule_tab rule_tab_condition{if $current_tab=='condition'} active{/if}{if $current_tab=='action' || $current_tab=='discount'} active done{/if}" data-tab-id="condition"  data-step="2">
        {if $id_ets_sc_shipping_rule!=0}
            <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=condition">
        {/if}    
                <span>{l s='Conditions' mod='ets_shippingcost'}</span>
        {if $id_ets_sc_shipping_rule!=0}
            </a>
        {/if}
    </li>
    <li class="rule_tab rule_tab_action{if $current_tab=='action' } active{/if}{if $current_tab=='discount'} active done{/if}" data-tab-id="action" data-step="3">
        {if $id_ets_sc_shipping_rule!=0}
            <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=action">
        {/if}  
                <span>{l s='Action' mod='ets_shippingcost'}</span>
        {if $id_ets_sc_shipping_rule!=0}
            </a>
        {/if}
    </li>
    <li class="rule_tab rule_tab_discount{if $current_tab=='discount'} active{/if}" data-tab-id="discount" data-step="4">
        {if $id_ets_sc_shipping_rule!=0}
            <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$id_ets_sc_shipping_rule|intval}&current_tab=discount">
        {/if}  
                <span>{l s='Promote' mod='ets_shippingcost'}</span>
        {if $id_ets_sc_shipping_rule!=0}
            </a>
        {/if}
    </li>
    <li class="black_to_list">
        <a class="black_to_list" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}"><i class="icon-long-arrow-left"></i> {l s='Back to list' mod='ets_shippingcost'}</a>
    </li>
</ul>