{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<div class="row">
    <div class="col-lg-12">
        <div class="row ets_sc_rule_report-row">
            <h3 class="session-header">{l s='View shipping rule report' mod='ets_shippingcost'}</h3>
            <div class="session-body">
                <div class="row">
                    <div class="card">
                        <div class="card-body">
                            <ul>
                                <li>
                                    <span class="title">{l s='Rule name' mod='ets_shippingcost'}</span>
                                    <span class="cotent"><a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&editshipping_rule=1&id_ets_sc_shipping_rule={$rule->id|intval}"> {$rule->name|escape:'html':'UTF-8'}</a> </span>
                                </li>
                                <li>
                                    <span class="title">{l s='Created date' mod='ets_shippingcost'}</span>
                                    <span class="cotent">{$rule->date_add|escape:'html':'UTF-8'} </span>
                                </li>
                                {assign var='total_applied' value=$rule->getTotaldiscountApplied()}
                                {assign var='total_order_amount' value=$rule->getTotalOrderAmount()}
                                {if $total_applied >0}
                                    <li>
                                        <span class="title">{l s='Total discount applied' mod='ets_shippingcost'}</span>
                                        <span class="cotent">{$total_applied|escape:'html':'UTF-8'} </span>
                                    </li>
                                {/if}
                                {if $rule->new_customer > 0}
                                    <li>
                                        <span class="title">{l s='Total new customers' mod='ets_shippingcost'}</span>
                                        <span class="cotent">{$rule->new_customer|intval} </span>
                                    </li>
                                {/if}
                                {if $total_order_amount>0}
                                    <li>
                                        <span class="title">{l s='Total order amount' mod='ets_shippingcost'}</span>
                                        <span class="cotent">{displayPrice price=$total_order_amount} </span>
                                    </li>
                                {/if}
                            </ul>
                        </div>
                    </div>
                    {if $applied_orders}
                        <div class="card ets_sc_list_applied_orders">
                            <h3 class="card-header">{l s='Order applied shipping rule' mod='ets_shippingcost'}</h3>
                             <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>{l s='Order ID' mod='ets_shippingcost'}</th>
                                            <th>{l s='Order reference' mod='ets_shippingcost'}</th>
                                            <th>{l s='Customer' mod='ets_shippingcost'}</th>
                                            <th>{l s='Products' mod='ets_shippingcost'}</th>
                                            <th class="text-center">{l s='Base cost' mod='ets_shippingcost'}</th>
                                            <th class="text-center">{l s='Impact cost' mod='ets_shippingcost'}</th>
                                            <th class="text-center">{l s='Final cost' mod='ets_shippingcost'}</th>
                                            <th>{l s='Date added' mod='ets_shippingcost'}</th>
                                            <th class="text-right">{l s='Action' mod='ets_shippingcost'}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from= $applied_orders item='order'}
                                            <tr>
                                                <td>{$order.id_order|intval}</td>
                                                <td><a href="{$order.link_view|escape:'html':'UTF-8'}">{$order.reference|escape:'html':'UTF-8'}</a></td>
                                                <td><a href="{$order.view_customer|escape:'html':'UTF-8'}">{$order.firstname|escape:'html':'UTF-8'} {$order.lastname|escape:'html':'UTF-8'}</a><br /> {$order.email|escape:'html':'UTF-8'}</td>
                                                <td>{$order.products nofilter}</td>
                                                <td class="text-center">{$order.shipping_price_default|escape:'html':'UTF-8'}</td>
                                                <td class="text-center">
                                                    {if $order.type=='increase'}
                                                        {l s='Increase' mod='ets_shippingcost'} {$order.rule_price|escape:'html':'UTF-8'}
                                                    {elseif $order.type=='decrease'}
                                                        {l s='Decrease' mod='ets_shippingcost'} {$order.rule_price|escape:'html':'UTF-8'}
                                                    {elseif $order.type=='replace'}
                                                        {l s='Replace by' mod='ets_shippingcost'} {$order.rule_price|escape:'html':'UTF-8'}
                                                    {elseif $order.type=='free'}
                                                        {l s='Mark as free shipping' mod='ets_shippingcost'}
                                                    {/if}
                                                </td>
                                                <td class="text-center">{$order.shipping_price|escape:'html':'UTF-8'}</td>
                                                <td>{dateFormat date=$order.date_add full=1}</td>
                                                <td class="text-right">
                                                    <div class="btn-group-action">
                                                        <div class="btn-group pull-right">
                                                            <a class="btn btn-default link_view" href="{$order.link_view|escape:'html':'UTF-8'}">
                                                            <i class="icon-search-plus fa fa-search-plus"></i> {l s='View order' mod='ets_shippingcost'}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                             </div>
                        </div>
                    {/if}
                </div>
                <div class="row">
                    <a class="btn btn-default back_to_list" href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&rule_tab=shipping_report">
                        <i class="icon-arrow-left"></i>
                        {l s='Back to list' mod='ets_shippingcost'}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>