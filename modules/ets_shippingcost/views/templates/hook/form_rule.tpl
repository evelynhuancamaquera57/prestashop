{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<script type="text/javascript">
var sc_link_search_product = '{$sc_link_search_product nofilter}';
var Delete_text = '{l s='Delete' mod='ets_shippingcost' js=1}';
var sc_link_search_customer = '{$sc_link_search_customer nofilter}';
</script>
<div class="panel-heading">
    {if !$id_ets_sc_shipping_rule}
        {l s='Add shipping rule' mod='ets_shippingcost'}
    {else}
        {l s='Edit shipping rule' mod='ets_shippingcost'}: {$shippingrule->name|escape:'html':'UTF-8'}
    {/if}
</div>
<div class="form-wrapper form-rule{if $current_tab=='information'} form_codition{/if}">
    {$tab_rule nofilter}
    {$form_html nofilter}
</div>
