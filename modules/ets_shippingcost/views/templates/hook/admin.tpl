{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
<script type="text/javascript">
var text_Click_to_copy = '{l s='Click to copy' mod='ets_shippingcost' js=1}';
var text_Copied = '{l s='Copied' mod='ets_shippingcost' js=1}';
if (typeof $.fn.mColorPicker !== "undefined") {
    $.fn.mColorPicker.defaults.imageFolder =   '../img/admin/';
}
</script>
<ul class="ets_sc_tabs">
    <li class="tab{if $rule_tab=='shipping_rule'} active{/if}" data-tab="shipping_rule">
        <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}">
            <img src="../modules/ets_shippingcost/views/img/mn1.png" alt="" /> {l s='Shipping cost rule' mod='ets_shippingcost'}
        </a>
    </li>
    <li class="tab{if $rule_tab=='shipping_report'} active{/if}" data-tab="shipping_report">
        <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&rule_tab=shipping_report">
            <img src="../modules/ets_shippingcost/views/img/mn2.png" alt="" /> {l s='Statistics' mod='ets_shippingcost'}
        </a>
    </li>
    <li class="tab{if $rule_tab=='settings'} active{/if}" data-tab="settings">
        <a href="{$link->getAdminLink('AdminShippingCostRule')|escape:'html':'UTF-8'}&rule_tab=settings">
            <img src="../modules/ets_shippingcost/views/img/mn3.png" alt="" /> {l s='Settings' mod='ets_shippingcost'}
        </a>
    </li>
</ul>
<div class="ets_sc_content_tabs">
    <div class="tab_content shipping_rule{if $rule_tab=='shipping_rule'} active{/if}">
        {if $rule_tab=='shipping_rule'}
            {$shipping_rule nofilter}
        {/if}
    </div>
    <div class="tab_content shipping_report{if $rule_tab=='shipping_report'} active{/if}">
        {if $rule_tab=='shipping_report'}
            {$shipping_rule_report nofilter}
        {/if}
    </div>
    <div class="tab_content settings{if $rule_tab=='settings'} active{/if}">
        {if $rule_tab=='settings'}
            {$setting_form nofilter}
        {/if}
    </div>
</div>