{*
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*}
{if $promoteDiscountRule.way_to_promote=='display_notification'}
    <div class="ets_sc_display_notification">
        <div class="alert alert-info" style="{if $promoteDiscountRule.notification_text_color} color:{$promoteDiscountRule.notification_text_color|escape:'html':'UTF-8'};{/if} {if $promoteDiscountRule.notification_border_color} border-color:{$promoteDiscountRule.notification_border_color|escape:'html':'UTF-8'};{/if}{if $promoteDiscountRule.notification_background_color} background-color:{$promoteDiscountRule.notification_background_color|escape:'html':'UTF-8'};{/if}">{$promoteDiscountRule.notification nofilter}</div>
    </div>
{elseif $promoteDiscountRule.way_to_promote =='display_banner'}
    <div class="ets_sc_display_banner">
        {if $promoteDiscountRule.banner_link}
            <a href="{$promoteDiscountRule.banner_link|escape:'html':'UTF-8'}">
        {/if}
             <img src="{$link->getMediaLink("`$smarty.const._PS_ETS_SC_IMG_``$promoteDiscountRule.banner|escape:'htmlall':'UTF-8'`")}" />
        {if $promoteDiscountRule.banner_link}
            </a>
        {/if}
    </div>
{elseif $promoteDiscountRule.way_to_promote =='display_highlight_bar'}
    {if !in_array($promoteDiscountRule.id_ets_sc_promote_rule,$sc_closed_highlightbars)}
        <div class="ets_sc_display_highlight_bar highlight_bar_{$promoteDiscountRule.id_ets_sc_promote_rule|intval} show" style="{if $promoteDiscountRule.text_color} color:{$promoteDiscountRule.text_color|escape:'html':'UTF-8'};{/if}{if $promoteDiscountRule.background_color} background-color:{$promoteDiscountRule.background_color|escape:'html':'UTF-8'};{/if}">
            <div class="ets_sc_table">
                <div class="ets_sc_tablecell">
                    <div class="container">
                        <div class="ets_sc_container">
                            <div data-href="{$link->getModuleLink('ets_shippingcost','ajax')|escape:'html':'UTF-8'}" class="ets_sc_close close_highlight_bar" title="{l s='Close' mod='ets_shippingcost'}" data-id="{$promoteDiscountRule.id_ets_sc_promote_rule|intval}">
                                {if $promoteDiscountRule.button_close_color}
                                    {literal}
                                        <style>
                                            .highlight_bar_{/literal}{$promoteDiscountRule.id_ets_sc_promote_rule|intval}{literal} .ets_sc_close:before,.highlight_bar_{/literal}{$promoteDiscountRule.id_ets_sc_promote_rule|intval}{literal} .ets_sc_close:after{background-color: {/literal}{$promoteDiscountRule.button_close_color|escape:'html':'UTF-8'}{literal};}
                                        </style>
                                    {/literal}
                                {/if}
                            </div>
                            <div class="ets_sc_highlight_bar_wrapper">
                                {$promoteDiscountRule.content_bar nofilter}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}
{elseif $promoteDiscountRule.way_to_promote =='display_popup'}
    {if !in_array($promoteDiscountRule.id_ets_sc_promote_rule,$sc_closed_popups)}
        <div class="ets_sc_display_popup ets_sc_popup_{$promoteDiscountRule.id_ets_sc_promote_rule|intval}" data-delay="{$promoteDiscountRule.delay_popup|intval}">
            <div class="ets_sc_table">
                <div class="ets_sc_tablecell">
                    <div class="ets_sc_container" style="{if $promoteDiscountRule.border_radius} border-radius:{$promoteDiscountRule.border_radius|escape:'html':'UTF-8'}px;{/if}">
                        <div data-href="{$link->getModuleLink('ets_shippingcost','ajax')|escape:'html':'UTF-8'}" class="ets_sc_close ets_sc_close_popup" title="{l s='Close' mod='ets_shippingcost'}" data-id="{$promoteDiscountRule.id_ets_sc_promote_rule|intval}">
                            {if $promoteDiscountRule.close_button_popup_color}
                                {literal}
                                    <style>
                                        .ets_sc_popup_{/literal}{$promoteDiscountRule.id_ets_sc_promote_rule|intval}{literal} .ets_sc_close:before,.ets_sc_popup_{/literal}{$promoteDiscountRule.id_ets_sc_promote_rule|intval}{literal} .ets_sc_close:after{background-color: {/literal}{$promoteDiscountRule.close_button_popup_color|escape:'html':'UTF-8'}{literal};}
                                    </style>
                                {/literal}
                            {/if}
                        </div>
                        <div class="ets_sc_popup_wrapper">
                            {if $promoteDiscountRule.title_popup}
                                <div class="ets_sc_popup_title" style="{if $promoteDiscountRule.title_popup_color} color:{$promoteDiscountRule.title_popup_color|escape:'html':'UTF-8'};{/if}{if $promoteDiscountRule.background_title_popup_color} background-color:{$promoteDiscountRule.background_title_popup_color|escape:'html':'UTF-8'};{/if} ">
                                    {$promoteDiscountRule.title_popup|escape:'html':'UTF-8'}
                                </div>
                            {/if}
                            <div class="ets_sc_popup_content" style="{if $promoteDiscountRule.background_content_popup_color} background-color:{$promoteDiscountRule.background_content_popup_color|escape:'html':'UTF-8'};{/if}">
                                {$promoteDiscountRule.content_popup nofilter}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}
{/if}