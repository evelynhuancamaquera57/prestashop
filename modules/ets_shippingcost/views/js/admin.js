/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */
var name_sc_search_product='';
function copyToClipboard(el) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(el.text()).select();
    document.execCommand("copy");
    $temp.remove();
    el.append('<span class="copied_text">Copied</span>')
    //showSuccessMessage(copied_translate);
    setTimeout(function () {
        el.removeClass('copy');
        $('.copied_text').remove();
    }, 500);
}
$(document).ready(function(){
    $(document).on('click', '.way_to_promote .help-block code', function () {
        copyToClipboard($(this));
    });

    $(document).on('click','.btn_delete_row_delivery_zipcode',function(e){
       $(this).parents('.form-group.parent_codition.delivery_zipcode').remove(); 
    });
    $(document).on('click','.btn_add_new_row_delivery_zipcode',function(e){
        e.preventDefault();
        $(this).parent().parent().before($(this).parent().parent().next('.new_row_delivery_zipcode').html().replace(new RegExp('data-name', 'g'),'name'));
    });
    $(document).on('change','.select_delivery_zipcode_type',function(){
        var zipcode_type = $(this).val();
        $(this).parents('.parent_codition.delivery_zipcode').find('.delivery_zipcode_type').hide();
        $(this).parents('.parent_codition.delivery_zipcode').find('.delivery_zipcode_type_'+zipcode_type).show();
    });
    $(document).on('change','select[name="formular_defined"]',function(){
        $('#formular').val($(this).val());
    });
    $(document).on('click','input[name="specific_category"]',function(){
       ets_sc_rule.displayFieldRule(); 
    });
    if($('.help-block code').length)
    {
        $('.help-block code').attr('title',text_Click_to_copy);
        $(document).on('click','.help-block code',function(){
            $('.pr-text-copy').remove();
            if($('input#sc_select_code').length==0)
                $('body').append('<input id="sc_select_code" value="'+$(this).html()+'" type="text">');
            else
                $('#sc_select_code').val($(this).html());
            $('#sc_select_code').select();
            document.execCommand("copy");
            var copy_text = $('<span class="pr-text-copy">'+text_Copied+'</span>');
            $(this).append(copy_text);
            setTimeout(function() { copy_text.remove(); }, 2000);
        });
    }
    $(document).on('change','.specific_occasion_month_of_year',function(){
        ets_sc_rule.changeDayOfMonth($(this).parents('.row.month_of_year')); 
    });
    if($('.specific_occasion_month_of_year').length)
    {
        $('.specific_occasion_month_of_year').each(function(){
            ets_sc_rule.changeDayOfMonth($(this).parents('.row.month_of_year')); 
        });
    }
    $(document).on('click','input[name="sc_search_product"]',function(){
        name_sc_search_product = $(this).data('name');
    });
    $(document).on('click','.btn_delete_row_specific_occasion',function(e){
       $(this).parents('.form-group.parent_codition.specific_occasion').remove(); 
    });
    $(document).on('click','.btn_add_new_row_specific_occasion',function(e){
        e.preventDefault();
        $('.new_row_specific_occasion .datetimepicker').removeClass('hasDatepicker');
        $('.new_row_specific_occasion .datepicker').removeClass('hasDatepicker');
        $(this).parent().parent().before($(this).parent().parent().next('.new_row_specific_occasion').html().replace(new RegExp('data-name', 'g'),'name'));
        if ($(".datepicker").length > 0)
			$(".datepicker").datepicker({
				prevText: '',
				nextText: '',
				dateFormat: 'yy-mm-dd'
			});

		if ($(".datetimepicker").length > 0)
		{
            $('.datetimepicker').datetimepicker({
    			prevText: '',
    			nextText: '',
    			dateFormat: 'yy-mm-dd',
    			// Define a custom regional settings in order to use PrestaShop translation tools
    			currentText: 'Now',
    			closeText: 'Done',
    			ampm: false,
    			amNames: ['AM', 'A'],
    			pmNames: ['PM', 'P'],
    			timeFormat: 'hh:mm:ss tt',
    			timeSuffix: '',
    			timeOnlyTitle: 'Choose Time',
    			timeText: 'Time',
    			hourText: 'Hour',
    			minuteText: 'Minute',
	      });
        }
    });
    if($('.module_confirmation.alert-success').length)
    {
        setTimeout(function(){$('.module_confirmation.alert-success').remove();},5000);
    }
    if($('input.color').length)
    {
        $('input.color').each(function(){
           if($(this).val()=='')
           {
                $(this).css('background-color','#ffffff');
                $(this).css('color','black');
           }
        });
    }
    $(document).on('change','#way_to_promote,input.display_hook',function(){
        ets_sc_rule.displayPromoteRule();
    });
    $(document).on('change','input[type="range"]',function(){
        ets_sc_rule.changeRange($(this)); 
    });

    $(document).on('click','.btn-new-promote',function(e){
        e.preventDefault();
        $('.ets_condition_popup .form_promote').html($('#new_from_promote_rule').data('form'));
        $('.ets_condition_popup').addClass('show');
        ets_sc_rule.displayPromoteRule();
        ets_sc_rule.searchProduct();
        if($('input[type="range"]').length)
        {
            $('input[type="range"]').each(function(){
               ets_sc_rule.changeRange($(this)); 
            });
        }
        if($('input[type="color"]').length)
        {
            if ($.fn.mColorPicker.init.replace) {
                  $('input[data-mcolorpicker!="true"]').filter(function() {
    
                    return ($.fn.mColorPicker.init.replace == '[type=color]')? this.getAttribute("type") == 'color': $(this).is($.fn.mColorPicker.init.replace);
                    }).mColorPicker({
                      imageFolder: '../img/admin/'});
            
                  $.fn.mColorPicker.liveEvents();
            }
            setTimeout(function(){
                $('input.color').each(function(){
                    if($(this).val()=='')
                    {
                        $(this).css('background-color','#ffffff');
                        $(this).css('color','black');
                    }
                    else
                    {
                        $(this).css('background-color',$(this).val());
                    }     
                });
            },300);
            
        }
        $('.help-block code').attr('title',text_Click_to_copy);
        if($('.autoload_rte').length)
        {
            var date_time = new Date();
            $('.autoload_rte').each(function(){
                $(this).attr('id',$(this).attr('id')+date_time.getTime()); 
            });
            tinySetup({
    			editor_selector :"autoload_rte"
    		});
        }
        ets_sc_rule.topFunction();
    });
    $(document).on('click','button[name="btnSubmitCombine"]',function(e){
        e.preventDefault();
        if(!$(this).hasClass('loading'))
        {
            $('.module_error.alert').parent().remove();
            $(this).addClass('loading');
            var $this=  $(this);
            var formData = new FormData($(this).parents('form').get(0));
            $.ajax({
                url: $(this).parents('form').eq(0).attr('action'),
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(json){
                    $this.removeClass('loading');
                    if(json.errors)
                    {
                        $this.before(json.errors);
                    }
                    if(json.success)
                    {
                        showSuccessMessage(json.success);
                    }
                },
                error: function(xhr, status, error)
                {
                    $this.removeClass('loading');
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);                    
                }
            });
        }
    });
    $(document).on('click','button[name="save_condition_rule"],button[name="save_promote_discount_rule"],button[name="btnSubmitSettingsRule"]',function(e){
        e.preventDefault();
        if(!$(this).hasClass('loading'))
        {
            if(typeof tinymce !== 'undefined' && tinymce.editors.length > 0)
            {                
                tinyMCE.triggerSave();
            }
            $('.module_error.alert').parent().remove();
            $(this).addClass('loading');
            var $this=  $(this);
            var formData = new FormData($(this).parents('form').get(0));
            $.ajax({
                url: $(this).parents('form').eq(0).attr('action'),
                data: formData,
                type: 'post',
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function(json){
                    $this.removeClass('loading');
                    if(json.errors)
                    {
                        $this.before(json.errors);
                    }
                    if(json.success)
                    {
                        if($this.attr('name')=='save_condition_rule' || $this.attr('name')=='save_promote_discount_rule')
                        {
                            $('.ets_condition_popup').removeClass('show');
                            if($this.attr('name')=='save_condition_rule')
                            {
                                if($('.row-condtion-'+json.id_ets_sc_condition_rule).length >0)
                                    $('.row-condtion-'+json.id_ets_sc_condition_rule).replaceWith(json.row_condtion);
                                else
                                {
                                    $('.list-rule tbody').append(json.row_condtion);
                                    $('tr.no-data').hide();
                                }
                            }
                            else
                            {
                                if($('.row-promote-'+json.id_ets_sc_promote_rule).length >0)
                                    $('.row-promote-'+json.id_ets_sc_promote_rule).replaceWith(json.row_promote);
                                else
                                {
                                    $('.list-rule tbody').append(json.row_promote);
                                    $('tr.no-data').hide();
                                }
                            }
                        }
                        showSuccessMessage(json.success);
                    }
                },
                error: function(xhr, status, error)
                {
                    $this.removeClass('loading');
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);                    
                }
            });
        }
        
    });
    if ($(".ets_sc_datepicker input").length > 0) {
		$(".ets_sc_datepicker input").datepicker({
			dateFormat: 'yy-mm-dd',
		});
	}
    ets_sc_rule.init();
    $(document).on('click','input[name="apply_customer"],input[name="apply_cart"]',function(){
        ets_sc_rule.displayFieldRule()
    });
    $(document).on('click','.ets_sc_row_code .input-group-addon',function(){
        ets_sc_rule.gencode(8);
    });
    $(document).on('change','#parent_codition,input[name="type_action"]',function(){
        ets_sc_rule.displayFieldRule();
    });
    $(document).on('click','.link_delete_condition',function(e){
        e.preventDefault();
        if(!$('body').hasClass('loading') && confirm($(this).data('confirm')))
        {
            $('body').addClass('loading');
            var link_delelte = $(this).attr('href');
            $.ajax({
                url: link_delelte,
                dataType: 'json',
                type: 'post',
                data: {
                    ajax: 1,              
                },
                success: function(json){
                    if(json.success)
                    {
                        if(json.id_ets_sc_condition_rule)
                            $('.row-condtion-'+json.id_ets_sc_condition_rule).remove();
                        if(json.id_ets_sc_promote_rule)
                            $('.row-promote-'+json.id_ets_sc_promote_rule).remove();
                        showSuccessMessage(json.success);
                    } 
                    $('body').removeClass('loading');   
                    if($('.list-rule tbody tr').length==1)
                        $('tr.no-data').show();                
                },
                error: function(xhr, status, error)
                {
                    $('body').removeClass('loading');
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                    
                }
            });
        }
    });
    $(document).on('click','.btn-new-condition',function(){
        $('.ets_condition_popup').addClass('show');
        $(".datetimepicker").removeClass('hasDatepicker');
        $(".datepicker").removeClass('hasDatepicker');
        $('#block-form-popup-condition').html($('#new_from_condition_rule').html());
        ets_sc_rule.searchProduct();
        ets_sc_rule.searchCustomer();
        ets_sc_rule.displayFieldRule();
    });
    $(document).on('click','.link_edit_condition',function(e){
        e.preventDefault();
        if(!$('body').hasClass('loading'))
        {
            $('body').addClass('loading');
            var link_edit = $(this).attr('href');
            $.ajax({
                url: link_edit,
                dataType: 'json',
                type: 'post',
                data: {
                    ajax: 1,              
                },
                success: function(json){
                    if(json.form_html)
                    {
                        $('.ets_condition_popup').addClass('show');
                        $('#block-form-popup-condition').html(json.form_html);
                        ets_sc_rule.searchProduct();
                        ets_sc_rule.searchCustomer();
                        ets_sc_rule.displayFieldRule();
                        ets_sc_rule.displayPromoteRule();
                        if ( $('input[type="range"]').length > 0 ){
                            $('input[type="range"]').each(function(){
                                ets_sc_rule.changeRange($(this));
                            });
                        }
                        if($('input[type="color"]').length)
                        {
                            if ($.fn.mColorPicker.init.replace) {
                                  $('input[data-mcolorpicker!="true"]').filter(function() {
                    
                                    return ($.fn.mColorPicker.init.replace == '[type=color]')? this.getAttribute("type") == 'color': $(this).is($.fn.mColorPicker.init.replace);
                                    }).mColorPicker();
                            
                                  $.fn.mColorPicker.liveEvents();
                            }
                            setTimeout(function(){
                                $('input.color').each(function(){
                                    if($(this).val()=='')
                                    {
                                        $(this).css('background-color','#ffffff');
                                        $(this).css('color','black');
                                    } 
                                    else
                                    {
                                        $(this).css('background-color',$(this).val());
                                    }    
                                });
                            },500);
                        }
                        $('.help-block code').attr('title',text_Click_to_copy);
                        if($('.autoload_rte').length)
                        {
                            var date_time = new Date();
                            $('.autoload_rte').each(function(){
                                $(this).attr('id',$(this).attr('id')+date_time.getTime()); 
                            });
                            tinySetup({
                    			editor_selector :"autoload_rte"
                    		});
                        }
                    } 
                    ets_sc_rule.topFunction();
                    $('body').removeClass('loading');                    
                },
                error: function(xhr, status, error)
                {
                    $('body').removeClass('loading');
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                    
                }
            });
        }
    });
    $(document).on('change','#block-form-popup-condition #id_country',function(){
        var id_country = $(this).val();
        $.ajax({
            url: '',
            dataType: 'json',
            type: 'post',
            data: {
                id_country: id_country,     
                getStates:1           
            },
            success: function(json){
                if(json.list_state)
                {
                    $('#id_state').html(json.list_state);
                    $('.ets_sc_row_id_state').show(); 
                }
                else
                    $('.ets_sc_row_id_state').hide();                       
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });
    $(document).on('change','#delivery_country_zone',function(){
        var id_zone = $(this).val();
        $.ajax({
            url: '',
            dataType: 'json',
            type: 'post',
            data: {  
                id_zone :  id_zone, 
                getCountries :1,         
            },
            success: function(json){
                if(json.list_countries)
                {
                    $('.rule_tab_content_delivery_country select').html(json.list_countries);
                    $('.rule_tab_content_delivery_country').show();
                    $('.delivery_country .help-block').hide();
                }
                else
                {
                    $('.rule_tab_content_delivery_country').show();
                    $('.delivery_country .help-block').show();
                    $('.rule_tab_content_delivery_country select').html('');
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });
    $(document).on('change','#delivery_state_zone',function(){
        var id_country = 0;
        var id_zone = $(this).val();
        $.ajax({
            url: '',
            dataType: 'json',
            type: 'post',
            data: {
                id_country: id_country,   
                id_zone :  id_zone, 
                getCountries :1,
                getStates:1           
            },
            success: function(json){
                if(json.list_countries)
                {
                    $('.ets_sc_row_delivery_state_country select').html(json.list_countries);
                    $('.ets_sc_row_delivery_state_country').show(); 
                }
                else
                    $('.ets_sc_row_delivery_state_country').hide();
                if(json.list_state)
                {
                    $('.rule_tab_content_delivery_state select').html(json.list_state);
                    $('.rule_tab_content_delivery_state').show();
                    $('.rule_tab_content_delivery_state .help-block').hide();
                }
                else
                {
                    $('.rule_tab_content_delivery_state select').html('');
                    $('.rule_tab_content_delivery_state .help-block').show();
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });
    $(document).on('change','#delivery_state_country',function(){
        var id_country = $(this).val();
        var id_zone = $('#delivery_state_zone').val();
        $.ajax({
            url: '',
            dataType: 'json',
            type: 'post',
            data: {
                id_country: id_country,   
                id_zone :  id_zone, 
                getStates:1           
            },
            success: function(json){
                if(json.list_state)
                {
                    $('.rule_tab_content_delivery_state select').html(json.list_state);
                    $('.rule_tab_content_delivery_state').show();
                    $('.rule_tab_content_delivery_state .help-block').hide();
                }
                else
                {
                    $('.rule_tab_content_delivery_state select').html('');
                    $('.rule_tab_content_delivery_state .help-block').show();
                }
            },
            error: function(xhr, status, error)
            {
                var err = eval("(" + xhr.responseText + ")");
                alert(err.Message);
            }
        });
    });
    $(document).on('click','.close_popup,button[name="btncancel"]',function(){
        $('.ets_condition_popup').removeClass('show');
        return false;
    });
    $(document).keyup(function(e) { 
        if(e.keyCode == 27 && $('.ets_condition_popup.show').length) {
            $('.ets_condition_popup').removeClass('show');
        }
    });
    $(document).on('change','input[name="specific_occasion"],input[name="first_order_of_customer"],input[name="order_criteria"],input[name="order_time_in"],input[name="applicable_product_categories"],input[name="apply_all_attribute"],input[name="apply_all_features"],input[name="apply_all_supplier"],input[name="apply_all_manufacturer"]',function(){
        if($('#ets_sc_action_rule_form').length)
            ets_sc_rule.displayRowActionRule();
        else
            ets_sc_rule.displayRowConditionRule();
    });
    $(document).on('click','.checkbox_all input',function(){
        if($(this).is(':checked'))
        {
            $(this).closest('.form-group').find('input').prop('checked',true);
        }
        else
        {
            $(this).closest('.form-group').find('input').prop('checked',false);
        }
        if($('input.display_hook').length)
            ets_sc_rule.displayPromoteRule();
    });
    $(document).on('click','.checkbox input',function(){
        if($(this).is(':checked'))
        {
            if($(this).closest('.form-group').find('input:checked').length==$(this).closest('.form-group').find('input').length-1)
                 $(this).closest('.form-group').find('.checkbox_all input').prop('checked',true);
        }
        else
        {
            $(this).closest('.form-group').find('.checkbox_all input').prop('checked',false);
        } 
    });
    $(document).on('click','.sc_product_item .sc_block_item_close',function(){
        var $ul = $(this).parents('ul.sc_products');
        var ids = '';
        $(this).parent().remove();
        if($ul.find('.sc_product_item').length)
        {
            $ul.find('.sc_product_item').each(function(){
                ids += $(this).attr('data-id')+',';
                
            });
        }
        $ul.prev('.sc_ids_product').val(ids.trim(',')); 
    });
    $(document).on('click','.form_codition .sc_customer_item .sc_block_item_close',function(){
        var $ul = $(this).parents('ul.sc_customers');
        var ids = '';
        $(this).parent().remove();
        if($ul.find('.sc_customer_item').length)
        {
            $ul.find('.sc_customer_item').each(function(){
                ids += $(this).attr('data-id')+',';
                
            });
        }
        $ul.prev('.sc_ids_customer').val(ids.trim(',')); 
    });
    $(document).on('click','.btn-advanced-settings',function(e){
        e.preventDefault();
        $('.wapper-advanced-settings').addClass('active');
        $('.btn-advanced-settings').hide();
        $('input[name="display_advanced_setting"]').val('1');
    });
    $(document).on('click','.btn-hide-advanced-settings',function(e){
        e.preventDefault();
        $('.wapper-advanced-settings').removeClass('active');
        $('.btn-advanced-settings').show();
        $('input[name="display_advanced_setting"]').val('0');
    });
    $(document).on('change','.ets_sc_row_id_groups input[type="checkbox"]',function(){
        
        if($('#ets_sc_action_rule_form').length)
            ets_sc_rule.displayRowActionRule();
        else
            ets_sc_rule.displayRowConditionRule();
    });
    $(document).on('change','input[name="calcalate_cost_by"]',function(){
        ets_sc_rule.displayRowActionRule(); 
    });
    if ( $('input[type="range"]').length > 0 ){
        $('input[type="range"]').each(function(){
            ets_sc_rule.changeRange($(this));
        });
    }
    $(document).on('change','#code',function(){
       if($(this).val()=='')
            $('.ets_sc_row_highlight').hide();
       else
            $('.ets_sc_row_highlight').show(); 
    });
    $(document).on('click','.ets_rule_tabs_configs .rule_tab',function(){
        if(!$(this).hasClass('active'))
        {
            $('.ets_rule_tabs_configs .rule_tab').removeClass('active');
            $(this).addClass('active');
            ets_sc_rule.displayFromSettings();
        } 
    });
    $(document).on('click','button[name="etsprSubmitUpdateToken"]',function(e){
        e.preventDefault();
        if(!$(this).hasClass('loading'))
        {
            $(this).addClass('loading');
            $.ajax({
    			type: 'POST',
    			headers: { "cache-control": "no-cache" },
    			url: '',
    			async: true,
    			cache: false,
    			dataType : "json",
    			data:'etsprSubmitUpdateToken=1&ETS_sc_CRONJOB_TOKEN='+$('#ETS_sc_CRONJOB_TOKEN').val(),
    			success: function(json)
    			{
                    $('button[name="etsprSubmitUpdateToken"]').removeClass('loading');
                    if(json.success)
                    {
                        showSuccessMessage(json.success)
                        $('.js-emp-test-cronjob').attr('data-secure',$('#ETS_sc_CRONJOB_TOKEN').val());
                        $('.emp-cronjob-secure-value').html($('#ETS_sc_CRONJOB_TOKEN').val());
                    }
                    if(json.errors)
                    {
                        showErrorMessage(json.errors);
                    }
                }
    		});
        }
    });
    $(document).on('click','.js-emp-test-cronjob',function(e){
        e.preventDefault();
        if(!$(this).hasClass('loading'))
        {$(this).addClass('loading');
            var secure = $(this).attr('data-secure');
            $.ajax({
    			type: 'POST',
    			headers: { "cache-control": "no-cache" },
    			url: '',
    			async: true,
    			cache: false,
    			dataType : "json",
    			data:'submitCronjob&ajax=1&secure='+secure,
    			success: function(json)
    			{
                    if(json.success)
                    {
                        showSuccessMessage(json.success);
                        $('.cronjob_log').val(json.cronjob_log);
                    }
                    if(json.errors)
                    {
                        showErrorMessage(json.errors);
                    }
                    $('.js-emp-test-cronjob').removeClass('loading');
                }
    		});
        }
    });
    $(document).on('click','button[name="etsprSubmitClearLog"]',function(e){
        e.preventDefault();
        $(this).addClass('loading');
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: '',
			async: true,
			cache: false,
			dataType : "json",
			data:'ajax=1&etsprSubmitClearLog=1',
			success: function(json)
			{
                if(json.success)
                {
                    showSuccessMessage(json.success);
                    $('.cronjob_log').val('');
                }
                if(json.errors)
                {
                    showErrorMessage(json.errors)
                }
                $('button[name="etsprSubmitClearLog"]').removeClass('loading');
            }
		});
    });
    $(document).on('click','input[name="ETS_sc_SAVE_CRONJOB_LOG"]',function(){
        $.ajax({
			type: 'POST',
			headers: { "cache-control": "no-cache" },
			url: '',
			async: true,
			cache: false,
			dataType : "json",
			data:'ETS_sc_SAVE_CRONJOB_LOG='+$('input[name="ETS_sc_SAVE_CRONJOB_LOG"]:checked').val(),
			success: function(json)
			{
                if(json.success)
                {
                    showSuccessMessage(json.success);
                }
                if(json.errors)
                {
                    showErrorMessage(json.errors);
                }
            }
		});
    });
});
var ets_sc_rule = {
    init : function(){
        ets_sc_rule.searchProduct();
        ets_sc_rule.searchCustomer();
        ets_sc_rule.displayFieldRule();
        ets_sc_rule.displayFromSettings();
    },
    displayFromSettings:function()
    {
        if($('.ets_rule_tabs_configs .rule_tab').length)
        {
            var curentTab = $('.ets_rule_tabs_configs .rule_tab.active').data('tab-id');
            $('.ets_rule_tab_content').hide();
            $('.ets_rule_tab_content.rule_tab_content_'+curentTab).show();
            if(curentTab=='cronjob')
                $('.panel-footer').hide();
            else
               $('.panel-footer').show(); 
        }
    },
    displayPromoteRule:function()
    {
        if($('#way_to_promote').length)
        {
            var way_to_promote = $('#way_to_promote').val();
            $('.form-group.way_to_promote').hide();
            $('.form-group.way_to_promote.'+way_to_promote).show();
            if(way_to_promote=='display_notification' || way_to_promote=='display_banner')
            {
                if($('input.display_hook:checked').length)
                    $('.form-group.display_hook').show();
                else
                    $('.form-group.display_hook').hide();
            }
            else
                $('.form-group.display_hook').hide();
        }
        if($('input[name="applicable_product_categories"]:checked').val()=='all_product')
        {
            $('.ets_sc_row_applicable_categories').hide();
            $('.ets_sc_row_include_sub_categories').hide();
            $('.ets_sc_row_include_specific_products').hide();
        }
        else
        {
            $('.ets_sc_row_applicable_categories').show();
            $('.ets_sc_row_include_sub_categories').show();
            $('.ets_sc_row_include_specific_products').show();
        }
    },
    displayRowActionRule:function(){
        calcalate_cost_by = $('input[name="calcalate_cost_by"]:checked').val();
        if(calcalate_cost_by=='percent_shipping_cost' || calcalate_cost_by=='percent_product_price')
        {
            $('.ets_sc_row_fees_percent,.ets_sc_row_fees_max').show();
            $('.ets_sc_row_fees_amount').hide();
            if(calcalate_cost_by=='percent_product_price')
            {
                $('.ets_sc_row_cal_percent_from').show();
                $('.ets_sc_row_ignore_product_discounted').show();
                $('.ets_sc_row_exclude_product_tax').show();
            }
            else
            {
                $('.ets_sc_row_cal_percent_from').hide();
                $('.ets_sc_row_ignore_product_discounted').hide();
                $('.ets_sc_row_exclude_product_tax').hide();
            }
        }
        else
        {
            $('.ets_sc_row_cal_percent_from').hide();
            $('.ets_sc_row_ignore_product_discounted').hide();
            $('.ets_sc_row_exclude_product_tax').hide();
        }
        if(calcalate_cost_by=='fixed_amount')
        {
            $('.ets_sc_row_fees_percent,.ets_sc_row_fees_max').hide();
            $('.ets_sc_row_fees_amount').show();
        }
        if(calcalate_cost_by=='formula')
        {
            $('.ets_sc_row_fees_percent,.ets_sc_row_fees_max').hide();
            $('.ets_sc_row_fees_amount').hide();
            $('.ets_sc_row_id_currency').show();
        }
        else
            $('.ets_sc_row_id_currency').hide();
    },
    displayRowConditionRule: function(){
        var specific_occasion = $('#block-form-popup-condition input[name="specific_occasion"]:checked').val();
        $('.ets_sc_row_specific_occasion_day_of_week').hide();
        $('.ets_sc_row_specific_occasion_month_of_year').hide();
        $('.ets_sc_row_specific_occasion_date_from').hide();
        $('.ets_sc_row_specific_occasion_hour_of_day_from').hide();
        if(specific_occasion=='hour_of_day')
        {
            $('.ets_sc_row_specific_occasion_hour_of_day_from').show();
        }
        else if(specific_occasion=='day_of_week')
        {
            $('.ets_sc_row_specific_occasion_day_of_week').show();
        }
        else if(specific_occasion=='month_of_year')
        {
            $('.ets_sc_row_specific_occasion_month_of_year').show();
        }
        else if(specific_occasion=='from_to')
        {
            $('.ets_sc_row_specific_occasion_date_from').show();
        }
        if($('#block-form-popup-condition input[name="order_criteria"]:checked').val()=='number_of_orders')
        {
            $('#block-form-popup-condition .ets_sc_row_number_of_order').show();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent').hide();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_tax_incl').hide();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_shipping_incl').hide();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_discount_incl').hide();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_number_of_order').hide();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent').show();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_tax_incl').show();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_shipping_incl').show();
            $('#block-form-popup-condition .ets_sc_row_amount_of_money_spent_discount_incl').show();
        }
        if($('#block-form-popup-condition input[name="order_time_in"]:checked').val()=='from_to')
            $('#block-form-popup-condition .ets_sc_row_order_time_in_from').show();
        else
            $('#block-form-popup-condition .ets_sc_row_order_time_in_from').hide();
        if($('#block-form-popup-condition input[name="order_time_in"]:checked').val()=='in_last_day')
            $('#block-form-popup-condition .ets_sc_row_order_time_in_last_day').show();
        else
            $('#block-form-popup-condition .ets_sc_row_order_time_in_last_day').hide();
        if($('#block-form-popup-condition input[name="applicable_product_categories"]:checked').val()=='all_product')
        {
            $('#block-form-popup-condition .ets_sc_row_applicable_categories').hide();
            $('#block-form-popup-condition .ets_sc_row_include_sub_categories').hide();
            $('.ets_sc_row_include_specific_products').hide();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_applicable_categories').show();
            $('#block-form-popup-condition .ets_sc_row_include_sub_categories').show();
            $('.ets_sc_row_include_specific_products').show();
        }
        if($('#block-form-popup-condition input[name="apply_all_attribute"]:checked').val()==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_attributes').show();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_select_attributes').hide();
        }
        if($('#block-form-popup-condition input[name="apply_all_features"]:checked').val()==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_features').show();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_select_features').hide();
        }
        if($('#block-form-popup-condition input[name="apply_all_supplier"]:checked').val()==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_suppliers').show();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_select_suppliers').hide();
        }
        if($('#block-form-popup-condition input[name="apply_all_manufacturer"]:checked').val()==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_manufacturers').show();
        }
        else
        {
            $('#block-form-popup-condition .ets_sc_row_select_manufacturers').hide();
        }
        if($('#block-form-popup-condition input[name="first_order_of_customer"]:checked').val()==1)
            $('#block-form-popup-condition .wapper-first_order_of_customer').hide();
        else
            $('#block-form-popup-condition .wapper-first_order_of_customer').show();
        if($('#block-form-popup-condition .ets_sc_row_select_manufacturers input[type="checkbox"]').length==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_manufacturers').hide();
            $('#block-form-popup-condition .ets_sc_row_apply_all_manufacturer').hide();
        }
        if($('#block-form-popup-condition .ets_sc_row_select_suppliers input[type="checkbox"]').length==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_suppliers').hide();
            $('#block-form-popup-condition .ets_sc_row_apply_all_supplier').hide();
        }
        if($('#block-form-popup-condition .ets_sc_row_select_features input[type="checkbox"]').length==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_features').hide();
            $('#block-form-popup-condition .ets_sc_row_apply_all_features').hide();
        }
        if($('#block-form-popup-condition .ets_sc_row_select_attributes input[type="checkbox"]').length==0)
        {
            $('#block-form-popup-condition .ets_sc_row_select_attributes').hide();
            $('#block-form-popup-condition .ets_sc_row_apply_all_attribute').hide();
        }
        if($('#block-form-popup-condition #id_groups_all').is(':checked'))
            $('#block-form-popup-condition .ets_sc_row_only_apply_on_default_group').hide();
        else
            $('#block-form-popup-condition .ets_sc_row_only_apply_on_default_group').show();
    },
    displayFieldRule : function(){
        if($('#block-form-popup-condition #parent_codition').length)
        {
            var parent_codition = $('#block-form-popup-condition #parent_codition').val();
            $('#block-form-popup-condition .form-group.parent_codition').hide();
            if(parent_codition)
                $('#block-form-popup-condition .form-group.parent_codition.'+parent_codition).show();
            if($('#block-form-popup-condition #id_state option').length<=1)
                $('#block-form-popup-condition .ets_sc_row_id_state').hide();
            if($('#block-form-popup-condition #delivery_id_state option').length<=1)
                $('#block-form-popup-condition .ets_sc_row_delivery_id_state').hide();
            ets_sc_rule.displayRowConditionRule();
        }
        if($('input[name="type_action"]').length)
        {
            var type_action = $('input[name="type_action"]:checked').val();
            $('.form-group.type_action').hide();
            if(type_action)
                $('.form-group.type_action.'+type_action).show();
            ets_sc_rule.displayRowActionRule();
        }
        if($('#code').length >0)
        {
            if($('#code').val()=='')
                $('.ets_sc_row_highlight').hide();
            else
                $('.ets_sc_row_highlight').show();
        }
        if($('input[name="specific_category"]').length)
        {
            if($('input[name="specific_category"]:checked').val()=='all_product')
                $('.form-group.specific_product').hide();
            else
                $('.form-group.specific_product').show();
        }
    },
    gencode :function(size)
    {
        code = '';
    	/* There are no O/0 in the codes in order to avoid confusion */
    	var chars = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
    	for (var i = 1; i <= size; ++i)
    		code += chars.charAt(Math.floor(Math.random() * chars.length));
        $('input[name="code"]').val(code).change(); 
    },
    searchCustomer : function() {
        if ($('.sc_ids_customer').length > 0 && $('.sc_search_customer').length > 0 && typeof sc_link_search_customer !== "undefined")
        {
            var sc_autocomplete = $('.sc_search_customer');
            sc_autocomplete.autocomplete(sc_link_search_customer, {
                resultsClass: "sc_results",
                minChars: 1,
                delay: 300,
                appendTo: '.sc_search_customer_form',
                autoFill: false,
                max: 20,
                matchContains: false,
                mustMatch: false,
                scroll: true,
                cacheLength: 100,
                scrollHeight: 180,
                extraParams: {
                    excludeIds: $('input[name="id_customers"]').val(),
                },
                formatItem: function (item) {
                    return '<span data-item-id="'+item[0]+'" class="sc_item_title">' +item[1]+' ('+item[2]+')</span>';
                },
            }).result(function (event, data, formatted) {
                if (data)
                {
                    ets_sc_rule.addCustomer(data);
                }
            });
        }
    },
    searchProduct : function() {
        if ($('.form_codition .sc_ids_product').length > 0 && $('.form_codition .sc_search_product').length > 0 && typeof sc_link_search_product !== "undefined")
        {
            var sc_autocomplete = $('.form_codition .sc_search_product');
            sc_autocomplete.autocomplete(sc_link_search_product, {
                resultsClass: "sc_results product_result",
                minChars: 1,
                delay: 300,
                appendTo: '.sc_search_product_form',
                autoFill: false,
                max: 20,
                matchContains: false,
                mustMatch: false,
                scroll: true,
                cacheLength: 100,
                scrollHeight: 180,
                formatItem: function (item) {
                    return '<span data-item-id="'+item[0]+'-'+item[1]+'" class="sc_item_title">' + (item[5] ? '<img src="'+item[5]+'" alt=""/> ' : '') + item[2] + (item[3]? item[3] : '') + (item[4] ? ' (Ref:' + item[4] + ')' : '') + '</span>';
                },
            }).result(function (event, data, formatted) {
                if (data)
                {
                    ets_sc_rule.addProduct(data);
                }
            });
        }
    },
    addCustomer : function(data)
    {
        var input_name = 'id_customers';
        if ($('.form_codition #block_search_'+input_name).length > 0 && $('.form_codition #block_search_'+input_name+' .sc_customer_item[data-id="'+data[0]+'"]').length==0)
        {
            if ($('.form_codition #block_search_'+input_name+' .sc_customer_loading.active').length <=0)
            {
                $('.form_codition #block_search_'+input_name+' .sc_customer_loading').addClass('active');
                var row_html ='<li class="sc_customer_item " data-id="'+data[0]+'">';
                    row_html +='<div class="sc_cusotmer_info"><span class="customer_name">'+data[1]+' ('+data[2]+')</span></div>';
                    row_html +='<div class="sc_block_item_close" title="'+Delete_text+'">';
                    row_html += '<i class="ets_svg_fill_lightgray"><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"> </svg></i>';    
                    row_html +='</div>';
                row_html +='</li>';
                $('.form_codition #block_search_'+input_name+' .sc_customer_loading.active').before(row_html);
                $('.form_codition #block_search_'+input_name+' .sc_customer_loading').removeClass('active');
                $('.form_codition .sc_search_customer').val('');
                if (!$('.form_codition input[name="'+input_name+'"]').val()) 
                {
                    $('.form_codition input[name="'+input_name+'"]').val(data[0]);
                } 
                else 
                {
                    if ($('.form_codition input[name="'+input_name+'"]').val().split(',').indexOf(data[0]) == -1) 
                    {
                        $('.form_codition input[name="'+input_name+'"]').val($('input[name="'+input_name+'"]').val() + ',' + data[0]);

                    } 
                    else 
                    {
                        showErrorMessage(data[2].toString() + ' has been tagged.');
                    }
                }
            }
        }
        $('.form_codition .sc_search_customer').val('');
    },
    addProduct: function (data) {
        var input_name = name_sc_search_product ;
        if ($('.form_codition #block_search_'+input_name).length > 0 && $('.form_codition #block_search_'+input_name+' .sc_product_item[data-id="'+data[0] + '-' + data[1]+'"]').length==0)
        {
            if ($('.form_codition #block_search_'+input_name+' .sc_product_loading.active').length <=0)
            {
                $('.form_codition #block_search_'+input_name+' .sc_product_loading').addClass('active');
                var row_html ='<li class="sc_product_item " data-id="'+data[0]+'-'+data[1]+'">';
                    row_html +='<a class="product_img_link" href="'+data[6]+'" target="_blank">';
                        row_html +='<img class="sc_product_image" src="'+data[5]+'" alt="'+data[2]+ (data[3] ? ' - '+data[3]:'')+'">';
                        row_html +='<div class="sc_product_info"><span class="product_name">'+data[2]+ (data[3] ? ' - '+data[3]:'')+'</span></div>';
                    row_html +='</a>';
                    row_html +='<div class="sc_block_item_close" title="'+Delete_text+'">';
                    row_html += '<i class="ets_svg_fill_lightgray"><svg width="14" height="14" viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1490 1322q0 40-28 68l-136 136q-28 28-68 28t-68-28l-294-294-294 294q-28 28-68 28t-68-28l-136-136q-28-28-28-68t28-68l294-294-294-294q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 294 294-294q28-28 68-28t68 28l136 136q28 28 28 68t-28 68l-294 294 294 294q28 28 28 68z"> </svg></i>';    
                    row_html +='</div>';
                row_html +='</li>';
                $('.form_codition #block_search_'+input_name+' .sc_product_loading.active').before(row_html);
                $('.form_codition #block_search_'+input_name+' .sc_product_loading').removeClass('active');
                if (!$('.form_codition input[name="'+input_name+'"]').val()) 
                {
                    $('.form_codition input[name="'+input_name+'"]').val(data[0] + '-' + data[1]);
                } 
                else 
                {
                    if ($('.form_codition input[name="'+input_name+'"]').val().split(',').indexOf(data[0] + '-' + data[1]) == -1) 
                    {
                        $('.form_codition input[name="'+input_name+'"]').val($('input[name="'+input_name+'"]').val() + ',' + data[0] + '-' + data[1]);

                    } 
                    else 
                    {
                        showErrorMessage(data[2].toString() + ' has been tagged.');
                    }
                }
            }
        }
        $('.form_codition .sc_search_product').val('');
    },
    removeIds: function (parent, element) {
        var ax = -1;
        if ((ax = parent.indexOf(element)) !== -1)
        {
            parent.splice(ax, 1);
        }
        return parent;
    },
    changeRange : function($range){
        if($range.val()<=1){
            $range.next('.range_new').next('.input-group-unit').html($range.val());
        }
        else
        {
            $range.next('.range_new').next('.input-group-unit').html($range.val());
        }
        var newPoint = ($range.val() - $range.attr("min")) / ($range.attr("max") - $range.attr("min"));
        var offset = -1;
        var  percent = ( $range.val() / $range.attr("max") )*100;
        
        $range.next('.range_new').find('.range_new_run').css({width: percent+'%'});
        $range.next('.range_new').next('.input-group-unit').css({left: percent+'%'});
        /**/
        var range = $range,value = range.next().next('.input-group-unit');
        var max = range.attr('max');
        var min = range.attr('min');
        var beginvalue = range.value;
        range.on('input', function(){
          value.html(this.value);
          var current_value = this.value;
          var max = range.attr('max'),
                        min = range.attr('min'),
                        percent = (current_value / max) * 100;
          range.next('.range_new').find('.range_new_run').css('width', percent + '%');
          range.next('.range_new').next('.input-group-unit').css({left: percent+'%'});
        });
    },
    changeDayOfMonth: function($row)
    {
        
        if($row.find('.specific_occasion_month_of_year').length)
        {
            var row_month = $row.find('.specific_occasion_month_of_year').val()
            if(row_month==2)
            {
                $row.find('.day_month_of_year_from option[value="30"]').hide();
                $row.find('.day_month_of_year_to option[value="30"]').hide();
                if($row.find('.day_month_of_year_from').val()==30)
                    $row.find('.day_month_of_year_from').val('').change();
                if($row.find('.day_month_of_year_to').val()==30)
                    $row.find('.day_month_of_year_to').val('').change();
            }
            else
            {
                $row.find('.day_month_of_year_from option[value="30"]').show();
                $row.find('.day_month_of_year_to option[value="30"]').show();
            }
            if(row_month==2 || row_month==4 || row_month==6 || row_month==9 || row_month==11)
            {
                $row.find('.day_month_of_year_from option[value="31"]').hide();
                $row.find('.day_month_of_year_to option[value="31"]').hide();
                if($row.find('.day_month_of_year_from').val()==31)
                    $row.find('.day_month_of_year_from').val('').change();
                if($row.find('.day_month_of_year_to').val()==31)
                    $row.find('.day_month_of_year_to').val('').change();
            }
            else
            {
                $row.find('.day_month_of_year_from option[value="31"]').show();
                $row.find('.day_month_of_year_to option[value="31"]').show();
            }
        }
    },
    topFunction : function() {
          document.body.scrollTop = 0; // For Safari
          document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
}