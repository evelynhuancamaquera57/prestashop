/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */
$(document).ready(function(){
    if($('.ets_sc_display_popup').length)
    {
        $('.ets_sc_display_popup').each(function(){
           if($(this).data('delay') >0)
           {
                var time_delay = $(this).data('delay')*1000;
                var popup = $(this);
                setTimeout(function(){
                    popup.addClass('show');
                },time_delay);
           }
           else
              $(this).addClass('show');  
        });
    }
    $(document).on('click','.close_highlight_bar',function(){
       $(this).parents('.ets_sc_display_highlight_bar').removeClass('show'); 
       var id_promote = $(this).data('id');
       var link_ajax = $(this).data('href');
        $.ajax({
            url: link_ajax,
            data: 'submitEtsScCloseHighlightBar='+id_promote,
            type: 'post',
            dataType: 'json',                
            success: function(json){ 
            
            }
        });
    });
    $(document).on('click','.ets_sc_close_popup',function(){
        $(this).parents('.ets_sc_display_popup').removeClass('show');
        var id_promote = $(this).data('id');
        var link_ajax = $(this).data('href');
        $.ajax({
            url: link_ajax,
            data: 'submitEtsScClosePopup='+id_promote,
            type: 'post',
            dataType: 'json',                
            success: function(json){ 
            
            }
        });
    });
    $(document).on('click','.add-to-cart-gift-product',function(e){
        e.preventDefault();
        if($(".js-cart-payment-step-refresh").length)
            $(".js-cart-payment-step-refresh").addClass('js-cart-payment-step-refresh2').removeClass('js-cart-payment-step-refresh');
        if(!$(this).hasClass('loading'))
        {
            $(this).addClass('loading');
            var $this = $(this);
            var url_link = $(this).data('link');
            $.ajax({
                url: url_link,
                data: 'action=update&ajax=1',
                type: 'post',
                dataType: 'json',                
                success: function(json){ 

                    if(json.hasError && json.errors)
                    {
                        alert(json.errors[0]);
                        $this.removeClass('loading');
                    }
                    else
                    {
                        prestashop.emit("updateCart", {
                            reason: {
                                idProduct: json.id_product,
                                idProductAttribute: json.id_product_attribute,
                                idCustomization: 0,
                                linkAction: "",
                                cart: json.cart
                            },
                            resp: json
                        });
                    }
                    
                }
            });
        }
    });
    if($('.cart-grid-body .ets_sc_gift_products').length)
    {
        $(document ).ajaxComplete(function( event, xhr, settings ) {
            if( xhr.responseText && xhr.responseText.indexOf('cart_detailed')>=0)
            {
                setTimeout(function(){
                    if($('.li_ets_sc_gift_products').length)
                    {
                        $('.cart-grid-body .ets_sc_gift_products').replaceWith($('.li_ets_sc_gift_products').html());
                    }
                    else
                        $('.cart-grid-body .ets_sc_gift_products').html('');
                },1000);
                var data = JSON.parse(xhr.responseText);
                if(data.cart_sumary_products)
                {
                    $('.cart-grid-right .cart-summary-products').replaceWith(data.cart_sumary_products);
                }
            }
        });
    }
    //custom hook;
    if ($('#js-product-list-header').length > 0) {
        if ($('#etsSChookDisplayProductListHeaderBefore').length > 0) {
            $('#js-product-list-header').prepend($('#etsSChookDisplayProductListHeaderBefore').html());
        }
        if ($('#etsSChookDisplayProductListHeaderAfter').length > 0) {
            $('#js-product-list-header').append($('#etsSChookDisplayProductListHeaderAfter').html());
        }
    }
    if ($('.content_scene_cat').length > 0) {
        if ($('#etsSChookDisplayProductListHeaderBefore').length > 0) {
            $('.content_scene_cat').before($('#etsSChookDisplayProductListHeaderBefore').html());
        }
        if ($('#etsSChookDisplayProductListHeaderAfter').length > 0) {
            $('.content_scene_cat').after($('#etsSChookDisplayProductListHeaderAfter').html());
        }
    }
    if ($('#left-column').length > 0) {
        if ($('#etsSChookDisplayLeftColumnBefore').length > 0) {
            $('#left-column').prepend($('#etsSChookDisplayLeftColumnBefore').html());
        }
    }
    if ($('#left_column').length > 0) {
        if ($('#etsSChookDisplayLeftColumnBefore').length > 0) {
            $('#left_column').prepend($('#etsSChookDisplayLeftColumnBefore').html());
        }
    }
    if ($('#right-column').length > 0) {
        if ($('#etsSChookDisplayRightColumnBefore').length > 0) {
            $('#right-column').prepend($('#etsSChookDisplayRightColumnBefore').html());
        }
    }
    if ($('#right_column').length > 0) {
        if ($('#etsSChookDisplayRightColumnBefore').length > 0) {
            $('#right_column').prepend($('#etsSChookDisplayRightColumnBefore').html());
        }
    }
    if ($('.product-variants').length > 0) {
        if ($('#etsSChookDisplayProductVariantsBefore').length > 0) {
            $('.product-variants').prepend($('#etsSChookDisplayProductVariantsBefore').html());
        }
    }
    if ($('.product_attributes').length > 0) {
        if ($('#etsSChookDisplayProductVariantsBefore').length > 0) {
            $('.product_attributes').prepend($('#etsSChookDisplayProductVariantsBefore').html());
        }
    }
    if ($('.product-variants').length > 0) {
        if ($('#etsSChookDisplayProductVariantsAfter').length > 0) {
            $('.product-variants').append($('#etsSChookDisplayProductVariantsAfter').html());
        }
    }
    if ($('.product_attributes').length > 0) {
        if ($('#etsSChookDisplayProductVariantsAfter').length > 0) {
            $('.product_attributes').append($('#etsSChookDisplayProductVariantsAfter').html());
        }
    }
    if ($('#product-comments-list-header').length > 0) {
        if ($('#etsSChookDisplayProductCommentsListHeaderBefore').length > 0) {
            $('#product-comments-list-header').before($('#etsSChookDisplayProductCommentsListHeaderBefore').html());
        }
    }
    if ($('.cart-grid-body').length > 0) {
        if ($('#etsSChookDisplayCartGridBodyBefore1').length > 0) {
            $('.cart-grid-body').prepend($('#etsSChookDisplayCartGridBodyBefore1').html());
        }
        if ($('#etsSChookDisplayCartGridBodyBefore2').length > 0) {
            $('.cart-grid-body').prepend($('#etsSChookDisplayCartGridBodyBefore2').html());
        }
        if ($('#etsSChookDisplayCartGridBodyAfter').length > 0) {
            $('.cart-grid-body').append($('#etsSChookDisplayCartGridBodyAfter').html());
        }
    }
    if ($('#order_step').length > 0){
        if ($('#etsSChookDisplayCartGridBodyBefore2').length > 0) {
            $('#order_step').after($('#etsSChookDisplayCartGridBodyBefore2').html());
        }
    }
    if($('.cart_navigation').length > 0){
        if ($('#etsSChookDisplayCartGridBodyAfter').length > 0) {
            $('.cart_navigation').after($('#etsSChookDisplayCartGridBodyAfter').html());
        }
    }
    if($('body#product').length)
    {
         $(document ).ajaxComplete(function( event, xhr, settings ) {
            if(xhr.responseText && xhr.responseText.indexOf("product_prices")>=0)
            {
                 if ($('.product-variants').length > 0) {
                if ($('#etsSChookDisplayProductVariantsBefore').length > 0) {
                    $('.product-variants').prepend($('#etsSChookDisplayProductVariantsBefore').html());
                }
            }
            if ($('.product_attributes').length > 0) {
                if ($('#etsSChookDisplayProductVariantsBefore').length > 0) {
                    $('.product_attributes').prepend($('#etsSChookDisplayProductVariantsBefore').html());
                }
            }
            if ($('.product-variants').length > 0) {
                if ($('#etsSChookDisplayProductVariantsAfter').length > 0) {
                    $('.product-variants').append($('#etsSChookDisplayProductVariantsAfter').html());
                }
            }
            if ($('.product_attributes').length > 0) {
                if ($('#etsSChookDisplayProductVariantsAfter').length > 0) {
                    $('.product_attributes').append($('#etsSChookDisplayProductVariantsAfter').html());
                }
            }
            if ($('#product-comments-list-header').length > 0) {
                if ($('#etsSChookDisplayProductCommentsListHeaderBefore').length > 0) {
                    $('#product-comments-list-header').before($('#etsSChookDisplayProductCommentsListHeaderBefore').html());
                }
            }
            }
        });
    }
});