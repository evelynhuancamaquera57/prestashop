<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }
class Ets_sc_condition_rule extends Ets_sc_obj
{
    public static $instance;
    public $id_ets_sc_shipping_rule;
    public $id_ets_sc_condition_rule;
    public $parent_codition;
    public $id_customers;
    public $id_groups;
    public $only_apply_on_default_group;
    public $customer_signed_up_from;
    public $customer_signed_up_to;
    public $days_since_signed_up_cal;
    public $days_since_singed_up_day; 
    public $cart_amount_cal;
    public $cart_amount;
    public $cart_amount_tax_incl;
    public $cart_amount_shipping_incl;
    public $cart_amount_discount_incl;
    public $specific_occasion;
    public $specific_occasion_hour_of_day_from;
    public $specific_occasion_hour_of_day_to;
    public $specific_occasion_day_of_week;
    public $specific_occasion_day_of_week_from;
    public $specific_occasion_day_of_week_to;
    public $specific_occasion_month_of_year;
    public $specific_occasion_month_of_year_from;
    public $specific_occasion_month_of_year_to;
    public $specific_occasion_date_from;
    public $specific_occasion_date_to;
    public $total_weight_cal;
    public $total_weight;
    public $total_product_quantity_cal;
    public $total_product_quantity;
    public $quantity_of_same_product_cal;
    public $quantity_of_same_product;
    public $number_of_different_product_cal;
    public $number_of_different_product;
    public $number_of_product_in_same_category_cal;
    public $number_of_product_in_same_category;
    public $apply_for_discounted_products;
    public $products_with_different_attribute;
    public $applicable_product_categories;
    public $include_specific_products;
    public $exclude_products;
    public $apply_all_attribute;
    public $select_attributes;
    public $apply_all_features;
    public $select_features;
    public $apply_all_supplier;
    public $select_suppliers;
    public $apply_all_manufacturer;
    public $select_manufacturers;
    public $apply_for_product_price_cal;
    public $apply_for_product_price;
    public $apply_for_availabled_quantity_stock_cal;
    public $apply_for_availabled_quantity_stock;
    public $include_sub_categories;
    public $applicable_categories;
    public $delivery_zone;
    public $delivery_zipcode;
    public $delivery_country_zone;
    public $delivery_country;
    public $delivery_state_zone;
    public $delivery_state_country;
    public $delivery_state;
    public $delivery_zipcode_type;
    public $delivery_zipcode_from;
    public $delivery_zipcode_to;
    public $delivery_zipcode_start_from;
    public $delivery_zipcode_end_at;
    public $delivery_zipcode_is_exactly;
    public $delivery_zipcode_different;
    public static $definition = array(
		'table' => 'ets_sc_condition_rule',
		'primary' => 'id_ets_sc_condition_rule',
		'fields' => array(
            'id_ets_sc_shipping_rule' => array('type' => self::TYPE_INT),
            'parent_codition' => array('type' => self::TYPE_STRING),
            'id_customers' => array('type' => self::TYPE_STRING),
            'id_groups' => array('type' => self::TYPE_STRING),
            'only_apply_on_default_group' => array('type' => self::TYPE_INT),
            'customer_signed_up_from' => array('type' => self::TYPE_DATE),
            'customer_signed_up_to' => array('type' => self::TYPE_DATE),
            'days_since_signed_up_cal' => array('type' => self::TYPE_HTML),
            'days_since_singed_up_day' => array('type' => self::TYPE_INT), 
            'cart_amount_cal' => array('type' => self::TYPE_HTML),
            'cart_amount' => array('type' => self::TYPE_FLOAT),
            'cart_amount_tax_incl' => array('type' => self::TYPE_INT),
            'cart_amount_shipping_incl' => array('type' => self::TYPE_INT),
            'cart_amount_discount_incl' => array('type' => self::TYPE_INT),     
            'specific_occasion' => array('type' => self::TYPE_STRING),
            'specific_occasion_hour_of_day_from' => array('type' => self::TYPE_STRING),
            'specific_occasion_hour_of_day_to' => array('type' => self::TYPE_STRING),
            'specific_occasion_day_of_week' => array('type' => self::TYPE_STRING),
            'specific_occasion_day_of_week_from' => array('type' => self::TYPE_STRING),
            'specific_occasion_day_of_week_to' => array('type' => self::TYPE_STRING),
            'specific_occasion_month_of_year' => array('type' => self::TYPE_STRING),
            'specific_occasion_month_of_year_from' => array('type' => self::TYPE_STRING),
            'specific_occasion_month_of_year_to' => array('type' => self::TYPE_STRING),
            'specific_occasion_date_from' => array('type' => self::TYPE_STRING),
            'specific_occasion_date_to' => array('type' => self::TYPE_STRING),   
            'total_product_quantity_cal' => array('type' => self::TYPE_HTML),
            'total_product_quantity' => array('type' => self::TYPE_INT),
            'total_weight_cal' => array('type' => self::TYPE_HTML),
            'total_weight' => array('type' => self::TYPE_FLOAT),
            'quantity_of_same_product_cal' => array('type' => self::TYPE_HTML),
            'quantity_of_same_product' => array('type' => self::TYPE_INT),
            'number_of_different_product_cal' => array('type' => self::TYPE_HTML),
            'number_of_different_product' => array('type' => self::TYPE_INT),
            'number_of_product_in_same_category_cal' => array('type' => self::TYPE_HTML),
            'number_of_product_in_same_category' => array('type' => self::TYPE_INT),
            'apply_for_discounted_products' => array('type' => self::TYPE_INT),
            'products_with_different_attribute' => array('type' => self::TYPE_INT),
            'applicable_product_categories' => array('type' => self::TYPE_STRING),
            'include_specific_products' => array('type' => self::TYPE_STRING),
            'exclude_products' => array('type' => self::TYPE_STRING),
            'apply_all_attribute' => array('type' => self::TYPE_INT),
            'select_attributes' => array('type' => self::TYPE_STRING),
            'apply_all_features' => array('type' => self::TYPE_INT),
            'select_features' => array('type' => self::TYPE_STRING),
            'apply_all_supplier' => array('type' => self::TYPE_INT),
            'select_suppliers' => array('type' => self::TYPE_STRING),
            'apply_all_manufacturer' => array('type' => self::TYPE_INT),
            'select_manufacturers' => array('type' => self::TYPE_STRING),
            'apply_for_product_price_cal' => array('type' => self::TYPE_HTML),
            'apply_for_product_price' => array('type' => self::TYPE_FLOAT),
            'apply_for_availabled_quantity_stock_cal' => array('type' => self::TYPE_HTML),
            'apply_for_availabled_quantity_stock' => array('type' => self::TYPE_INT),
            'include_sub_categories' => array('type' => self::TYPE_INT),
            'applicable_categories' => array('type' => self::TYPE_STRING),
            'delivery_zone' => array('type' => self::TYPE_STRING),
            'delivery_country_zone' => array('type' => self::TYPE_INT),
            'delivery_country' => array('type' => self::TYPE_STRING),
            'delivery_state_zone' => array('type' => self::TYPE_INT),
            'delivery_state_country' => array('type' => self::TYPE_INT),
            'delivery_state' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_type' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_from' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_to' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_start_from' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_end_at' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_is_exactly' => array('type' => self::TYPE_STRING),
            'delivery_zipcode_different' => array('type' => self::TYPE_STRING),
        )
    );
    public	function __construct($id_item = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_item, $id_lang, $id_shop);
        $this->delivery_zipcode='';
	}
    public static function getInstance()
    {
        if (!(isset(self::$instance)) || !self::$instance) {
            self::$instance = new Ets_sc_condition_rule();
        }
        return self::$instance;
    }
    public function l($string,$file_name='')
    {
        return Translate::getModuleTranslation('ets_shippingcost', $string, $file_name ? : pathinfo(__FILE__, PATHINFO_FILENAME));
    }
    public function getListFields()
    {
        $configs = array(
            'id_ets_sc_shipping_rule' => array(
                'type' => 'hidden',
                'label' => '',
                'default' => $this->id_ets_sc_shipping_rule,
                'validate'=>'isUnsignedId',
            ),
            'parent_codition'=>array(
                'type'=>'select',
                'label'=>$this->l('Condition'),
                'options' => array(
                    'optiongroup' => array(
                        'query' => array(
                            array(
                                'label' => $this->l('Customer'),
                                'options' => array(
                                    array(
                                        'label' => $this->l('Specific customer'),
                                        'value' => 'specific_customer'
                                    ),
                                    array(
                                        'label' => $this->l('Customer group'),
                                        'value' => 'customer_group'
                                    ),
                                    array(
                                        'label' => $this->l('Customer registration time'),
                                        'value' => 'customer_membership',
                                    ),
                                )
                            ),
                            array(
                                'label' => $this->l('Delivery location'),
                                'options' => array(
                                    array(
                                        'label' => $this->l('Zone'),
                                        'value' => 'delivery_zone'
                                    ),
                                    array(
                                        'label' => $this->l('Country'),
                                        'value' => 'delivery_country'
                                    ),
                                    array(
                                        'label' => $this->l('State'),
                                        'value' => 'delivery_state',
                                    ),
                                    array(
                                        'label' => $this->l('ZIP code/ Postal code'),
                                        'value' => 'delivery_zipcode',
                                    ),
                                )
                            ),
                            array(
                                 'label' => $this->l('Shopping cart'),
                                 'options' => array(
                                    array(
                                        'label' =>$this->l('Cart amount'),
                                        'value' => 'cart_amount',
                                    ),
                                    array(
                                        'label' => $this->l('Products in cart'),
                                        'value' => 'product_in_cart',
                                    )
                                 )
                            ),
                            array(
                                'label' => $this->l('Others'),
                                'options' => array(
                                    array(
                                        'label' => $this->l('Specific occasion'),
                                        'value' => 'specific_occasion'
                                    )
                                ),
                            ),
                        ),
                        'label' => 'label',
                        
                    ),
                    'options' => array(
                        'query' => 'options',
                        'id' => 'value',
                        'name' => 'label'
                    ),
                    'default' => array(
                        'value' => '',
                        'label' => $this->l('-- Select condition --'),
                    ), 
                ),
                'required' => true,
            ),
            'delivery_zone' => array(
                'type' => 'select',
                'label' => $this->l('Select zone'),
                'multiple' => true,
                'options' => array(
                    'query' => $this->getListZones(),
                    'id' => 'id_zone',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_zone',
                'validate'=>'isCleanHtml',
                'showRequired' => true,
            ),
            'delivery_country_zone' => array(
                'type' => 'select',
                'label' => $this->l('Filter by zone'),
                'options' => array(
                    'query' => array_merge(array(array('id_zone'=>0,'name'=>'All zones')), $this->getListZones()),
                    'id' => 'id_zone',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_country',
                'validate'=>'isUnsignedId',
            ),
            'delivery_country' => array(
                'type' => 'select',
                'label' => $this->l('Select country'),
                'multiple' => true,
                'options' => array(
                    'query' => ($countries = $this->getListCountries($this->delivery_country_zone)) ? $countries :array(),
                    'id' => 'id_country',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_country',
                'validate'=>'isCleanHtml',
                'tab' => 'delivery_country',
                'showRequired' => true,
                'desc' => $this->displayDescCountry($countries ? true : false),
            ),
            'delivery_state_zone' => array(
                'type' => 'select',
                'label' => $this->l('Filter by zone'),
                'options' => array(
                    'query' => array_merge(array(array('id_zone'=>0,'name'=>'All zones')), $this->getListZones()),
                    'id' => 'id_zone',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_state',
                'validate'=>'isUnsignedId',
            ),
            'delivery_state_country' => array(
                'type' => 'select',
                'label' => $this->l('Filter by country'),
                'options' => array(
                    'query' => array_merge(array(array('id_country'=>0,'name' => $this->l('All countries'))), $this->getListCountries($this->delivery_state_zone)),
                    'id' => 'id_country',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_state',
                'validate'=>'isUnsignedId',
            ),
            'delivery_state' => array(
                'type' => 'select',
                'label' => $this->l('Select state'),
                'multiple' => true,
                'options' => array(
                    'query' => ($states = $this->getListStates($this->delivery_state_country, $this->delivery_state_zone)) ? $states : array(),
                    'id' => 'id_state',
                    'name' => 'name',
                ),
                'form_group_class' => 'parent_codition delivery_state',
                'validate'=>'isCleanHtml',
                'tab' => 'delivery_state',
                'showRequired' => true,
                'desc' => $this->displayDescState($states ? true :false),
            ),
            'delivery_zipcode' => array(
                'type' => 'html',
                'label' => '',
                'html_content' => $this->displayFormZipCode(),
                'form_group_class' => 'parent_codition delivery_zipcode',
            ),
            'id_customers' => array(
                'type' => 'search_customer',
                'label' => $this->l('Customer'),
                'form_group_class' => 'parent_codition specific_customer',
                'validate'=>'isCleanHtml',
                'showRequired' => true,
                'placeholder' => $this->l('Search by customer name, email')
            ),
            'id_groups' => array(
                'type' => 'checkbox',
                'label' => $this->l('Customer group'),
                'form_group_class' => 'parent_codition customer_group',
                'values' => array(
                    'query' => $this->getGroups(),
                    'id' => 'id_group',
                    'name' => 'name'
                ),
                'validate'=>'isCleanHtml',
                'showRequired' => true,
            ),
            'only_apply_on_default_group' => array(
                'type'=>'switch',
                'label'=>$this->l('Only apply to the default group'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'only_apply_on_default_group_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'only_apply_on_default_group_off',
                        'value' => 0,
                    )
                ),
                'form_group_class' => 'parent_codition customer_group',
                'validate'=>'isInt',
            ),
            'customer_signed_up_from' => array(
                'type' => 'date',
                'label' => $this->l('Registration date from'),
                'form_group_class' => 'parent_codition customer_membership',
                'validate' => 'isDate',
            ),
            'customer_signed_up_to' => array(
                'type' => 'date',
                'label' => $this->l('Registration date to'),
                'form_group_class' => 'parent_codition customer_membership',
                'validate' => 'isDate',
            ),
            'days_since_signed_up_cal' => array(
                'type' => 'select',
                'label' => $this->l('Days since registration'),
                'form_group_class' => 'parent_codition customer_membership',
                'validate' => 'isCleanHtml'
            ),
            'days_since_singed_up_day' => array(
                'type' => 'text',
                'label' => $this->l('Days since registration'),
                'validate'=>'isUnsignedInt',
                'form_group_class' => 'parent_codition customer_membership',
                
            ),
            'cart_amount_cal' => array(
                'label' =>$this->l('Cart amount calculator'),
                'type' => "select",
                'form_group_class' => 'parent_codition cart_amount',
                'validate' => 'isCleanHtml',
            ),
            'cart_amount' => array(
                'label' =>$this->l('Cart amount'),
                'type' => "text",
                'suffix' => Context::getContext()->currency->sign,
                'form_group_class' => 'parent_codition cart_amount',
                'validate'=>'isUnsignedFloat',
            ),
            'cart_amount_tax_incl' => array(
                'type'=>'switch',
                'label'=>$this->l('Tax included'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'cart_amount_tax_incl_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'cart_amount_tax_incl_off',
                        'value' => 0,
                    )
                ),
                'form_group_class' => 'parent_codition cart_amount',
                'validate'=>'isInt',
            ),
            'cart_amount_shipping_incl' => array(
                'type'=>'switch',
                'label'=>$this->l('Shipping included'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'cart_amount_shipping_incl_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'cart_amount_shipping_incl_off',
                        'value' => 0,
                    )
                ),
                'form_group_class' => 'parent_codition cart_amount',
                'validate'=>'isInt',
            ),
            'cart_amount_discount_incl' => array(
                'type'=>'switch',
                'label'=>$this->l('Discount included'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'cart_amount_discount_incl_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'cart_amount_discount_incl_off',
                        'value' => 0,
                    )
                ),
                'form_group_class' => 'parent_codition cart_amount',
                'validate'=>'isInt',
            ),
            'specific_occasion' => array(
                'type' => 'radio',
                'default' => 'hour_of_day',
                'label' => $this->l('Specific occasion'),
                'values' => array(
                    array(
                        'id' => 'specific_occasion_hour_of_day',
                        'value' => 'hour_of_day',
                        'label' => $this->l('Hour of day'),
                    ),
                    array(
                        'id' => 'specific_occasion_day_of_week',
                        'value' => 'day_of_week',
                        'label' => $this->l('Day of week'),
                    ),
                    array(
                        'id' => 'specific_occasion_month_of_year',
                        'value' => 'month_of_year',
                        'label' => $this->l('Month of year'),
                    ),
                    array(
                        'id' => 'specific_occasion_from_to',
                        'value' => 'from_to',
                        'label' => $this->l('From - To'),
                    ),
                ),
                'form_group_class' => 'parent_codition specific_occasion',
                'validate' => 'isCleanHtml',
            ),
            'specific_occasion_hour_of_day_from' => array(
                'label' => '',
                'type' => 'html',
                'html_content' => $this->displayFormHourOfDay(),
                'form_group_class' => 'parent_codition specific_occasion',
            ),
            'specific_occasion_day_of_week' => array(
                'type' => 'html',
                'label' => '',
                'html_content' => $this->displayFormDayOfWeek(),
                'form_group_class' => 'parent_codition specific_occasion',
            ),
            'specific_occasion_month_of_year' => array(
                'type' => 'html',
                'label' => '',
                'html_content' => $this->displayFormMonthOfYear(),
                'form_group_class' => 'parent_codition specific_occasion',
            ),
            'specific_occasion_date_from' => array(
                'type' => 'html',
                'label' => '',
                'html_content' => $this->displayFormDateFromTo(),
                'form_group_class' => 'parent_codition specific_occasion',
            ), 
            'total_weight_cal' => array(
                'type' => 'select',
                'label' => $this->l('Total weight calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'total_weight' => array(
                'type' => 'text',
                'label' => $this->l('Total weight'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isUnsignedFloat',
                'suffix' => 'Kg'
            ),
            'total_product_quantity_cal' => array(
                'type' => 'select',
                'label' => $this->l('Total product quantity calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'total_product_quantity' => array(
                'type' => 'text',
                'label' => $this->l('Total product quantity'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isUnsignedInt',
            ),
            'quantity_of_same_product_cal' => array(
                'type' => 'select',
                'label' => $this->l('Quantity of same product calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'quantity_of_same_product' => array(
                'type' => 'text',
                'label' => $this->l('Quantity of the same product'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isUnsignedInt',
            ),
            'number_of_different_product_cal' => array(
                'type' => 'select',
                'label' => $this->l('Number of different product calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'number_of_different_product' => array(
                'type' => 'text',
                'label' => $this->l('Number of different products'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isUnsignedInt',
            ),
            'number_of_product_in_same_category_cal' => array(
                'type' => 'select',
                'label' => $this->l('Number of product in same category calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'number_of_product_in_same_category' => array(
                'type' => 'text',
                'label' => $this->l('Number of product in the same category '),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isUnsignedInt',
            ),
            'apply_for_discounted_products' => array(
                'type' => 'switch',
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'apply_for_discounted_products_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'apply_for_discounted_products_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Apply for discounted products'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isInt',
                'default'=>1,
            ),
            'products_with_different_attribute' => array(
                'type' => 'switch',
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'products_with_different_attribute_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'products_with_different_attribute_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Product with different attributes are counted as different products'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isInt',
                'default'=>1,
            ),
            'applicable_product_categories' => array(
                'type' => 'radio',
                'label' => $this->l('Applicable product categories'),
                'default' => 'all_product',
                'values' => array(
                    array(
                        'id' => 'applicable_product_categories_all_product',
                        'value' => 'all_product',
                        'label' => $this->l('All product categories'),
                    ),
                    array(
                        'id' => 'applicable_product_categories_specific_product',
                        'value' => 'specific_product',
                        'label' => $this->l('Specific product categories'),
                    ),
                ),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'applicable_categories' => array(
                'label' => $this->l('Select categories to apply condition'),
                'type' => 'categories',
                'tree' => array(
                    'id' => 'loyalty-categories-tree',
                    'selected_categories' => $this->applicable_categories ? explode(',',$this->applicable_categories):array(),
                    'disabled_categories' => null,
                    'use_checkbox' => true,
                    'root_category' => Category::getRootCategory()->id
                ),
                'form_group_class' => 'parent_codition product_in_cart',
            ),
            'include_sub_categories' => array(
                'type' => 'switch',
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'include_sub_categories_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'include_sub_categories_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Include products in subcategories'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isInt',
            ),
            'include_specific_products' => array(
                'type' => 'search_product',
                'label' => $this->l('Include specific products'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'placeholder' => $this->l('Search by product name, reference, id')
            ),
            'exclude_products' => array(
                'type' => 'search_product',
                'label' => $this->l('Exclude products'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'placeholder' => $this->l('Search by product name, reference, id')
            ),
            'apply_all_attribute' => array(
                'type' => 'switch',
                'default' => 1,
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'apply_all_attribute_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'apply_all_attribute_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Apply to all attributes'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate'=>'isInt',
            ),
            'select_attributes' => array(
                'type'=> 'checkbox',
                'label' => $this->l('Select attributes'),
                'values' => array(
                    'query' => $this->getAttributes(),
                    'id' => 'id_attribute',
                    'name' => 'name'
                ),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'showRequired' => true,
            ),
            'apply_all_features' => array(
                'type' => 'switch',
                'default' => 1,
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'apply_all_features_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'apply_all_features_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Apply to all features'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate'=>'isUnsignedInt',
            ),
            'select_features' => array(
                'type'=> 'checkbox',
                'label' => $this->l('Select features'),
                'values' => array(
                    'query' => $this->getFeatures(),
                    'id' => 'id_feature',
                    'name' => 'name'
                ),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'showRequired' => true,
            ),
            'apply_all_supplier' => array(
                'type' => 'switch',
                'default' => 1,
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'apply_all_supplier_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'apply_all_supplier_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Apply to all suppliers'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate'=>'isInt',
            ),
            'select_suppliers' => array(
                'type'=> 'checkbox',
                'label' => $this->l('Select suppliers'),
                'values' => array(
                    'query' => $this->getSuppliers(),
                    'id' => 'id_supplier',
                    'name' => 'name'
                ),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'showRequired' => true,
            ),
            'apply_all_manufacturer' => array(
                'type' => 'switch',
                'default' => 1,
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'apply_all_manufacturer_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'apply_all_manufacturer_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Apply to all manufacturers'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate'=>'isInt',
            ),
            'select_manufacturers' => array(
                'type'=> 'checkbox',
                'label' => $this->l('Select manufacturers'),
                'values' => array(
                    'query' => $this->getManufacturers(),
                    'id' => 'id_manufacturer',
                    'name' => 'name'
                ),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
                'showRequired' => true,
            ),
            'apply_for_product_price_cal' => array(
                'type' => 'select',
                'label' => $this->l('Apply for product price calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'apply_for_product_price' => array(
                'type' => 'text',
                'label' => $this->l('Apply for product price'),
                'form_group_class' => 'parent_codition product_in_cart',
                'suffix' => Context::getContext()->currency->sign,
                'validate' => 'isPrice',
            ),
            'apply_for_availabled_quantity_stock_cal' => array(
                'type' => 'select',
                'label' => $this->l('Apply for available stock quantity calculator'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate' => 'isCleanHtml',
            ),
            'apply_for_availabled_quantity_stock' => array(
                'type' => 'text',
                'label' => $this->l('Apply for available stock quantity'),
                'form_group_class' => 'parent_codition product_in_cart',
                'validate'=>'isUnsignedInt',
            ),          
        );
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->id ? $this->l('Edit condition') : $this->l('Add condition') ,
                ),
                'input' => array(),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                'buttons'=> array(
                    array(
                        'title' => $this->l('Cancel'),
                        'type' => 'submit',
                        'class' => 'pull-left',
                        'name' => 'btncancel',
                        'icon' => 'process-icon-cancel',
                    )
                ),
                'name' => 'condition_rule',
                'key' => 'id_ets_sc_condition_rule',
            ),
            'configs' =>$configs, 
        );
    }
    public function getListConditionsByRule($id_shipping_rule)
    {
        $sql ='SELECT * FROM `'._DB_PREFIX_.'ets_sc_condition_rule` WHERE id_ets_sc_shipping_rule='.(int)$id_shipping_rule;
        $conditions = Db::getInstance()->executeS($sql);
        if($conditions)
        {
            foreach($conditions as &$condition)
            {
                
                switch($condition['parent_codition'])
                {
                    case 'specific_customer':
                        $condition['condition'] = $this->l('Customer - Specific customer');
                        $condition['detail'] = self::displayListCustomer($condition);
                        break;
                    case 'customer_group':
                        $condition['condition'] = $this->l('Customer - Customer group');
                        $condition['detail'] =  self::displayListGroup($condition);
                        break;
                    case 'customer_membership':
                        $condition['condition'] = $this->l('Customer - Customer registration time');
                        $condition['detail'] = self::displayCustomerMembership($condition);
                        break;
                    case 'cart_amount':
                        $condition['condition'] = $this->l('Shopping cart - Cart amount');
                        $condition['detail'] = self::displayCartAmount($condition);
                        break;
                    case 'product_in_cart':
                        $condition['condition'] = $this->l('Shopping cart - Products in cart');
                        $condition['detail'] = self::displayProductInCart($condition);
                        break;
                    case 'delivery_zone':
                        $condition['condition'] = $this->l('Delivery location - Zone');
                         $condition['detail'] = self::displayDeliveryZone($condition);
                        break;
                    case 'delivery_country':
                        $condition['condition'] = $this->l('Delivery location - Country');
                         $condition['detail'] = self::displayDeliveryCountry($condition);
                        break;
                    case 'delivery_state':
                        $condition['condition'] = $this->l('Delivery location - State');
                        $condition['detail'] = self::displayDeliveryState($condition);
                        break;
                    case 'delivery_zipcode' :
                        $condition['condition'] = $this->l('Delivery location - ZIP code/Postal code');
                        $condition['detail'] = self::displayDeliveryZipcode($condition);
                        break;
                    case 'specific_occasion':
                        $condition['condition'] = $this->l('Specific occasion');
                        $condition['detail'] =self::displaySpecificOccasions($condition);
                        break;
                    
                    default :
                        $condition['condition'] = $condition['parent_codition'];
                        $condition['detail'] =$condition['parent_codition'];
                }
            }
        }
        return $conditions;
    }
    public static function displayDeliveryZone($condition)
    {
        if(is_array($condition))
        {
            $delivery_zone = $condition['delivery_zone'];
        }
        else
        {
            $delivery_zone = $condition->delivery_zone;
        }
        if($delivery_zone && ($id_zones = explode(',',$delivery_zone)))
        {
            $context = Context::getContext();
            $sql = 'SELECT z.id_zone,z.name FROM  `'._DB_PREFIX_.'zone` z
            INNER JOIN  `'._DB_PREFIX_.'zone_shop` zs ON (z.id_zone = zs.id_zone AND zs.id_shop="'.(int)$context->shop->id.'")
            WHERE z.active=1 AND z.id_zone IN ('.implode(',',array_map('intval',$id_zones)).')';
            $zones = Db::getInstance()->executeS($sql);
	        if ($zones && count($zones)) {
		        /** @var Ets_shippingcost $module */
		        $module = Module::getInstanceByName('ets_shippingcost');
		        $content = '';
		        foreach ($zones as $zone) {
			        $content .= $module->displayText($zone['name'], 'li');
		        }
		        return $module->displayText($content, 'ul');
	        }
        }
        return '--';
    }
    public static function displayDeliveryCountry($condition)
    {
        if(is_array($condition))
        {
            $delivery_country = $condition['delivery_country'];
        }
        else
        {
            $delivery_country = $condition->delivery_country;
        }
        if($delivery_country && ($id_countries = explode(',',$delivery_country)))
        {
            $context = Context::getContext();
            $sql = 'SELECT c.id_country, cl.name FROM  `'._DB_PREFIX_.'country` c
            INNER JOIN  `'._DB_PREFIX_.'country_shop` cs ON (c.id_country = cs.id_country AND cs.id_shop="'.(int)$context->shop->id.'")
            LEFT JOIN  `'._DB_PREFIX_.'country_lang` cl ON (c.id_country= cl.id_country AND cl.id_lang="'.(int)$context->language->id.'")
            WHERE c.active=1 AND c.id_country in ('.implode(',',array_map('intval',$id_countries)).')';
            $countries = Db::getInstance()->executeS($sql);
            if ($countries && count($countries)) {
            	/** @var Ets_shippingcost $module */
            	$module = Module::getInstanceByName('ets_shippingcost');
            	$content = '';
            	foreach ($countries as $country) {
		            $content .= $module->displayText($country['name'], 'li');
	            }
	            return $module->displayText($content, 'ul');
            }
        }
        return '--';
    }
    public static function displayDeliveryState($condition)
    {
        if(is_array($condition))
        {
            $delivery_state = $condition['delivery_state'];
        }
        else
        {
            $delivery_state = $condition->delivery_state;
        }
        if($delivery_state && ($id_states = explode(',',$delivery_state)))
        {
            $sql = 'SELECT id_state,name FROM  `'._DB_PREFIX_.'state` 
            WHERE id_state IN ('.implode(',',array_map('intval',$id_states)).')';
            $states = Db::getInstance()->executeS($sql);
	        if ($states && count($states)) {
		        /** @var Ets_shippingcost $module */
		        $module = Module::getInstanceByName('ets_shippingcost');
		        $content = '';
		        foreach ($states as $state) {
			        $content .= $module->displayText($state['name'], 'li');
		        }
		        return $module->displayText($content, 'ul');
	        }
        }
    }
    public function validateCustomField(&$errors)
    {
        $parent_codition = Tools::getValue('parent_codition');
        $advance_setting = false;
        if($parent_codition=='specific_customer')
        {
            $id_customers = trim(Tools::getValue('id_customers'),',');
            if(!$id_customers)
                $errors[] = $this->l('Customer is required');
            elseif(!Validate::isCleanHtml($id_customers))
                $errors[] = $this->l('Customer is not valid');  
        }
        elseif($parent_codition=='customer_membership')
        {
            $customer_signed_up_from = Tools::getValue('customer_signed_up_from');
            $customer_signed_up_to = Tools::getValue('customer_signed_up_to');
            if($customer_signed_up_from && !Validate::isDate($customer_signed_up_from))
                $errors[]= $this->l('Registration date from is not valid');
            if($customer_signed_up_to && !Validate::isDate($customer_signed_up_to))
                $errors[]= $this->l('Registration date to is not valid');
            if($customer_signed_up_from && Validate::isDate($customer_signed_up_from) && $customer_signed_up_to && Validate::isDate($customer_signed_up_to) && strtotime($customer_signed_up_from) > strtotime($customer_signed_up_to) )
                $errors[] = $this->l('"Registration date - From" value must be smaller than "Registration date - To" value');
        }
        elseif($parent_codition=='customer_group')
        {
            $customer_group = Tools::getValue('id_groups');
            if(!$customer_group)
                $errors[] = $this->l('Customer group is required');
            elseif(!Ets_shippingcost::validateArray($customer_group))
                $errors[] = $this->l('Customer group is not valid');
        }
        elseif($parent_codition=='cart_amount')
        {
            $cart_amount = Tools::getValue('cart_amount');
            if($cart_amount=='')
                $errors[] = $this->l('Cart amount is required');
            elseif($cart_amount=='0' || !Validate::isPrice($cart_amount))
                $errors[] = $this->l('Cart amount is not valid');
        }
        elseif($parent_codition =='specific_occasion')
        {
            $specific_occasion = Tools::getValue('specific_occasion');
            if($specific_occasion=='hour_of_day')
            {
                $specific_occasion_hour_of_day_from = Tools::getValue('specific_occasion_hour_of_day_from');
                $specific_occasion_hour_of_day_to = Tools::getValue('specific_occasion_hour_of_day_to');
                if($specific_occasion_hour_of_day_from && count($specific_occasion_hour_of_day_from)!=count($specific_occasion_hour_of_day_to))
                    $errors[] = $this->l('Hour of day is not valid');
                else
                {
                    $ok = false;
                    if($specific_occasion_hour_of_day_from)
                    {
                        foreach($specific_occasion_hour_of_day_from as $index => $from)
                        {
                            if($from!=='' && isset($specific_occasion_hour_of_day_to[$index]) && $specific_occasion_hour_of_day_to[$index]!=='')
                            {
                                $ok = true;
                                break;
                            }
                        }
                    }
                    if(!$ok)
                        $errors[] = $this->l('Hour of day is required');
                    if($specific_occasion_hour_of_day_from)
                    {
                        foreach($specific_occasion_hour_of_day_from as $index => $from)
                        {
                            if($from && (!Validate::isUnsignedInt($from) || $from > 23 ))
                            {
                                $errors[] = $this->l('Hour of day is not valid');
                                break;
                            }
                            if($specific_occasion_hour_of_day_to[$index] && (!Validate::isUnsignedInt($specific_occasion_hour_of_day_to[$index]) || $specific_occasion_hour_of_day_to[$index] > 23 ) )
                            {
                                $errors[] = $this->l('Hour of day is not valid');
                                break;
                            }
                            if($from && $specific_occasion_hour_of_day_to[$index] && $from > $specific_occasion_hour_of_day_to[$index])
                            {
                                $errors[] = $this->l('"Hour of day - From" value must be smaller than "Hour of day - To" value');
                                break;
                            }    
                        }
                    }
                }
                if(!$errors)
                {
                    
                    $this->specific_occasion_hour_of_day_from = json_encode($specific_occasion_hour_of_day_from);
                    $this->specific_occasion_hour_of_day_to = json_encode($specific_occasion_hour_of_day_to);
                }
                    
            }
            elseif($specific_occasion=='day_of_week')
            {
                $specific_occasion_day_of_week_from = Tools::getValue('specific_occasion_day_of_week_from');
                $specific_occasion_day_of_week_to = Tools::getValue('specific_occasion_day_of_week_to');
                $specific_occasion_day_of_week = Tools::getValue('specific_occasion_day_of_week');
                if(!Ets_shippingcost::validateArray($specific_occasion_day_of_week))
                    $errors[] = $this->l('Day of week is not valid');
                elseif($specific_occasion_day_of_week_from)
                {
                    foreach($specific_occasion_day_of_week_from as $index => $from)
                    {
                        $ok = true;
                        if($from && !Validate::isUnsignedInt($from))
                        {
                            $errors[] = $this->l('"Day of week - From" value is not valid');
                            $ok = false;
                        }
                        if($specific_occasion_day_of_week_to[$index] && !Validate::isUnsignedInt($specific_occasion_day_of_week_to[$index]))
                        {
                            $errors[] = $this->l('"Day of week - To" value is not valid');
                            $ok = false;
                        }
                        if($from && $specific_occasion_day_of_week_to[$index] && $from > $specific_occasion_day_of_week_to[$index])
                        {
                            $errors[] = $this->l('"Day of week - From" value must be smaller than "Day of week - To" value');
                            $ok = false;
                        } 
                        if(!$ok)
                            break;
                           
                    }
                }
                if(!$errors)
                {
                    
                    $this->specific_occasion_day_of_week =  json_encode($specific_occasion_day_of_week);
                    $this->specific_occasion_day_of_week_from = json_encode($specific_occasion_day_of_week_from);
                    $this->specific_occasion_day_of_week_to = json_encode($specific_occasion_day_of_week_to);
                }
            }
            elseif($specific_occasion=='month_of_year')
            {
                $specific_occasion_month_of_year_from = Tools::getValue('specific_occasion_month_of_year_from');
                $specific_occasion_month_of_year_to = Tools::getValue('specific_occasion_month_of_year_to');
                $specific_occasion_month_of_year = Tools::getValue('specific_occasion_month_of_year');
                if($specific_occasion_month_of_year_from)
                {
                    foreach($specific_occasion_month_of_year_from as $index => $from)
                    {
                        $ok = true;
                        if($from && !Validate::isUnsignedInt($from))
                        {
                            $errors[] = $this->l('"Month of year - From" value is not valid');
                            $ok = false;
                        }
                        if($specific_occasion_month_of_year_to[$index] && !Validate::isUnsignedInt($specific_occasion_month_of_year_to[$index]) )
                        {
                            $errors[] = $this->l('"Month of year - To" value is not valid');
                            $ok = false;
                        }
                        if($from && $specific_occasion_month_of_year_to[$index] && $from > $specific_occasion_month_of_year_to[$index])
                        {
                            $errors[] = $this->l('"Month of year - From" value must be smaller than "Month of year - To" value');
                            $ok = false;
                        } 
                        if(!$ok)
                            break;  
                    }
                }
                if(!$errors)
                {
                    $this->specific_occasion_month_of_year = json_encode($specific_occasion_month_of_year);
                    $this->specific_occasion_month_of_year_from = json_encode($specific_occasion_month_of_year_from);
                    $this->specific_occasion_month_of_year_to = json_encode($specific_occasion_month_of_year_to);
                }
            }
            elseif($specific_occasion=='from_to')
            {
                $specific_occasion_date_from = Tools::getValue('specific_occasion_date_from');
                $specific_occasion_date_to = Tools::getValue('specific_occasion_date_to');
                if($specific_occasion_date_from && count($specific_occasion_date_from)!=count($specific_occasion_date_to))
                    $errors[] = $this->l('From - To value is not valid');
                else
                {
                    $ok = false;
                    if($specific_occasion_date_from)
                    {
                        foreach($specific_occasion_date_from as $index => $from)
                        {
                            if($from && isset($specific_occasion_date_to[$index]) && $specific_occasion_date_to[$index])
                            {
                                $ok = true;
                                break;
                            }
                        }
                    }
                    if(!$ok)
                        $errors[] = $this->l('From - To value is required');
                    if($specific_occasion_date_from)
                    {
                        foreach($specific_occasion_date_from as $index => $from)
                        {
                            if($from && !Validate::isDate($from))
                            {
                                $errors[] = $this->l('From - To value is not valid');
                                break;
                            }
                            if($specific_occasion_date_to[$index] && !Validate::isDate($specific_occasion_date_to[$index]))
                            {
                                $errors[] = $this->l('From - To value is not valid');
                                break;
                            }
                            if($from && $specific_occasion_date_to[$index] && strtotime($from) > strtotime($specific_occasion_date_to[$index]))
                            {
                                $errors[] = $this->l('From value must be smaller than To value');
                                break;
                            }    
                        }
                    }
                }
                if(!$errors)
                {
                    $this->specific_occasion_date_from = json_encode($specific_occasion_date_from);
                    $this->specific_occasion_date_to = json_encode($specific_occasion_date_to);
                }
            }
        }
        elseif($parent_codition =='delivery_zone')
        {
            $delivery_zone = Tools::getValue('delivery_zone');
            if(!$delivery_zone)
                $errors[] = $this->l('Zone is required');
            elseif($delivery_zone && !Ets_shippingcost::validateArray($delivery_zone))
                 $errors[] = $this->l('Zone is not valid');
        }
        elseif($parent_codition =='delivery_country')
        {
            $delivery_country = Tools::getValue('delivery_country');
            if(!$delivery_country)
                $errors[] = $this->l('Country is required');
            elseif($delivery_country && !Ets_shippingcost::validateArray($delivery_country))
                 $errors[] = $this->l('Country is not valid');
        }
        elseif($parent_codition =='delivery_state')
        {
            $delivery_state = Tools::getValue('delivery_state');
            if(!$delivery_state)
                $errors[] = $this->l('State is required');
            elseif($delivery_state && !Ets_shippingcost::validateArray($delivery_state))
                 $errors[] = $this->l('State is not valid');
        }
        elseif($parent_codition=='delivery_zipcode')
        {
            $delivery_zipcode_type = Tools::getValue('delivery_zipcode_type');
            $delivery_zipcode_from = Tools::getValue('delivery_zipcode_from');
            $delivery_zipcode_to = Tools::getValue('delivery_zipcode_to');
            $delivery_zipcode_start_from  = Tools::getValue('delivery_zipcode_start_from');
            $delivery_zipcode_end_at = Tools::getValue('delivery_zipcode_end_at');
            $delivery_zipcode_is_exactly = Tools::getValue('delivery_zipcode_is_exactly');
            $delivery_zipcode_different = Tools::getValue('delivery_zipcode_different');
            if($delivery_zipcode_type)
            {
                foreach($delivery_zipcode_type as $index=>$zipcode_type)
                {
                    if($zipcode_type=='from_to')
                    {
                        $zipcode_from = isset($delivery_zipcode_from[$index]) ? $delivery_zipcode_from[$index]:'';
                        $zipcode_to = isset($delivery_zipcode_to[$index]) ? $delivery_zipcode_to[$index]:'';
                        if($zipcode_from=='' && $zipcode_to=='')
                            $errors[] = $this->l('ZIP code From - To value is required');
                        else
                        {
                            if($zipcode_from && !Validate::isInt($zipcode_from))
                                $errors[] = $this->l('ZIP code From value is not valid');
                            if($zipcode_to && !Validate::isInt($zipcode_to))
                                $errors[] = $this->l('ZIP code To value is not valid');
                            if($zipcode_from && $zipcode_from && Validate::isInt($zipcode_from) && Validate::isInt($zipcode_to)&& $zipcode_from > $zipcode_to)
                            {
                                $errors[] = $this->l('ZIP code From value must be smaller than ZIP code To value');
                                $ok = false;
                            } 
                        }
                    }
                    elseif($zipcode_type=='start_from')
                    {
                        $start_from = isset($delivery_zipcode_start_from[$index]) ? $delivery_zipcode_start_from[$index]:'';
                        if($start_from=='')
                            $errors[] = $this->l('"Start from" is required');
                        elseif(!Validate::isPostCode($start_from))
                            $errors[] = $this->l('"Start from" is not valid');
                    }
                    elseif($zipcode_type=='end_at')
                    {
                        $end_at = isset($delivery_zipcode_end_at[$index]) ? $delivery_zipcode_end_at[$index]:'';
                        if($end_at=='')
                            $errors[] = $this->l('"End at" is required');
                        elseif(!Validate::isPostCode($end_at))
                            $errors[] = $this->l('"End at" is not valid');
                    }
                    elseif($zipcode_type=='is_exactly')
                    {
                        $is_exactly = isset($delivery_zipcode_is_exactly[$index]) ? $delivery_zipcode_is_exactly[$index]:'';
                        if($is_exactly=='')
                            $errors[] = $this->l('"Is exactly" is required');
                        elseif(!Validate::isPostCode($is_exactly))
                            $errors[] = $this->l('"Is exactly" is not valid');
                    }
                    elseif($zipcode_type=='different')
                    {
                        $different = isset($delivery_zipcode_different[$index]) ? $delivery_zipcode_different[$index]:'';
                        if($different=='')
                            $errors[] = $this->l('"Different" value is required');
                        elseif(!Validate::isPostCode($different))
                            $errors[] = $this->l('"Different" value is not valid');
                    }
                }
            }
            if(!$errors)
            {
               $this->delivery_zipcode_type = json_encode($delivery_zipcode_type);
               $this->delivery_zipcode_from = json_encode($delivery_zipcode_from);
               $this->delivery_zipcode_to = json_encode($delivery_zipcode_to);
               $this->delivery_zipcode_start_from = json_encode($delivery_zipcode_start_from) ;
               $this->delivery_zipcode_end_at = json_encode($delivery_zipcode_end_at);
               $this->delivery_zipcode_is_exactly = json_encode($delivery_zipcode_is_exactly) ;
               $this->delivery_zipcode_different = json_encode($delivery_zipcode_different);
            }
        }
        elseif($parent_codition=='product_in_cart')
        {
            $advance_setting = true;
            $total_weight = (int)Tools::getValue('total_weight');
            $total_product_quantity = (int)Tools::getValue('total_product_quantity');
            $quantity_of_same_product = (int)Tools::getValue('quantity_of_same_product');
            $number_of_different_product = (int)Tools::getValue('number_of_different_product');
            $number_of_product_in_same_category = (int)Tools::getValue('number_of_product_in_same_category');
            if(!$total_weight && !$total_product_quantity && !$quantity_of_same_product && !$number_of_different_product && !$number_of_product_in_same_category)
                $errors[] = $this->l('The products in the cart need to meet at least one of the conditions listed below.');
        }
        if($advance_setting)
        {
            $apply_all_attribute = (int)Tools::getValue('apply_all_attribute');
            $select_attributes = Tools::getValue('select_attributes');
            $apply_all_features = (int)Tools::getValue('apply_all_features');
            $select_features = Tools::getValue('select_features');
            $apply_all_manufacturer = (int)Tools::getValue('apply_all_manufacturer');
            $select_manufacturers = Tools::getValue('select_manufacturers');
            $apply_all_supplier = (int)Tools::getValue('apply_all_supplier');
            $select_suppliers = Tools::getValue('select_suppliers');
            if(!$apply_all_attribute && !$select_attributes)
                $errors[] = $this->l('"Select attributes" is required');
            elseif($select_attributes && !Ets_shippingcost::validateArray($select_attributes))
                $errors[] = $this->l('"Select attributes" is not valid');
            if(!$apply_all_features && !$select_features)
                $errors[] = $this->l('"Select features" is required');
            elseif($select_features && !Ets_shippingcost::validateArray($select_features))
                $errors[] = $this->l('"Select features" is not valid');
            if(!$apply_all_supplier && !$select_suppliers)
                $errors[] = $this->l('"Select suppliers" is required');
            elseif($select_suppliers && !Ets_shippingcost::validateArray($select_suppliers))
                $errors[] = $this->l('"Select suppliers" is not valid');
            if(!$apply_all_manufacturer && !$select_manufacturers)
                $errors[] = $this->l('"Select manufacturers" is required');
            elseif($select_manufacturers && !Ets_shippingcost::validateArray($select_manufacturers))
                $errors[] = $this->l('"Select manufacturers" is not valid');
        }
    }
    public static function getCustomersByIds($id_customers)
    {
        if(!is_array($id_customers))
        {
            $id_customers = explode(',',$id_customers);
        }
        $customers = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'customer` WHERE id_customer IN ('.implode(',',array_map('intval',$id_customers)).')');
        return $customers;    
    }
    public static function displayListCustomer($condition)
    {
        if(is_array($condition))
        {
            $id_customers = $condition['id_customers'];
        }
        else
        {
            $id_customers = $condition->id_customers;
        }
        if($id_customers)
        {
            $customers = self::getCustomersByIds($id_customers);
            if($customers)
            {
                Context::getContext()->smarty->assign(
                    array(
                        'condition_customers' => $customers,
                    )
                );
                return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/specific_customer.tpl');
            }
        }
    }
    public static function displayListGroup($condition)
    {
        if(is_array($condition))
        {
            $id_groups = $condition['id_groups'];
            $only_apply_on_default_group = (int)$condition['only_apply_on_default_group'];
        }
        else
        {
            $id_groups = $condition->id_groups;
            $only_apply_on_default_group = (int)$condition->only_apply_on_default_group;
        }
        if($id_groups!='all')
        {
            $id_groups = explode(',',$id_groups);
            $customer_groups = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'group` g
            INNER JOIN `'._DB_PREFIX_.'group_shop` gs ON (g.id_group = gs.id_group AND gs.id_shop="'.(int)Context::getContext()->shop->id.'")
            LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON (g.id_group = gl.id_group AND gl.id_lang="'.(int)Context::getContext()->language->id.'")
            WHERE g.id_group IN ('.implode(',',array_map('intval',$id_groups)).')');
        }
        else
            $customer_groups = 'all';
        Context::getContext()->smarty->assign(
            array(
                'customer_groups' => $customer_groups,
                'only_apply_on_default_group' => $only_apply_on_default_group,
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/customer_group.tpl');
    }
    public static function displayCustomerMembership($condition)
    {
        if(is_array($condition))
        {
            $customer_signed_up_from = $condition['customer_signed_up_from'];
            $customer_signed_up_to = $condition['customer_signed_up_to'];
            $days_since_signed_up_cal = $condition['days_since_signed_up_cal'];
            $days_since_singed_up_day = $condition['days_since_singed_up_day'];
        }
        else
        {
            $customer_signed_up_from = $condition->customer_signed_up_from;
            $customer_signed_up_to = $condition->customer_signed_up_to;
            $days_since_signed_up_cal = $condition->days_since_signed_up_cal;
            $days_since_singed_up_day = $condition->days_since_singed_up_day;
        }
        Context::getContext()->smarty->assign(
            array(
                'customer_signed_up_from' => $customer_signed_up_from!='0000-00-00 00:00:00' ? $customer_signed_up_from:'',
                'customer_signed_up_to' => $customer_signed_up_to!='0000-00-00 00:00:00' ? $customer_signed_up_to :'',
                'days_since_signed_up_cal' => $days_since_signed_up_cal,
                'days_since_singed_up_day' => $days_since_singed_up_day,
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/customer_membership.tpl');
    } 
    public static function displayCartAmount($condition)
    {
        if(is_array($condition))
        {
            $cart_amount_cal = $condition['cart_amount_cal'];
            $cart_amount = $condition['cart_amount'];
            $cart_amount_tax_incl = (int)$condition['cart_amount_tax_incl'];
            $cart_amount_shipping_incl = (int)$condition['cart_amount_shipping_incl'];
            $cart_amount_discount_incl = (int)$condition['cart_amount_discount_incl'];
        }
        else
        {
            $cart_amount_cal = $condition->cart_amount_cal;
            $cart_amount = $condition->cart_amount;
            $cart_amount_tax_incl = (int)$condition->cart_amount_tax_incl;
            $cart_amount_shipping_incl = (int)$condition->cart_amount_shipping_incl;
            $cart_amount_discount_incl = (int)$condition->cart_amount_discount_incl;
        }
        Context::getContext()->smarty->assign(
            array(
                'cart_amount_cal' => $cart_amount_cal,
                'cart_amount' => Tools::displayPrice($cart_amount),
                'cart_amount_tax_incl' => $cart_amount_tax_incl,
                'cart_amount_shipping_incl' =>$cart_amount_shipping_incl,
                'cart_amount_discount_incl' => $cart_amount_discount_incl,
            )
        ); 
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/cart_amount.tpl');
    }
    public function getHours()
    {
        return array(
            '0' => '00:00',
            '1' => '01:00',
            '2' => '02:00',
            '3' => '03:00',
            '4' => '04:00',
            '5' => '05:00',
            '6' => '06:00',
            '7' => '07:00',
            '8' => '08:00',
            '9' => '09:00',
            '10' => '10:00',
            '11' => '11:00',
            '12' => '12:00',
            '13' => '13:00',
            '14' => '14:00',
            '15' => '15:00',
            '16' => '16:00',
            '17' => '17:00',
            '18' => '18:00',
            '19' => '19:00',
            '20' => '20:00',
            '21' => '21:00',
            '22' => '22:00',
            '23' => '23:00',
        );
    }
    public function getDays()
    {
        return array(
            '1' => $this->l('Monday'),
            '2' => $this->l('Tuesday'),
            '3' => $this->l('Wednesday'),
            '4' => $this->l('Thursday'),
            '5' => $this->l('Friday'),
            '6' => $this->l('Saturday'),
            '7' => $this->l('Sunday'),
        );
    }
    public function getMonths()
    {
        return array(
            '1' => $this->l('January'),
            '2' => $this->l('February'),
            '3' => $this->l('March'),
            '4' => $this->l('April'),
            '5' => $this->l('May'),
            '6' => $this->l('June'),
            '7' => $this->l('July'),
            '8' => $this->l('August'),
            '9' => $this->l('September'),
            '10' => $this->l('October'),
            '11' => $this->l('November'),
            '12' => $this->l('December'),
        );
    }
    public function displayFormDateFromTo()
    {
        Context::getContext()->smarty->assign(
            array(
                'specific_occasion_date_from' => $this->specific_occasion_date_from ? json_decode($this->specific_occasion_date_from,true):array(),
                'specific_occasion_date_to' => $this->specific_occasion_date_to ? json_decode($this->specific_occasion_date_to,true):array(),
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/from_to.tpl');
    }
    public function displayFormMonthOfYear()
    {
        Context::getContext()->smarty->assign(
            array(
                'specific_occasion_month_of_year' => $this->specific_occasion_month_of_year ? json_decode($this->specific_occasion_month_of_year,true):array(),
                'specific_occasion_month_of_year_from' => $this->specific_occasion_month_of_year_from ? json_decode($this->specific_occasion_month_of_year_from,true):array(),
                'specific_occasion_month_of_year_to' => $this->specific_occasion_month_of_year_to ? json_decode($this->specific_occasion_month_of_year_to,true):array(),
                'months' => $this->getMonths(),
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/month_of_year.tpl');
    }
    public function displayFormDayOfWeek()
    {
        Context::getContext()->smarty->assign(
            array(
                'specific_occasion_day_of_week' => $this->specific_occasion_day_of_week ? json_decode($this->specific_occasion_day_of_week,true):array(),
                'specific_occasion_day_of_week_from' => $this->specific_occasion_day_of_week_from ? json_decode($this->specific_occasion_day_of_week_from,true):array(),
                'specific_occasion_day_of_week_to' => $this->specific_occasion_day_of_week_to ? json_decode($this->specific_occasion_day_of_week_to,true):array(),
                'days' => $this->getDays(),
                'hours' => $this->getHours(),
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/day_of_week.tpl');
    }
    public function displayFormHourOfDay()
    {
        Context::getContext()->smarty->assign(
            array(
                'specific_occasion_hour_of_day_from' => $this->specific_occasion_hour_of_day_from ? json_decode($this->specific_occasion_hour_of_day_from,true):array(),
                'specific_occasion_hour_of_day_to' => $this->specific_occasion_hour_of_day_to ? json_decode($this->specific_occasion_hour_of_day_to,true):array(),
                'hours' => $this->getHours(),
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/hour_of_day.tpl');
    }
    public function displayFormZipCode()
    {
        Context::getContext()->smarty->assign(
            array(
                'delivery_zipcode_type' => $this->delivery_zipcode_type ? json_decode($this->delivery_zipcode_type,true):array(),
                'delivery_zipcode_from' => $this->delivery_zipcode_from ? json_decode($this->delivery_zipcode_from,true):array(),
                'delivery_zipcode_to' => $this->delivery_zipcode_to ? json_decode($this->delivery_zipcode_to,true):array(),
                'delivery_zipcode_start_from' => $this->delivery_zipcode_start_from ? json_decode($this->delivery_zipcode_start_from,true):array(),
                'delivery_zipcode_end_at' => $this->delivery_zipcode_end_at ? json_decode($this->delivery_zipcode_end_at,true):array(),
                'delivery_zipcode_is_exactly' => $this->delivery_zipcode_is_exactly ? json_decode($this->delivery_zipcode_is_exactly):array(),
                'delivery_zipcode_different' => $this->delivery_zipcode_different ? json_decode($this->delivery_zipcode_different,true):array(),
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/form_zipcode.tpl');
    }
    public static function displayDeliveryZipcode($condition)
    {
        if(is_array($condition))
        {
            $delivery_zipcode_type = $condition['delivery_zipcode_type'] ? json_decode($condition['delivery_zipcode_type'],true):array();
            $delivery_zipcode_from = $condition['delivery_zipcode_from'] ? json_decode($condition['delivery_zipcode_from'],true):array();
            $delivery_zipcode_to = $condition['delivery_zipcode_to'] ? json_decode($condition['delivery_zipcode_to'],true):array();
            $delivery_zipcode_start_from = $condition['delivery_zipcode_start_from'] ? json_decode($condition['delivery_zipcode_start_from'],true):array();
            $delivery_zipcode_end_at = $condition['delivery_zipcode_end_at'] ? json_decode($condition['delivery_zipcode_end_at'],true):array();
            $delivery_zipcode_is_exactly = $condition['delivery_zipcode_is_exactly'] ? json_decode($condition['delivery_zipcode_is_exactly'],true) :array();
            $delivery_zipcode_different = $condition['delivery_zipcode_different'] ? json_decode($condition['delivery_zipcode_different'],true) : array();
        }
        else
        {
            $delivery_zipcode_type = $condition->delivery_zipcode_type? json_decode($condition->delivery_zipcode_type,true):array();
            $delivery_zipcode_from = $condition->delivery_zipcode_from ? json_decode($condition->delivery_zipcode_from,true):array();
            $delivery_zipcode_to = $condition->delivery_zipcode_to ? json_decode($condition->delivery_zipcode_to,true):array();
            $delivery_zipcode_start_from = $condition->delivery_zipcode_start_from ? json_decode($condition->delivery_zipcode_start_from,true):array();
            $delivery_zipcode_end_at = $condition->delivery_zipcode_end_at ? json_decode($condition->delivery_zipcode_end_at,true):array();
            $delivery_zipcode_is_exactly = $condition->delivery_zipcode_is_exactly ? json_decode($condition->delivery_zipcode_is_exactly,true) :array();
            $delivery_zipcode_different = $condition->delivery_zipcode_different ? json_decode($condition->delivery_zipcode_different,true) : array();
        }
        Context::getContext()->smarty->assign(
            array(
                'delivery_zipcode_type' => $delivery_zipcode_type,
                'delivery_zipcode_from' => $delivery_zipcode_from,
                'delivery_zipcode_to' => $delivery_zipcode_to,
                'delivery_zipcode_start_from' => $delivery_zipcode_start_from,
                'delivery_zipcode_end_at' => $delivery_zipcode_end_at,
                'delivery_zipcode_is_exactly' => $delivery_zipcode_is_exactly,
                'delivery_zipcode_different' => $delivery_zipcode_different,
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/delivery_zipcode.tpl');
    }
    public static function displaySpecificOccasions($condition)
    {
        if(is_array($condition))
        {
            $specific_occasion = $condition['specific_occasion'];
            $specific_occasion_hour_of_day_from = $condition['specific_occasion_hour_of_day_from'] ? json_decode($condition['specific_occasion_hour_of_day_from'],true):array();
            $specific_occasion_hour_of_day_to = $condition['specific_occasion_hour_of_day_to'] ? json_decode($condition['specific_occasion_hour_of_day_to'],true):array();
            $specific_occasion_day_of_week = $condition['specific_occasion_day_of_week'] ? json_decode($condition['specific_occasion_day_of_week'],true) : array();
            $specific_occasion_day_of_week_from = $condition['specific_occasion_day_of_week_from'] ? json_decode($condition['specific_occasion_day_of_week_from'],true) : array();
            $specific_occasion_day_of_week_to = $condition['specific_occasion_day_of_week_to'] ? json_decode($condition['specific_occasion_day_of_week_to'],true) : array();
            $specific_occasion_month_of_year = $condition['specific_occasion_month_of_year'] ? json_decode($condition['specific_occasion_month_of_year'],true) : array();
            $specific_occasion_month_of_year_from = $condition['specific_occasion_month_of_year_from'] ? json_decode($condition['specific_occasion_month_of_year_from'],true) : array();
            $specific_occasion_month_of_year_to = $condition['specific_occasion_month_of_year_to'] ? json_decode($condition['specific_occasion_month_of_year_to'],true) : array();
            $specific_occasion_date_from = $condition['specific_occasion_date_from'] ? json_decode($condition['specific_occasion_date_from'],true):array();
            $specific_occasion_date_to = $condition['specific_occasion_date_to'] ? json_decode($condition['specific_occasion_date_to'],true):array();
        }
        else
        {
            $specific_occasion = $condition->specific_occasion;
            $specific_occasion_hour_of_day_from = $condition->specific_occasion_hour_of_day_from ? json_decode($condition->specific_occasion_hour_of_day_from,true):array();
            $specific_occasion_hour_of_day_to = $condition->specific_occasion_hour_of_day_to? json_decode($condition->specific_occasion_hour_of_day_to,true):array();
            $specific_occasion_day_of_week = $condition->specific_occasion_day_of_week ? json_decode($condition->specific_occasion_day_of_week,true) : array();
            $specific_occasion_day_of_week_from = $condition->specific_occasion_day_of_week_from ? json_decode($condition->specific_occasion_day_of_week_from,true) : array();
            $specific_occasion_day_of_week_to = $condition->specific_occasion_day_of_week_to ? json_decode($condition->specific_occasion_day_of_week_to,true) : array();
            $specific_occasion_month_of_year = $condition->specific_occasion_month_of_year ? json_decode($condition->specific_occasion_month_of_year,true) : array();
            $specific_occasion_month_of_year_from = $condition->specific_occasion_month_of_year_from ? json_decode($condition->specific_occasion_month_of_year_from,true) : array();
            $specific_occasion_month_of_year_to = $condition->specific_occasion_month_of_year_to ? json_decode($condition->specific_occasion_month_of_year_to,true) : array();
            $specific_occasion_date_from = $condition->specific_occasion_date_from ? json_decode($condition->specific_occasion_date_from,true):array();
            $specific_occasion_date_to = $condition->specific_occasion_date_to ? json_decode($condition->specific_occasion_date_to,true):array();
        }
        Context::getContext()->smarty->assign(
            array(
                'days' => Ets_sc_condition_rule::getInstance()->getDays(),
                'months' => Ets_sc_condition_rule::getInstance()->getMonths(),
                'specific_occasion' => $specific_occasion,
                'specific_occasion_hour_of_day_from' => $specific_occasion_hour_of_day_from,
                'specific_occasion_hour_of_day_to' => $specific_occasion_hour_of_day_to,
                'specific_occasion_day_of_week' => $specific_occasion_day_of_week,
                'specific_occasion_day_of_week_from' => $specific_occasion_day_of_week_from,
                'specific_occasion_day_of_week_to' => $specific_occasion_day_of_week_to,
                'specific_occasion_month_of_year' => $specific_occasion_month_of_year,
                'specific_occasion_month_of_year_from' => $specific_occasion_month_of_year_from,
                'specific_occasion_month_of_year_to' => $specific_occasion_month_of_year_to,
                'specific_occasion_date_from' => $specific_occasion_date_from,
                'specific_occasion_date_to' => $specific_occasion_date_to,
            )
        );
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/specific_occasions.tpl');
    }
    public static function displayProductInCart($condition)
    {
        if(is_array($condition))
        {
            $total_product_quantity_cal = $condition['total_product_quantity_cal'];
            $total_product_quantity = (int)$condition['total_product_quantity'];
            $total_weight_cal = $condition['total_weight_cal'];
            $total_weight = (float)$condition['total_weight'];
            $quantity_of_same_product_cal = $condition['quantity_of_same_product_cal'];
            $quantity_of_same_product = (int)$condition['quantity_of_same_product'];
            $number_of_different_product_cal = $condition['number_of_different_product_cal'];
            $number_of_different_product = (int)$condition['number_of_different_product'];
            $number_of_product_in_same_category_cal = $condition['number_of_product_in_same_category_cal'];
            $number_of_product_in_same_category = (int)$condition['number_of_product_in_same_category'];
            $apply_for_discounted_products = (int)$condition['apply_for_discounted_products'];
            $products_with_different_attribute = (int)$condition['products_with_different_attribute'];
            $applicable_product_categories = $condition['applicable_product_categories'];
            $applicable_categories = $condition['applicable_categories'];
            $include_sub_categories = $condition['include_sub_categories'];
            $exclude_products = $condition['exclude_products'];
            $include_specific_products = $condition['include_specific_products'];
            $apply_all_attribute = (int)$condition['apply_all_attribute'];
            $select_attributes = $condition['select_attributes'];
            $apply_all_features = (int)$condition['apply_all_features'];
            $select_features = $condition['select_features'];
            $apply_all_manufacturer = (int)$condition['apply_all_manufacturer'];
            $select_manufacturers = $condition['select_manufacturers'];
            $apply_all_supplier = (int)$condition['apply_all_supplier'];
            $select_suppliers = $condition['select_suppliers'];
            $apply_for_product_price_cal = $condition['apply_for_product_price_cal'];
            $apply_for_product_price = $condition['apply_for_product_price'];
            $apply_for_availabled_quantity_stock_cal = $condition['apply_for_availabled_quantity_stock_cal'];
            $apply_for_availabled_quantity_stock = (int)$condition['apply_for_availabled_quantity_stock'];
        }
        else
        {
            $total_weight_cal = $condition->total_weight_cal;
            $total_weight = (float)$condition->total_weight;
            $total_product_quantity_cal = $condition->total_product_quantity_cal;
            $total_product_quantity = (int)$condition->total_product_quantity;
            $quantity_of_same_product_cal = $condition->quantity_of_same_product_cal;
            $quantity_of_same_product = (int)$condition->quantity_of_same_product;
            $number_of_different_product_cal = $condition->number_of_different_product_cal;
            $number_of_different_product = (int)$condition->number_of_different_product;
            $number_of_product_in_same_category_cal = $condition->number_of_product_in_same_category_cal;
            $number_of_product_in_same_category = $condition->number_of_product_in_same_category;
            $apply_for_discounted_products = (int)$condition->apply_for_discounted_products;
            $products_with_different_attribute = (int)$condition->products_with_different_attribute;
            $applicable_product_categories = $condition->applicable_product_categories;
            $applicable_categories = $condition->applicable_categories;
            $include_sub_categories = $condition->include_sub_categories;
            $exclude_products = $condition->exclude_products;
            $include_specific_products = $condition->include_specific_products;
            $apply_all_attribute = (int)$condition->apply_all_attribute;
            $select_attributes = $condition->select_attributes;
            $apply_all_features = (int)$condition->apply_all_features;
            $select_features = $condition->select_features;
            $apply_all_manufacturer = (int)$condition->apply_all_manufacturer;
            $select_manufacturers = $condition->select_manufacturers;
            $apply_all_supplier = (int)$condition->apply_all_supplier;
            $select_suppliers = $condition->select_suppliers;
            $apply_for_product_price_cal = $condition->apply_for_product_price_cal;
            $apply_for_product_price = $condition->apply_for_product_price;
            $apply_for_availabled_quantity_stock_cal = $condition->apply_for_availabled_quantity_stock_cal;
            $apply_for_availabled_quantity_stock = (int)$condition->apply_for_availabled_quantity_stock;
        }
        Context::getContext()->smarty->assign(
            array(
                'total_weight_cal' => $total_weight_cal,
                'total_weight' => $total_weight,
                'total_product_quantity_cal' => $total_product_quantity_cal,
                'total_product_quantity' => $total_product_quantity,
                'quantity_of_same_product_cal' => $quantity_of_same_product_cal,
                'quantity_of_same_product' => $quantity_of_same_product,
                'number_of_different_product_cal' => $number_of_different_product_cal,
                'number_of_different_product' => $number_of_different_product,
                'number_of_product_in_same_category_cal' => $number_of_product_in_same_category_cal,
                'number_of_product_in_same_category' => $number_of_product_in_same_category,
                'apply_for_discounted_products' => $apply_for_discounted_products,
                'products_with_different_attribute' => $products_with_different_attribute,
                'applicable_product_categories' => $applicable_product_categories,
                'applicable_categories' =>  $applicable_product_categories=='specific_product' && $applicable_categories ? Ets_sc_defines::getCategoriesById($applicable_categories):'',
                'include_sub_categories' => $include_sub_categories,
                'exclude_products' => $exclude_products ? Ets_sc_defines::getInstance()->getProductsByIds($exclude_products):false,
                'include_specific_products' => $include_specific_products ? Ets_sc_defines::getInstance()->getProductsByIds($include_specific_products):false,
                'apply_all_attribute' => $apply_all_attribute,
                'select_attributes' => $select_attributes!='all' && !$apply_all_attribute ? Ets_sc_condition_rule::getInstance()->getAttributes(' AND a.id_attribute IN ('.implode(',',array_map('intval',explode(',',$select_attributes))).')'):$select_attributes,
                'apply_all_features' => $apply_all_features,
                'select_features' => $select_features!='all' && !$apply_all_features ? Ets_sc_condition_rule::getInstance()->getFeatures(' AND f.id_feature IN ('.implode(',',array_map('intval',explode(',',$select_features))).')'):$select_features,
                'apply_all_manufacturer' => $apply_all_manufacturer,
                'select_manufacturers' => $select_manufacturers!='all' && !$apply_all_manufacturer ? Ets_sc_condition_rule::getInstance()->getManufacturers(' AND m.id_manufacturer IN ('.implode(',',array_map('intval',explode(',',$select_manufacturers))).')'):$select_manufacturers,
                'apply_all_supplier' => $apply_all_supplier,
                'select_suppliers' => $select_suppliers!='all' && !$apply_all_supplier ? Ets_sc_condition_rule::getInstance()->getSuppliers(' AND s.id_supplier IN ('.implode(',',array_map('intval',explode(',',$select_suppliers))).')'):$select_suppliers,
                'apply_for_product_price_cal' => $apply_for_product_price_cal,
                'apply_for_product_price' => $apply_for_product_price >0 ? Tools::displayPrice($apply_for_product_price) : false,
                'apply_for_availabled_quantity_stock_cal' => $apply_for_availabled_quantity_stock_cal,
                'apply_for_availabled_quantity_stock' => $apply_for_availabled_quantity_stock,
            )
        ); 
        return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/condition/product_in_cart.tpl');
    }
    public function displayDescCountry($has_country)
    {
    	$content = $this->l('No country available. Configure') . ' ';
    	$content .= $this->module->displayText($this->l('here'), 'a', '', '', Context::getContext()->link->getAdminLink('AdminCountries'));
    	return $this->module->displayText($content, 'p', 'help-block', '', '', '', '', '', '', '', '', '', '', [['name' => 'style', 'value' => $has_country ? 'display: none;' : 'display: block;']]);
    }
    public function displayDescState($has_state)
    {
	    $content = $this->l('No state available. Configure') . ' ';
	    $content .= $this->module->displayText($this->l('here'), 'a', '', '', Context::getContext()->link->getAdminLink('AdminStates'));
	    return $this->module->displayText($content, 'p', 'help-block', '', '', '', '', '', '', '', '', '', '', [['name' => 'style', 'value' => $has_state ? 'display: none;' : 'display: block;']]);
    }

    public function saveData()
    {
    	$this->module->_clearSmartyCacheWhenUpdateConditionRule();
	    return parent::saveData(); // TODO: Change the autogenerated stub
    }
}