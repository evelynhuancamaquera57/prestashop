<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
*/

if (!defined('_PS_VERSION_')) { exit; }
class Ets_sc_obj extends ObjectModel 
{
	/** @var Ets_shippingcost */
	public $module;
    public $fields = array();
    public function getListFields(){
        return array();
    }
	public	function __construct($id_item = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_item, $id_lang, $id_shop);
		if (!$this->module)
			$this->module = new Ets_shippingcost();
	}
    public function l($string,$file_name='')
    {
        return Translate::getModuleTranslation('ets_promotion', $string, $file_name ? : pathinfo(__FILE__, PATHINFO_FILENAME));
    }
    public function renderForm()
    {
        $this->fields = $this->getListFields();
        $helper = new HelperForm();
        $helper->module = new Ets_shippingcost();
        $configs = $this->fields['configs'];
        $fields_form = array();
        $fields_form['form'] = $this->fields['form'];               
        if($configs)
        {
            foreach($configs as $key => $config)
            {                
                if(isset($config['type']) && in_array($config['type'],array('sort_order')))
                    continue;
                $confFields = array(
                    'name' => $key,
                    'type' => $config['type'],
                    'class'=>isset($config['class'])?$config['class']:'',
                    'label' => $config['label'],
                    'desc' => isset($config['desc']) ? $config['desc'] : false,
                    'required' => isset($config['required']) && $config['required'] ? true : false,
                    'readonly' => isset($config['readonly']) ? $config['readonly'] : false,
                    'autoload_rte' => isset($config['autoload_rte']) && $config['autoload_rte'] ? true : false,
                    'options' => isset($config['options']) && $config['options'] ? $config['options'] : array(),
                    'suffix' => isset($config['suffix']) && $config['suffix'] ? $config['suffix']  : false,
                    'values' => isset($config['values']) ? $config['values'] : false,
                    'lang' => isset($config['lang']) ? $config['lang'] : false,
                    'showRequired' => isset($config['showRequired']) && $config['showRequired'],
                    'hide_delete' => isset($config['hide_delete']) ? $config['hide_delete'] : false,
                    'placeholder' => isset($config['placeholder']) ? $config['placeholder'] : false,
                    'display_img' => $this->id && isset($config['type']) && $config['type']=='file' && $this->$key!='' && @file_exists(_PS_ETS_SC_IMG_DIR_.$this->$key) ? _PS_ETS_SC_IMG_DIR_.$this->$key : false,
                    'img_del_link' => $this->id && isset($config['type']) && $config['type']=='file' && $this->$key!='' && @file_exists(_PS_ETS_SC_IMG_DIR_.$this->$key) ? Context::getContext()->link->getAdminBaseLink('AdminModules').'&configure=ets_shippingcost&deleteimage='.$key.'&itemId='.(isset($this->id)?$this->id:'0').'&Ets_sc_object=PRMN_'.Tools::ucfirst($fields_form['form']['name']) : false,
                    'min' => isset($config['min']) ? $config['min'] : false,
                    'max' => isset($config['max']) ? $config['max'] : false, 
                    'data_suffix' => isset($config['data_suffix']) ? $config['data_suffix'] :'',
                    'data_suffixs' => isset($config['data_suffixs']) ? $config['data_suffixs'] :'',
                    'multiple' => isset($config['multiple']) ? $config['multiple']: false,
                    'tab' => isset($config['tab']) ? $config['tab']:false,
                    'html_content' => isset($config['html_content']) ? $config['html_content']:'',
                    'form_group_class' => isset($config['form_group_class']) ? $config['form_group_class']:'',
                );
                if(isset($config['col']) && $config['col'])
                    $confFields['col'] = $config['col'];
                if(isset($config['tree']) && $config['tree'])
                {
                    $confFields['tree'] = $config['tree'];
                    if(isset($config['tree']['use_checkbox']) && $config['tree']['use_checkbox'])
                        $confFields['tree']['selected_categories'] = explode(',',$this->$key);
                    else
                        $confFields['tree']['selected_categories'] = array($this->$key);
                }                    
                if(!$confFields['suffix'])
                    unset($confFields['suffix']);                
                $fields_form['form']['input'][] = $confFields;
            }
        }        
        $fields_form['form']['input'][] = array(
            'type' => 'hidden',
            'name' => $fields_form['form']['key'],
        );
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'save_'.$this->fields['form']['name'];
		$helper->currentIndex = '';
		$language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $fields = array();        
        $languages = Language::getLanguages(false);
        if($configs)
        {
            foreach($configs as $key => $config)
            {
                if($config['type']=='checkbox' || (isset($config['multiple']) && $config['multiple']))
                {
                    if(Tools::isSubmit($key))
                        $fields[$key] = Tools::getValue($key);
                    else
                        $fields[$key] = $this->id ? explode(',',$this->$key) : (isset($config['default']) && $config['default'] ? $config['default'] : array());
                    if((isset($config['multiple']) && $config['multiple']))
                    {
                        $fields[$key.'[]'] =$fields[$key];
                    }
                }
                elseif(isset($config['lang']) && $config['lang'])
                {                    
                    foreach($languages as $l)
                    {
                        $temp = $this->$key;
                        if(Tools::isSubmit($key.'_'.$l['id_lang']))
                            $fields[$key][$l['id_lang']] = Tools::getValue($key.'_'.$l['id_lang']);
                        else
                            $fields[$key][$l['id_lang']] = $this->id ? $temp[$l['id_lang']] : (isset($config['default']) && $config['default'] ? $config['default'] : null);
                    }
                }
                elseif(isset($config['type']) && $config['type']=='file_lang')
                {
                    foreach($languages as $l)
                    {
                        $temp = $this->$key;
                        $fields[$key][$l['id_lang']] = $this->id ? $temp[$l['id_lang']] : (isset($config['default']) && $config['default'] ? $config['default'] : null);
                    }
                }
                elseif(!isset($config['tree']))
                {
                    if(Tools::isSubmit($key))
                        $fields[$key] = Tools::getValue($key);
                    else
                    {
                        $fields[$key] = $this->id ? $this->$key : (isset($config['default']) && $config['default'] ? $config['default'] : null);
                        if(isset($config['validate']) && ($config['validate']=='isUnsignedFloat' ||  $config['validate']=='isUnsignedInt') && $fields[$key]==0)
                        {
                            if($config['type']=='switch')
                                $fields[$key] =0;
                            else
                                $fields[$key] = '';
                        }
                        if(isset($config['validate']) && $config['validate']=='isDate' && $fields[$key]=='0000-00-00 00:00:00')
                            $fields[$key] ='';
                    }
                     
                }    
                                        
            }
        }
        $fields[$fields_form['form']['key']] = $this->id;
        $helper->tpl_vars = array(
			'base_url' => Context::getContext()->shop->getBaseURL(),
			'language' => array(
				'id_lang' => $language->id,
				'iso_code' => $language->iso_code
			),
			'fields_value' => $fields,
			'languages' => Context::getContext()->controller->getLanguages(),
			'id_language' => Context::getContext()->language->id, 
            'key_name' => 'id_'.$fields_form['form']['name'],
            'item_id' => $this->id,  
            'list_item' => true,
            'image_baseurl' => _PS_ETS_SC_IMG_, 
            'configTabs'=>  isset($this->fields['tabs']) ?  $this->fields['tabs']:false, 
            'display_advanced_setting' => (int)Tools::getValue('display_advanced_setting'),
            'name_controller' =>  isset($this->fields['name_controller']) ?  $this->fields['name_controller']:'',            
        );        
        return str_replace(array('id="ets_pr_rule_form"','id="fieldset_0"'),'',$helper->generateForm(array($fields_form)));	
    }
    public function saveData()
    {
        $this->fields = $this->getListFields();
        $errors = array();
        $success = array();
        $languages = Language::getLanguages(false);
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        $parent=isset($this->fields['form']['parent'])? $this->fields['form']['parent']:'1';
        $configs = $this->fields['configs'];  
        $files = array();  
        $old_files = array(); 
        if(method_exists($this,'validateCustomField'))
            $this->validateCustomField($errors);  
        if($configs)
        {
            foreach($configs as $key => $config)
            {
                $value_key = Tools::getValue($key);
                if($config['type']=='sort_order' || $config['type']=='html')
                    continue;
                if(isset($config['lang']) && $config['lang'])
                {
                    $key_value_lang_default = trim(Tools::getValue($key.'_'.$id_lang_default));
                    if(isset($config['required']) && $config['required'] && $config['type']!='switch' && $key_value_lang_default == '')
                    {
                        $errors[] = sprintf($this->l('%s is required','ets_sc_obj'),$config['label']);
                    }
                    elseif($key_value_lang_default!='' && isset($config['validate']) && ($config['validate']=='isColor' || $config['validate']=='isLink'))
                    {
                        $validate = $config['validate'];
                        if(!Ets_shippingcost::$validate(trim($key_value_lang_default)))
                            $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                        unset($validate);
                    }
                    elseif($key_value_lang_default!='' && !is_array($key_value_lang_default) && isset($config['validate']) && method_exists('Validate',$config['validate']))
                    {
                        $validate = $config['validate'];
                        if(!Validate::$validate(trim($key_value_lang_default)))
                            $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                        unset($validate);
                    }
                    elseif(!Validate::isCleanHtml($key_value_lang_default))
                        $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                    else
                    {
                        foreach($languages as $language)
                        {
                            if($language['id_lang']!=$id_lang_default)
                            {
                                $value_lang = trim(Tools::getValue($key.'_'.$language['id_lang']));
                                if($value_lang!='' && isset($config['validate']) && ($config['validate']=='isColor' || $config['validate']=='isLink'))
                                {
                                    $validate = $config['validate'];
                                    if(!Ets_shippingcost::$validate(trim($value_lang)))
                                        $errors[] = sprintf($this->l('%s is not valid in %s','ets_sc_obj'),$config['label'],$language['iso_code']);
                                    unset($validate);
                                }
                                elseif($value_lang!='' && !is_array($value_lang) && isset($config['validate']) && method_exists('Validate',$config['validate']))
                                {
                                    $validate = $config['validate'];
                                    if(!Validate::$validate(trim($value_lang)))
                                        $errors[] = sprintf($this->l('%s is not valid in %s','ets_sc_obj'),$config['label'],$language['iso_code']);
                                    unset($validate);
                                }
                                elseif(!Validate::isCleanHtml($value_lang))
                                    $errors[] = sprintf($this->l('%s is not valid in %s','ets_sc_obj'),$config['label'],$language['iso_code']);
                            }
                        }
                    }                    
                }
                elseif($config['type']=='file_lang')
                {
                    $files[$key] = array();
                    foreach($languages as $l)
                    {
                        $name = $key.'_'.$l['id_lang'];
                        if(isset($_FILES[$name]['tmp_name']) && isset($_FILES[$name]['name']) && $_FILES[$name]['name'])
                        {
                            $_FILES[$name]['name'] = str_replace(array(' ','(',')','!','@','#','+'),'_',$_FILES[$name]['name']);
                            $type = Tools::strtolower(Tools::substr(strrchr($_FILES[$name]['name'], '.'), 1));
                            $imageName = @file_exists(_PS_ETS_SC_IMG_DIR_.Tools::strtolower($_FILES[$name]['name'])) ? Tools::passwdGen().'-'.Tools::strtolower($_FILES[$name]['name']) : Tools::strtolower($_FILES[$name]['name']);
                            $fileName = _PS_ETS_SC_IMG_DIR_.$imageName;  
                            $max_file_size = Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE')*1024*1024;
                            if(!Validate::isFileName($_FILES[$name]['name']))
                                $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                            elseif($_FILES[$name]['size'] > $max_file_size)
                                $errors[] = sprintf($this->l('%s file is too large','ets_sc_obj'),$config['label']);
                            elseif(file_exists($fileName))
                            {
                                $errors[] =sprintf($this->l('%s file is already existed','ets_sc_obj'),$config['label']);
                            }
                            else
                            {                                    
                    			$imagesize = @getimagesize($_FILES[$name]['tmp_name']);                                    
                                if (!$errors && isset($_FILES[$name]) &&				
                    				!empty($_FILES[$name]['tmp_name']) &&
                    				!empty($imagesize) &&
                    				in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
                    			)
                    			{
                    				$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');    				
                    				if ($error = ImageManager::validateUpload($_FILES[$name]))
                    					$errors[] = $error;
                    				elseif (!$temp_name || !move_uploaded_file($_FILES[$name]['tmp_name'], $temp_name))
                    					$errors[] = sprintf($this->l('%s cannot upload in %s','ets_sc_obj'),$config['label'],$l['iso_code']);
                    				elseif (!ImageManager::resize($temp_name, $fileName, null, null, $type))
                    					$errors[] = printf($this->l('%s An error occurred during the image upload process in %s','ets_sc_obj'),$config['label'],$l['iso_code']);
                    				if ($temp_name && file_exists($temp_name))
                    					@unlink($temp_name);
                                    if(!$errors)
                                    {
                                        $files[$key][$l['id_lang']] = $imageName;  
                                    }
                                }
                                else
                                    $errors[] = sprintf($this->l('%s file in %s is not in the correct format, accepted formats: jpg, gif, jpeg, png.','ets_sc_obj'),$config['label'],$l['iso_code']);
                            }
                        }
                    }
                }
                else
                {
                    if(isset($config['required']) && $config['required'] && isset($config['type']) && $config['type']=='file')
                    {
                        if($this->$key=='' && !isset($_FILES[$key]['size']))
                            $errors[] = sprintf($this->l('%s is required','ets_sc_obj'),$config['label']);
                        elseif(isset($_FILES[$key]['size']))
                        {
                            $fileSize = round((int)$_FILES[$key]['size'] / (1024 * 1024));
                			if($fileSize > 100)
                                $errors[] = sprintf($this->l('%s file is too large','ets_sc_obj'),$config['label']);
                        }   
                    }
                    else
                    {
                        if(isset($config['required']) && $config['required'] && $config['type']!='switch' && !is_array($value_key) && trim($value_key) == '')
                        {
                            $errors[] = sprintf($this->l('%s is required','ets_sc_obj'),$config['label']);
                        }
                        elseif($value_key!='' && isset($config['validate']) && ($config['validate']=='isColor' || $config['validate']=='isLink'))
                        {
                            $validate = $config['validate'];
                            if(!Ets_shippingcost::$validate(trim($value_key)))
                                $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                            unset($validate);
                        }
                        elseif($value_key!='' && !is_array($value_key) && isset($config['validate']) && method_exists('Validate',$config['validate']))
                        {
                            $validate = $config['validate'];
                            if(!Validate::$validate(trim($value_key)))
                                $errors[] = sprintf($this->l('%s is not valid','ets_sc_obj'),$config['label']);
                            unset($validate);
                        }
                        elseif($value_key!='' && !is_array($value_key)  && !Validate::isCleanHtml(trim($value_key)))
                        {
                            $errors[] = sprintf($this->l('%s is required','ets_sc_obj'),$config['label']);
                        } 
                    }                          
                }                    
            }
        }            
        if(!$errors)
        {            
            if($configs)
            {
                foreach($configs as $key => $config)
                {
                    if( $config['type']=='html')
                        continue;
                    $value_key = Tools::getValue($key);
                    if(isset($config['type']) && $config['type']=='sort_order')
                    {
                        if(!$this->id)
                        {
                            if(!isset($config['order_group'][$parent]) || isset($config['order_group'][$parent]) && !$config['order_group'][$parent])
                                $this->$key = $this->maxVal($key)+1;
                            else
                            {
                                $orderGroup = $config['order_group'][$parent];
                                $this->$key = $this->maxVal($key,$orderGroup,(int)$this->$orderGroup)+1;
                            }                                                         
                        }
                    }
                    elseif(isset($config['lang']) && $config['lang'])
                    {
                        $valules = array();
                        $key_value_lang_default = trim(Tools::getValue($key.'_'.$id_lang_default));
                        foreach($languages as $lang)
                        {
                            $key_value_lang = trim(Tools::getValue($key.'_'.$lang['id_lang']));
                            if($config['type']=='switch')                                                           
                                $valules[$lang['id_lang']] = (int)$key_value_lang ? 1 : 0;                                
                            elseif(Validate::isCleanHtml($key_value_lang))
                                $valules[$lang['id_lang']] = $key_value_lang ? : (Validate::isCleanHtml($key_value_lang_default) ? $key_value_lang_default:'');
                        }
                        $this->$key = $valules;
                    }
                    elseif($config['type']=='file_lang')
                    {
                        if(isset($files[$key]))
                        {
                            $valules = array();
                            $old_values = $this->$key;
                            $old_files[$key] = array();
                            foreach($languages as $lang)
                            {
                                if(isset($files[$key][$lang['id_lang']]) && $files[$key][$lang['id_lang']])
                                {
                                    $valules[$lang['id_lang']] = $files[$key][$lang['id_lang']];
                                    if($old_values && isset($old_values[$lang['id_lang']]) && $old_values[$lang['id_lang']])
                                        $old_files[$key][$lang['id_lang']] = $old_values[$lang['id_lang']];
                                }
                                elseif(!($old_values && isset($old_values[$lang['id_lang']]) && $old_values[$lang['id_lang']]) && isset($files[$key][$id_lang_default]) && $files[$key][$id_lang_default])
                                    $valules[$lang['id_lang']] = $files[$key][$id_lang_default];
                                else
                                    $valules[$lang['id_lang']] = $old_values && isset($old_values[$lang['id_lang']]) ? $old_values[$lang['id_lang']]:'';
                            }
                            $this->$key = $valules;
                        }
                    }
                    elseif($config['type']=='switch')
                    {                           
                        $this->$key = (int)$value_key ? 1 : 0;                                                      
                    }
                    elseif($config['type']=='categories' && is_array($value_key) && isset($config['tree']['use_checkbox']) && $config['tree']['use_checkbox'] || $config['type']=='checkbox' || (isset($config['multiple']) && $config['multiple']))
                    {
                        if($value_key)
                        {
                            if(in_array('all',$value_key))
                                $this->$key = 'all';
                            else
                                $this->$key = implode(',',$value_key); 
                        }
                        else
                            $this->$key='';
                    }                                                  
                    elseif(Validate::isCleanHtml($value_key))
                        $this->$key = trim($value_key);   
                    }
                }
        }     
        if (!count($errors))
        { 
            $this->id_shop = Context::getContext()->shop->id;
            if($this->id && $this->update() || !$this->id && $this->add(true,true))
            {
                $success[] = $this->l('Saved successfully','ets_sc_obj');
                if($old_files)
                {
                    foreach($old_files as $key_file => $file)
                    {
                        if($file)
                        {
                            if(is_array($file))
                            {
                                foreach($file as $f)
                                {
                                    if(!in_array($f,$this->$key_file))
                                        @unlink(_PS_ETS_SC_IMG_DIR_.$f);
                                }
                            }
                            else
                                @unlink(_PS_ETS_SC_IMG_DIR_.$file);
                        }
                    }
                }
            }                
            else
            {
                if($files)
                {
                    foreach($files as $key_file => $file)
                    {
                        if($file)
                        {
                            if(is_array($file))
                            {
                                foreach($file as $f)
                                {
                                    @unlink(_PS_ETS_SC_IMG_DIR_.$f); 
                                }
                            }
                            else
                                @unlink(_PS_ETS_SC_IMG_DIR_.$file);
                        }
                    }
                }
                $errors[] = $this->l('Saving failed','ets_sc_obj');
            }
        }
        return array('errors' => $errors, 'success' => $success);  
    }
    public function update($null_value=false)
    {
        $ok = parent::update($null_value);
        return $ok;
    }  
    public function maxVal($key,$group = false, $groupval=0)
    {  
       return ($max = Db::getInstance()->getValue("SELECT max(".bqSQL($key).") FROM "._DB_PREFIX_."ets_pr_".bqSQL($this->fields['form']['name']).($group && ($groupval > 0) ? " WHERE ".bqSQL($group)."=".(int)$groupval : ''))) ? (int)$max : 0;
    }
    public static function getCarriers($filter ='')
    {
        $context = Context::getContext();
        $sql ='SELECT c.id_reference,c.name FROM `'._DB_PREFIX_.'carrier` c
        INNER JOIN `'._DB_PREFIX_.'carrier_shop` cs ON (cs.id_carrier = c.id_carrier AND cs.id_shop="'.(int)$context->shop->id.'")
        LEFT JOIN `'._DB_PREFIX_.'carrier_lang` cl ON (c.id_carrier = cl.id_carrier AND cl.id_lang="'.(int)$context->language->id.'")
        WHERE c.deleted=0 AND c.active=1 '.(string)$filter. ' ORDER BY c.id_reference';
        $carriers = Db::getInstance()->executeS($sql);
        if($carriers)
        {
            foreach($carriers as &$carrier)
            {
                if(!$carrier['name'])
                    $carrier['name'] = self::getCarrierNameFromShopName();
            }
        }
        return $carriers;
    }
    public static function getCarrierNameFromShopName()
    {
        return str_replace(
            ['#', ';'],
            '',
            Configuration::get('PS_SHOP_NAME')
        );
    }
    public function getGroups()
    {
        $context = Context::getContext();
        $sql ='SELECT g.id_group,gl.name FROM `'._DB_PREFIX_.'group` g 
        INNER JOIN `'._DB_PREFIX_.'group_shop` gs ON (gs.id_group = g.id_group AND gs.id_shop="'.(int)$context->shop->id.'")
        LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON (g.id_group = gl.id_group AND gl.id_lang="'.(int)$context->language->id.'")
        ';
        return Db::getInstance()->executeS($sql);
    }
    public function getAttributes($filter ='')
    {
        $context = Context::getContext();
        $sql = 'SELECT a.id_attribute, CONCAT(agl.name,": ",al.name) as name FROM `'._DB_PREFIX_.'attribute` a
        INNER JOIN `'._DB_PREFIX_.'attribute_shop` ats ON (a.id_attribute = ats.id_attribute AND ats.id_shop="'.(int)$context->shop->id.'")
        INNER JOIN `'._DB_PREFIX_.'attribute_group` ag ON (a.id_attribute_group = ag.id_attribute_group)
        LEFT JOIN `'._DB_PREFIX_.'attribute_lang` al ON (a.id_attribute= al.id_attribute AND al.id_lang="'.(int)$context->language->id.'")
        LEFT JOIN `'._DB_PREFIX_.'attribute_group_lang` agl ON (ag.id_attribute_group = agl.id_attribute_group AND agl.id_lang="'.(int)$context->language->id.'")
        WHERE 1'.(string)$filter;
        return Db::getInstance()->executeS($sql);
    }
    public function getFeatures($filter='')
    {
        $context = Context::getContext();
        $sql = 'SELECT f.id_feature, fl.name FROM `'._DB_PREFIX_.'feature` f
        INNER JOIN `'._DB_PREFIX_.'feature_shop` fs ON (f.id_feature = fs.id_feature AND fs.id_shop="'.(int)$context->shop->id.'")
        LEFT JOIN `'._DB_PREFIX_.'feature_lang` fl ON (f.id_feature= fl.id_feature AND fl.id_lang="'.(int)$context->language->id.'")
        WHERE 1 '.(string)$filter;
        return Db::getInstance()->executeS($sql);
    }
    public function getSuppliers($filter='')
    {
        $context = Context::getContext();
        $sql = 'SELECT s.id_supplier, s.name FROM `'._DB_PREFIX_.'supplier` s
        INNER JOIN `'._DB_PREFIX_.'supplier_shop` ss ON (s.id_supplier = ss.id_supplier AND ss.id_shop="'.(int)$context->shop->id.'")
        WHERE 1 '.(string)$filter;
        return Db::getInstance()->executeS($sql);
    }
    public function getManufacturers($filter='')
    {
         $context = Context::getContext();
        $sql = 'SELECT m.id_manufacturer, m.name FROM `'._DB_PREFIX_.'manufacturer` m
        INNER JOIN `'._DB_PREFIX_.'manufacturer_shop` ms ON (m.id_manufacturer = ms.id_manufacturer AND ms.id_shop="'.(int)$context->shop->id.'")
        WHERE 1 '.(string)$filter;
        return Db::getInstance()->executeS($sql);
    }
     public function getListZones()
    {
        $context = Context::getContext();
        $sql = 'SELECT z.id_zone,z.name FROM  `'._DB_PREFIX_.'zone` z
        INNER JOIN  `'._DB_PREFIX_.'zone_shop` zs ON (z.id_zone = zs.id_zone AND zs.id_shop="'.(int)$context->shop->id.'")
        WHERE z.active=1';
        return Db::getInstance()->executeS($sql);
    }
    public function getListCountries($id_zone=0)
    {
        $context = Context::getContext();
        $sql = 'SELECT c.id_country, cl.name FROM  `'._DB_PREFIX_.'country` c
        INNER JOIN  `'._DB_PREFIX_.'country_shop` cs ON (c.id_country = cs.id_country AND cs.id_shop="'.(int)$context->shop->id.'")
        LEFT JOIN  `'._DB_PREFIX_.'country_lang` cl ON (c.id_country= cl.id_country AND cl.id_lang="'.(int)$context->language->id.'")
        WHERE c.active=1'.($id_zone ? ' AND c.id_zone="'.(int)$id_zone.'"':'');
        return Db::getInstance()->executeS($sql);
    }
    public function getListStates($id_country=0,$id_zone=0)
    {
        $sql = 'SELECT id_state,name FROM  `'._DB_PREFIX_.'state` 
        WHERE active=1 '. ($id_country ? ' AND (id_country='.(int)$id_country.' OR id_country=0)':'').($id_zone ? ' AND (id_zone = '.(int)$id_zone.' OR id_zone=0)':'');
        return Db::getInstance()->executeS($sql);
    }
    public function delete()
    {
        return parent::delete();
    }
}