<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }
class Ets_sc_action_rule extends Ets_sc_obj
{
    public static $instance;
    public $id_ets_sc_shipping_rule;
    public $type_action;
    public $calcalate_cost_by;
    public $fees_percent;
    public $fees_amount;
    public $fees_max;
    public $id_tax_rule_group; 
    public $cal_percent_from;
    public $ignore_product_discounted;
    public $exclude_product_tax;
    public $formular;
    public $id_currency;
    public static $definition = array(
		'table' => 'ets_sc_action_rule',
		'primary' => 'id_ets_sc_action_rule',
		'fields' => array(
            'id_ets_sc_shipping_rule' => array('type' => self::TYPE_INT),
            'type_action' => array('type' => self::TYPE_STRING),
            'calcalate_cost_by'=> array('type' => self::TYPE_STRING),
            'fees_percent'=> array('type' => self::TYPE_FLOAT),
            'fees_amount' => array('type' => self::TYPE_FLOAT),
            'fees_max' => array('type' => self::TYPE_FLOAT),
            'id_tax_rule_group' => array('type' => self::TYPE_INT),
            'cal_percent_from' => array('type' => self::TYPE_STRING),
            'ignore_product_discounted' => array('type' => self::TYPE_INT),
            'exclude_product_tax' => array('type' => self::TYPE_INT),
            'formular' => array('type' => self::TYPE_HTML),
            'id_currency' => array('type' => self::TYPE_INT),
        )
    );
    public	function __construct($id_item = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_item, $id_lang, $id_shop);
	}
    public static function getInstance()
    {
        if (!(isset(self::$instance)) || !self::$instance) {
            self::$instance = new Ets_sc_action_rule();
        }
        return self::$instance;
    }
    public function l($string,$file_name='')
    {
        return Translate::getModuleTranslation('ets_shippingcost', $string, $file_name ? : pathinfo(__FILE__, PATHINFO_FILENAME));
    }
    public function getListFields()
    {
        $configs = array(
            'id_ets_sc_shipping_rule' => array(
                'type' => 'hidden',
                'label' => '',
                'default' => $this->id_ets_sc_shipping_rule,
            ),
            'type_action'=>array(
                'type'=>'radio',
                'label'=>$this->l('How this rule affects the default shipping cost'),
                'values' => array(
                    array(
                        'id' => 'type_action_increase',
                        'value' => 'increase',
                        'label' => $this->l('Increase default shipping cost'),
                    ),
                    array(
                        'id' => 'type_action_decrease',
                        'value' => 'decrease',
                        'label' => $this->l('Decrease default shipping cost'),
                    ),
                    array(
                        'id' => 'type_action_replace',
                        'value' => 'replace',
                        'label' => $this->l('Replace default shipping cost by the cost of this rule'),
                    ),
                    array(
                        'id' => 'type_action_free',
                        'value' => 'free',
                        'label' => $this->l('Mark as "Free shipping"'),
                    ),
                ),
                'required' => true,
                'default'=> 'increase',
            ),
            'calcalate_cost_by' => array(
                'type'=>'radio',
                'label'=>$this->l('Calculate cost by'),
                'values' => array(
                    array(
                        'id' => 'calcalate_cost_by_percent_shipping_cost',
                        'value' => 'percent_shipping_cost',
                        'label' => $this->l('Percentage of default shipping cost'),
                    ),
                    array(
                        'id' => 'calcalate_cost_by_percent_product_price',
                        'value' => 'percent_product_price',
                        'label' => $this->l('Percentage of the total product price'),
                    ),
                    array(
                        'id' => 'calcalate_cost_by_fixed_amount',
                        'value' => 'fixed_amount',
                        'label' => $this->l('Fixed amount'),
                    ),
                    array(
                        'id' => 'calcalate_cost_by_formula',
                        'value' => 'formula',
                        'label' => $this->l('Formula'),
                    ),
                ),
                'form_group_class' => 'type_action increase decrease replace',
                'default'=> 'percent_shipping_cost',
            ),
            'fees_percent' => array(
                'type' => 'text',
                'col' =>3,
                'label' => $this->l('Percentage value'),
                'suffix' => '%',
                'form_group_class' => 'type_action increase decrease replace',
                'validate' => 'isUnsignedFloat',
                'showRequired' => true,
            ),
            'fees_max' => array(
                'type' => 'text',
                'col' =>3,
                'label' => $this->l('Maximum cost value'),
                'suffix' => Context::getContext()->currency->sign,
                'form_group_class' => 'type_action increase decrease replace',
                'validate' => 'isUnsignedFloat',
            ),
            'fees_amount' => array(
                'type' => 'text',
                'col' =>3,
                'label' => $this->l('Amount value'),
                'suffix' => Context::getContext()->currency->sign,
                'form_group_class' => 'type_action increase decrease replace',
                'validate' => 'isUnsignedFloat',
                'showRequired' => true,
            ),
            'cal_percent_from' => array(
                'type'=>'radio',
                'label'=>$this->l('Calculate percentage from'),
                'values' => array(
                    array(
                        'id' => 'cal_percent_from_all_product',
                        'value' => 'all_product',
                        'label' => $this->l('Price of all products in shopping cart'),
                    ),
                    array(
                        'id' => 'cal_percent_from_product_of_condition',
                        'value' => 'product_of_condition',
                        'label' => $this->l('Price of the products satisfied the "Conditions" step of this shipping rule'),
                    ),
                ),
                'form_group_class' => 'type_action increase decrease replace',
                'default'=> 'all_product',
            ),
            'ignore_product_discounted'=> array(
                'type'=> 'switch',
                'label' => $this->l('Ignore price of discounted product'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'ignore_product_discounted_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'ignore_product_discounted_off',
                        'value' => 0,
                    )
                ),
                'default'=>1,
                'form_group_class' => 'type_action increase decrease replace',
            ),
            'exclude_product_tax'=> array(
                'type'=> 'switch',
                'label' => $this->l('Exclude product tax'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'exclude_product_tax_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'exclude_product_tax_off',
                        'value' => 0,
                    )
                ),
                'default'=>1,
                'form_group_class' => 'type_action increase decrease replace',
            ),
            'formular'=> array(
                'label' => $this->l('Formula'),
                'type'=> 'text',
                'form_group_class' => 'type_action increase decrease replace',
            ),
            'id_currency'=> array(
                'type'=> 'select',
                'label' => $this->l('Formula'),
                'default'=> Context::getContext()->currency->id,
                'options' => array(
                    'query' => Currency::getCurrencies(),
                    'id' => 'id',
                    'name' => 'sign'
                ),
                'form_group_class' => 'type_action increase decrease replace',
                'desc'=> $this->l('Variables available for the formula [highlight]totalproductamounttaxincl[end_highlight] [highlight]totalproductamounttaxexcl[end_highlight] [highlight]totalquantity[end_highlight]
[highlight]totalweight[end_highlight] [highlight]defaultshipping[end_highlight]'),
            ),
            'id_tax_rule_group' => array(
                'type' => 'select',
                'label' => $this->l('Apply tax rule to the cost after calculating'),
                'options' => array(
                    'query' => array_merge(array(array('id_tax_rules_group'=>0,'name'=> $this->l('No tax'))), TaxRulesGroup::getTaxRulesGroups()),
                    'id' => 'id_tax_rules_group',
                    'name' => 'name'
                ),
                'form_group_class' => 'type_action increase decrease replace',
            ),
            
        );
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->id ? $this->l('Action') : $this->l('Action') ,
                ),
                'input' => array(),
                'submit' => array(
                    'title' => $this->l('Save & next'),
                ),
                'name' => 'action_rule',
                'key' => 'id_ets_sc_action_rule',
                'buttons' => array(
                    array(
                        'href' => Context::getContext()->link->getAdminLink('AdminShippingCostRule').'&editshipping_rule=1&id_ets_sc_shipping_rule='.$this->id_ets_sc_shipping_rule.'&current_tab=condition',
                        'class'=> 'pull-left',
                        'icon'=> 'process-icon-back',
                        'title' => $this->l('Back'),
                    ),
                    array(
                        'title' => $this->l('Save & stay'),
                        'type' => 'submit',
                        'class' => 'pull-right',
                        'name' => 'btnSubmitActionRuleStay',
                        'icon' => 'process-icon-save',
                    )
                ),
            ),
            'configs' =>$configs, 
            'name_controller' => 'form_codition',
        );
    }
    public function validateCustomField(&$errors)
    {
        $type_action = Tools::getValue('type_action');
        if($type_action=='increase')
        {
            $calcalate_cost_by = Tools::getValue('calcalate_cost_by');
            if($calcalate_cost_by=='percent_shipping_cost' || $calcalate_cost_by=='percent_product_price')
            {
                $fees_percent = Tools::getValue('fees_percent');
                if($fees_percent=='')
                    $errors[] = $this->l('Percentage value is required');
                elseif(Validate::isUnsignedFloat($fees_percent) && $fees_percent==0)
                    $errors[] = $this->l('Percentage value is not valid');
            }
            elseif($calcalate_cost_by=='fixed_amount')
            {
                $fees_amount = Tools::getValue('fees_amount');
                if($fees_amount=='')
                    $errors[] = $this->l('Amount value is required');
                elseif(Validate::isUnsignedFloat($fees_amount) && $fees_amount==0)
                    $errors[] = $this->l('Amount value is not valid');
            }
            elseif($calcalate_cost_by=='formula')
            {
                $formular = Tools::getValue('formular');
                if($formular=='')
                    $errors[] = $this->l('Formula value is required');
                elseif(!Validate::isCleanHtml($formular))
                    $errors[] = $this->l('Formula value is not valid');
            }
        }
    }
    public static function getActionByIdRule($id_rule)
    {
    	$cache_id = 'ets_sc_action_rule_getActionByIdRule' . $id_rule;
    	if (!Cache::isStored($cache_id)) {
		    $id = (int)Db::getInstance()->getValue('SELECT id_ets_sc_action_rule FROM `'._DB_PREFIX_.'ets_sc_action_rule` WHERE id_ets_sc_shipping_rule='.(int)$id_rule);
		    if($id)
			    $action = new Ets_sc_action_rule($id);
		    else
		    {
			    $action = new Ets_sc_action_rule();
			    $action->id_ets_sc_shipping_rule = $id_rule;
		    }
		    Cache::store($cache_id, $action);
		    return $action;
	    } else {
    		return Cache::retrieve($cache_id);
	    }
    }
}