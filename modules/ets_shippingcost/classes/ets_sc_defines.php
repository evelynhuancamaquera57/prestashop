<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }

class Ets_sc_defines
{
    public static $instance;
    public $name;
    public $is17;
    public function __construct()
    {
        $this->name='ets_shippingcost';
        if(version_compare(_PS_VERSION_, '1.7', '>='))
            $this->is17 = true;
    }
    public static function getInstance()
    {
        if (!(isset(self::$instance)) || !self::$instance) {
            self::$instance = new Ets_sc_defines();
        }
        return self::$instance;
    }
    public function l($string)
    {
        return Translate::getModuleTranslation('ets_shippingcost', $string, pathinfo(__FILE__, PATHINFO_FILENAME));
    }
    public function installDb(){
        if(!is_dir(_PS_ETS_SC_IMG_DIR_))
        {
            @mkdir(_PS_ETS_SC_IMG_DIR_,0755,true);
            @copy(dirname(__FILE__).'/index.php', _PS_ETS_SC_IMG_DIR_. 'index.php');
        }
        $res = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_shipping_rule` ( 
        `id_ets_sc_shipping_rule` INT(11) NOT NULL AUTO_INCREMENT , 
        `id_shop` INT(11),   
        `active` INT(1) , 
        `priority` INT(11), 
        `new_customer` INT(11), 
        `id_carriers` VARCHAR(300),
        `type_combine_condition` VARCHAR(6),
        `date_add` DATETIME ,
        `date_upd` DATETIME ,
        INDEX(id_shop),
        PRIMARY KEY (`id_ets_sc_shipping_rule`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &=  Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_shipping_rule_lang` ( 
        `id_ets_sc_shipping_rule` INT(11) NOT NULL , 
        `id_lang` INT(11) NOT NULL,
        `name` VARCHAR(1000), 
        `description` TEXT, 
        PRIMARY KEY (`id_ets_sc_shipping_rule`,`id_lang`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci') ;
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_condition_rule` ( 
        `id_ets_sc_condition_rule` INT(11) NOT NULL AUTO_INCREMENT ,
        `id_ets_sc_shipping_rule` INT(11) NOT NULL , 
        `parent_codition` VARCHAR(222) , 
        `id_customers` VARCHAR(222),
        `only_apply_on_default_group` INT(1),
        `id_groups` VARCHAR(222),
        `customer_signed_up_from` datetime,
        `customer_signed_up_to` datetime,
        `days_since_signed_up_cal` VARCHAR(22),
        `days_since_singed_up_day` INT(11),
        `cart_amount_cal` VARCHAR(50),
        `cart_amount` FLOAT(10,2),
        `cart_amount_tax_incl` INT(1),
        `cart_amount_shipping_incl` INT(1),
        `cart_amount_discount_incl` INT(1),
        `specific_occasion` VARCHAR(50),
        `specific_occasion_hour_of_day_from` text,
        `specific_occasion_hour_of_day_to` text,
        `specific_occasion_day_of_week` text,
        `specific_occasion_day_of_week_from` text,
        `specific_occasion_day_of_week_to` text,
        `specific_occasion_month_of_year` text,
        `specific_occasion_month_of_year_from` text,
        `specific_occasion_month_of_year_to` text,
        `specific_occasion_date_from` text,
        `specific_occasion_date_to` text,
        `total_product_quantity_cal` VARCHAR(50),
        `total_product_quantity` INT(11),
        `total_weight_cal` VARCHAR(50),
        `total_weight` FLOAT(10,2),
        `quantity_of_same_product_cal` VARCHAR(50),
        `quantity_of_same_product` INT(11),
        `number_of_different_product_cal` VARCHAR(50),
        `number_of_different_product` INT(11),
        `number_of_product_in_same_category_cal` VARCHAR(50),
        `number_of_product_in_same_category` INT(11),
        `apply_for_discounted_products` INT(1),
        `products_with_different_attribute` INT(1),
        `applicable_product_categories` VARCHAR(50),
        `include_specific_products` VARCHAR(300),
        `exclude_products` VARCHAR(300),
        `apply_all_attribute` INT(1),
        `select_attributes` VARCHAR(300),
        `apply_all_features` INT(1),
        `select_features` VARCHAR(300),
        `apply_all_supplier` INT(1),
        `select_suppliers` VARCHAR(300),
        `apply_all_manufacturer` INT(1),
        `select_manufacturers` VARCHAR(300),
        `apply_for_product_price_cal` VARCHAR(50),
        `apply_for_product_price` FLOAT(10,2),
        `apply_for_availabled_quantity_stock_cal` VARCHAR(50),
        `apply_for_availabled_quantity_stock` INT(11),
        `include_sub_categories` INT(1),
        `applicable_categories` text,
        `delivery_zone` text,
        `delivery_country_zone` INT(11),
        `delivery_country` text,
        `delivery_state_zone` INT(11),
        `delivery_state_country` INT(11),
        `delivery_state` text,
        `delivery_zipcode_type` text,
        `delivery_zipcode_from` text,
        `delivery_zipcode_to` text,
        `delivery_zipcode_start_from` text,
        `delivery_zipcode_end_at` text,
        `delivery_zipcode_is_exactly` text,
        `delivery_zipcode_different` text,
        INDEX(id_ets_sc_shipping_rule), PRIMARY KEY (`id_ets_sc_condition_rule`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_action_rule` ( 
        `id_ets_sc_action_rule` INT(11) NOT NULL AUTO_INCREMENT , 
        `type_action` VARCHAR(100) NULL , 
        `calcalate_cost_by` VARCHAR(100) NULL , 
        `fees_percent` FLOAT(10,2) NULL , 
        `fees_amount` FLOAT(10,2) NULL , 
        `fees_max` FLOAT(10,2) NULL , 
        `id_tax_rule_group` INT(11) NULL , 
        `id_ets_sc_shipping_rule` INT(11) NULL ,
        `id_currency` INT(11),
        `cal_percent_from` VARCHAR(50),
        `ignore_product_discounted` INT(1),
        `exclude_product_tax` INT(1),
        `formular` text,
        PRIMARY KEY (`id_ets_sc_action_rule`), INDEX (`id_ets_sc_shipping_rule`))
        ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_promote_rule` ( 
        `id_ets_sc_promote_rule` INT(11) NOT NULL AUTO_INCREMENT ,
        `id_ets_sc_shipping_rule` INT(11) NOT NULL , 
        `way_to_promote` VARCHAR(55),
        `position_to_display` VARCHAR(600),
        `text_color` VARCHAR(55),
        `background_color` VARCHAR(55),
        `button_close_color` VARCHAR(55),
        `title_popup_color` VARCHAR(55),
        `background_title_popup_color` VARCHAR(55),
        `background_content_popup_color` VARCHAR(55),
        `border_radius` VARCHAR(55),
        `close_button_popup_color` VARCHAR(55),
        `applicable_product_categories` VARCHAR(500),
        `applicable_categories`  VARCHAR(500),
        `include_sub_categories` VARCHAR(500),
        `include_specific_products` VARCHAR(500),
        `exclude_products` VARCHAR(500),
        `delay_popup` INT(11),
        `notification_text_color` VARCHAR(55),
        `notification_background_color` VARCHAR(55),
        `notification_border_color` VARCHAR(55),
        `only_apply_on_default_group` INT(1),
        `id_groups` VARCHAR(222),
        `enabled` INT(1),
        INDEX(id_ets_sc_shipping_rule),INDEX(enabled), PRIMARY KEY (`id_ets_sc_promote_rule`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_promote_rule_lang` ( 
        `id_ets_sc_promote_rule` INT(11) NOT NULL,
        `id_lang` INT(11) NOT NULL , 
        `notification` text,
        `banner` VARCHAR(200),
        `banner_link` VARCHAR(300),
        `content_bar` VARCHAR(500),
        `title_popup` VARCHAR(500),
        `content_popup` VARCHAR(500),
         PRIMARY KEY (`id_ets_sc_promote_rule`,`id_lang`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_cart_rule` ( 
        `id_cart` INT(11) NOT NULL , 
        `id_carrier` INT(11) NOT NULL , 
        `id_ets_sc_shipping_rule` INT(11) NOT NULL , 
        `type` VARCHAR(15) NOT NULL , 
        `shipping_price_default` FLOAT(10,2) NOT NULL , 
        `rule_price` FLOAT(10,2) NOT NULL , 
        PRIMARY KEY (`id_cart`, `id_carrier`,`id_ets_sc_shipping_rule`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        $res &= Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ets_sc_order_rule` ( 
        `id_order` INT(11) NOT NULL , 
        `id_ets_sc_shipping_rule` INT(11) NOT NULL , 
        `id_carrier` INT(11) NOT NULL , 
        `shipping_price_default` FLOAT(10,2) NOT NULL , 
        `shipping_price` FLOAT(10,2) NOT NULL , 
        `rule_price` FLOAT(10,2),
        `type` VARCHAR(30) NOT NULL , 
        PRIMARY KEY (`id_order`, `id_ets_sc_shipping_rule`, `id_carrier`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci');
        return $res;
    }
    public function checkCreatedColumn($table, $column)
    {
        $fieldsCustomers = Db::getInstance()->ExecuteS('DESCRIBE ' . _DB_PREFIX_ . bqSQL($table));
        $check_add = false;
        foreach ($fieldsCustomers as $field) {
            if ($field['Field'] == $column) {
                $check_add = true;
                break;
            }
        }
        return $check_add;
    }
    public function unInstallDb()
    {
        $tables = array(
            'ets_sc_shipping_rule',
            'ets_sc_shipping_rule_lang',
            'ets_sc_condition_rule',
            'ets_sc_action_rule',
            'ets_sc_promote_rule',
            'ets_sc_promote_rule_lang',
            'ets_sc_cart_rule',
            'ets_sc_order_rule'
        );
        if($tables)
        {
            foreach($tables as $table)
               Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . bqSQL($table).'`');
        }
        return true;
    }
    public static function getFormattedName($name)
    {
        $themeName = Context::getContext()->shop->theme_name;
        $nameWithoutThemeName = str_replace(['_' . $themeName, $themeName . '_'], '', $name);

        //check if the theme name is already in $name if yes only return $name
        if ($themeName !== null && strstr($name, $themeName) && ImageType::getByNameNType($name)) {
            return $name;
        }

        if (ImageType::getByNameNType($nameWithoutThemeName . '_' . $themeName)) {
            return $nameWithoutThemeName . '_' . $themeName;
        }

        if (ImageType::getByNameNType($themeName . '_' . $nameWithoutThemeName)) {
            return $themeName . '_' . $nameWithoutThemeName;
        }
        return $nameWithoutThemeName . '_default';
    }
    public function searchProduct()
    {
        if (($query = Tools::getValue('q', false)) && Validate::isCleanHtml($query))
        {
            $imageType = self::getFormattedName('cart');
            if ($pos = strpos($query, ' (ref:')) {
                $query = Tools::substr($query, 0, $pos);
            }
            $excludeIds = Tools::getValue('excludeIds', false);
            $excludedProductIds = array();
            if ($excludeIds && $excludeIds != 'NaN' && Validate::isCleanHtml($excludeIds)) {
                $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
                if($excludeIds && ($ids = explode(',',$excludeIds)) ) {
                    foreach($ids as $id) {
                        $id = explode('-',$id);
                        if(isset($id[0]) && isset($id[1]) && !$id[1]) {
                            $excludedProductIds[] = (int)$id[0];
                        }
                    }
                }
            } else {
                $excludeIds = false;
            }
            $excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);
            $exclude_packs = (bool)Tools::getValue('exclude_packs', false);
            if (version_compare(_PS_VERSION_, '1.6.1.0', '<'))
            {
                $imgLeftJoin = ' LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`) '.Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover = 1');
            }
            else
            {
                $imgLeftJoin = ' LEFT JOIN `' . _DB_PREFIX_ . 'image_shop` image_shop ON (image_shop.`id_product` = p.`id_product` AND image_shop.id_shop=' . (int)Context::getContext()->shop->id . ' AND image_shop.cover = 1) ';
            }
            $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
            		FROM `' . _DB_PREFIX_ . 'product` p
            		' . Shop::addSqlAssociation('product', 'p') . '
                    LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = ' . (int)Context::getContext()->language->id . Shop::addSqlRestrictionOnLang('pl') . ')
            		'. pSQL($imgLeftJoin) .' 
            		LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int)Context::getContext()->language->id . ')
            		LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (p.`id_product` = ps.`id_product`) 
            		WHERE '.($excludedProductIds ? 'p.`id_product` NOT IN('.pSQL(implode(',',$excludedProductIds)).') AND ' : '').' (pl.name LIKE \'%' . pSQL($query) . '%\' OR p.reference LIKE \'%' . pSQL($query) . '%\' OR p.id_product = '.(int)$query.') AND ps.`active` = 1 AND ps.`id_shop` = '.(int)Context::getContext()->shop->id .
                   ($excludeVirtuals ? ' AND NOT EXISTS (SELECT 1 FROM `' . _DB_PREFIX_ . 'product_download` pd WHERE (pd.id_product = p.id_product))' : '') .
                   ($exclude_packs ? ' AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '') .
                   '  GROUP BY p.id_product';

            if (($items = Db::getInstance()->executeS($sql)))
            {
                $results = array();
                foreach ($items as $item)
                {
                    if(!$item['id_image'])
                    {
                        $image = Product::getCover($item['id_product']);
                        if($image)
                            $item['id_image'] = $image['id_image'];
                    }
                    $results[] = array(
                        'id_product' => (int)($item['id_product']),
                        'id_product_attribute' => 0,
                        'name' => $item['name'],
                        'attribute' => '',
                        'ref' => (!empty($item['reference']) ? $item['reference'] : ''),
                        'image' => str_replace('http://', Tools::getShopProtocol(), Context::getContext()->link->getImageLink($item['link_rewrite'], $item['id_image'] ? : Context::getContext()->language->iso_code.'-default', $imageType)),
                    );
                }
                if ($results)
                {
                    foreach ($results as &$item)
                        echo trim($item['id_product'] . '|' . (int)($item['id_product_attribute']) . '|' . Tools::ucfirst($item['name']). '|' . $item['attribute'] . '|' . $item['ref'] . '|' . $item['image']).'|'.Context::getContext()->link->getProductLink($item['id_product'],null,null,null,null,null,$item['id_product_attribute']). "\n";
                }
            }
            die;
        }
        die;
    }
    public function searchCustomer()
    {
        if (($query = Tools::getValue('q', false)) && Validate::isCleanHtml($query))
        {
            $excludeIds = Tools::getValue('excludeIds', false);
            $excludedCustomerds = array();
            if ($excludeIds && $excludeIds != 'NaN' && Validate::isCleanHtml($excludeIds)) {
                $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
                if($excludeIds && ($ids = explode(',',$excludeIds)) ) {
                    foreach($ids as $id) {
                        $excludedCustomerds[] = $id;
                    }
                }
            } else {
                $excludeIds = false;
            }
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'customer` WHERE id_shop="'.(int)Context::getContext()->shop->id.'" AND (CONCAT(firstname," ",lastname) LIKE "%'.pSQL($query).'%" OR email like "%'.pSQL($query).'%")'.($excludedCustomerds ? ' AND id_customer NOT IN ('.implode(',',array_map('intval',$excludedCustomerds)).')':'');
            $customers = Db::getInstance()->executeS($sql);   
            if ($customers)
                {
                    foreach ($customers as &$item)
                        echo $item['id_customer'] . '|' . $item['firstname'].' '.$item['lastname'] . '|' . $item['email']. "\n";
                }
        }
        die;
    }
    public function getProductsByIds($products)
    {
        if (!$products)
                return false;
        if (!is_array($products))
        {
            $IDs = explode(',', $products);
            $products = array();
            foreach ($IDs as $ID) {
                if ($ID &&($tmpIDs = explode('-', $ID)) && $tmpIDs[0] ) {
                    $products[] = array(
                        'id_product' => $tmpIDs[0],
                        'id_product_attribute' => isset($tmpIDs[1])? $tmpIDs[1] : 0,
                    );
                }
            }
        }
        if($products)
        {
            $context = Context::getContext();
            $id_group = isset($context->customer->id) && $context->customer->id? Customer::getDefaultGroupId((int)$context->customer->id) : (int)Group::getCurrent()->id;
            $group = new Group($id_group);
            $useTax = $group->price_display_method? false : true;
            $imageType =  self::getFormattedName('cart');
            foreach($products as &$product)
            {
                $p = new Product($product['id_product'], true, $context->language->id, $context->shop->id);
                $product['link_rewrite'] = $p->link_rewrite;
                $product['price_float'] = $p->getPrice($useTax,$product['id_product_attribute'] ? $product['id_product_attribute'] : null);
                $product['price'] = Tools::displayPrice($product['price_float']);
                $product['name'] = $p->name;
                $product['description_short'] = $p->description_short;
                $image = ($product['id_product_attribute'] && ($image = self::getCombinationImageById($product['id_product_attribute'],$context->language->id))) ? $image : Product::getCover($product['id_product']);
                $product['link'] = $context->link->getProductLink($product,null,null,null,null,null,$product['id_product_attribute'] ? $product['id_product_attribute'] : 0);
                $product['id_image'] = isset($image['id_image']) && $image['id_image'] ? $image['id_image'] : $context->language->iso_code.'-default';
                $product['image'] = $context->link->getImageLink($p->link_rewrite, isset($image['id_image']) ? $image['id_image'] : $context->language->iso_code.'-default', $imageType);
                if($product['id_product_attribute'])
                {
                    $attributes = $p->getAttributeCombinationsById((int)$product['id_product_attribute'],$context->language->id);
                    if($attributes)
                    {
                        $product['attributes']='';
                        foreach($attributes as $attribute)
                        {
                            $product['attributes'] .= $attribute['group_name'].': '.$attribute['attribute_name'].', ';
                        }
                        $product['attributes'] = trim($product['attributes'],', ');
                    }
                }
            }
            unset($context);
        }
        return $products;
    }
    public static function getCombinationImageById($id_product_attribute, $id_lang)
    {
        if(version_compare(_PS_VERSION_,'1.6.1.0', '>=')) {
            return Product::getCombinationImageById($id_product_attribute, $id_lang);
        }
        else
        {
            if (!Combination::isFeatureActive() || !$id_product_attribute) {
                return false;
            }
            $result = Db::getInstance()->executeS('
                SELECT pai.`id_image`, pai.`id_product_attribute`, il.`legend`
                FROM `'._DB_PREFIX_.'product_attribute_image` pai
                LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (il.`id_image` = pai.`id_image`)
                LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_image` = pai.`id_image`)
                WHERE pai.`id_product_attribute` = '.(int)$id_product_attribute.' AND il.`id_lang` = '.(int)$id_lang.' ORDER by i.`position` LIMIT 1'
            );
            if (!$result) {
                return false;
            }
            return $result[0];
        }
    }
    public static function getCategoriesById($id_categories)
    {
        if($id_categories)
        {
            $ids = explode(',',$id_categories);
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'category` c
            LEFT JOIN `'._DB_PREFIX_.'category_lang` cl on (c.id_category = cl.id_category AND cl.id_lang="'.(int)Context::getContext()->language->id.'" AND cl.id_shop="'.(int)Context::getContext()->shop->id.'")
            WHERE c.id_category IN ('.implode(',',array_map('intval',$ids)).')
            GROUP BY c.id_category';
            return Db::getInstance()->executeS($sql);
        }
    }
    public function getConfigInputs()
    {
        return array(
            array(
                'name' => 'ETS_SC_STATUS_ORDER_VALIDATED',
                'label' => $this->l('Total order amount will be calculated if order status is'),
                'type'=> 'checkbox',
                'values' => array(
                    'query' => OrderState::getOrderStates(Context::getContext()->language->id),
                    'id' => 'id_order_state',
                    'name' => 'name',
                ),
                'default' => $this->getOrderStateDefault(),
                'tab' => 'general',
            ),
        );
    }
    public function getOrderStateDefault()
    {
        $orderStates = OrderState::getOrderStates(Context::getContext()->language->id);
        $defaults= array();
        if($orderStates)
        {
            foreach($orderStates as $orderState)
            {
                if($orderState['paid']==1)
                    $defaults[] = $orderState['id_order_state'];
            }
        }
        if($defaults)
            return implode(',',$defaults);
        else
            return '';
    }

	public static function displayText($content=null,$tag=null,$class=null,$id=null,$href=null,$blank=false,$src = null,$alt = null,$name = null,$value = null,$type = null,$data_id_product = null,$rel = null,$attr_datas=null, $title = null) {
		$text ='';
		if($tag)
		{
			$text .= '<'.$tag.($class ? ' class="'.$class.'"':'').($id ? ' id="'.$id.'"':'');
			if($href)
				$text .=' href="'.$href.'"';
			if($blank && $tag ='a')
				$text .=' target="_blank"';
			if($src)
				$text .=' src ="'.$src.'"';
			if($name)
				$text .=' name="'.$name.'"';
			if($value)
				$text .=' value ="'.$value.'"';
			if($type)
				$text .= ' type="'.$type.'"';
			if($data_id_product)
				$text .=' data-id_product="'.(int)$data_id_product.'"';
			if($rel) {
				$text .=' rel="'.$rel.'"';
			}
			if($alt)
				$text .=' alt="'.$alt.'"';
			if($title)
				$text .=' title="'.$title.'"';
			if($attr_datas)
			{
				foreach($attr_datas as $data)
				{
					$text .=' '.$data['name'].'='.'"'.$data['value'].'"';
				}
			}
			if($tag=='img' || $tag=='br' || $tag=='input')
				$text .='/>';
			else
				$text .='>';
			if ($tag && $tag != 'img' && $tag != 'input' && $tag != 'br' && !is_null($content))
				$text .= $content;
			if ($tag && $tag != 'img' && $tag != 'path' && $tag != 'input' && $tag != 'br')
				$text .= '<'.'/' . $tag . '>';
		}
		else if ($content)
		{
			$text = $content;
		}
		return $text;
	}
    public static function checkEnableOtherShop($id_module)
    {
        $sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'module_shop` WHERE `id_module` = ' . (int) $id_module . ' AND `id_shop` NOT IN(' . implode(', ', Shop::getContextListShopID()) . ')';
        return Db::getInstance()->executeS($sql);
    }
    public static function activeTab($module_name)
    {
        return Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'tab` SET enabled=1 where module ="'.pSQL($module_name).'"');
    }
}