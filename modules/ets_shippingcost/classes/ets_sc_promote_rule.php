<?php
/**
 * Copyright ETS Software Technology Co., Ltd
 *
 * NOTICE OF LICENSE
 *
 * This file is not open source! Each license that you purchased is only available for 1 website only.
 * If you want to use this file on more websites (or projects), you need to purchase additional licenses.
 * You are not allowed to redistribute, resell, lease, license, sub-license or offer our resources to any third party.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future.
 *
 * @author ETS Software Technology Co., Ltd
 * @copyright  ETS Software Technology Co., Ltd
 * @license    Valid for 1 website (or project) for each purchase of license
 */

if (!defined('_PS_VERSION_')) { exit; }
class Ets_sc_promote_rule extends Ets_sc_obj
{
    public static $instance;
    public $id_ets_sc_shipping_rule;
    public $way_to_promote;
    public $position_to_display;
    public $notification;
    public $banner;
    public $banner_link;
    public $content_bar;
    public $text_color;
    public $background_color;
    public $button_close_color;
    public $title_popup;
    public $content_popup;
    public $title_popup_color;
    public $background_title_popup_color;
    public $background_content_popup_color;
    public $border_radius;
    public $close_button_popup_color;
    public $applicable_product_categories;
    public $applicable_categories;
    public $include_sub_categories;
    public $include_specific_products;
    public $exclude_products;
    public $delay_popup;
    public $notification_text_color;
    public $notification_background_color;
    public $notification_border_color;
    public $id_groups;
    public $only_apply_on_default_group;
    public $enabled;
    public static $definition = array(
		'table' => 'ets_sc_promote_rule',
		'primary' => 'id_ets_sc_promote_rule',
        'multilang' => true,
		'fields' => array(
            'id_ets_sc_shipping_rule' => array('type' => self::TYPE_INT),
            'way_to_promote' => array('type' => self::TYPE_STRING),
            'position_to_display' => array('type' => self::TYPE_STRING),
            'text_color' => array('type' => self::TYPE_STRING),
            'background_color' => array('type' => self::TYPE_STRING),
            'button_close_color' => array('type' => self::TYPE_STRING),
            'title_popup_color' => array('type' => self::TYPE_STRING),
            'background_title_popup_color' => array('type' => self::TYPE_STRING),
            'background_content_popup_color' => array('type' => self::TYPE_STRING),
            'border_radius' => array('type' => self::TYPE_STRING),
            'close_button_popup_color' => array('type' => self::TYPE_STRING),
            'applicable_product_categories' => array('type' => self::TYPE_STRING),
            'applicable_categories' => array('type' => self::TYPE_STRING),
            'include_sub_categories' => array('type' => self::TYPE_STRING),
            'include_specific_products' => array('type' => self::TYPE_STRING),
            'exclude_products' => array('type' => self::TYPE_STRING),
            'delay_popup' => array('type'=> self::TYPE_INT),
            'enabled' => array('type'=> self::TYPE_INT),
            'notification_text_color' => array('type' => self::TYPE_STRING),
            'notification_background_color' => array('type' => self::TYPE_STRING),
            'notification_border_color' => array('type' => self::TYPE_STRING),
            'id_groups' => array('type' => self::TYPE_STRING),
            'only_apply_on_default_group' => array('type' => self::TYPE_INT),
            'notification' => array('type' => self::TYPE_STRING,'lang'=>true),
            'banner' => array('type' => self::TYPE_STRING,'lang'=>true),
            'banner_link' => array('type' => self::TYPE_STRING,'lang'=>true),
            'content_bar' => array('type' => self::TYPE_STRING,'lang'=>true),
            'title_popup' => array('type' => self::TYPE_STRING,'lang'=>true),
            'content_popup' => array('type' => self::TYPE_HTML,'lang'=>true),
        ),
    );
    public	function __construct($id_item = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_item, $id_lang, $id_shop);
		if (!$this->module)
			$this->module = new Ets_shippingcost();
	}
    public static function getInstance()
    {
        if (!(isset(self::$instance)) || !self::$instance) {
            self::$instance = new Ets_sc_promote_rule();
        }
        return self::$instance;
    }
    public function l($string,$file_name='')
    {
        return Translate::getModuleTranslation('ets_shippingcost', $string, $file_name ? : pathinfo(__FILE__, PATHINFO_FILENAME));
    }
    public function getListFields()
    {
        $configs = array(
            'id_ets_sc_shipping_rule' => array(
                'type' => 'hidden',
                'label' => '',
                'default' => $this->id_ets_sc_shipping_rule,
                'validate'=>'isUnsignedId',
            ),
            'way_to_promote' =>array(
                'type' => 'select',
                'label' => $this->l('How to promote shipping rule:'),
                'options' => array(
                    'query' => array(
                        array(
                            'id' => 'display_notification',
                            'name' => $this->l('Display notification message'),
                        ),
                        array(
                            'id' => 'display_banner',
                            'name' => $this->l('Display banner'),
                        ),
                        array(
                            'id' => 'display_popup',
                            'name' => $this->l('Display popup'),
                        ),
                        array(
                            'id' => 'display_highlight_bar',
                            'name' => $this->l('Display highlight bar notification'),
                        )
                    ),
                    'id' => 'id',
                    'name' => 'name'
                ),
                'validate'=>'isCleanHtml',
            ),
            'notification' => array(
                'type' => 'textarea',
                'lang'=> true,
                'label' => $this->l('Notification message'),
                'form_group_class' => 'way_to_promote display_notification',
                'desc' => $this->l('Available tags: [highlight][shipping_rule][end_highlight] [highlight][value][end_highlight] [highlight][carrier][end_highlight]'),
                'validate'=>'isCleanHtml',
                'showRequired' => true,
            ),
            'banner' => array(
                'type' => 'file_lang',
                'label' => $this->l('Banner'),
                'form_group_class' => 'way_to_promote display_banner',
                'desc' => sprintf($this->l('Available image type: jpg, png, gif, jpeg. Limit %sMb'),Configuration::get('PS_ATTACHMENT_MAXIMUM_SIZE')),
                'showRequired' => true,
            ),
            'banner_link' => array(
                'type' => 'text',
                'label' => $this->l('Banner link'),
                'form_group_class' => 'way_to_promote display_banner',
                'lang' => true,
                'validate'=>'isLink',
            ),
            'content_bar' => array(
                'type' => 'text',
                'label' => $this->l('Content'),
                'lang' => true,
                'form_group_class' => 'way_to_promote display_highlight_bar',
                'validate'=>'isCleanHtml',
                'desc' => $this->l('Available tags: [highlight][shipping_rule][end_highlight] [highlight][value][end_highlight] [highlight][carrier][end_highlight]'),
                'showRequired' => true,
            ),
            'text_color' => array(
                'type' => 'color',
                'label' => $this->l('Text color'),
                'form_group_class' => 'way_to_promote display_highlight_bar',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#31708f',
            ),
            'background_color' => array(
                'type' => 'color',
                'label' => $this->l('Background color'),
                'form_group_class' => 'way_to_promote display_highlight_bar',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#d9edf7',
            ),
            'button_close_color' => array(
                'type' => 'color',
                'label' => $this->l('Close button color'),
                'form_group_class' => 'way_to_promote display_highlight_bar',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#31708f',
            ),
            'title_popup' => array(
                'type' => 'text',
                'lang' => true,
                'label' => $this->l('Title'),
                'form_group_class' => 'way_to_promote display_popup',
                'validate'=>'isCleanHtml',
                
            ),
            'content_popup' => array(
                'type' => 'textarea',
                'lang' => true,
                'label' => $this->l('Content'),
                'autoload_rte' => true,
                'form_group_class' => 'way_to_promote display_popup',
                'validate'=>'isCleanHtml',
                'showRequired' => true,
                'desc' =>$this->l('Available tags: [highlight][shipping_rule][end_highlight] [highlight][value][end_highlight] [highlight][carrier][end_highlight]'),
            ),
            'title_popup_color' => array(
                'type' => 'color',
                'label' => $this->l('Title text color'),
                'form_group_class' => 'way_to_promote display_popup',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#232323',
            ),
            'background_title_popup_color' => array(
                'type' => 'color',
                'label' => $this->l('Title background color'),
                'form_group_class' => 'way_to_promote display_popup',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#ffffff',
            ),
            'background_content_popup_color' => array(
                'type' => 'color',
                'label' => $this->l('Content background color'),
                'form_group_class' => 'way_to_promote display_popup',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#ffffff',
            ),
            'close_button_popup_color' => array(
                'type' => 'color',
                'label' => $this->l('Close button color'),
                'form_group_class' => 'way_to_promote display_popup',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#232323',
            ),
            'border_radius' => array(
                'label' => $this->l('Border radius'),
                'type' => 'range',
                'data_suffix' => 'px',
                'data_suffixs' => 'px',
                'min' =>0,
                'max' => 20,
                'form_group_class' => 'way_to_promote display_popup',
                'validate' => 'isCleanHtml',
                'default' => 5,
            ),
            'delay_popup' => array(
                'label' => $this->l('Delay to display since customer accessed website'),
                'type' => 'text',
                'suffix' => $this->l('Seconds'),
                'default' => 5,
                'validate' => 'isUnsignedInt',
                'form_group_class' => 'way_to_promote display_popup',
                'col'=>3,
                'desc' => $this->l('Leaving it blank will display a popup right when customer accessed website'),
            ),
            'position_to_display' => array(
                'type' => 'checkbox',
                'label' => $this->l('Display position'),
                'values' => array(
                    'query' => $this->getPositions(),
                    'id' => 'id_option',
                    'name' => 'name'
                ),
                'form_group_class' => 'way_to_promote display_notification display_banner',
                'validate'=>'isCleanHtml',
                'showRequired' => true,
            ),
            'applicable_product_categories' => array(
                'type' => 'radio',
                'label' => $this->l('Select product category page(s) to display'),
                'desc' => $this->l('Select the category page(s) you want to display shipping notification and/or banner.'),
                'default' => 'all_product',
                'values' => array(
                    array(
                        'id' => 'applicable_product_categories_all_product',
                        'value' => 'all_product',
                        'label' => $this->l('All product categories'),
                    ),
                    array(
                        'id' => 'applicable_product_categories_specific_product',
                        'value' => 'specific_product',
                        'label' => $this->l('Specific product categories'),
                    ),
                ),
               'form_group_class' => 'display_hook',
                'validate' => 'isCleanHtml',
            ),
            'applicable_categories' => array(
                'label' => $this->l('Select categories to apply condition'),
                'type' => 'categories',
                'tree' => array(
                    'id' => 'loyalty-categories-tree',
                    'selected_categories' => $this->applicable_categories ? explode(',',$this->applicable_categories):array(),
                    'disabled_categories' => null,
                    'use_checkbox' => true,
                    'root_category' => Category::getRootCategory()->id
                ),
                'form_group_class' => 'display_hook',
            ),
            'include_sub_categories' => array(
                'type' => 'switch',
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'include_sub_categories_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'include_sub_categories_off',
                        'value' => 0,
                    )
                ),
                'label' => $this->l('Include products in subcategories'),
               'form_group_class' => 'display_hook',
                'validate' => 'isUnsignedInt',
            ),
            'include_specific_products' => array(
                'type' => 'search_product',
                'label' => $this->l('Include specific products'),
                'form_group_class' => 'display_hook',
                'validate' => 'isCleanHtml',
                'placeholder' => $this->l('Search by product name, reference, id')
                
            ),
            'exclude_products' => array(
                'type' => 'search_product',
                'label' => $this->l('Exclude products'),
                'form_group_class' => 'display_hook',
                'validate' => 'isCleanHtml',
                'placeholder' => $this->l('Search by product name, reference, id')
            ),
            'id_groups' => array(
                'type' => 'checkbox',
                'label' => $this->l('Customer group'),
                'values' => array(
                    'query' => $this->getGroups(),
                    'id' => 'id_group',
                    'name' => 'name'
                ),
                'validate'=>'isCleanHtml',
            ),
            'only_apply_on_default_group' => array(
                'type'=>'switch',
                'label'=>$this->l('Only apply on the default group'),
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'only_apply_on_default_group_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'only_apply_on_default_group_off',
                        'value' => 0,
                    )
                ),
                'validate'=>'isInt',
            ),
            'notification_text_color' => array(
                'type' => 'color',
                'label' => $this->l('Text color'),
                'form_group_class' => 'way_to_promote display_notification',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#31708f',
            ),
            'notification_background_color' => array(
                'type' => 'color',
                'label' => $this->l('Background color'),
                'form_group_class' => 'way_to_promote display_notification',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#d9edf7',
            ),
            'notification_border_color' => array(
                'type' => 'color',
                'label' => $this->l('Border color'),
                'form_group_class' => 'way_to_promote display_notification',
                'validate' => 'isColor',
                'class' => 'color',
                'default' => '#d9edf7',
            ),
            'enabled'=>array(
                'type'=>'switch',
                'label'=>$this->l('Active'),
                'default' => 1,
                'values' => array(
                    array(
                        'label' => $this->l('Yes'),
                        'id' => 'enabled_on',
                        'value' => 1,
                    ),
                    array(
                        'label' => $this->l('No'),
                        'id' => 'enabled_off',
                        'value' => 0,
                    )
                ),
            ),
        );
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->id ? $this->l('Edit promotion way') : $this->l('Add promotion way') ,
                ),
                'input' => array(),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                'buttons'=> array(
                    array(
                        'title' => $this->l('Cancel'),
                        'type' => 'submit',
                        'class' => 'pull-left',
                        'name' => 'btncancel',
                        'icon' => 'process-icon-cancel',
                    )
                ),
                'name' => 'promote_discount_rule',
                'key' => 'id_ets_sc_promote_rule',
            ),
            'configs' =>$configs, 
        );
    }
    public function validateCustomField(&$errors)
    {
        $way_to_promote = Tools::getValue('way_to_promote');
        $id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
        if($way_to_promote=='display_notification')
        {
            $notification = Tools::getValue('notification_'.$id_lang_default);
            if($notification=='')
                $errors[] = $this->l('Notification is required');
            elseif(!Validate::isCleanHtml($notification))
                $errors[] = $this->l('Notification is not valid');
        }
        elseif($way_to_promote=='display_banner')
        {
            if(!$this->banner[$id_lang_default] && ( !isset($_FILES['banner_'.$id_lang_default]) || !isset($_FILES['banner_'.$id_lang_default]['name']) || !$_FILES['banner_'.$id_lang_default]['name'] ))
                $errors[] = $this->l('Banner is required');
        }
        elseif($way_to_promote=='display_popup')
        {
            $content_popup = Tools::getValue('content_popup_'.$id_lang_default);
            if($content_popup=='')
                $errors[] = $this->l('Popup content is required');
            elseif(!Validate::isCleanHtml($content_popup))
                $errors[] = $this->l('Popup content is not valid');
        }
        elseif($way_to_promote=='display_highlight_bar')
        {
            $content_bar = Tools::getValue('content_bar_'.$id_lang_default);
            if($content_bar=='')
                $errors[] = $this->l('Content is required');
            elseif(!Validate::isCleanHtml($content_bar))
                $errors[] = $this->l('Content is not valid');
        }
        if($way_to_promote=='display_notification' || $way_to_promote=='display_banner')
        {
            $position_to_display = Tools::getValue('position_to_display');
            if(!$position_to_display)
                $errors[] = $this->l('Display position is required');
            elseif(!Ets_shippingcost::validateArray($position_to_display))
                $errors[] = $this->l('Display position is not valid');
        }
    }
    public function getPositions()
    {
        $positions =  array(
            'displayHome' => array(
                'id_option' => 'displayHome',
                'name' => $this->l('[highlight]Home page[end_highlight]')
            ),
            'displayLeftColumn' => array(
                'id_option' => 'displayLeftColumn',
                'name' => $this->l('[highlight]Left column:[end_highlight] On the bottom of the left column')
            ),
            'displayLeftColumnBefore' => array(
                'id_option' => 'displayBeforeLeftColumn',
                'name' => $this->l('[highlight]Left column:[end_highlight] On the top of the left column')
            ),
            'displayRightColumn' => array(
                'id_option' => 'displayRightColumn',
                'name' => $this->l('[highlight]Right column:[end_highlight] On the bottom of the right column')
            ),
            'displayRightColumnBefore' => array(
                'id_option' => 'displayBeforeRightColumn',
                'name' => $this->l('[highlight]Right column:[end_highlight] On the top of the right column')
            ),
            'displayCartGridBodyBefore2' => array(
                'id_option' => 'displayCartGridBodyBefore2',
                'name' => $this->l('[highlight]Checkout page:[end_highlight] On top of the checkout page')
            ),
            'displayCartGridBodyAfter' => array(
                'id_option' => 'displayCartGridBodyAfter',
                'name' => $this->l('[highlight]Checkout page:[end_highlight] On the bottom of the checkout page')
            ),
            'displayProductListHeaderBefore' => array(
                'id_option' => 'displayProductListHeaderBefore',
                'name'=> $this->l('[highlight]Category page:[end_highlight] On top of the header of product listing page'),
                'class'=> 'display_hook',
            ),
            'displayProductListHeaderAfter' => array(
                'id_option' => 'displayProductListHeaderAfter',
                'name'=> $this->l('[highlight]Category page:[end_highlight] Under the header of product listing page'),
                'class'=> 'display_hook',
            ),
            'displayFooterCategory' => array(
                'id_option' => 'displayFooterCategory',
                'name'=> $this->l('[highlight]Category page:[end_highlight] On the bottom of product category page'),
                'class'=> 'display_hook',
            ),
            'displayNav1' => array(
                'id_option' => 'displayNav1',
                'name'=> $this->l('[highlight]Header:[end_highlight] On the top navigation bar'),
            ),
            'displayFooterBefore' => array(
                'id_option' => 'displayFooterBefore',
                'name' => $this->l('[highlight]Footer:[end_highlight] On top of Footer section'),
            ),
            'displayFooterAfter' => array(
                'id_option' => 'displayFooterAfter',
                'name' => $this->l('[highlight]Footer:[end_highlight] On the bottom of Footer section'),
            ),
            'displayCartGridBodyBefore1' => array(
                'id_option' => 'displayCartGridBodyBefore1',
                'name' => sprintf($this->l('[highlight]Cart page:[end_highlight] On the top of shopping cart detail on %sShopping cart%s page'),'"','"'),
            ),
            'displayShoppingCartFooter' => array(
                'id_option' => 'displayShoppingCartFooter',
                'name' => sprintf($this->l('[highlight]Cart page:[end_highlight] On the bottom of shopping cart detail on %sShopping cart%s page'),'"','"'),
            ),
            'displayFooterProduct' => array(
                'id_option' => 'displayFooterProduct',
                'name' => $this->l('[highlight]Product page:[end_highlight] Under the product description section'),
                'class'=> 'display_hook',
            ),
            'displayProductVariantsBefore' => array(
                'id_option' => 'displayProductVariantsBefore',
                'name' => $this->l('[highlight]Product page:[end_highlight] On top of the product combination block'),
                'class'=> 'display_hook',
            ),
            'displayProductVariantsAfter' => array(
                'id_option' => 'displayProductVariantsAfter',
                'name' => $this->l('[highlight]Product page:[end_highlight] On the bottom of the product combination block'),
                'class'=> 'display_hook',
            ),
            'displayReassurance' => array(
                'id_option' => 'displayReassurance',
                'name' => sprintf($this->l('[highlight]Product page:[end_highlight] Under the %sCustomer reassurance%s block'),'"','"'),
                'class'=> 'display_hook',
            ),
            'displayAfterProductThumbs' => array(
                'id_option' => 'displayAfterProductThumbs',
                'name' => $this->l('[highlight]Product page:[end_highlight] Under the product thumbnail images on product detail page'),
                'class'=> 'display_hook',
            ),
            'displayProductAdditionalInfo' => array(
                'id_option' => 'displayProductAdditionalInfo',
                'name' => sprintf($this->l('[highlight]Product page:[end_highlight] On bottom of %sSocial sharing%s block on product detail page'),'"','"'),
                'class'=> 'display_hook',
            ),
            'displayProductCommentsListHeaderBefore' => array(
                'id_option' => 'displayProductCommentsListHeaderBefore',
                'name' => sprintf($this->l('[highlight]Product page:[end_highlight] On top of %sProduct Comments%s block on product detail page'),'"','"'),
                'class'=> 'display_hook',
            ),
        );
        if(version_compare(_PS_VERSION_,'1.7.7.0','<'))
        {
            unset($positions['displayFooterCategory']);
        } 
        if(version_compare(_PS_VERSION_,'1.7.0.0','<'))
        {
            unset($positions['displayNav1']);
            unset($positions['displayFooterBefore']);
            unset($positions['displayFooterAfter']);
            unset($positions['displayCartGridBodyBefore1']);
            unset($positions['displayReassurance']);
        } 
        if(version_compare(_PS_VERSION_,'1.7.1.0','<'))
        {
            unset($positions['displayAfterProductThumbs']);
        } 
        if(version_compare(_PS_VERSION_,'1.7.6.0','<'))
        {
            unset($positions['displayProductCommentsListHeaderBefore']);
        } 
        return $positions;
    }        
    public function getListPromoteByRule($id_rule)
    {
        $sql ='SELECT * FROM  `'._DB_PREFIX_.'ets_sc_promote_rule` p
        LEFT JOIN  `'._DB_PREFIX_.'ets_sc_promote_rule_lang` pl ON (p.id_ets_sc_promote_rule = pl.id_ets_sc_promote_rule AND pl.id_lang="'.(int)Context::getContext()->language->id.'" )
        WHERE id_ets_sc_shipping_rule='.(int)$id_rule;
        $promotes = Db::getInstance()->executeS($sql);
        if($promotes)
        {
            foreach($promotes as &$promote)
            {
                
                switch($promote['way_to_promote'])
                {
                    case 'display_notification':
                        $promote['way_to_promote'] = $this->l('Display notification');
                        $promote['detail'] =  $this->l('Notification: ').$promote['notification'];
                        $promote['position_to_display'] = self::displayPoisition($promote);
                        break;
                    case 'display_banner':
                        $promote['way_to_promote'] = $this->l('Display banner');
                        $promote['detail'] =  self::displayDetailBanner($promote);
                         $promote['position_to_display'] = self::displayPoisition($promote);
                        break;
                    case 'display_popup':
                        $promote['way_to_promote'] = $this->l('Display popup');
                        $promote['detail'] =  self::displayDetailPopup($promote);
                        $promote['position_to_display'] = '--';
                        break;
                    case 'display_highlight_bar':
                        $promote['way_to_promote'] = $this->l('Display highlight bar');
                        $promote['detail'] =  $this->l('Content: ').$promote['content_bar'];
                        $promote['position_to_display'] = '--';
                        break;
                }
            }
        }
        return $promotes;
    }
    public static function displayDetailBanner($promote)
    {
        if(is_array($promote))
        {
            $banner = $promote['banner'];
            $banner_link = $promote['banner_link'];
        }
        else
        {
            $id_lang = Context::getContext()->language->id;
            $banner = $promote->banner[$id_lang];
            $banner_link = $promote->banner_link[$id_lang];
        }
        if($banner)
        {
            Context::getContext()->smarty->assign(
                array(
                    'banner' => $banner,
                    'banner_link' => $banner_link,
                    'link' =>  Context::getContext()->link,
                )
            );
            return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/promote/detail_banner.tpl');
        }
    }
    public static function displayPoisition($promote)
    {
        if(is_array($promote))
        {
            $position_to_display = $promote['position_to_display'];
        }
        else
        {
            $position_to_display = $promote->position_to_display;
        }
        if($position_to_display)
        {
            if($position_to_display)
            {
                $display_positions = Ets_sc_promote_rule::getInstance()->getPositions();
                $positions = array();
                foreach($display_positions as $display)
                {
                    $positions[$display['id_option']] = $display['name'];
                }
                Context::getContext()->smarty->assign(
                    array(
                        'display_positions' => explode(',',$position_to_display),
                        'positions' => $positions,
                    )
                );
                return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/promote/position.tpl');
            }
        }
        
    }
    public static function displayDetailPopup($promote)
    {
        if(is_array($promote))
        {
            $title_popup = $promote['title_popup'];
            $content_popup = $promote['content_popup'];
        }
        else
        {
            $id_lang = Context::getContext()->language->id;
            $title_popup = $promote->title_popup[$id_lang];
            $content_popup = $promote->content_popup[$id_lang];
        }
        if($content_popup)
        {
            Context::getContext()->smarty->assign(
                array(
                    'title_popup' => $title_popup,
                    'content_popup' => $content_popup,
                )
            );
            return Context::getContext()->smarty->fetch(_PS_MODULE_DIR_.'ets_shippingcost/views/templates/hook/promote/detail_popup.tpl');
        }
    }
    public function delete()
    {
	    $this->module->_clearSmartyCacheWhenUpdatePromoteRule($this->id);
        if(parent::delete())
        {
            if($this->banner)
            {
                foreach($this->banner as $banner)
                {
                    if($banner)
                        @unlink(_PS_ETS_SC_IMG_DIR_.$banner);
                }
                
            }
            return true;
        }
        return false;
    }

    public function update($null_value = false)
    {
	    $this->module->_clearSmartyCacheWhenUpdatePromoteRule($this->id);
	    return parent::update($null_value); // TODO: Change the autogenerated stub
    }
    public function saveData() {
	    $this->module->_clearSmartyCacheWhenUpdatePromoteRule();
	    return parent::saveData();
    }
}