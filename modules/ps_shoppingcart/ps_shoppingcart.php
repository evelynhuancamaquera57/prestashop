<?php
/**
 * 2007-2020 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2020 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;

if (!defined('_PS_VERSION_')) {
    exit;
}

use PrestaShop\PrestaShop\Core\Module\WidgetInterface;

class Ps_Shoppingcart extends Module implements WidgetInterface
{
    /**
     * @var string Name of the module running on PS 1.6.x. Used for data migration.
     */
    const PS_16_EQUIVALENT_MODULE = 'blockcart';

    public function __construct()
    {
        $this->name = 'ps_shoppingcart';
        $this->tab = 'front_office_features';
        $this->version = '2.0.7';
        $this->author = 'PrestaShop';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->trans('Shopping cart', [], 'Modules.Shoppingcart.Admin');
        $this->description = $this->trans('Display a shopping cart icon on your pages and the number of items it contains.', [], 'Modules.Shoppingcart.Admin');
        $this->ps_versions_compliancy = ['min' => '1.7.1.0', 'max' => _PS_VERSION_];
        $this->controllers = ['ajax'];

        $this->uppdatePricesStock();
    }

    /**
     * @return void
     */
    public function hookDisplayHeader()
    {
        if (Configuration::isCatalogMode()) {
            return;
        }

        if (Configuration::get('PS_BLOCK_CART_AJAX')) {
            $this->context->controller->registerJavascript('modules-shoppingcart', 'modules/' . $this->name . '/ps_shoppingcart.js', ['position' => 'bottom', 'priority' => 150]);
        }
    }

    /**
     * @return string
     */
    private function getCartSummaryURL()
    {
        return $this->context->link->getPageLink(
            'cart',
            null,
            $this->context->language->id,
            [
                'action' => 'show',
            ],
            false,
            null,
            true
        );
    }

    /**
     * @param string|null $hookName
     * @param array<string,mixed> $params
     *
     * @return array<string,mixed>
     */
    public function getWidgetVariables($hookName, array $params)
    {
        $cart_url = $this->getCartSummaryURL();

        return [
            'cart' => (new CartPresenter())->present(isset($params['cart']) ? $params['cart'] : $this->context->cart),
            'refresh_url' => $this->context->link->getModuleLink('ps_shoppingcart', 'ajax', [], null, null, null, true),
            'cart_url' => $cart_url,
        ];
    }

    /**
     * @param string|null $hookName
     * @param array<string,mixed> $params
     *
     * @return string
     */
    public function renderWidget($hookName, array $params)
    {
        if (Configuration::isCatalogMode()) {
            return '';
        }

        $this->smarty->assign($this->getWidgetVariables($hookName, $params));

        return $this->fetch('module:ps_shoppingcart/ps_shoppingcart.tpl');
    }

    /**
     * @param Cart $cart
     * @param int $id_product
     * @param int $id_product_attribute
     * @param int $id_customization
     *
     * @return string
     *
     * @throws Exception
     */
    public function renderModal(Cart $cart, $id_product, $id_product_attribute, $id_customization)
    {
        $data = (new CartPresenter())->present($cart);
        $product = null;
        foreach ($data['products'] as $p) {
            if ((int) $p['id_product'] == $id_product &&
                (int) $p['id_product_attribute'] == $id_product_attribute &&
                (int) $p['id_customization'] == $id_customization) {
                $product = $p;
                break;
            }
        }

        $this->smarty->assign([
            'product' => $product,
            'cart' => $data,
            'cart_url' => $this->getCartSummaryURL(),
        ]);

        return $this->fetch('module:ps_shoppingcart/modal.tpl');
    }

    /**
     * @return string
     */
    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitBlockCart')) {
            $ajax = Tools::getValue('PS_BLOCK_CART_AJAX');
            if ($ajax != 0 && $ajax != 1) {
                $output .= $this->displayError($this->trans('Ajax: Invalid choice.', [], 'Modules.Shoppingcart.Admin'));
            } else {
                Configuration::updateValue('PS_BLOCK_CART_AJAX', (int) ($ajax));
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules') . '&configure=' . $this->name . '&conf=6');
            }
        }

        return $output . $this->renderForm();
    }

    /**
     * @return bool
     */
    public function install()
    {
        $this->uninstallPrestaShop16Module();

        return
            parent::install()
            && $this->registerHook('displayHeader')
            && $this->registerHook('displayNav2')
            && Configuration::updateValue('PS_BLOCK_CART_AJAX', 1);
    }

    /**
     * Migrate data from 1.6 equivalent module (if applicable), then uninstall
     *
     * @return bool
     */
    public function uninstallPrestaShop16Module()
    {
        if (!Module::isInstalled(self::PS_16_EQUIVALENT_MODULE)) {
            return false;
        }
        $oldModule = Module::getInstanceByName(self::PS_16_EQUIVALENT_MODULE);
        if ($oldModule) {
            // This closure calls the parent class to prevent data to be erased
            // It allows the new module to be configured without migration
            $parentUninstallClosure = function () {
                return parent::uninstall();
            };
            $parentUninstallClosure = $parentUninstallClosure->bindTo($oldModule, get_class($oldModule));
            $parentUninstallClosure();
        }

        return true;
    }

    /**
     * @return string
     */
    public function renderForm()
    {
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->trans('Settings', [], 'Admin.Global'),
                    'icon' => 'icon-cogs',
                ],
                'input' => [
                    [
                        'type' => 'switch',
                        'label' => $this->trans('Ajax cart', [], 'Modules.Shoppingcart.Admin'),
                        'name' => 'PS_BLOCK_CART_AJAX',
                        'is_bool' => true,
                        'desc' => $this->trans('Activate Ajax mode for the cart (compatible with the default theme).', [], 'Modules.Shoppingcart.Admin'),
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->trans('Yes', [], 'Admin.Global'),
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->trans('No', [], 'Admin.Global'),
                            ],
                        ],
                    ],
                ],
                'submit' => [
                    'title' => $this->trans('Save', [], 'Admin.Actions'),
                ],
            ],
        ];

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitBlockCart';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab
            . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        return $helper->generateForm([$fields_form]);
    }

    /**
     * @return bool[]
     */
    public function getConfigFieldsValues()
    {
        return [
            'PS_BLOCK_CART_AJAX' => (bool) Tools::getValue('PS_BLOCK_CART_AJAX', Configuration::get('PS_BLOCK_CART_AJAX')),
        ];
    }

    public function uppdatePricesStock ()
    {
// URL de la API
        $url_api = 'http://cloud.chingolito.com:8080/api/stocks?branch=1&warehouse=1';

// Token de autorización (Bearer token)
        $mi_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNjhhZDU1MTZkMjk2YzJlYWY1ODY2MWE3MjRlNmM3OWE4NGYzNjBlMjJkYzg3NmYwNDc3ZTAzOTNjOWUxN2FmOTcwMTVlNGFiZmRjYWU4MzgiLCJpYXQiOjE3MDcyNDQ0OTYuNTg3OTg4LCJuYmYiOjE3MDcyNDQ0OTYuNTg3OTkyLCJleHAiOjE3Mzg4NjY4OTYuNTY2ODksInN1YiI6IjEiLCJzY29wZXMiOltdfQ.Tkaa9KQxoJG1RGNH__Dnzxpdk221D6EDRwkqRe69Yr8qH6-sciz6TZ0-y7U2bJUahgqhYm4Hnq8OZVdJubklGqOy0-OuBucO8hRbhTmpN7CaHPqCxdS-Au-WTHnkz5esWnkAR3rgTsqLN3g6oaZsLyKab7L3UUYVMF2IioBtn3s-Rkxc3j6RPZViP22rh_IhOP4cyXrB9pRwqkT5Vvh79eo5VMFVPQG7jX_Ju8qZdNbzIZiuYcznSStNhxxB0z4N_cuNwkSLm9KS2Z_cksN8S5ow6CxoVad3MiZsRzd10Goun-D_wy0F9NgBcjtmI5iC-8rFHncK2QjQfWIrv1Cxfi7bj2BpL1uwz4ZQnSL0IB0uANkwDHFS4-cUs1ajlTeGYrIxLrMLZAOVuv2YGX5iykzmK3hV6xZKp52XYTTSpQ0q_qx4mcf8SnfJw4tfJBBH6N8aZmr0TdqN1RnYPnjhAckNnGYntwJEPGXi4IZ9gq2ySDRv0qQ-2j2PB0iiAtcKIt3vLdm05qb4IN3rvatlkn3peO3ljXAXF5VzRwXShwLBIWovENN9hnHV1ke-IvBGbsTWlmDu9BedMPL-BlMqwu9qN1wuOkfWCZN5RJ0u7N_r_KQbPKjOHWNDRDfajofBE_gtT8GMBboH3yC5r3FqqHkyhRgppE-CXxi0vFg4VSI';
        $ch = curl_init($url_api); // Inicializar cURL
// Configurar las opciones de cURL
        $headers = [
            'Authorization: Bearer ' . $mi_token,
            'Content-Type: application/json', // Si es necesario, ajusta el tipo de contenido según la API
        ];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);     // Establecer otras opciones si es necesario, como el método HTTP (GET, POST, etc.)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Ejecutar la solicitud cURL
        $response = curl_exec($ch);

// Verificar si hay errores
        if ($response === false) {
            echo 'Error de cURL: ' . curl_error($ch);
        }
        curl_close($ch);     // Cerrar la sesión cURL

        $resultado = $response;

// Decodificar la respuesta JSON (si se espera un JSON)
        $datos = json_decode($resultado, true);

        $productos = $datos['result']['data'];

        foreach ($productos as $producto) {
            $productoJson = json_encode($producto); // Convertir el array $producto a un string JSON}

            $array = json_decode($productoJson, true);

            // PAra actualizar precios

            $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>');

            // Crear la estructura XML a partir del array
            $product = $xml->addChild('product');
            $product->addChild('id', htmlspecialchars('{{product_id}}'));
            $product->addChild('price', htmlspecialchars('{{price}}')); //

            // Reemplazar los valores específicos con los datos del array
            $product->id = htmlspecialchars($array['product_id']);
            $product->price = htmlspecialchars($array['price']);

            $xmlProducto = $xml->asXML();

            // para actualizar stock
            $xmlStock = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>');

            // Crear la estructura XML a partir del array
            $productStock = $xmlStock->addChild('stock_available');
            $productStock->addChild('id', htmlspecialchars('{{id}}'));
            $productStock->addChild('id_product', htmlspecialchars('{{product_id}}'));
            $productStock->addChild('quantity', htmlspecialchars('{{stock}}'));
            // Reemplazar los valores específicos con los datos del array
            $productStock->id = htmlspecialchars($array['product_id']);
            $productStock->id_product = htmlspecialchars($array['product_id']);
            $productStock->quantity = htmlspecialchars($array['stock']);

            $xmlProductoStock = $xmlStock->asXML();

            $product_id = $producto['product_id'];

            $token_prestashop = 'GJRLVKYTTJGV6JE5C4HTM7VPXUBL4KU9'; // usuario:password
            $mi_token_prestashop = 'TDUzSjQyQlRQUFNKVlVDVlYzOVI2VDU3UTk1VkFVTDI6'; // usuario:password

            $url_api_prestaProducts = 'http://prestashopinst1.live-website.com/api/products/'.$product_id.'?ws_key='.$token_prestashop;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url_api_prestaProducts);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlProducto);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/xml',
                'Authorization: Basic ' . $mi_token_prestashop
            ));

            // Ignorar la verificación del certificado SSL (cuidado: no recomendado para producción)
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            // Ejecutar la petición
            $response = curl_exec($curl);

            // Verificar la respuesta
            if ($response) {
                // Por ejemplo, puedes imprimir la respuesta de la API
                echo curl_error($curl); // Imprimir el error cURL, si hay alguno
            } else {
                echo 'Hubo un error al intentar actualizar los datos.';
                echo curl_error($curl); // Imprimir el error cURL, si hay alguno
            }

            // Cerrar la sesión cURL
            curl_close($curl);

            $url_api_prestaStock = 'http://prestashopinst1.live-website.com/api/stock_availables/'.$product_id.'?ws_key='.$token_prestashop;

            // Configurar la petición HTTP utilizando cURL
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url_api_prestaStock);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlProductoStock);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/xml',
                'Authorization: Basic ' . $mi_token_prestashop
            ));

            // Ignorar la verificación del certificado SSL (cuidado: no recomendado para producción)
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            // Ejecutar la petición
            $response = curl_exec($curl);

            // Verificar la respuesta
            if ($response) {
                //echo 'Los datos se actualizaron correctamente.';
                echo curl_error($curl); // Imprimir el error cURL, si hay alguno
            } else {
                echo curl_error($curl); // Imprimir el error cURL, si hay alguno
            }

            // Cerrar la sesión cURL
            curl_close($curl);

        }
    }
}
