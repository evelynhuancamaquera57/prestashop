<?php
/* Smarty version 4.3.4, created on 2024-02-03 22:27:20
  from '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/templates/_partials/helpers.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_65bf04181c1399_02498662',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '435b9f369657df171c5002bc55a7aa706233b30a' => 
    array (
      0 => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/templates/_partials/helpers.tpl',
      1 => 1697815446,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_65bf04181c1399_02498662 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/var/cache/dev/smarty/compile/classiclayouts_layout_full_width_tpl/43/5b/9f/435b9f369657df171c5002bc55a7aa706233b30a_2.file.helpers.tpl.php',
    'uid' => '435b9f369657df171c5002bc55a7aa706233b30a',
    'call_name' => 'smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694',
  ),
));
?> 

<?php }
/* smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694 */
if (!function_exists('smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694')) {
function smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694(Smarty_Internal_Template $_smarty_tpl,$params) {
foreach ($params as $key => $value) {
$_smarty_tpl->tpl_vars[$key] = new Smarty_Variable($value, $_smarty_tpl->isRenderingCache);
}
?>

  <a href="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
    <img
      class="logo img-fluid"
      src="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['src'], ENT_QUOTES, 'UTF-8');?>
"
      alt="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
"
      width="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['width'], ENT_QUOTES, 'UTF-8');?>
"
      height="<?php echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['shop']->value['logo_details']['height'], ENT_QUOTES, 'UTF-8');?>
">
  </a>
<?php
}}
/*/ smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694 */
}
