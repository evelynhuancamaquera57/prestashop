<?php
/* Smarty version 4.3.4, created on 2024-02-03 22:28:53
  from '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/templates/customer/_partials/customer-form.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_65bf0475e06591_38327215',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '50769ed897889f76e2eed401014c8eae5c45ca40' => 
    array (
      0 => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/templates/customer/_partials/customer-form.tpl',
      1 => 1707016958,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_partials/form-errors.tpl' => 1,
  ),
),false)) {
function content_65bf0475e06591_38327215 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_207554168765bf0475df9cb4_43470774', 'customer_form');
?>

<?php }
/* {block 'customer_form_errors'} */
class Block_75497551565bf0475dfa126_66142643 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php $_smarty_tpl->_subTemplateRender('file:_partials/form-errors.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('errors'=>$_smarty_tpl->tpl_vars['errors']->value['']), 0, false);
?>
  <?php
}
}
/* {/block 'customer_form_errors'} */
/* {block 'customer_form_actionurl'} */
class Block_182391745565bf0475dfc955_97350005 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
echo htmlspecialchars((string) $_smarty_tpl->tpl_vars['action']->value, ENT_QUOTES, 'UTF-8');
}
}
/* {/block 'customer_form_actionurl'} */
/* {block "form_field"} */
class Block_144844219165bf0475e01294_24179954 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php if ($_smarty_tpl->tpl_vars['field']->value['type'] === "password") {?>
            <div class="field-password-policy">
              <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['form_field'][0], array( array('field'=>$_smarty_tpl->tpl_vars['field']->value),$_smarty_tpl ) );?>

            </div>
          <?php } else { ?>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['form_field'][0], array( array('field'=>$_smarty_tpl->tpl_vars['field']->value),$_smarty_tpl ) );?>

          <?php }?>
        <?php
}
}
/* {/block "form_field"} */
/* {block "form_fields"} */
class Block_127034404265bf0475dfe521_35876233 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['formFields']->value, 'field');
$_smarty_tpl->tpl_vars['field']->do_else = true;
if ($_from !== null) foreach ($_from as $_smarty_tpl->tpl_vars['field']->value) {
$_smarty_tpl->tpl_vars['field']->do_else = false;
?>
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_144844219165bf0475e01294_24179954', "form_field", $this->tplIndex);
?>

      <?php
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      <?php echo $_smarty_tpl->tpl_vars['hook_create_account_form']->value;?>

    <?php
}
}
/* {/block "form_fields"} */
/* {block "form_buttons"} */
class Block_43119704265bf0475e04eb7_09418631 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <button class="btn btn-primary form-control-submit float-xs-right" data-link-action="save-customer" type="submit">
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Save','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

        </button>
      <?php
}
}
/* {/block "form_buttons"} */
/* {block 'customer_form_footer'} */
class Block_171351868865bf0475e049a0_43826874 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <footer class="form-footer clearfix">
      <input type="hidden" name="submitCreate" value="1">
      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_43119704265bf0475e04eb7_09418631', "form_buttons", $this->tplIndex);
?>

    </footer>
  <?php
}
}
/* {/block 'customer_form_footer'} */
/* {block 'customer_form'} */
class Block_207554168765bf0475df9cb4_43470774 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'customer_form' => 
  array (
    0 => 'Block_207554168765bf0475df9cb4_43470774',
  ),
  'customer_form_errors' => 
  array (
    0 => 'Block_75497551565bf0475dfa126_66142643',
  ),
  'customer_form_actionurl' => 
  array (
    0 => 'Block_182391745565bf0475dfc955_97350005',
  ),
  'form_fields' => 
  array (
    0 => 'Block_127034404265bf0475dfe521_35876233',
  ),
  'form_field' => 
  array (
    0 => 'Block_144844219165bf0475e01294_24179954',
  ),
  'customer_form_footer' => 
  array (
    0 => 'Block_171351868865bf0475e049a0_43826874',
  ),
  'form_buttons' => 
  array (
    0 => 'Block_43119704265bf0475e04eb7_09418631',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_75497551565bf0475dfa126_66142643', 'customer_form_errors', $this->tplIndex);
?>


<form action="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_182391745565bf0475dfc955_97350005', 'customer_form_actionurl', $this->tplIndex);
?>
" id="customer-form" class="js-customer-form" method="post">
  <div>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_127034404265bf0475dfe521_35876233', "form_fields", $this->tplIndex);
?>

  </div>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_171351868865bf0475e049a0_43826874', 'customer_form_footer', $this->tplIndex);
?>


</form>
<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.6.3.min.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
$(document).ready(function(){
	$("#field-dni").change(validateDni);

	function validateDni () {
		var dni = $("#field-dni").val();

		if (!isNaN(dni)) {
			var dniNumber = parseInt(dni, 10);

			if (dniNumber >= 0 && dniNumber <= 99999999) {
				console.log('DNI válido:', dni);

				// Validar que el DNI tenga exactamente 8 dígitos
				if (dni.length === 8) {
					console.log('DNI válido con 8 dígitos');
					
					// llamar a la api 
					// URL de la API
					const apiUrl = 'https://dniruc.apisperu.com/api/v1/dni/'+dni+'?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImV2ZWx5bmh1YW5jYW1hcXVlcmE1N0BnbWFpbC5jb20ifQ.whsn3sWCaXuDxSz-LRSjosX1CGtXJSaTZ-oEcVwNLzo';

					// Realizar la petición GET utilizando fetch
					fetch(apiUrl)
					  .then(response => {
						// Verificar si la respuesta es exitosa (código de estado 200-299)
						if (!response.ok) {
						  throw new Error('Error en la petición: ' + response.status);
						}
						
						// Convertir la respuesta a formato JSON
						return response.json();
					  })
					  .then(data => {
						// Manipular los datos obtenidos de la API
						console.log(data);
						$("#field-firstname").val(data.nombres);
						$("#field-lastname").val(data.apellidoPaterno+' '+data.apellidoMaterno);
					  })
					  .catch(error => {
						// Capturar y manejar errores
						console.error('Error: ', error);
					  });


				}
			} else {
				console.log('DNI inválido: no es un número positivo o tiene más de 8 dígitos');
				$("#field-dni").val('');
			}
		} else {
			console.log('DNI inválido: contiene caracteres no numéricos');
			$("#field-dni").val('');
		}
	}
});
<?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'customer_form'} */
}
