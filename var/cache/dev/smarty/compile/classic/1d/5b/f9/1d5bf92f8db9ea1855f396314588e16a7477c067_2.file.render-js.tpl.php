<?php
/* Smarty version 4.3.4, created on 2024-02-03 22:27:20
  from '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/ets_shippingcost/views/templates/hook/render-js.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_65bf04180b5dc1_01806004',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1d5bf92f8db9ea1855f396314588e16a7477c067' => 
    array (
      0 => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/ets_shippingcost/views/templates/hook/render-js.tpl',
      1 => 1706679899,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_65bf04180b5dc1_01806004 (Smarty_Internal_Template $_smarty_tpl) {
if ((isset($_smarty_tpl->tpl_vars['hookDisplayProductListHeaderAfter']->value)) && $_smarty_tpl->tpl_vars['hookDisplayProductListHeaderAfter']->value) {?>
    <div id="etsSChookDisplayProductListHeaderAfter" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayProductListHeaderAfter']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayProductListHeaderBefore']->value)) && $_smarty_tpl->tpl_vars['hookDisplayProductListHeaderBefore']->value) {?>
    <div id="etsSChookDisplayProductListHeaderBefore" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayProductListHeaderBefore']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayLeftColumnBefore']->value)) && $_smarty_tpl->tpl_vars['hookDisplayLeftColumnBefore']->value) {?>
    <div id="etsSChookDisplayLeftColumnBefore" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayLeftColumnBefore']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayRightColumnBefore']->value)) && $_smarty_tpl->tpl_vars['hookDisplayRightColumnBefore']->value) {?>
    <div id="etsSChookDisplayRightColumnBefore" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayRightColumnBefore']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayProductVariantsBefore']->value)) && $_smarty_tpl->tpl_vars['hookDisplayProductVariantsBefore']->value) {?>
    <div id="etsSChookDisplayProductVariantsBefore" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayProductVariantsBefore']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayProductVariantsAfter']->value)) && $_smarty_tpl->tpl_vars['hookDisplayProductVariantsAfter']->value) {?>
    <div id="etsSChookDisplayProductVariantsAfter" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayProductVariantsAfter']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayProductCommentsListHeaderBefore']->value)) && $_smarty_tpl->tpl_vars['hookDisplayProductCommentsListHeaderBefore']->value) {?>
    <div id="etsSChookDisplayProductCommentsListHeaderBefore" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayProductCommentsListHeaderBefore']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore1']->value)) && $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore1']->value) {?>
    <div id="etsSChookDisplayCartGridBodyBefore1" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore1']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore2']->value)) && $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore2']->value) {?>
    <div id="etsSChookDisplayCartGridBodyBefore2" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyBefore2']->value;?>

    </div>
<?php }
if ((isset($_smarty_tpl->tpl_vars['hookDisplayCartGridBodyAfter']->value)) && $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyAfter']->value) {?>
    <div id="etsSChookDisplayCartGridBodyAfter" style="display: none">
        <?php echo $_smarty_tpl->tpl_vars['hookDisplayCartGridBodyAfter']->value;?>

    </div>
<?php }
}
}
