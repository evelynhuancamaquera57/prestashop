<?php
/* Smarty version 4.3.4, created on 2024-02-03 22:27:20
  from 'module:ps_customeraccountlinksps_customeraccountlinks.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_65bf04183c95e2_91097541',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '42f9461127ce7396a601c2484841253ea5ba658f' => 
    array (
      0 => 'module:ps_customeraccountlinksps_customeraccountlinks.tpl',
      1 => 1697815446,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_65bf04183c95e2_91097541 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/var/cache/dev/smarty/compile/classiclayouts_layout_full_width_tpl/43/5b/9f/435b9f369657df171c5002bc55a7aa706233b30a_2.file.helpers.tpl.php',
    'uid' => '435b9f369657df171c5002bc55a7aa706233b30a',
    'call_name' => 'smarty_template_function_renderLogo_100197581765bf04181bd9d4_65747694',
  ),
));
?><!-- begin /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/modules/ps_customeraccountlinks/ps_customeraccountlinks.tpl -->
<div id="block_myaccount_infos" class="col-md-3 links wrapper">
  <p class="h3 myaccount-title hidden-sm-down">
    <a class="text-uppercase" href="http://prestashopinst1.live-website.com/index.php?controller=my-account" rel="nofollow">
      Su cuenta
    </a>
  </p>
  <div class="title clearfix hidden-md-up" data-target="#footer_account_list" data-toggle="collapse">
    <span class="h3">Su cuenta</span>
    <span class="float-xs-right">
      <span class="navbar-toggler collapse-icons">
        <i class="material-icons add">&#xE313;</i>
        <i class="material-icons remove">&#xE316;</i>
      </span>
    </span>
  </div>
  <ul class="account-list collapse" id="footer_account_list">
            <li><a href="http://prestashopinst1.live-website.com/index.php?controller=identity" title="Información" rel="nofollow">Información</a></li>
                  <li><a href="http://prestashopinst1.live-website.com/index.php?controller=address" title="Añadir primera dirección" rel="nofollow">Añadir primera dirección</a></li>
                          <li><a href="http://prestashopinst1.live-website.com/index.php?controller=history" title="Pedidos" rel="nofollow">Pedidos</a></li>
                          <li><a href="http://prestashopinst1.live-website.com/index.php?controller=order-slip" title="Facturas por abono" rel="nofollow">Facturas por abono</a></li>
                                
<!-- begin module:blockwishlist/views/templates/hook/account/myaccount-block.tpl -->
<!-- begin /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/blockwishlist/views/templates/hook/account/myaccount-block.tpl -->
  <li>
    <a href="http://prestashopinst1.live-website.com/index.php?fc=module&amp;module=blockwishlist&amp;controller=lists&amp;id_lang=2" title="My wishlists" rel="nofollow">
      Lista de deseos
    </a>
  </li>
<!-- end /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/blockwishlist/views/templates/hook/account/myaccount-block.tpl -->
<!-- end module:blockwishlist/views/templates/hook/account/myaccount-block.tpl -->

<!-- begin /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/modules/ps_emailalerts/views/templates/hook/my-account-footer.tpl -->
<li>
  <a href="//prestashopinst1.live-website.com/index.php?fc=module&module=ps_emailalerts&controller=account&id_lang=2" title="Mis alertas">
    Mis alertas
  </a>
</li>

<!-- end /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/modules/ps_emailalerts/views/templates/hook/my-account-footer.tpl -->

        <li><a href="http://prestashopinst1.live-website.com/index.php?mylogout=" title="Cerrar sesión" rel="nofollow">Cerrar sesión</a></li>
       
	</ul>
</div>
<!-- end /homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/themes/classic/modules/ps_customeraccountlinks/ps_customeraccountlinks.tpl --><?php }
}
