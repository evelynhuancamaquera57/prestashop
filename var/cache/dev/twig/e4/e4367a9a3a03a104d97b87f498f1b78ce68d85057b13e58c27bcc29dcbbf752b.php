<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__fd1e66f123bc6e1ea381ae972b56136f6a13e67f944c90022739f1078379ddf8 */
class __TwigTemplate_f093ecd266aee5e16b62939b9317dcaca7b8180185ab56f6cd62a6db6b515ec1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "__string_template__fd1e66f123bc6e1ea381ae972b56136f6a13e67f944c90022739f1078379ddf8"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "__string_template__fd1e66f123bc6e1ea381ae972b56136f6a13e67f944c90022739f1078379ddf8"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Rendimiento • Prestashop_Inst1</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminPerformance';
    var iso_user = 'es';
    var lang_is_rtl = '0';
    var full_language_code = 'es';
    var full_cldr_language_code = 'es-ES';
    var country_iso_code = 'PE';
    var _PS_VERSION_ = '8.1.3';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Se ha recibido un nuevo pedido en tu tienda.';
    var order_number_msg = 'Número de pedido: ';
    var total_msg = 'Total: ';
    var from_msg = 'Desde: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Un nuevo cliente se ha registrado en tu tienda.';
    var customer_name_msg = 'Nombre del cliente: ';
    var new_msg = 'Un nuevo mensaje ha sido publicado en tu tienda.';
    var see_msg = 'Leer este mensaje';
    var token = '529d635b00b415cad464397d1feeb236';
    var currentIndex = 'index.php?controller=AdminPerformance';
    var employee_token = '3c0153e7b54a7d2ec35210ab31c994c9';
    var choose_language_translate = 'Selecciona el idioma:';
    var default_language = '2';
    var admin_modules_link = '/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho';
    var admin_notification_get_link = '/avtkttftlp0d3jsj/index.php/common/notifications?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho';
    var admin_notification_push_link = adminNotificationPushLink = '/avtkttftlp0d3jsj/index.php/common/notifications/ack?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho';
    var tab_modules_list = '';
    var update_success_msg = 'Actualizaci";
        // line 42
        echo "ón correcta';
    var search_product_msg = 'Buscar un producto';
  </script>



<link
      rel=\"preload\"
      href=\"/avtkttftlp0d3jsj/themes/new-theme/public/2d8017489da689caedc1.preload..woff2\"
      as=\"font\"
      crossorigin
    >
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product_default_theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/blockwishlist/public/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/avtkttftlp0d3jsj\\/\";
var baseDir = \"\\/\";
var changeFormLanguageUrl = \"\\/avtkttftlp0d3jsj\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var prest";
        // line 68
        echo "ashop = {\"debug\":true};
var show_new_customers = \"1\";
var show_new_messages = \"1\";
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/modules/blockwishlist/public/vendors.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_emailalerts/js/admin/ps_emailalerts.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/avtkttftlp0d3jsj/index.php/common/notifications?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


";
        // line 102
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>";
        echo "

<body
  class=\"lang-es adminperformance developer-mode\"
  data-base-url=\"/avtkttftlp0d3jsj/index.php\"  data-token=\"cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">

  <header id=\"header\" class=\"d-print-none\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
      <span id=\"shop_version\">8.1.3</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item quick-row-link new-product-button\"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog";
        // line 136
        echo "/products-v2/create?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
        <div class=\"dropdown-divider\"></div>
          <a id=\"quick-add-link\"
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"162\"
        data-icon=\"icon-AdminAdvancedParameters\"
        data-method=\"add\"
        data-url=\"index.php/configure/advanced/performance/?-KlMFhsHVJho\"
        data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Rendimiento - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir página actual al Acceso Rápido
      </a>
        <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component component-search\" id=\"header-search-container\">
        <div class=\"component-search-body\">
          <div class=\"component-search-top\">
            <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/avtkttftlp0d";
        // line 175
        echo "3jsj/index.php?controller=AdminSearch&amp;token=0587329b7e0e65ce9ee1448093576fe6\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\" aria-label=\"Barra de búsqueda\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, referencia, etc.\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Nombre\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Numero de Factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
";
        // line 192
        echo "        <a class=\"dropdown-item\" data-item=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
            <button class=\"component-search-cancel d-none\">Cancelar</button>
          </div>

          <div class=\"component-search-quickaccess d-none\">
  <p class=\"component-search-title\">Acceso rápido</p>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
             data-item=\"Evaluación del catálogo\"
    >Evaluación del catálogo</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Módulos instalados\"
    >Módulos instalados</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Nueva categoría\"
    >Nueva categoría</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/products-v2/create?token=ce99db6ef6e5efbce6760c2365ec0188\"
           ";
        // line 226
        echo "  data-item=\"Nuevo\"
    >Nuevo</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
             data-item=\"Nuevo cupón de descuento\"
    >Nuevo cupón de descuento</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Pedidos\"
    >Pedidos</a>
    <div class=\"dropdown-divider\"></div>
      <a id=\"quick-add-link\"
      class=\"dropdown-item js-quick-link\"
      href=\"#\"
      data-rand=\"26\"
      data-icon=\"icon-AdminAdvancedParameters\"
      data-method=\"add\"
      data-url=\"index.php/configure/advanced/performance/?-KlMFhsHVJho\"
      data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
      data-prompt-text=\"Por favor, renombre este acceso rápido:\"
      data-link=\"Rendimiento - Lista\"
    >
      <i class=\"material-icons\">add_circle</i>
      Añadir página actual al Acceso Rápido
    </a>
    <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
    <i class=\"material-icons\">settings</i>
    Administrar accesos rápidos
  </a>
</div>
        </div>

        <div class=\"component-search-background d-none\"></div>
      </div>

              <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
          <a class=\"link shop-state\"
             id=\"debug-mode\"
             data-toggle=\"pstooltip\"
             data-placement=\"bottom\"
             data-html=\"true\"
             title=\"<p class=&quot;text-left&quot;><strong>Tu tienda está en modo depuración.</strong></p><p class=&quot;text-left&quot;>S";
        // line 267
        echo "e muestran todos los errores y mensajes PHP. Cuando ya no los necesites, &lt;strong&gt;desactiva&lt;/strong&gt; este modo.</p>\"
             href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
          >
            <i class=\"material-icons\">bug_report</i>
            <span>Modo depuración</span>
          </a>
        </div>
      
      
      <div class=\"header-right\">
                  <div class=\"component\" id=\"header-shop-list-container\">
              <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://prestashopinst1.live-website.com/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      <span>Ver mi tienda</span>
    </a>
  </div>
          </div>
                          <div class=\"component header-right-component\" id=\"header-notifications-container\">
            <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
      ";
        // line 315
        echo "        Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Has revisado tus <strong><a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarts&action=filterOnlyAbandonedCarts&token=7efad6290481526ed649f512ccd820de\">carritos abandonados</a></strong>?<br>?. ¡Tu próximo pedido podría estar ocultándose allí!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Se mantiene activo en las redes sociales en estos momentos?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Parece que todos tus clientes están contentos :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
   ";
        // line 361
        echo " <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
          </div>
        
        <div class=\"component\" id=\"header-employee-container\">
          <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      <div class=\"employee-top\">
        <span class=\"employee-avatar\"><img class=\"avatar rounded-circle\" src=\"http://prestashopinst1.live-website.com/img/pr/default.jpg\" alt=\"Site\" /></span>
        <span class=\"employee_profile\">Bienvenido de nuevo, Site</span>
      </div>

      <a class=\"dropdown-item employee-link profile-link\" href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/1/edit?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">
      <i class=\"material-icons\">edit</i>
      <span>Tu perfil</span>
    </a>
    </div>

    <p class=\"divider\"></p>

    
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminLogin&amp;logout=1&amp;token=5f1235c96f61a139637625a50965a110\">
      <i class=\"material-icons d-lg-n";
        // line 406
        echo "one\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
        </div>
              </div>
    </nav>
  </header>

  <nav class=\"nav-bar d-none d-print-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/toggle-navigation?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">
    <i class=\"material-icons rtl-flip\">chevron_left</i>
    <i class=\"material-icons rtl-flip\">chevron_left</i>
  </span>

  <div class=\"nav-bar-overflow\">
      <div class=\"logo-container\">
          <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
          <span id=\"shop_version\" class=\"header-version\">8.1.3</span>
      </div>

      <ul class=\"main-menu\">
              
                    
                    
          
            <li class=\"link-levelone\" data-submenu=\"1\" id=\"tab-AdminDashboard\">
              <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"link\" >
                <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
              </a>
            </li>

          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"2\" id=\"tab-SELL\">
                <span class=\"title\">Vender</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-shopping_basket";
        // line 454
        echo "\">shopping_basket</i>
                      <span>
                      Pedidos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/invoices/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Facturas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/credit-slips/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Facturas por abono
                                </a>
                 ";
        // line 485
        echo "             </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/delivery-slips/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Albaranes de entrega
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarts&amp;token=7efad6290481526ed649f512ccd820de\" class=\"link\"> Carritos de compra
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-store\">store</i>
                      <span>
                      Catálogo
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                    ";
        // line 517
        echo "                        </i>
                                            </a>
                                              <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/categories?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Categorías
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/monitoring/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Monitoreo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
  ";
        // line 548
        echo "                              <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminAttributesGroups&amp;token=2eb46092d80c0b3e72aec32eb4043440\" class=\"link\"> Atributos y Características
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/brands/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Marcas y Proveedores
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/attachments/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Archivos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\" class=\"link\"> Descuentos
                                </a>
                              </li>

                                                                                  
    ";
        // line 577
        echo "                          
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/stocks/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Stock
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/customers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-account_circle\">account_circle</i>
                      <span>
                      Clientes
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Clientes
                                </a>
                              </li>

       ";
        // line 609
        echo "                                                                           
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/addresses/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Direcciones
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\">
                      <i class=\"material-icons mi-chat\">chat</i>
                      <span>
                      Servicio al Cliente
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                                <a href=\"http://prest";
        // line 638
        echo "ashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\"> Servicio al Cliente
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customer-service/order-messages/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Mensajes de Pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminReturn&amp;token=2586f6075464f1f2accf0f67761c20c8\" class=\"link\"> Devoluciones de mercancía
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;token=b18d3d66929e4bc598af8f146c1fdafe\" class=\"link\">
                      <i class=\"material-icons mi-assessment\">assessment</i>
                      <span>
            ";
        // line 668
        echo "          Estadísticas
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"37\" id=\"tab-IMPROVE\">
                <span class=\"title\">Personalizar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"38\" id=\"subtab-AdminParentModulesSf\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-extension\">extension</i>
                      <span>
                      Módulos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-38\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"39\" id=\"subtab-AdminModulesSf\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPX";
        // line 704
        echo "Udx-KlMFhsHVJho\" class=\"link\"> Administrador de módulos
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentThemes\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                      <span>
                      Diseño
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"125\" id=\"subtab-AdminThemesParent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tema y logotipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\"";
        // line 736
        echo " data-submenu=\"45\" id=\"subtab-AdminParentMailTheme\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/mail_theme/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tema Email
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"47\" id=\"subtab-AdminCmsContent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/cms-pages/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Páginas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"48\" id=\"subtab-AdminModulesPositions\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/modules/positions/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Posiciones
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"49\" id=\"subtab-AdminImages\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminImages&amp;token=0095a57f51f7bb591a88a227e9214d47\" class=\"link\"> Ajustes de imágenes
                                </a>
                              </li>

                                                                                  ";
        // line 765
        echo "
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"118\" id=\"subtab-AdminLinkWidget\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/modules/link-widget/list?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Lista de enlaces
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentShipping\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/carriers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                      <span>
                      Transporte
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"51\" id=\"subtab-AdminCarriers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/carriers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Transportistas
                            ";
        // line 795
        echo "    </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"52\" id=\"subtab-AdminShipping\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"132\" id=\"subtab-AdminShippingCostRule\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminShippingCostRule&amp;token=168210935506050aa6fe18be8e9f9a42\" class=\"link\"> Costo de envío
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"53\" id=\"subtab-AdminParentPayment\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-payment\">payment</i>
                      <span>
                      Pago
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    ke";
        // line 827
        echo "yboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"54\" id=\"subtab-AdminPayment\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Métodos de pago
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"55\" id=\"subtab-AdminPaymentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminInternational\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-language\">language</i>
                      <span>
                      Internacional
                      </span>
                         ";
        // line 859
        echo "                           <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"57\" id=\"subtab-AdminParentLocalization\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Localización
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"62\" id=\"subtab-AdminParentCountries\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/zones/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Ubicaciones Geográficas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"66\" id=\"subtab-AdminParentTaxes\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/taxes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Impuestos
                                </a>
                              </li>

                                        ";
        // line 888
        echo "                                          
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"69\" id=\"subtab-AdminTranslations\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/translations/settings?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Traducciones
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title link-active\" data-submenu=\"70\" id=\"tab-CONFIGURE\">
                <span class=\"title\">Configurar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"71\" id=\"subtab-ShopParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-settings\">settings</i>
                      <span>
                      Parámetros de la tienda
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-71\" class=\"submenu panel-collapse\">
                                                      
                              
              ";
        // line 925
        echo "                                              
                              <li class=\"link-leveltwo\" data-submenu=\"72\" id=\"subtab-AdminParentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"75\" id=\"subtab-AdminParentOrderPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/order-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración de pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"78\" id=\"subtab-AdminPPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/product-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración de Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"79\" id=\"subtab-AdminParentCustomerPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/customer-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Con";
        // line 951
        echo "figuración de clientes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"83\" id=\"subtab-AdminParentStores\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/contacts/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Contacto
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"86\" id=\"subtab-AdminParentMeta\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/seo-urls/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tráfico &amp; SEO
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"89\" id=\"subtab-AdminParentSearchConf\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminSearchConf&amp;token=96decb83384f0a6b84940c6e3080dd7d\" class=\"link\"> Buscar
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                                       ";
        // line 984
        echo "                   
                  <li class=\"link-levelone has_submenu link-active open ul-open\" data-submenu=\"92\" id=\"subtab-AdminAdvancedParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                      <span>
                      Parámetros Avanzados
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_up
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-92\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"93\" id=\"subtab-AdminInformation\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Información
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo link-active\" data-submenu=\"94\" id=\"subtab-AdminPerformance\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Rendimiento
                                </a>
                              </li>

                               ";
        // line 1012
        echo "                                                   
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"95\" id=\"subtab-AdminAdminPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/administration/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Administración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"96\" id=\"subtab-AdminEmails\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/emails/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> E-mail
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"97\" id=\"subtab-AdminImport\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/import/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Importar
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"98\" id=\"subtab-AdminParentEmployees\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Equ";
        // line 1040
        echo "ipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"102\" id=\"subtab-AdminParentRequestSql\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/sql-requests/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Base de datos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"105\" id=\"subtab-AdminLogs\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/logs/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Registros/Logs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"106\" id=\"subtab-AdminWebservice\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/webservice-keys/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Webservice
                                </a>
                              </li>

                                                                                                                                                                                                                                                    
                              
                                                ";
        // line 1070
        echo "            
                              <li class=\"link-leveltwo\" data-submenu=\"110\" id=\"subtab-AdminFeatureFlag\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/feature-flags/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Características nuevas y experimentales
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"111\" id=\"subtab-AdminParentSecurity\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/security/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Seguridad
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                  </ul>
  </div>
  
</nav>


<div class=\"header-toolbar d-print-none\">
    
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Parámetros Avanzados</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" aria-current=\"page\">Rendimiento</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Rendimiento          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary pointer\"          ";
        // line 1121
        echo "        id=\"page-header-desc-configuration-clear_cache\"
                  href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/clear-cache?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"                  title=\"Borrar la caché\"                                  >
                  <i class=\"material-icons\">delete</i>                  Borrar la caché
                </a>
                                      
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminAdvancedParametersPerformance%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>

      
    </div>
  </div>

  
  
  <div class=\"btn-floating\">
    <button class=\"btn btn-primary collapsed\" data-toggle=\"collapse\" data-target=\".btn-floating-container\" aria-expanded=\"false\">
      <i class=\"material-icons\">add</i>
    </button>
    <div class=\"btn-floating-container collapse\">
      <div class=\"btn-floating-menu\">
        
                              <a
              class=\"btn btn-floating-item   pointer\"              id=\"page-header-desc-floating-configuration-clear_cache\"
              href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/clear-cache?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"              title=\"Borrar la caché\"            >
              Borrar la caché
              <i class=\"material-icons\">delete</i>            </a>
                  
                              <a class=\"btn btn-floating-item btn-help btn-sidebar\" h";
        // line 1158
        echo "ref=\"#\"
               title=\"Ayuda\"
               data-toggle=\"sidebar\"
               data-target=\"#right-sidebar\"
               data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminAdvancedParametersPerformance%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
            >
              Ayuda
            </a>
                        </div>
    </div>
  </div>
  
</div>

<div id=\"main-div\">
          
      <div class=\"content-div  \">

        

                                                        
        <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>
<div id=\"content-message-box\"></div>


  ";
        // line 1183
        $this->displayBlock('content_header', $context, $blocks);
        $this->displayBlock('content', $context, $blocks);
        $this->displayBlock('content_footer', $context, $blocks);
        $this->displayBlock('sidebar_right', $context, $blocks);
        echo "

        

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons rtl-flip\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    
</div>
  

      <div class=\"bootstrap\">
      
    </div>
  
";
        // line 1217
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>";
        echo "
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 102
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1183
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_header"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_content_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_footer"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content_footer"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_sidebar_right($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sidebar_right"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "sidebar_right"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 1217
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "translate_javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "translate_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "__string_template__fd1e66f123bc6e1ea381ae972b56136f6a13e67f944c90022739f1078379ddf8";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1466 => 1217,  1397 => 1183,  1362 => 102,  1347 => 1217,  1307 => 1183,  1280 => 1158,  1241 => 1121,  1188 => 1070,  1156 => 1040,  1126 => 1012,  1096 => 984,  1061 => 951,  1033 => 925,  994 => 888,  963 => 859,  929 => 827,  895 => 795,  863 => 765,  832 => 736,  798 => 704,  760 => 668,  728 => 638,  697 => 609,  663 => 577,  632 => 548,  599 => 517,  565 => 485,  532 => 454,  482 => 406,  435 => 361,  387 => 315,  337 => 267,  294 => 226,  258 => 192,  239 => 175,  198 => 136,  159 => 102,  123 => 68,  95 => 42,  52 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ '<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Rendimiento • Prestashop_Inst1</title>

  <script type=\"text/javascript\">
    var help_class_name = \\'AdminPerformance\\';
    var iso_user = \\'es\\';
    var lang_is_rtl = \\'0\\';
    var full_language_code = \\'es\\';
    var full_cldr_language_code = \\'es-ES\\';
    var country_iso_code = \\'PE\\';
    var _PS_VERSION_ = \\'8.1.3\\';
    var roundMode = 2;
    var youEditFieldFor = \\'\\';
        var new_order_msg = \\'Se ha recibido un nuevo pedido en tu tienda.\\';
    var order_number_msg = \\'Número de pedido: \\';
    var total_msg = \\'Total: \\';
    var from_msg = \\'Desde: \\';
    var see_order_msg = \\'Ver este pedido\\';
    var new_customer_msg = \\'Un nuevo cliente se ha registrado en tu tienda.\\';
    var customer_name_msg = \\'Nombre del cliente: \\';
    var new_msg = \\'Un nuevo mensaje ha sido publicado en tu tienda.\\';
    var see_msg = \\'Leer este mensaje\\';
    var token = \\'529d635b00b415cad464397d1feeb236\\';
    var currentIndex = \\'index.php?controller=AdminPerformance\\';
    var employee_token = \\'3c0153e7b54a7d2ec35210ab31c994c9\\';
    var choose_language_translate = \\'Selecciona el idioma:\\';
    var default_language = \\'2\\';
    var admin_modules_link = \\'/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\\';
    var admin_notification_get_link = \\'/avtkttftlp0d3jsj/index.php/common/notifications?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\\';
    var admin_notification_push_link = adminNotificationPushLink = \\'/avtkttftlp0d3jsj/index.php/common/notifications/ack?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\\';
    var tab_modules_list = \\'\\';
    var update_success_msg = \\'Actualizaci' | raw }}{{ 'ón correcta\\';
    var search_product_msg = \\'Buscar un producto\\';
  </script>



<link
      rel=\"preload\"
      href=\"/avtkttftlp0d3jsj/themes/new-theme/public/2d8017489da689caedc1.preload..woff2\"
      as=\"font\"
      crossorigin
    >
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product_default_theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/blockwishlist/public/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\\\/avtkttftlp0d3jsj\\\\/\";
var baseDir = \"\\\\/\";
var changeFormLanguageUrl = \"\\\\/avtkttftlp0d3jsj\\\\/index.php\\\\/configure\\\\/advanced\\\\/employees\\\\/change-form-language?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\\\u00d7\",\"\\\\u2030\",\"\\\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\\\u00d7\",\"\\\\u2030\",\"\\\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.00\\\\u00a0\\\\u00a4\",\"negativePattern\":\"-#,##0.00\\\\u00a0\\\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\\\u00d7\",\"\\\\u2030\",\"\\\\u221e\",\"NaN\"],\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\\\u00d7\",\"\\\\u2030\",\"\\\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var prest' | raw }}{{ 'ashop = {\"debug\":true};
var show_new_customers = \"1\";
var show_new_messages = \"1\";
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/modules/blockwishlist/public/vendors.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_emailalerts/js/admin/ps_emailalerts.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: \\'#DF0067\\',
      textColor: \\'#FFFFFF\\',
      notificationGetUrl: \\'/avtkttftlp0d3jsj/index.php/common/notifications?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\\',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


' | raw }}{% block stylesheets %}{% endblock %}{% block extra_stylesheets %}{% endblock %}</head>{{ '

<body
  class=\"lang-es adminperformance developer-mode\"
  data-base-url=\"/avtkttftlp0d3jsj/index.php\"  data-token=\"cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">

  <header id=\"header\" class=\"d-print-none\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
      <span id=\"shop_version\">8.1.3</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item quick-row-link new-product-button\"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog' | raw }}{{ '/products-v2/create?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
        <div class=\"dropdown-divider\"></div>
          <a id=\"quick-add-link\"
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"162\"
        data-icon=\"icon-AdminAdvancedParameters\"
        data-method=\"add\"
        data-url=\"index.php/configure/advanced/performance/?-KlMFhsHVJho\"
        data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Rendimiento - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir página actual al Acceso Rápido
      </a>
        <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component component-search\" id=\"header-search-container\">
        <div class=\"component-search-body\">
          <div class=\"component-search-top\">
            <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/avtkttftlp0d' | raw }}{{ '3jsj/index.php?controller=AdminSearch&amp;token=0587329b7e0e65ce9ee1448093576fe6\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\" aria-label=\"Barra de búsqueda\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, referencia, etc.\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Nombre\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Numero de Factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
' | raw }}{{ '        <a class=\"dropdown-item\" data-item=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$(\\'#bo_query\\').one(\\'click\\', function() {
    \$(this).closest(\\'form\\').removeClass(\\'collapsed\\');
  });
});
</script>
            <button class=\"component-search-cancel d-none\">Cancelar</button>
          </div>

          <div class=\"component-search-quickaccess d-none\">
  <p class=\"component-search-title\">Acceso rápido</p>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
             data-item=\"Evaluación del catálogo\"
    >Evaluación del catálogo</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Módulos instalados\"
    >Módulos instalados</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Nueva categoría\"
    >Nueva categoría</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/products-v2/create?token=ce99db6ef6e5efbce6760c2365ec0188\"
           ' | raw }}{{ '  data-item=\"Nuevo\"
    >Nuevo</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
             data-item=\"Nuevo cupón de descuento\"
    >Nuevo cupón de descuento</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Pedidos\"
    >Pedidos</a>
    <div class=\"dropdown-divider\"></div>
      <a id=\"quick-add-link\"
      class=\"dropdown-item js-quick-link\"
      href=\"#\"
      data-rand=\"26\"
      data-icon=\"icon-AdminAdvancedParameters\"
      data-method=\"add\"
      data-url=\"index.php/configure/advanced/performance/?-KlMFhsHVJho\"
      data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
      data-prompt-text=\"Por favor, renombre este acceso rápido:\"
      data-link=\"Rendimiento - Lista\"
    >
      <i class=\"material-icons\">add_circle</i>
      Añadir página actual al Acceso Rápido
    </a>
    <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
    <i class=\"material-icons\">settings</i>
    Administrar accesos rápidos
  </a>
</div>
        </div>

        <div class=\"component-search-background d-none\"></div>
      </div>

              <div class=\"component hide-mobile-sm\" id=\"header-debug-mode-container\">
          <a class=\"link shop-state\"
             id=\"debug-mode\"
             data-toggle=\"pstooltip\"
             data-placement=\"bottom\"
             data-html=\"true\"
             title=\"<p class=&quot;text-left&quot;><strong>Tu tienda está en modo depuración.</strong></p><p class=&quot;text-left&quot;>S' | raw }}{{ 'e muestran todos los errores y mensajes PHP. Cuando ya no los necesites, &lt;strong&gt;desactiva&lt;/strong&gt; este modo.</p>\"
             href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
          >
            <i class=\"material-icons\">bug_report</i>
            <span>Modo depuración</span>
          </a>
        </div>
      
      
      <div class=\"header-right\">
                  <div class=\"component\" id=\"header-shop-list-container\">
              <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://prestashopinst1.live-website.com/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      <span>Ver mi tienda</span>
    </a>
  </div>
          </div>
                          <div class=\"component header-right-component\" id=\"header-notifications-container\">
            <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
      ' | raw }}{{ '        Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Has revisado tus <strong><a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarts&action=filterOnlyAbandonedCarts&token=7efad6290481526ed649f512ccd820de\">carritos abandonados</a></strong>?<br>?. ¡Tu próximo pedido podría estar ocultándose allí!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Se mantiene activo en las redes sociales en estos momentos?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Parece que todos tus clientes están contentos :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
   ' | raw }}{{ ' <a class=\"notif\" href=\\'order_url\\'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href=\\'customer_url\\'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href=\\'message_url\\'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
          </div>
        
        <div class=\"component\" id=\"header-employee-container\">
          <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      <div class=\"employee-top\">
        <span class=\"employee-avatar\"><img class=\"avatar rounded-circle\" src=\"http://prestashopinst1.live-website.com/img/pr/default.jpg\" alt=\"Site\" /></span>
        <span class=\"employee_profile\">Bienvenido de nuevo, Site</span>
      </div>

      <a class=\"dropdown-item employee-link profile-link\" href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/1/edit?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">
      <i class=\"material-icons\">edit</i>
      <span>Tu perfil</span>
    </a>
    </div>

    <p class=\"divider\"></p>

    
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminLogin&amp;logout=1&amp;token=5f1235c96f61a139637625a50965a110\">
      <i class=\"material-icons d-lg-n' | raw }}{{ 'one\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
        </div>
              </div>
    </nav>
  </header>

  <nav class=\"nav-bar d-none d-print-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/toggle-navigation?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\">
    <i class=\"material-icons rtl-flip\">chevron_left</i>
    <i class=\"material-icons rtl-flip\">chevron_left</i>
  </span>

  <div class=\"nav-bar-overflow\">
      <div class=\"logo-container\">
          <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
          <span id=\"shop_version\" class=\"header-version\">8.1.3</span>
      </div>

      <ul class=\"main-menu\">
              
                    
                    
          
            <li class=\"link-levelone\" data-submenu=\"1\" id=\"tab-AdminDashboard\">
              <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"link\" >
                <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
              </a>
            </li>

          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"2\" id=\"tab-SELL\">
                <span class=\"title\">Vender</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-shopping_basket' | raw }}{{ '\">shopping_basket</i>
                      <span>
                      Pedidos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/invoices/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Facturas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/credit-slips/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Facturas por abono
                                </a>
                 ' | raw }}{{ '             </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/delivery-slips/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Albaranes de entrega
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarts&amp;token=7efad6290481526ed649f512ccd820de\" class=\"link\"> Carritos de compra
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-store\">store</i>
                      <span>
                      Catálogo
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                    ' | raw }}{{ '                        </i>
                                            </a>
                                              <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/categories?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Categorías
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/monitoring/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Monitoreo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
  ' | raw }}{{ '                              <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminAttributesGroups&amp;token=2eb46092d80c0b3e72aec32eb4043440\" class=\"link\"> Atributos y Características
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/brands/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Marcas y Proveedores
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/attachments/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Archivos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\" class=\"link\"> Descuentos
                                </a>
                              </li>

                                                                                  
    ' | raw }}{{ '                          
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/stocks/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Stock
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/customers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-account_circle\">account_circle</i>
                      <span>
                      Clientes
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Clientes
                                </a>
                              </li>

       ' | raw }}{{ '                                                                           
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/addresses/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Direcciones
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\">
                      <i class=\"material-icons mi-chat\">chat</i>
                      <span>
                      Servicio al Cliente
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                                <a href=\"http://prest' | raw }}{{ 'ashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\"> Servicio al Cliente
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customer-service/order-messages/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Mensajes de Pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminReturn&amp;token=2586f6075464f1f2accf0f67761c20c8\" class=\"link\"> Devoluciones de mercancía
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;token=b18d3d66929e4bc598af8f146c1fdafe\" class=\"link\">
                      <i class=\"material-icons mi-assessment\">assessment</i>
                      <span>
            ' | raw }}{{ '          Estadísticas
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"37\" id=\"tab-IMPROVE\">
                <span class=\"title\">Personalizar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"38\" id=\"subtab-AdminParentModulesSf\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-extension\">extension</i>
                      <span>
                      Módulos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-38\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"39\" id=\"subtab-AdminModulesSf\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPX' | raw }}{{ 'Udx-KlMFhsHVJho\" class=\"link\"> Administrador de módulos
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentThemes\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                      <span>
                      Diseño
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"125\" id=\"subtab-AdminThemesParent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tema y logotipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\"' | raw }}{{ ' data-submenu=\"45\" id=\"subtab-AdminParentMailTheme\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/mail_theme/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tema Email
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"47\" id=\"subtab-AdminCmsContent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/cms-pages/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Páginas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"48\" id=\"subtab-AdminModulesPositions\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/modules/positions/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Posiciones
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"49\" id=\"subtab-AdminImages\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminImages&amp;token=0095a57f51f7bb591a88a227e9214d47\" class=\"link\"> Ajustes de imágenes
                                </a>
                              </li>

                                                                                  ' | raw }}{{ '
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"118\" id=\"subtab-AdminLinkWidget\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/modules/link-widget/list?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Lista de enlaces
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentShipping\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/carriers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                      <span>
                      Transporte
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"51\" id=\"subtab-AdminCarriers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/carriers/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Transportistas
                            ' | raw }}{{ '    </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"52\" id=\"subtab-AdminShipping\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"132\" id=\"subtab-AdminShippingCostRule\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminShippingCostRule&amp;token=168210935506050aa6fe18be8e9f9a42\" class=\"link\"> Costo de envío
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"53\" id=\"subtab-AdminParentPayment\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-payment\">payment</i>
                      <span>
                      Pago
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    ke' | raw }}{{ 'yboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"54\" id=\"subtab-AdminPayment\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Métodos de pago
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"55\" id=\"subtab-AdminPaymentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminInternational\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-language\">language</i>
                      <span>
                      Internacional
                      </span>
                         ' | raw }}{{ '                           <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"57\" id=\"subtab-AdminParentLocalization\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Localización
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"62\" id=\"subtab-AdminParentCountries\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/zones/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Ubicaciones Geográficas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"66\" id=\"subtab-AdminParentTaxes\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/taxes/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Impuestos
                                </a>
                              </li>

                                        ' | raw }}{{ '                                          
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"69\" id=\"subtab-AdminTranslations\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/translations/settings?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Traducciones
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title link-active\" data-submenu=\"70\" id=\"tab-CONFIGURE\">
                <span class=\"title\">Configurar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"71\" id=\"subtab-ShopParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-settings\">settings</i>
                      <span>
                      Parámetros de la tienda
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-71\" class=\"submenu panel-collapse\">
                                                      
                              
              ' | raw }}{{ '                                              
                              <li class=\"link-leveltwo\" data-submenu=\"72\" id=\"subtab-AdminParentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"75\" id=\"subtab-AdminParentOrderPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/order-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración de pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"78\" id=\"subtab-AdminPPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/product-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Configuración de Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"79\" id=\"subtab-AdminParentCustomerPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/customer-preferences/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Con' | raw }}{{ 'figuración de clientes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"83\" id=\"subtab-AdminParentStores\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/contacts/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Contacto
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"86\" id=\"subtab-AdminParentMeta\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/seo-urls/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Tráfico &amp; SEO
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"89\" id=\"subtab-AdminParentSearchConf\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminSearchConf&amp;token=96decb83384f0a6b84940c6e3080dd7d\" class=\"link\"> Buscar
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                                       ' | raw }}{{ '                   
                  <li class=\"link-levelone has_submenu link-active open ul-open\" data-submenu=\"92\" id=\"subtab-AdminAdvancedParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\">
                      <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                      <span>
                      Parámetros Avanzados
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_up
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-92\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"93\" id=\"subtab-AdminInformation\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Información
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo link-active\" data-submenu=\"94\" id=\"subtab-AdminPerformance\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Rendimiento
                                </a>
                              </li>

                               ' | raw }}{{ '                                                   
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"95\" id=\"subtab-AdminAdminPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/administration/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Administración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"96\" id=\"subtab-AdminEmails\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/emails/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> E-mail
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"97\" id=\"subtab-AdminImport\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/import/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Importar
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"98\" id=\"subtab-AdminParentEmployees\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Equ' | raw }}{{ 'ipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"102\" id=\"subtab-AdminParentRequestSql\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/sql-requests/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Base de datos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"105\" id=\"subtab-AdminLogs\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/logs/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Registros/Logs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"106\" id=\"subtab-AdminWebservice\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/webservice-keys/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Webservice
                                </a>
                              </li>

                                                                                                                                                                                                                                                    
                              
                                                ' | raw }}{{ '            
                              <li class=\"link-leveltwo\" data-submenu=\"110\" id=\"subtab-AdminFeatureFlag\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/feature-flags/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Características nuevas y experimentales
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"111\" id=\"subtab-AdminParentSecurity\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/security/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" class=\"link\"> Seguridad
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                  </ul>
  </div>
  
</nav>


<div class=\"header-toolbar d-print-none\">
    
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Parámetros Avanzados</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\" aria-current=\"page\">Rendimiento</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Rendimiento          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                                                          <a
                  class=\"btn btn-primary pointer\"          ' | raw }}{{ '        id=\"page-header-desc-configuration-clear_cache\"
                  href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/clear-cache?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"                  title=\"Borrar la caché\"                                  >
                  <i class=\"material-icons\">delete</i>                  Borrar la caché
                </a>
                                      
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminAdvancedParametersPerformance%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>

      
    </div>
  </div>

  
  
  <div class=\"btn-floating\">
    <button class=\"btn btn-primary collapsed\" data-toggle=\"collapse\" data-target=\".btn-floating-container\" aria-expanded=\"false\">
      <i class=\"material-icons\">add</i>
    </button>
    <div class=\"btn-floating-container collapse\">
      <div class=\"btn-floating-menu\">
        
                              <a
              class=\"btn btn-floating-item   pointer\"              id=\"page-header-desc-floating-configuration-clear_cache\"
              href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/clear-cache?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"              title=\"Borrar la caché\"            >
              Borrar la caché
              <i class=\"material-icons\">delete</i>            </a>
                  
                              <a class=\"btn btn-floating-item btn-help btn-sidebar\" h' | raw }}{{ 'ref=\"#\"
               title=\"Ayuda\"
               data-toggle=\"sidebar\"
               data-target=\"#right-sidebar\"
               data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminAdvancedParametersPerformance%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=cmTFl_L5rMX_VG8BnrwrAiuowRPXUdx-KlMFhsHVJho\"
            >
              Ayuda
            </a>
                        </div>
    </div>
  </div>
  
</div>

<div id=\"main-div\">
          
      <div class=\"content-div  \">

        

                                                        
        <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>
<div id=\"content-message-box\"></div>


  ' | raw }}{% block content_header %}{% endblock %}{% block content %}{% endblock %}{% block content_footer %}{% endblock %}{% block sidebar_right %}{% endblock %}{{ '

        

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons rtl-flip\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    
</div>
  

      <div class=\"bootstrap\">
      
    </div>
  
' | raw }}{% block javascripts %}{% endblock %}{% block extra_javascripts %}{% endblock %}{% block translate_javascripts %}{% endblock %}</body>{{ '
</html>' | raw }}", "__string_template__fd1e66f123bc6e1ea381ae972b56136f6a13e67f944c90022739f1078379ddf8", "");
    }
}
