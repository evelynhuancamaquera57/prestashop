<?php

class ModuleRepository_091bb2f extends \PrestaShop\PrestaShop\Core\Module\ModuleRepository implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \PrestaShop\PrestaShop\Core\Module\ModuleRepository|null wrapped object, if the proxy is initialized
     */
    private $valueHolder49487 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializera2768 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties32bba = [
        
    ];

    public function getList() : \PrestaShop\PrestaShop\Core\Module\ModuleCollection
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getList', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getList();
    }

    public function getInstalledModules() : \PrestaShop\PrestaShop\Core\Module\ModuleCollection
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getInstalledModules', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getInstalledModules();
    }

    public function getMustBeConfiguredModules() : \PrestaShop\PrestaShop\Core\Module\ModuleCollection
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getMustBeConfiguredModules', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getMustBeConfiguredModules();
    }

    public function getUpgradableModules() : \PrestaShop\PrestaShop\Core\Module\ModuleCollection
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getUpgradableModules', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getUpgradableModules();
    }

    public function getModule(string $moduleName) : \PrestaShop\PrestaShop\Core\Module\ModuleInterface
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getModule', array('moduleName' => $moduleName), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getModule($moduleName);
    }

    public function getModulePath(string $moduleName) : ?string
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getModulePath', array('moduleName' => $moduleName), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->getModulePath($moduleName);
    }

    public function setActionUrls(\PrestaShop\PrestaShop\Core\Module\ModuleCollection $collection) : \PrestaShop\PrestaShop\Core\Module\ModuleCollection
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'setActionUrls', array('collection' => $collection), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->setActionUrls($collection);
    }

    public function clearCache(?string $moduleName = null, bool $allShops = false) : bool
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'clearCache', array('moduleName' => $moduleName, 'allShops' => $allShops), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return $this->valueHolder49487->clearCache($moduleName, $allShops);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\PrestaShop\PrestaShop\Core\Module\ModuleRepository $instance) {
            unset($instance->moduleDataProvider, $instance->adminModuleDataProvider, $instance->hookManager, $instance->cacheProvider, $instance->modulePath, $instance->installedModules, $instance->modulesFromHook, $instance->contextLangId);
        }, $instance, 'PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository')->__invoke($instance);

        $instance->initializera2768 = $initializer;

        return $instance;
    }

    public function __construct(\PrestaShop\PrestaShop\Adapter\Module\ModuleDataProvider $moduleDataProvider, \PrestaShop\PrestaShop\Adapter\Module\AdminModuleDataProvider $adminModuleDataProvider, \Doctrine\Common\Cache\CacheProvider $cacheProvider, \PrestaShop\PrestaShop\Adapter\HookManager $hookManager, string $modulePath, int $contextLangId)
    {
        static $reflection;

        if (! $this->valueHolder49487) {
            $reflection = $reflection ?? new \ReflectionClass('PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository');
            $this->valueHolder49487 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\PrestaShop\PrestaShop\Core\Module\ModuleRepository $instance) {
            unset($instance->moduleDataProvider, $instance->adminModuleDataProvider, $instance->hookManager, $instance->cacheProvider, $instance->modulePath, $instance->installedModules, $instance->modulesFromHook, $instance->contextLangId);
        }, $this, 'PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository')->__invoke($this);

        }

        $this->valueHolder49487->__construct($moduleDataProvider, $adminModuleDataProvider, $cacheProvider, $hookManager, $modulePath, $contextLangId);
    }

    public function & __get($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__get', ['name' => $name], $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        if (isset(self::$publicProperties32bba[$name])) {
            return $this->valueHolder49487->$name;
        }

        $realInstanceReflection = new \ReflectionClass('PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder49487;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        $realInstanceReflection = new \ReflectionClass('PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder49487;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__isset', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        $realInstanceReflection = new \ReflectionClass('PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder49487;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__unset', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        $realInstanceReflection = new \ReflectionClass('PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder49487;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__clone', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        $this->valueHolder49487 = clone $this->valueHolder49487;
    }

    public function __sleep()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__sleep', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;

        return array('valueHolder49487');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\PrestaShop\PrestaShop\Core\Module\ModuleRepository $instance) {
            unset($instance->moduleDataProvider, $instance->adminModuleDataProvider, $instance->hookManager, $instance->cacheProvider, $instance->modulePath, $instance->installedModules, $instance->modulesFromHook, $instance->contextLangId);
        }, $this, 'PrestaShop\\PrestaShop\\Core\\Module\\ModuleRepository')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializera2768 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializera2768;
    }

    public function initializeProxy() : bool
    {
        return $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'initializeProxy', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder49487;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder49487;
    }
}
