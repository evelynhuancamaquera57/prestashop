<?php

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder49487 = null;
    private $initializera2768 = null;
    private static $publicProperties32bba = [
        
    ];
    public function getConnection()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getConnection', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getMetadataFactory', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getExpressionBuilder', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'beginTransaction', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->beginTransaction();
    }
    public function getCache()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getCache', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getCache();
    }
    public function transactional($func)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'transactional', array('func' => $func), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'wrapInTransaction', array('func' => $func), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'commit', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->commit();
    }
    public function rollback()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'rollback', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getClassMetadata', array('className' => $className), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getClassMetadata($className);
    }
    public function createQuery($dql = '')
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'createQuery', array('dql' => $dql), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->createQuery($dql);
    }
    public function createNamedQuery($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'createNamedQuery', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'createQueryBuilder', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'flush', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getPartialReference($entityName, $identifier);
    }
    public function clear($entityName = null)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'clear', array('entityName' => $entityName), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->clear($entityName);
    }
    public function close()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'close', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->close();
    }
    public function persist($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'persist', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'remove', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->remove($entity);
    }
    public function refresh($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'refresh', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->refresh($entity);
    }
    public function detach($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'detach', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'merge', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getRepository', array('entityName' => $entityName), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'contains', array('entity' => $entity), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getEventManager', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getConfiguration', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'isOpen', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getUnitOfWork', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getProxyFactory', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'initializeObject', array('obj' => $obj), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'getFilters', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'isFiltersStateClean', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'hasFilters', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return $this->valueHolder49487->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializera2768 = $initializer;
        return $instance;
    }
    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;
        if (! $this->valueHolder49487) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder49487 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolder49487->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__get', ['name' => $name], $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        if (isset(self::$publicProperties32bba[$name])) {
            return $this->valueHolder49487->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder49487;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__set', array('name' => $name, 'value' => $value), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder49487;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__isset', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder49487;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__unset', array('name' => $name), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder49487;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder49487;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__clone', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        $this->valueHolder49487 = clone $this->valueHolder49487;
    }
    public function __sleep()
    {
        $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, '__sleep', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
        return array('valueHolder49487');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializera2768 = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializera2768;
    }
    public function initializeProxy() : bool
    {
        return $this->initializera2768 && ($this->initializera2768->__invoke($valueHolder49487, $this, 'initializeProxy', array(), $this->initializera2768) || 1) && $this->valueHolder49487 = $valueHolder49487;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder49487;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder49487;
    }
}
