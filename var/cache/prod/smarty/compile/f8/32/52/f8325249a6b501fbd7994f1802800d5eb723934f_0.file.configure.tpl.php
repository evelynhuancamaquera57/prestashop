<?php
/* Smarty version 4.3.4, created on 2024-01-09 13:06:36
  from '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/cronjobs/views/templates/admin/configure.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_659d8b2c937e88_51764749',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f8325249a6b501fbd7994f1802800d5eb723934f' => 
    array (
      0 => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/modules/cronjobs/views/templates/admin/configure.tpl',
      1 => 1704738629,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_659d8b2c937e88_51764749 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="panel">
    <h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'What does this module do?','mod'=>'cronjobs'),$_smarty_tpl ) );?>
</h3>
    <p>
        <img src="<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value ));?>
/logo.png" class="pull-left" id="cronjobs-logo" />
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Originally, cron is a Unix system tool that provides time-based job scheduling: you can create many cron jobs, which are then run periodically at fixed times, dates, or intervals.','mod'=>'cronjobs'),$_smarty_tpl ) );?>

        <br/>
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This module provides you with a cron-like tool: you can create jobs which will call a given set of secure URLs to your PrestaShop store, thus triggering updates and other automated tasks.','mod'=>'cronjobs'),$_smarty_tpl ) );?>

    </p>
</div>
<?php }
}
