<?php
/* Smarty version 4.3.4, created on 2024-01-09 13:12:54
  from 'module:ps_linklistviewstemplateshooklinkblock.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.3.4',
  'unifunc' => 'content_659d8ca62869a1_56677633',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:ps_linklistviewstemplateshooklinkblock.tpl',
      1 => 1697815446,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_659d8ca62869a1_56677633 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->smarty->ext->_tplFunction->registerTplFunctions($_smarty_tpl, array (
  'renderLogo' => 
  array (
    'compiled_filepath' => '/homepages/6/d991386046/htdocs/clickandbuilds/PrestashopInst1/var/cache/prod/smarty/compile/classiclayouts_layout_full_width_tpl/43/5b/9f/435b9f369657df171c5002bc55a7aa706233b30a_2.file.helpers.tpl.php',
    'uid' => '435b9f369657df171c5002bc55a7aa706233b30a',
    'call_name' => 'smarty_template_function_renderLogo_1782874728659d8ca5e6d877_75352285',
  ),
));
?><div class="col-md-6 links">
  <div class="row">
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Products</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_1" data-toggle="collapse">
        <span class="h3">Products</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_1" class="collapse">
                  <li>
            <a
                id="link-product-page-prices-drop-1"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=prices-drop"
                title="Nuestros productos especiales"
                            >
              Ofertas
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-new-products-1"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=new-products"
                title="Novedades"
                            >
              Novedades
            </a>
          </li>
                  <li>
            <a
                id="link-product-page-best-sales-1"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=best-sales"
                title="Los más vendidos"
                            >
              Los más vendidos
            </a>
          </li>
              </ul>
    </div>
      <div class="col-md-6 wrapper">
      <p class="h3 hidden-sm-down">Our company</p>
      <div class="title clearfix hidden-md-up" data-target="#footer_sub_menu_2" data-toggle="collapse">
        <span class="h3">Our company</span>
        <span class="float-xs-right">
          <span class="navbar-toggler collapse-icons">
            <i class="material-icons add">&#xE313;</i>
            <i class="material-icons remove">&#xE316;</i>
          </span>
        </span>
      </div>
      <ul id="footer_sub_menu_2" class="collapse">
                  <li>
            <a
                id="link-cms-page-1-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?id_cms=1&amp;controller=cms&amp;id_lang=2"
                title="Nuestros términos y condiciones de envío"
                            >
              Envío
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-2-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?id_cms=2&amp;controller=cms&amp;id_lang=2"
                title="Aviso legal"
                            >
              Aviso legal
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-3-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?id_cms=3&amp;controller=cms&amp;id_lang=2"
                title="Nuestros términos y condiciones"
                            >
              Términos y condiciones
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-4-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?id_cms=4&amp;controller=cms&amp;id_lang=2"
                title="Averigüe más sobre nosotros"
                            >
              Sobre nosotros
            </a>
          </li>
                  <li>
            <a
                id="link-cms-page-5-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?id_cms=5&amp;controller=cms&amp;id_lang=2"
                title="Nuestra forma de pago segura"
                            >
              Pago seguro
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-contact-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=contact"
                title="Contáctenos"
                            >
              Contacte con nosotros
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-sitemap-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=sitemap"
                title="¿Perdido? Encuentre lo que está buscando"
                            >
              Mapa del sitio
            </a>
          </li>
                  <li>
            <a
                id="link-static-page-stores-2"
                class="cms-page-link"
                href="http://prestashopinst1.live-website.com/index.php?controller=stores"
                title=""
                            >
              Tiendas
            </a>
          </li>
              </ul>
    </div>
    </div>
</div>
<?php }
}
