<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f5e3c07957310db824f46ec29180a86e64791898038f9c2c82936698d9380133 */
class __TwigTemplate_29a8113ffaf0a92987d7d8e01a1c2182201610171634b63e06a192a14368223c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/img/app_icon.png\" />

<title>Registros/Logs • Prestashop_Inst1</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminLogs';
    var iso_user = 'es';
    var lang_is_rtl = '0';
    var full_language_code = 'es';
    var full_cldr_language_code = 'es-ES';
    var country_iso_code = 'PE';
    var _PS_VERSION_ = '8.1.3';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Se ha recibido un nuevo pedido en tu tienda.';
    var order_number_msg = 'Número de pedido: ';
    var total_msg = 'Total: ';
    var from_msg = 'Desde: ';
    var see_order_msg = 'Ver este pedido';
    var new_customer_msg = 'Un nuevo cliente se ha registrado en tu tienda.';
    var customer_name_msg = 'Nombre del cliente: ';
    var new_msg = 'Un nuevo mensaje ha sido publicado en tu tienda.';
    var see_msg = 'Leer este mensaje';
    var token = '3445d4aacae4dc41467be865e106da5b';
    var currentIndex = 'index.php?controller=AdminLogs';
    var employee_token = '3c0153e7b54a7d2ec35210ab31c994c9';
    var choose_language_translate = 'Selecciona el idioma:';
    var default_language = '2';
    var admin_modules_link = '/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY';
    var admin_notification_get_link = '/avtkttftlp0d3jsj/index.php/common/notifications?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY';
    var admin_notification_push_link = adminNotificationPushLink = '/avtkttftlp0d3jsj/index.php/common/notifications/ack?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY';
    var tab_modules_list = '';
    var update_success_msg = 'Actualización correct";
        // line 42
        echo "a';
    var search_product_msg = 'Buscar un producto';
  </script>



<link
      rel=\"preload\"
      href=\"/avtkttftlp0d3jsj/themes/new-theme/public/2d8017489da689caedc1.preload..woff2\"
      as=\"font\"
      crossorigin
    >
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product_default_theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/modules/blockwishlist/public/backoffice.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/avtkttftlp0d3jsj/themes/default/css/vendor/nv.d3.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/avtkttftlp0d3jsj\\/\";
var baseDir = \"\\/\";
var changeFormLanguageUrl = \"\\/avtkttftlp0d3jsj\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\";
var currency = {\"iso_code\":\"PEN\",\"sign\":\"PEN\",\"name\":\"Sol\",\"format\":null};
var currency_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"PEN\",\"currencySymbol\":\"PEN\",\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var number_specifications = {\"symbol\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"numberSymbols\":[\",\",\".\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var prestashop = {\"d";
        // line 68
        echo "ebug\":false};
var show_new_customers = \"1\";
var show_new_messages = \"1\";
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>
<script type=\"text/javascript\" src=\"/js/admin.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/js/tools.js?v=8.1.3\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/new-theme/public/create_product.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/modules/blockwishlist/public/vendors.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_emailalerts/js/admin/ps_emailalerts.js\"></script>
<script type=\"text/javascript\" src=\"/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/avtkttftlp0d3jsj/themes/default/js/vendor/nv.d3.min.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/modules/ps_faviconnotificationbo/views/js/ps_faviconnotificationbo.js\"></script>

  <script>
  if (undefined !== ps_faviconnotificationbo) {
    ps_faviconnotificationbo.initialize({
      backgroundColor: '#DF0067',
      textColor: '#FFFFFF',
      notificationGetUrl: '/avtkttftlp0d3jsj/index.php/common/notifications?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY',
      CHECKBOX_ORDER: 1,
      CHECKBOX_CUSTOMER: 1,
      CHECKBOX_MESSAGE: 1,
      timer: 120000, // Refresh every 2 minutes
    });
  }
</script>


";
        // line 102
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>";
        echo "

<body
  class=\"lang-es adminlogs\"
  data-base-url=\"/avtkttftlp0d3jsj/index.php\"  data-token=\"l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\">

  <header id=\"header\" class=\"d-print-none\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
      <span id=\"shop_version\">8.1.3</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Acceso rápido
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
                 data-item=\"Evaluación del catálogo\"
      >Evaluación del catálogo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Módulos instalados\"
      >Módulos instalados</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nueva categoría\"
      >Nueva categoría</a>
          <a class=\"dropdown-item quick-row-link new-product-button\"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/products-v2/create?to";
        // line 136
        echo "ken=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Nuevo\"
      >Nuevo</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
                 data-item=\"Nuevo cupón de descuento\"
      >Nuevo cupón de descuento</a>
          <a class=\"dropdown-item quick-row-link \"
         href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
                 data-item=\"Pedidos\"
      >Pedidos</a>
        <div class=\"dropdown-divider\"></div>
          <a id=\"quick-add-link\"
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"78\"
        data-icon=\"icon-AdminAdvancedParameters\"
        data-method=\"add\"
        data-url=\"index.php/configure/advanced/logs\"
        data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
        data-prompt-text=\"Por favor, renombre este acceso rápido:\"
        data-link=\"Registros/Logs - Lista\"
      >
        <i class=\"material-icons\">add_circle</i>
        Añadir página actual al Acceso Rápido
      </a>
        <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
      <i class=\"material-icons\">settings</i>
      Administrar accesos rápidos
    </a>
  </div>
</div>
      </div>
      <div class=\"component component-search\" id=\"header-search-container\">
        <div class=\"component-search-body\">
          <div class=\"component-search-top\">
            <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/avtkttftlp0d3jsj/index.php?controller=AdminSearch&amp";
        // line 175
        echo ";token=0587329b7e0e65ce9ee1448093576fe6\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Buscar (p. ej.: referencia de producto, nombre de cliente...)\" aria-label=\"Barra de búsqueda\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        toda la tienda
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"toda la tienda\" href=\"#\" data-value=\"0\" data-placeholder=\"¿Qué estás buscando?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> toda la tienda</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catálogo\" href=\"#\" data-value=\"1\" data-placeholder=\"Nombre del producto, referencia, etc.\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catálogo</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por nombre\" href=\"#\" data-value=\"2\" data-placeholder=\"Nombre\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clientes por nombre</a>
        <a class=\"dropdown-item\" data-item=\"Clientes por dirección IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clientes por dirección IP</a>
        <a class=\"dropdown-item\" data-item=\"Pedidos\" href=\"#\" data-value=\"3\" data-placeholder=\"ID del pedido\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Pedidos</a>
        <a class=\"dropdown-item\" data-item=\"Facturas\" href=\"#\" data-value=\"4\" data-placeholder=\"Numero de Factura\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Facturas</a>
        <a class=\"dropdown-item\" data-ite";
        // line 192
        echo "m=\"Carritos\" href=\"#\" data-value=\"5\" data-placeholder=\"ID carrito\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Carritos</a>
        <a class=\"dropdown-item\" data-item=\"Módulos\" href=\"#\" data-value=\"7\" data-placeholder=\"Nombre del módulo\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Módulos</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">BÚSQUEDA</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
            <button class=\"component-search-cancel d-none\">Cancelar</button>
          </div>

          <div class=\"component-search-quickaccess d-none\">
  <p class=\"component-search-title\">Acceso rápido</p>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=b18d3d66929e4bc598af8f146c1fdafe\"
             data-item=\"Evaluación del catálogo\"
    >Evaluación del catálogo</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/improve/modules/manage?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Módulos instalados\"
    >Módulos instalados</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/categories/new?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Nueva categoría\"
    >Nueva categoría</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/catalog/products-v2/create?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Nuevo\"
    >Nuevo</a>
      ";
        // line 228
        echo "<a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\"
             data-item=\"Nuevo cupón de descuento\"
    >Nuevo cupón de descuento</a>
      <a class=\"dropdown-item quick-row-link\"
       href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php/sell/orders?token=ce99db6ef6e5efbce6760c2365ec0188\"
             data-item=\"Pedidos\"
    >Pedidos</a>
    <div class=\"dropdown-divider\"></div>
      <a id=\"quick-add-link\"
      class=\"dropdown-item js-quick-link\"
      href=\"#\"
      data-rand=\"95\"
      data-icon=\"icon-AdminAdvancedParameters\"
      data-method=\"add\"
      data-url=\"index.php/configure/advanced/logs\"
      data-post-link=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\"
      data-prompt-text=\"Por favor, renombre este acceso rápido:\"
      data-link=\"Registros/Logs - Lista\"
    >
      <i class=\"material-icons\">add_circle</i>
      Añadir página actual al Acceso Rápido
    </a>
    <a id=\"quick-manage-link\" class=\"dropdown-item\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminQuickAccesses&token=7944a42f5a4632d24655639f007aeb6d\">
    <i class=\"material-icons\">settings</i>
    Administrar accesos rápidos
  </a>
</div>
        </div>

        <div class=\"component-search-background d-none\"></div>
      </div>

      
      
      <div class=\"header-right\">
                  <div class=\"component\" id=\"header-shop-list-container\">
              <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://prestashopinst1.live-website.com/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      <span>Ver mi tienda</span>
    </a>
  </div>
          </div>
                          <div class=\"component header-right-component\" id";
        // line 272
        echo "=\"header-notifications-container\">
            <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Pedidos<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clientes<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Mensajes<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay pedidos nuevos por ahora :(<br>
              ¿Has revisado tus <strong><a href=\"http://prestashopinst1.live-website.com/a";
        // line 324
        echo "vtkttftlp0d3jsj/index.php?controller=AdminCarts&action=filterOnlyAbandonedCarts&token=7efad6290481526ed649f512ccd820de\">carritos abandonados</a></strong>?<br>?. ¡Tu próximo pedido podría estar ocultándose allí!
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay clientes nuevos por ahora :(<br>
              ¿Se mantiene activo en las redes sociales en estos momentos?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              No hay mensajes nuevo por ahora.<br>
              Parece que todos tus clientes están contentos :)
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - registrado <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
          </div>
        
        <div class=\"comp";
        // line 371
        echo "onent\" id=\"header-employee-container\">
          <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      <div class=\"employee-top\">
        <span class=\"employee-avatar\"><img class=\"avatar rounded-circle\" src=\"http://prestashopinst1.live-website.com/img/pr/default.jpg\" alt=\"Site\" /></span>
        <span class=\"employee_profile\">Bienvenido de nuevo, Site</span>
      </div>

      <a class=\"dropdown-item employee-link profile-link\" href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/1/edit?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\">
      <i class=\"material-icons\">edit</i>
      <span>Tu perfil</span>
    </a>
    </div>

    <p class=\"divider\"></p>

    
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminLogin&amp;logout=1&amp;token=5f1235c96f61a139637625a50965a110\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Cerrar sesión</span>
    </a>
  </div>
</div>
        </div>
              </div>
    </nav>
  </header>

  <nav class=\"nav-bar d-none d-print-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/toggle-navigation?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\">
    <i class=\"material-icons rtl-flip\">chevron_left</i>
    <i class=\"material-icons rtl-flip\">chevron_left</i>
  </span>

  <div class=\"nav-bar-overflow\">
      <div class=\"logo-container\">
          <a id=\"header_logo\" class=\"logo float-left\" href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\"></a>
          <span id=\"shop_version\" class=\"header-version\">8.1.3</span>
      </div";
        // line 413
        echo ">

      <ul class=\"main-menu\">
              
                    
                    
          
            <li class=\"link-levelone\" data-submenu=\"1\" id=\"tab-AdminDashboard\">
              <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"link\" >
                <i class=\"material-icons\">trending_up</i> <span>Inicio</span>
              </a>
            </li>

          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"2\" id=\"tab-SELL\">
                <span class=\"title\">Vender</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                      <span>
                      Pedidos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/?_token=l4dSTb9l2PsZ";
        // line 454
        echo "g50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/invoices/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Facturas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/credit-slips/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Facturas por abono
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/orders/delivery-slips/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Albaranes de entrega
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                      ";
        // line 486
        echo "          <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarts&amp;token=7efad6290481526ed649f512ccd820de\" class=\"link\"> Carritos de compra
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-store\">store</i>
                      <span>
                      Catálogo
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/products?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
           ";
        // line 518
        echo "                   <li class=\"link-leveltwo\" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/categories?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Categorías
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/monitoring/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Monitoreo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminAttributesGroups&amp;token=2eb46092d80c0b3e72aec32eb4043440\" class=\"link\"> Atributos y Características
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/catalog/brands/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Marcas y Proveedores
                                </a>
                              </li>

             ";
        // line 547
        echo "                                                                     
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/attachments/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Archivos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCartRules&amp;token=f08bdf2f86f4238b60c054aee2afc4f5\" class=\"link\"> Descuentos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/stocks/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Stock
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                    <a href=\"/avtkttftlp0d3jsj/ind";
        // line 578
        echo "ex.php/sell/customers/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-account_circle\">account_circle</i>
                      <span>
                      Clientes
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customers/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Clientes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/addresses/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Direcciones
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <l";
        // line 610
        echo "i class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\">
                      <i class=\"material-icons mi-chat\">chat</i>
                      <span>
                      Servicio al Cliente
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCustomerThreads&amp;token=b7de66d510d21b9621c7cadea539d7e1\" class=\"link\"> Servicio al Cliente
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/sell/customer-service/order-messages/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Mensajes de Pedidos
                                </a>
                              </li>

                                                                 ";
        // line 637
        echo "                 
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminReturn&amp;token=2586f6075464f1f2accf0f67761c20c8\" class=\"link\"> Devoluciones de mercancía
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                    <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminStats&amp;token=b18d3d66929e4bc598af8f146c1fdafe\" class=\"link\">
                      <i class=\"material-icons mi-assessment\">assessment</i>
                      <span>
                      Estadísticas
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title\" data-submenu=\"37\" id=\"tab-IMPROVE\">
                <span class=\"title\">Personalizar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelon";
        // line 676
        echo "e has_submenu\" data-submenu=\"38\" id=\"subtab-AdminParentModulesSf\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-extension\">extension</i>
                      <span>
                      Módulos
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-38\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"39\" id=\"subtab-AdminModulesSf\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/modules/manage?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Administrador de módulos
                                </a>
                              </li>

                                                                                                                                    </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"43\" id=\"subtab-AdminParentThemes\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                      <span>
                      Diseño
       ";
        // line 706
        echo "               </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"125\" id=\"subtab-AdminThemesParent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/themes/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Tema y logotipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"45\" id=\"subtab-AdminParentMailTheme\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/mail_theme/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Tema Email
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"47\" id=\"subtab-AdminCmsContent\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/cms-pages/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Páginas
                                </a>
                              </li>

                             ";
        // line 736
        echo "                                                     
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"48\" id=\"subtab-AdminModulesPositions\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/design/modules/positions/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Posiciones
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"49\" id=\"subtab-AdminImages\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminImages&amp;token=0095a57f51f7bb591a88a227e9214d47\" class=\"link\"> Ajustes de imágenes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"118\" id=\"subtab-AdminLinkWidget\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/modules/link-widget/list?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Lista de enlaces
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"50\" id=\"subtab-AdminParentShipping\">
                    <a hre";
        // line 767
        echo "f=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarriers&amp;token=3abe895508a054af93809a1ad7984df9\" class=\"link\">
                      <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                      <span>
                      Transporte
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-50\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"51\" id=\"subtab-AdminCarriers\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminCarriers&amp;token=3abe895508a054af93809a1ad7984df9\" class=\"link\"> Transportistas
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"52\" id=\"subtab-AdminShipping\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/shipping/preferences/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                       ";
        // line 797
        echo "                               
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"53\" id=\"subtab-AdminParentPayment\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-payment\">payment</i>
                      <span>
                      Pago
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-53\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"54\" id=\"subtab-AdminPayment\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/payment_methods?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Métodos de pago
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"55\" id=\"subtab-AdminPaymentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/payment/preferences?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Preferencias
                                </a>
                              </li>

                                                                              </ul>
           ";
        // line 827
        echo "                             </li>
                                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"56\" id=\"subtab-AdminInternational\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-language\">language</i>
                      <span>
                      Internacional
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-56\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"57\" id=\"subtab-AdminParentLocalization\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/localization/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Localización
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"62\" id=\"subtab-AdminParentCountries\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/zones/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Ubicaciones Geográficas
        ";
        // line 856
        echo "                        </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"66\" id=\"subtab-AdminParentTaxes\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/taxes/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Impuestos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"69\" id=\"subtab-AdminTranslations\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/improve/international/translations/settings?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Traducciones
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                      
                                          
                    
          
            <li class=\"category-title link-active\" data-submenu=\"70\" id=\"tab-CONFIGURE\">
                <span class=\"title\">Configurar</span>
            </li>

                              
                  
                                                      
                  
                  <li class=\"link-levelone has_submenu\" data-submenu=\"71\" id=\"subtab-ShopParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-";
        // line 893
        echo "icons mi-settings\">settings</i>
                      <span>
                      Parámetros de la tienda
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_down
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-71\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"72\" id=\"subtab-AdminParentPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/preferences/preferences?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Configuración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"75\" id=\"subtab-AdminParentOrderPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/order-preferences/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Configuración de pedidos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"78\" id=\"subtab-AdminPPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/product-preferences/?_to";
        // line 922
        echo "ken=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Configuración de Productos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"79\" id=\"subtab-AdminParentCustomerPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/customer-preferences/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Configuración de clientes
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"83\" id=\"subtab-AdminParentStores\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/contacts/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Contacto
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"86\" id=\"subtab-AdminParentMeta\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/shop/seo-urls/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Tráfico &amp; SEO
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=";
        // line 953
        echo "\"link-leveltwo\" data-submenu=\"89\" id=\"subtab-AdminParentSearchConf\">
                                <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminSearchConf&amp;token=96decb83384f0a6b84940c6e3080dd7d\" class=\"link\"> Buscar
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                                              
                  
                                                      
                                                          
                  <li class=\"link-levelone has_submenu link-active open ul-open\" data-submenu=\"92\" id=\"subtab-AdminAdvancedParameters\">
                    <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\">
                      <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                      <span>
                      Parámetros Avanzados
                      </span>
                                                    <i class=\"material-icons sub-tabs-arrow\">
                                                                    keyboard_arrow_up
                                                            </i>
                                            </a>
                                              <ul id=\"collapse-92\" class=\"submenu panel-collapse\">
                                                      
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"93\" id=\"subtab-AdminInformation\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/system-information/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Información
                ";
        // line 980
        echo "                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"94\" id=\"subtab-AdminPerformance\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/performance/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Rendimiento
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"95\" id=\"subtab-AdminAdminPreferences\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/administration/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Administración
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"96\" id=\"subtab-AdminEmails\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/emails/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> E-mail
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"97\" id=\"subtab-AdminImport\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/config";
        // line 1011
        echo "ure/advanced/import/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Importar
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"98\" id=\"subtab-AdminParentEmployees\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/employees/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Equipo
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"102\" id=\"subtab-AdminParentRequestSql\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/sql-requests/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Base de datos
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo link-active\" data-submenu=\"105\" id=\"subtab-AdminLogs\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/logs/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Registros/Logs
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-level";
        // line 1042
        echo "two\" data-submenu=\"106\" id=\"subtab-AdminWebservice\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/webservice-keys/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Webservice
                                </a>
                              </li>

                                                                                                                                                                                                                                                    
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"110\" id=\"subtab-AdminFeatureFlag\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/feature-flags/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Características nuevas y experimentales
                                </a>
                              </li>

                                                                                  
                              
                                                            
                              <li class=\"link-leveltwo\" data-submenu=\"111\" id=\"subtab-AdminParentSecurity\">
                                <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/security/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" class=\"link\"> Seguridad
                                </a>
                              </li>

                                                                              </ul>
                                        </li>
                              
          
                  </ul>
  </div>
  
</nav>


<div class=\"header-toolbar d-print-none\">
    
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Parámetros Av";
        // line 1080
        echo "anzados</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/avtkttftlp0d3jsj/index.php/configure/advanced/logs/?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\" aria-current=\"page\">Registros/Logs</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Registros/Logs          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
            
                        
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Ayuda\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminLogs%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\"
                   id=\"product_form_open_help\"
                >
                  Ayuda
                </a>
                                    </div>
        </div>

      
    </div>
  </div>

  
  
  <div class=\"btn-floating\">
    <button class=\"btn btn-primary collapsed\" data-toggle=\"collapse\" data-target=\".btn-floating-container\" aria-expanded=\"false\">
      <i class=\"material-icons\">add</i>
    </button>
    <div class=\"btn-floating-container collapse\">
      <div class=\"btn-floating-menu\">
        
        
                              <a class=\"btn btn-floating-item btn-help btn-sidebar\" href=\"#\"
               title=\"Ayuda\"
               data-toggle=\"sidebar\"
               data-target=\"#right-sidebar\"
               data-url=\"/avtkttftlp0d3jsj/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop-project.org%252Fes%252Fdoc%252FAdminLogs%253Fversion%253D8.1.3%2526country%253Des/Ayuda?_token=l4dSTb9l2PsZg50EctCJSxzhVkO_AuKRCNmRAdQrcfY\"
            >
";
        // line 1133
        echo "              Ayuda
            </a>
                        </div>
    </div>
  </div>
  
</div>

<div id=\"main-div\">
          
      <div class=\"content-div  \">

        

                                                        
        <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>
<div id=\"content-message-box\"></div>


  ";
        // line 1152
        $this->displayBlock('content_header', $context, $blocks);
        $this->displayBlock('content', $context, $blocks);
        $this->displayBlock('content_footer', $context, $blocks);
        $this->displayBlock('sidebar_right', $context, $blocks);
        echo "

        

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>¡Oh no!</h1>
  <p class=\"mt-3\">
    La versión para móviles de esta página no está disponible todavía.
  </p>
  <p class=\"mt-2\">
    Por favor, utiliza un ordenador de escritorio hasta que esta página sea adaptada para dispositivos móviles.
  </p>
  <p class=\"mt-2\">
    Gracias.
  </p>
  <a href=\"http://prestashopinst1.live-website.com/avtkttftlp0d3jsj/index.php?controller=AdminDashboard&amp;token=73a26e85efbad09a5f0375eb0b9d4b7f\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons rtl-flip\">arrow_back</i>
    Atrás
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    
</div>
  

      <div class=\"bootstrap\">
      
    </div>
  
";
        // line 1186
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>";
        echo "
</html>";
    }

    // line 102
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1152
    public function block_content_header($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_content_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_sidebar_right($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1186
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "__string_template__f5e3c07957310db824f46ec29180a86e64791898038f9c2c82936698d9380133";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1349 => 1186,  1328 => 1152,  1317 => 102,  1308 => 1186,  1268 => 1152,  1247 => 1133,  1192 => 1080,  1152 => 1042,  1119 => 1011,  1086 => 980,  1057 => 953,  1024 => 922,  993 => 893,  954 => 856,  923 => 827,  891 => 797,  859 => 767,  826 => 736,  794 => 706,  762 => 676,  721 => 637,  692 => 610,  658 => 578,  625 => 547,  594 => 518,  560 => 486,  526 => 454,  483 => 413,  439 => 371,  390 => 324,  336 => 272,  290 => 228,  252 => 192,  233 => 175,  192 => 136,  153 => 102,  117 => 68,  89 => 42,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__f5e3c07957310db824f46ec29180a86e64791898038f9c2c82936698d9380133", "");
    }
}
