<?php
/**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 */

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

header('Location: ../');

// ESTE ARCHIVO NO SUBIR

// URL de la API
$url_api = 'http://cloud.chingolito.com:8080/api/stocks?branch=1&warehouse=1';

// Token de autorización (Bearer token)
$mi_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMzc2M2Y5NzliNDk3NzcxMGExNzgyYjdhNDg2YWVmMDg4ODE4N2NkNzhhNDdhZDJkZmQ0MWVlMmIyYzU0NTZkOTc0NmNiZWE2YTRiODIyMjciLCJpYXQiOjE3MDY5MDY3ODUuNjk3MTE4LCJuYmYiOjE3MDY5MDY3ODUuNjk3MTIzLCJleHAiOjE3Mzg1MjkxODUuNjg3OTQzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Cp2p0OUs-hJiW8GXqrLSOn30t0B5PYxGQxQDvkxk0qtfKLsobQzEDlGjdYwR91GyAA64UC-RqBgnPWsqX1waoNcIgbbZyEeZTYey7iwQ2s7ecF0aY-TmPmgXVSdwJm2pnkItiErNkr5HN7MTgkk7nWhc7NP9cGbdQAcpAinBSLVor0Ahp2LDrC2JqJL2k40ohYdwhXs5FLjPtOzUfGM6MluNE2WCszMKJ0HQ1R4uRHQdhb6IrP78dKSNIWFbih-r8k3d6Il5x85Y0MPGmD7fUC-LxssD6xR9DuzXdd_ZFwkgNz2uy7EfvGdsHqu6g8BMgVHh22JKYJZ4q6FKCiMMlrFQ-bRWTRjl_81C7EB6iBy9xO08pR79uLrJPaSYQ1eaywxRw5JjQXLfe1_dbLNxdQfyflj08-HdQvg08opM8Rd4CLXFh50wiKbfbpBn8cmDOF7tYvNztH1EfmTxXGk18KhEHKzoj1_VZQ48vIbsNG6PygdUMP1mzG10ufaAhfa0iJ6zHHL9xl_FE6ELk27Uvt9th2H0eFHvfoYnlCS2mUfWZZoALwmaeJAIFIU47y6DtkGKiVIrCDVLWF87MPM2E3RlpHeccWWUzmxjX8njUgQNHU6tfdZI_eHI3hENLwxxCpSdRwvLPVr9YtUOryn-mjGrhKv9C8JDlDvvlxwoec8';
// o sea q el token a cambiado?? puede ser
$ch = curl_init($url_api); // Inicializar cURL
// Configurar las opciones de cURL
$headers = [
    'Authorization: Bearer ' . $mi_token,
    'Content-Type: application/json', // Si es necesario, ajusta el tipo de contenido según la API
];
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);     // Establecer otras opciones si es necesario, como el método HTTP (GET, POST, etc.)
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Ejecutar la solicitud cURL
$response = curl_exec($ch);

// Verificar si hay errores
if ($response === false) {
    echo 'Error de cURL: ' . curl_error($ch);
}
curl_close($ch);     // Cerrar la sesión cURL

// TODOS LOS DATOS
$resultado = $response;

// Decodificar la respuesta JSON (si se espera un JSON)
$datos = json_decode($resultado, true);

$productos = $datos['result']['data'];// acaaa

//actualizarPrecio($datos['result']['data'])
foreach ($productos as $producto) {
    $productoJson = json_encode($producto); // Convertir el array $producto a un string JSON}

    $array = json_decode($productoJson, true);

    // 2.  aqui creamos el xml para actualizar el precio, hay que crear otro xml para actualizar stock
    // crear otro xml con la estructura de stock
    /*
    <?xml version="1.0" encoding="UTF-8"?>
    <prestashop xmlns:xlink="http://www.w3.org/1999/xlink">
        <stock_available>
        <id>1</id>
        <id_product>1</id_product>
        <quantity><![CDATA[200]]></quantity>
      </stock_available>
    </prestashop>
    */
    // PAra actualizar precios

    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>');

    // Crear la estructura XML a partir del array
    $product = $xml->addChild('product');
    $product->addChild('id', htmlspecialchars('{{product_id}}'));
    $product->addChild('price', htmlspecialchars('{{price}}')); // aquì bb? aqui le agregamos ?

    // Reemplazar los valores específicos con los datos del array
    $product->id = htmlspecialchars($array['product_id']);
    $product->price = htmlspecialchars($array['price']);

    $xmlProducto = $xml->asXML();

    // para actualizar stock
    $xmlStock = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>');

    // Crear la estructura XML a partir del array
    $productStock = $xmlStock->addChild('stock_available');
    $productStock->addChild('id', htmlspecialchars('{{id}}'));
    $productStock->addChild('id_product', htmlspecialchars('{{product_id}}'));
    $productStock->addChild('quantity', htmlspecialchars('{{stock}}'));
    // Reemplazar los valores específicos con los datos del array
    $productStock->id = htmlspecialchars($array['product_id']);
    $productStock->id_product = htmlspecialchars($array['product_id']);
    $productStock->quantity = htmlspecialchars($array['stock']);

    $xmlProductoStock = $xmlStock->asXML();

    // Imprimir el XML resultante
    //echo $xml->asXML();
    echo "\n";
    echo('----------');
    //echo " Producto: ".$array['product_id'];
    //echo " id: ".$array['id'];
    //echo " Precio:".$array['price'];
    //echo " Stock:".$array['stock'];
    echo $xmlStock->asXML();
    echo('----------');
    echo "\n";

// siguele jeje




    //actualizarDatosConToken($producto['product_id'],$xmlProducto);

    $product_id = $producto['product_id'];

    // 2. va a ser lo mismo, es la misa misma api
    $token_prestashop = 'GJRLVKYTTJGV6JE5C4HTM7VPXUBL4KU9'; // usuario:password
    $mi_token_prestashop = 'TDUzSjQyQlRQUFNKVlVDVlYzOVI2VDU3UTk1VkFVTDI6'; // usuario:password

// 2. esto va a variar, esta es url para actualiaar precios, hay que declarar otra para actualizar stock
// sip, lanzalo en local... actualizara en el host pero se ejecuta en local ya bb
    $url_api_prestaProducts = 'http://prestashopinst1.live-website.com/api/products/'.$product_id.'?ws_key='.$token_prestashop;
    // $mi_token_prestashop = 'z60oGaXjYoRhvywCYyr9NXK28GVHgTEy';

    // Configurar la petición HTTP utilizando cURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url_api_prestaProducts);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlProducto);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/xml',
        'Authorization: Basic ' . $mi_token_prestashop
    ));

    // Ignorar la verificación del certificado SSL (cuidado: no recomendado para producción)
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    // Ejecutar la petición
    $response = curl_exec($curl);

    echo $response;
    // Verificar la respuesta
    if ($response) {
        // Por ejemplo, puedes imprimir la respuesta de la API
        echo $response;
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
    } else {
        echo 'Hubo un error al intentar actualizar los datos.';
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
    }

    // Cerrar la sesión cURL
    curl_close($curl);

    // 2. aqui hay que enviar otra petición igualita a la anterior pero a la url del stock
    // enviar peticion -- revisa bb, el de antes es de presta recuerda, el que pegaste es el otro

    $url_api_prestaStock = 'http://prestashopinst1.live-website.com/api/stock_availables/'.$product_id.'?ws_key='.$token_prestashop;
    // $mi_token_prestashop = 'z60oGaXjYoRhvywCYyr9NXK28GVHgTEy';

    // Configurar la petición HTTP utilizando cURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url_api_prestaStock);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $xmlProductoStock);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/xml',
        'Authorization: Basic ' . $mi_token_prestashop
    ));

    // Ignorar la verificación del certificado SSL (cuidado: no recomendado para producción)
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    // Ejecutar la petición
    $response = curl_exec($curl);

    // Verificar la respuesta
    if ($response) {
        //echo 'Los datos se actualizaron correctamente.';
        // Manejar la respuesta según sea necesario
        // Por ejemplo, puedes imprimir la respuesta de la API
        echo $response;
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
        echo "\n";
    } else {
        echo 'Hubo un error al intentar actualizar los datos.';
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
    }

    // Cerrar la sesión cURL
    curl_close($curl);

}
exit;
