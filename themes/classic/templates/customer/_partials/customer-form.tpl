{**
 * Copyright since 2007 PrestaShop SA and Contributors
 * PrestaShop is an International Registered Trademark & Property of PrestaShop SA
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.md.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://devdocs.prestashop.com/ for more information.
 *
 * @author    PrestaShop SA and Contributors <contact@prestashop.com>
 * @copyright Since 2007 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 *}
{block name='customer_form'}
  {block name='customer_form_errors'}
    {include file='_partials/form-errors.tpl' errors=$errors['']}
  {/block}

<form action="{block name='customer_form_actionurl'}{$action}{/block}" id="customer-form" class="js-customer-form" method="post">
  <div>
    {block "form_fields"}
      {foreach from=$formFields item="field"}
        {block "form_field"}
          {if $field.type === "password"}
            <div class="field-password-policy">
              {form_field field=$field}
            </div>
          {else}
            {form_field field=$field}
          {/if}
        {/block}
      {/foreach}
      {$hook_create_account_form nofilter}
    {/block}
  </div>

  {block name='customer_form_footer'}
    <footer class="form-footer clearfix">
      <input type="hidden" name="submitCreate" value="1">
      {block "form_buttons"}
        <button class="btn btn-primary form-control-submit float-xs-right" data-link-action="save-customer" type="submit">
          {l s='Save' d='Shop.Theme.Actions'}
        </button>
      {/block}
    </footer>
  {/block}

</form>
<script src="https://code.jquery.com/jquery-3.6.3.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$("#field-dni").change(validateDni);

	function validateDni () {
		var dni = $("#field-dni").val();

		if (!isNaN(dni)) {
			var dniNumber = parseInt(dni, 10);

			if (dniNumber >= 0 && dniNumber <= 99999999) {
				console.log('DNI válido:', dni);

				// Validar que el DNI tenga exactamente 8 dígitos
				if (dni.length === 8) {
					console.log('DNI válido con 8 dígitos');
					
					// llamar a la api 
					// URL de la API
					const apiUrl = 'https://dniruc.apisperu.com/api/v1/dni/'+dni+'?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbCI6ImV2ZWx5bmh1YW5jYW1hcXVlcmE1N0BnbWFpbC5jb20ifQ.whsn3sWCaXuDxSz-LRSjosX1CGtXJSaTZ-oEcVwNLzo';

					// Realizar la petición GET utilizando fetch
					fetch(apiUrl)
					  .then(response => {
						// Verificar si la respuesta es exitosa (código de estado 200-299)
						if (!response.ok) {
						  throw new Error('Error en la petición: ' + response.status);
						}
						
						// Convertir la respuesta a formato JSON
						return response.json();
					  })
					  .then(data => {
						// Manipular los datos obtenidos de la API
						console.log(data);
						$("#field-firstname").val(data.nombres);
						$("#field-lastname").val(data.apellidoPaterno+' '+data.apellidoMaterno);
					  })
					  .catch(error => {
						// Capturar y manejar errores
						console.error('Error: ', error);
					  });


				}
			} else {
				console.log('DNI inválido: no es un número positivo o tiene más de 8 dígitos');
				$("#field-dni").val('');
			}
		} else {
			console.log('DNI inválido: contiene caracteres no numéricos');
			$("#field-dni").val('');
		}
	}
});
</script>
{/block}
