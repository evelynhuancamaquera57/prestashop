<?php
/*
function obtenerDatosConToken($url, $token) {
    $ch = curl_init($url); // Inicializar cURL
    // Configurar las opciones de cURL
    $headers = [
        'Authorization: Bearer ' . $token,
        'Content-Type: application/json', // Si es necesario, ajusta el tipo de contenido según la API
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);     // Establecer otras opciones si es necesario, como el método HTTP (GET, POST, etc.)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Ejecutar la solicitud cURL
    $response = curl_exec($ch);

    // Verificar si hay errores
    if ($response === false) {
        echo 'Error de cURL: ' . curl_error($ch);
    }
    curl_close($ch);     // Cerrar la sesión cURL
    return $response;     // Devolver la respuesta en formato JSON
}

// metodo convertir json a xml

function decodificarJson($jsonString){
    // Decodificar el JSON a un array asociativo
    $array = json_decode($jsonString, true);

    // Crear el documento XML
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><prestashop xmlns:xlink="http://www.w3.org/1999/xlink"></prestashop>');

    // Crear la estructura XML a partir del array
    $product = $xml->addChild('product');
    $product->addChild('id', htmlspecialchars('{{product_id}}'));
    $product->addChild('price', htmlspecialchars('{{price}}'));

    // Reemplazar los valores específicos con los datos del array
    $product->id = htmlspecialchars($array['product_id']);
    $product->price = htmlspecialchars($array['price']);

    // Imprimir el XML resultante
    //echo $xml->asXML();
    echo "\n";
    echo('----------');
    echo "Producto: ".$array['product_id'];
    echo "Precio:".$array['price'];
    echo('----------');
    echo "\n";
    return $xml->asXML();
}


// metodo que tenga bucle para enviar todo el json a
function actualizarPrecio($productos){
    // recorrer array productos, enviar uno a uno los xml a la api de prestashop
    // Iterar sobre el JSON decodificado usando foreach
    foreach ($productos as $producto) {
        $productoJson = json_encode($producto); // Convertir el array $producto a un string JSON
        $xml = decodificarJson($productoJson);

        actualizarDatosConToken($producto['product_id'],$xml);
    }
}

function actualizarDatosConToken($product_id, $xml){

    $token_prestashop = 'GJRLVKYTTJGV6JE5C4HTM7VPXUBL4KU9'; // usuario:password
    $mi_token_prestashop = 'TDUzSjQyQlRQUFNKVlVDVlYzOVI2VDU3UTk1VkFVTDI6'; // usuario:password

    $url_api_prestashop = 'http://prestashopinst1.live-website.com/api/products/'.$product_id.'?ws_key='.$token_prestashop;
   // $mi_token_prestashop = 'z60oGaXjYoRhvywCYyr9NXK28GVHgTEy';

    // Configurar la petición HTTP utilizando cURL
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url_api_prestashop);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
    curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/xml',
        'Authorization: Basic ' . $mi_token_prestashop
    ));

    // Ignorar la verificación del certificado SSL (cuidado: no recomendado para producción)
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    // Ejecutar la petición
    $response = curl_exec($curl);

    // Verificar la respuesta
    if ($response) {
        //echo 'Los datos se actualizaron correctamente.';
        // Manejar la respuesta según sea necesario
        // Por ejemplo, puedes imprimir la respuesta de la API
        echo $response;
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
        echo "\n";
    } else {
        echo 'Hubo un error al intentar actualizar los datos.';
        echo "\n";
        echo curl_error($curl); // Imprimir el error cURL, si hay alguno
    }

    // Cerrar la sesión cURL
    curl_close($curl);
}



// URL de la API
$url_api = 'http://cloud.chingolito.com:8080/api/products-prices?price_rule=1';
// Token de autorización (Bearer token)
$mi_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjY5OWI4YmE3OWNhYjhlY2E4NjJkOGU5MzAzNjQyMzE3ZTc4OWQxMjFmZjc2MDJkNDY1NDQ3YTAyNjY3NTMzMjg2M2NkMmIzZmZkNGJhNzEiLCJpYXQiOjE3MDQ1NjAyMDcuNjEyNDgsIm5iZiI6MTcwNDU2MDIwNy42MTI0ODQsImV4cCI6MTczNjE4MjYwNy42MDc0MDgsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.tBScVuUWcgmDQM808nKsiMQaLbQ_MkUkIrVigbj3o35cJRwiX4PGvxMEsaYLxGfxzKolbF73tYs3N37yrmtNBlP4MTQ8Lq72NiAqv3mIA0OfWV8U1f2VPsbwYOEnJL2f5txrsngBXd8DOiQdaJi0LQX6U8obcxtzyaKMw81MDIGPt8pZPVPdbO7aD_g34dZPQe3WKzpRyFb8_7iMf03UbcjbQcfbQLk_LBhTTnGqi4x9KeDY74fFWzRVnHp0s1fTmp1z_IuHfR03z10egFPWt6NEcycNjGYBDag6oepqK2ikHvpv7j6zq32wNwBg2ioDk0mNifIdu0ROG2osEe64MRtFQXiZX3tkpStcRCMe4QgcHAPDO9u2RSy5jzzvzh5vAV-0-nUufOVz-x6eEZJGpfqltl6qNn6GfkufTHmpKvOc0SvNNyqUNHTA1U2MgwXKdMTKuVaKa92aZ_aV4OXI8MNiFO6UxoFADPU6Tn5aDZX1DgigfwA9Skog6xGjYQ0RyjCb-Z98LfCHJ7kTG3ema0vJNbVbeI6RGP-w4GKT3sn_MmNIBSbzl99xp5FSDIvfEAdgoHRUnRm6b-RMq3PqDBuTLhYmyVKRfvQP9f_jhFjMPi0oEhkpVBkBhJORWXFEz_KgqfKRidF548Y4idoDq5-R7VwSrK715XP_vEsfZsI';

// Llamar a la función para obtener datos
$resultado = obtenerDatosConToken($url_api, $mi_token);

// Decodificar la respuesta JSON (si se espera un JSON)
$datos = json_decode($resultado, true);

actualizarPrecio($datos['result']['data'])
// Imprimir la respuesta o realizar cualquier otra operación con los datos recibidos
//var_dump($datos);

//decodificarJson($datos['result']['data'][0])

////////


*/

?>
